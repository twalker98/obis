'use client';

import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import CreateFamilyMainComponent from '../taxa/collaborators/create-family/Main';

import {
  GenericForm,
  GenericFormItem,
  invalidInputAdornmentProps,
  validInputAdornmentProps,
  validatingInputAdornmentProps,
} from '@twalk-tech/react-lib/nextjs';
import { useState } from 'react';
import { FormAction } from '../../types/form-action';
import { Relations, TFValue } from '../../types/relations';
import { Tooltips } from '../../types/tooltips';
import {
  remapRelationalFieldsForDisplay,
  updateFormFieldValues,
} from '../../utils/functions';
import {
  acctax,
  fed_status,
  global_rank_lu,
  iucn_lu,
  nativity_lu,
  ok_swap,
  st_status,
  state_rank_lu,
} from '../../utils/obis-prisma';
import { relationalFieldMappings } from '../../utils/relational_mappings/Acctax';
import { theme } from '../../utils/theme';
import { tooltips } from '../../utils/tooltips/Acctax';
import { familyValidator, uniqueCodeValidator } from './validators';

import * as yup from 'yup';
import { OpenSnackbarFn } from '../../utils/hooks/snackbar';

export type AcctaxFormProps = {
  relations: Relations;
  action: FormAction;
  values?: acctax;
  onSubmit: (values: acctax) => Promise<void>;
  openSnackbar: OpenSnackbarFn;
};

export default function AcctaxForm(props: AcctaxFormProps) {
  const {
    iucn_lu,
    global_rank_lu,
    state_rank_lu,
    nativity_lu,
    fed_status,
    st_status,
    ok_swap,
    tfValues,
  } = props.relations;

  const [acodeValidationMessage, setAcodeValidationMessage] =
    useState<string>('');
  const [validatingAcode, setValidatingAcode] = useState(false);
  const [acodeValid, setAcodeValid] = useState<boolean | null>(null);
  const [acodeValue, setAcodeValue] = useState('');
  const [familyValidationMessage, setFamilyValidationMessage] =
    useState<string>('');
  const [validatingFamily, setValidatingFamily] = useState(false);
  const [familyValid, setFamilyValid] = useState<boolean | null>(null);
  const [familyValue, setFamilyValue] = useState('');
  const [createFamilyDialogOpen, setCreateFamilyDialogOpen] = useState(false);
  const [familyCreated, setFamilyCreated] = useState<boolean | null>(null);

  const acodeInputAdornmentProps = validatingAcode
    ? validatingInputAdornmentProps
    : acodeValid
    ? validInputAdornmentProps
    : acodeValid !== null
    ? invalidInputAdornmentProps
    : undefined;
  const familyInputAdornmentProps = validatingFamily
    ? validatingInputAdornmentProps
    : familyValid
    ? validInputAdornmentProps
    : familyValid !== null
    ? invalidInputAdornmentProps
    : undefined;

  const fields: Array<GenericFormItem> = [
    {
      fcName: 'acode',
      name: 'acode',
      type: 'text',
      value: acodeValue,
      error: acodeValidationMessage !== '',
      errorMessage: acodeValidationMessage,
      inputProps: acodeInputAdornmentProps,
    },
    {
      fcName: 'sname',
      name: 'Scientific Name',
      type: 'text',
      value: '',
    },
    {
      fcName: 'scientificnameauthorship',
      name: 'Scientific Name Authorship',
      type: 'text',
      value: '',
    },
    {
      fcName: 'family',
      name: 'Family',
      type: 'text',
      value: familyValue,
      error: familyValidationMessage !== '',
      errorMessage: familyValidationMessage,
      clickableErrorMessage:
        familyValidationMessage === "Family doesn't exist"
          ? {
              text: 'Create a new one?',
              action: () => setCreateFamilyDialogOpen(true),
            }
          : undefined,
      inputProps: familyInputAdornmentProps,
      tooltipZIndex: 1,
    },
    { fcName: 'genus', name: 'Genus', type: 'text', value: '' },
    { fcName: 'species', name: 'Species', type: 'text', value: '' },
    { fcName: 'subspecies', name: 'Subspecies', type: 'text', value: '' },
    { fcName: 'variety', name: 'Variety', type: 'text', value: '' },
    { fcName: 'forma', name: 'Forma', type: 'text', value: '' },
    { fcName: 'elcode', name: 'Element Code', type: 'text', value: '' },
    {
      fcName: 'gelcode',
      name: 'Global Element Code',
      type: 'number',
      value: '',
    },
    {
      fcName: 'iucncode',
      name: 'IUCN Code',
      type: 'dropdown',
      options: iucn_lu,
      value: iucn_lu.find((iucnCode) => iucnCode.id === -1),
      dropdownLabelFunctionPred: (option: iucn_lu) => option.description,
      optionKey: 'id',
      optionValueKey: 'description',
    },
    {
      fcName: 'g_rank',
      name: 'Global Rank',
      type: 'dropdown',
      options: global_rank_lu,
      value: global_rank_lu.find((globalRank) => globalRank.code === 'GNR'),
      dropdownLabelFunctionPred: (option: global_rank_lu) => option.code,
      optionKey: 'code',
      optionValueKey: 'code',
    },
    {
      fcName: 's_rank',
      name: 'State Rank',
      type: 'dropdown',
      options: state_rank_lu,
      value: state_rank_lu.find((stateRank) => stateRank.code === 'SNR'),
      dropdownLabelFunctionPred: (option: state_rank_lu) => option.code,
      optionKey: 'code',
      optionValueKey: 'code',
    },
    {
      fcName: 'nativity',
      name: 'Nativity',
      type: 'dropdown',
      options: nativity_lu,
      value: nativity_lu.find((nativity) => nativity.n_id === -1),
      dropdownLabelFunctionPred: (option: nativity_lu) => option.nativity,
      optionKey: 'n_id',
      optionValueKey: 'nativity',
    },
    { fcName: 'source', name: 'Source', type: 'text', value: '' },
    { fcName: 'usda_code', name: 'USDA Code', type: 'text', value: '' },
    {
      fcName: 'tsn',
      name: 'Taxonomic Serial Number',
      type: 'number',
      value: '',
    },
    {
      fcName: 'fed_status_id',
      name: 'Federal Status',
      type: 'dropdown',
      options: fed_status,
      value: fed_status.find((fedStatus) => fedStatus.status_id === -1),
      dropdownLabelFunctionPred: (option: fed_status) => option.description,
      optionKey: 'status_id',
      optionValueKey: 'description',
    },
    {
      fcName: 'st_status_id',
      name: 'State Status',
      type: 'dropdown',
      options: st_status,
      value: st_status.find((stateStatus) => stateStatus.status_id === -1),
      dropdownLabelFunctionPred: (option: st_status) => option.description,
      optionKey: 'status_id',
      optionValueKey: 'description',
    },
    {
      fcName: 'swap_id',
      name: 'OK SWAP Tier',
      type: 'dropdown',
      options: ok_swap,
      value: ok_swap.find((swap) => swap.swap_id === -1),
      dropdownLabelFunctionPred: (option: ok_swap) =>
        option.description === 'Not Included'
          ? option.description
          : `${option.tier}: ${option.description}`,
      optionKey: 'swap_id',
      optionValueKey: 'description',
      optionValueKeyPrefix: 'tier',
    },
    {
      fcName: 'scientificname',
      name: 'Full Scientific Name',
      type: 'text',
      value: '',
    },
    {
      fcName: 'sspscientificnameauthorship',
      name: 'Subspecies Authorship',
      type: 'text',
      value: '',
    },
    {
      fcName: 'varscientificnameauthorship',
      name: 'Variety Authorship',
      type: 'text',
      value: '',
    },
    {
      fcName: 'formascientificnameauthorship',
      name: 'Forma Authorship',
      type: 'text',
      value: '',
    },
    {
      fcName: 'taxonremarks',
      name: 'Taxon Remarks',
      type: 'multiline',
      value: '',
    },
    {
      fcName: 'tracked',
      name: 'Tracked',
      type: 'dropdown',
      options: tfValues,
      value: tfValues.find((tfValue) => tfValue.display_name === 'False'),
      dropdownLabelFunctionPred: (option: TFValue) => option.display_name,
      optionKey: 'display_name',
      optionValueKey: 'display_name',
    },
  ];

  updateFormFieldValues<Tooltips<acctax>>(tooltips, fields, 'tooltip');

  const validationSchema = yup.object({
    acode: yup
      .string()
      .required('acode is required')
      .test(
        'unique_code_validation',
        acodeValidationMessage,
        async function (value) {
          if (props.values) {
            return true;
          }

          if (value !== acodeValue) {
            setValidatingAcode(true);
            setAcodeValid(null);
          }

          let message = await uniqueCodeValidator(value, acodeValue);
          setAcodeValue(value);

          if (typeof message === 'string') {
            message = message.replace('{{field}}', 'acode');
            setAcodeValidationMessage(message);
            setAcodeValid(false);
            setValidatingAcode(false);
            return this.createError({ message: message });
          } else if (message === null) {
            setAcodeValidationMessage('');
            setAcodeValid(true);
            setValidatingAcode(false);
            return true;
          }

          return acodeValid as boolean;
        },
      ),
    sname: yup.string().required('Scientific Name is required'),
    family: yup
      .string()
      .required('Family is required')
      .test(
        'family_validator',
        familyValidationMessage,
        async function (value) {
          if (value !== familyValue) {
            setValidatingFamily(true);
            setFamilyValid(null);
          }

          let message = await familyValidator(value, familyValue);
          setFamilyValue(value);

          if (typeof message === 'string') {
            setFamilyValidationMessage(message);
            setFamilyValid(false);
            setValidatingFamily(false);
            return this.createError({ message: message });
          } else if (message === null) {
            setFamilyValidationMessage('');
            setFamilyValid(true);
            setValidatingFamily(false);
            return true;
          }

          return familyValid as boolean;
        },
      ),
    genus: yup.string().required('Genus is required'),
    species: yup.string().required('Species is required'),
    gelcode: yup.number().integer('Global Element Code must be a whole number'),
    iucncode: yup.mixed<iucn_lu>().required('IUCN Code is required'),
    g_rank: yup.mixed<global_rank_lu>().required('Global Rank is required'),
    s_rank: yup.mixed<state_rank_lu>().required('State Rank is required'),
    nativity: yup.mixed<nativity_lu>().required('Nativity is required'),
    tsn: yup.number().integer('Taxonomic Serial Number must be a whole number'),
    fed_status_id: yup
      .mixed<fed_status>()
      .required('Federal Status is required'),
    st_status_id: yup.mixed<st_status>().required('State Status is required'),
    swap_id: yup.mixed<ok_swap>().required('OK Swap is required'),
    tracked: yup.mixed<TFValue>().required('Tracked is required'),
  });

  const handleValidationError = () => {
    props.openSnackbar(
      'Please correct the errors highlighted in red.',
      'error',
    );
  };

  if (props.values) {
    remapRelationalFieldsForDisplay(
      relationalFieldMappings,
      props.values,
      props.relations,
    );
    updateFormFieldValues<acctax>(props.values, fields, 'value');

    props.values.tracked
      ? (fields.find((field) => field.fcName === 'tracked')!.value =
          props.relations.tfValues.find(
            (tfValue) => tfValue.display_name === 'True',
          ))
      : (fields.find((field) => field.fcName === 'tracked')!.value =
          props.relations.tfValues.find(
            (tfValue) => tfValue.display_name === 'False',
          ));

    fields.find((field) => field.fcName === 'acode')!.disabled = true;
  }

  const handleCreateFamilyDialogClose = () => {
    setCreateFamilyDialogOpen(false);

    if (familyCreated) {
      setFamilyValid(true);
      setFamilyValidationMessage('');
    }
  };

  return (
    <>
      <GenericForm
        title="Taxonomic Data"
        fields={fields}
        onSubmit={props.onSubmit}
        handleValidationError={handleValidationError}
        submitBtnConfig={{
          title: `${props.action} Taxon`,
          icon: props.action === 'Create' ? <AddIcon /> : <EditIcon />,
        }}
        validationSchema={validationSchema}
        borderColor={theme.palette.primary.main}
        width="100%"
      />
      <Dialog
        open={createFamilyDialogOpen}
        onClose={handleCreateFamilyDialogClose}
        // z-index set so that dialog covers all existing content (primarily tooltip on family field)
        style={{ zIndex: 2 }}
      >
        <DialogContent sx={{ minWidth: '500px' }}>
          <CreateFamilyMainComponent
            relations={props.relations}
            family={fields.find((field) => field.fcName === 'family')?.value}
            familyCreated={familyCreated}
            setFamilyCreated={setFamilyCreated}
            openSnackbar={props.openSnackbar}
          />
        </DialogContent>
      </Dialog>
    </>
  );
}
