'use client';

import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';

import {
  GenericForm,
  GenericFormItem,
  invalidInputAdornmentProps,
  validInputAdornmentProps,
  validatingInputAdornmentProps,
} from '@twalk-tech/react-lib/nextjs';
import { useState } from 'react';
import { Tooltips } from '../../types/tooltips';
import {
  capitalizeFirstLetter,
  updateFormFieldValues,
} from '../../utils/functions';
import { syntax } from '../../utils/managers/obis/Syntax';
import { theme } from '../../utils/theme';
import { tooltips } from '../../utils/tooltips/Syntax';
import { familyValidator, uniqueCodeValidator } from './validators';

import * as yup from 'yup';
import { OpenSnackbarFn } from '../../utils/hooks/snackbar';

type SyntaxFormProps = {
  action: 'create' | 'edit';
  values?: syntax;
  onSubmit: (values: syntax) => Promise<void>;
  openSnackbar: OpenSnackbarFn;
};

export default function SyntaxForm(props: SyntaxFormProps) {
  const [scodeValidationMessage, setScodeValidationMessage] =
    useState<string>('');
  const [validatingScode, setValidatingScode] = useState(false);
  const [scodeValid, setScodeValid] = useState<boolean | null>(null);
  const [scodeValue, setScodeValue] = useState('');
  const [familyValidationMessage, setFamilyValidationMessage] =
    useState<string>('');
  const [validatingFamily, setValidatingFamily] = useState(false);
  const [familyValid, setFamilyValid] = useState<boolean | null>(null);
  const [familyValue, setFamilyValue] = useState('');

  const scodeInputAdornmentProps = validatingScode
    ? validatingInputAdornmentProps
    : scodeValid
    ? validInputAdornmentProps
    : scodeValid !== null
    ? invalidInputAdornmentProps
    : undefined;
  const familyInputAdornmentProps = validatingFamily
    ? validatingInputAdornmentProps
    : familyValid
    ? validInputAdornmentProps
    : familyValid !== null
    ? invalidInputAdornmentProps
    : undefined;

  const fields: Array<GenericFormItem> = [
    {
      fcName: 'scode',
      name: 'scode',
      type: 'text',
      value: scodeValue,
      error: scodeValidationMessage !== '',
      errorMessage: scodeValidationMessage,
      inputProps: scodeInputAdornmentProps,
      disabled: props.action === 'edit',
    },
    { fcName: 'sname', name: 'Scientific Name', type: 'text', value: '' },
    {
      fcName: 'scientificnameauthorship',
      name: 'Scientific Name Authorship',
      type: 'text',
      value: '',
    },
    {
      fcName: 'family',
      name: 'Family',
      type: 'text',
      value: familyValue,
      error: familyValidationMessage !== '',
      errorMessage: familyValidationMessage,
      inputProps: familyInputAdornmentProps,
    },
    { fcName: 'genus', name: 'Genus', type: 'text', value: '' },
    { fcName: 'species', name: 'Species', type: 'text', value: '' },
    { fcName: 'subspecies', name: 'Subspecies', type: 'text', value: '' },
    { fcName: 'variety', name: 'Variety', type: 'text', value: '' },
    {
      fcName: 'tsn',
      name: 'Taxonomic Serial Number',
      type: 'number',
      value: '',
    },
    {
      fcName: 'scientificname',
      name: 'Full Scientific Name',
      type: 'text',
      value: '',
    },
    {
      fcName: 'sspscientificnameauthorship',
      name: 'Subspecies Authorship',
      type: 'text',
      value: '',
    },
    {
      fcName: 'varscientificnameauthorship',
      name: 'Variety Authorship',
      type: 'text',
      value: '',
    },
    {
      fcName: 'formascientificnameauthorship',
      name: 'Forma Authorship',
      type: 'text',
      value: '',
    },
  ];

  updateFormFieldValues<Tooltips<syntax>>(tooltips, fields, 'tooltip');

  const validationSchema = yup.object({
    scode: yup
      .string()
      .required('acode is required')
      .test(
        'unique_code_validator',
        scodeValidationMessage,
        async function (value) {
          if (props.action === 'edit') {
            return true;
          }

          if (value !== scodeValue) {
            setValidatingScode(true);
            setScodeValid(null);
          }

          let message = await uniqueCodeValidator(value, scodeValue);
          setScodeValue(value);

          if (typeof message === 'string') {
            message = message.replace('{{field}}', 'scode');
            setScodeValidationMessage(message);
            setScodeValid(false);
            setValidatingScode(false);
            return this.createError({ message: message });
          } else if (message === null) {
            setScodeValidationMessage('');
            setScodeValid(true);
            setValidatingScode(false);
            return true;
          }

          return scodeValid as boolean;
        },
      ),
    sname: yup.string().required('Scientific Name is required'),
    family: yup
      .string()
      .required('Family is required')
      .test(
        'family_validator',
        familyValidationMessage,
        async function (value) {
          if (value !== familyValue) {
            setValidatingFamily(true);
            setFamilyValid(null);
          }

          let message = await familyValidator(value, familyValue);
          setFamilyValue(value);

          if (typeof message === 'string') {
            setFamilyValidationMessage(message);
            setFamilyValid(false);
            setValidatingFamily(false);
            return this.createError({ message: message });
          } else if (message === null) {
            setFamilyValidationMessage('');
            setFamilyValid(true);
            setValidatingFamily(false);
            return true;
          }

          return familyValid as boolean;
        },
      ),
    genus: yup.string().required('Genus is required'),
    species: yup.string().required('Species is required'),
    tsn: yup.number().integer('Taxonomic Serial Number must be a whole number'),
  });

  const handleValidationError = () => {
    props.openSnackbar(
      'Please correct the errors highlighted in red.',
      'error',
    );
  };

  if (props.values) {
    updateFormFieldValues<syntax>(props.values, fields, 'value');
  }

  return (
    <GenericForm
      title="Synonym Data"
      fields={fields}
      onSubmit={props.onSubmit}
      handleValidationError={handleValidationError}
      submitBtnConfig={{
        title: `${capitalizeFirstLetter(props.action)} Synonym`,
        icon: props.action === 'create' ? <AddIcon /> : <EditIcon />,
      }}
      validationSchema={validationSchema}
      borderColor={theme.palette.primary.main}
      width="100%"
    />
  );
}
