'use client';

import AddIcon from '@mui/icons-material/Add';
import DownloadIcon from '@mui/icons-material/Download';
import EditIcon from '@mui/icons-material/Edit';
import HideRecordField from './custom-fields/HideRecord';
import IndividualsField from './custom-fields/Individuals';
import ScientificNameField, {
  OccurrenceScientificName,
} from './custom-fields/ScientificName';
import TownshipRangeField from './custom-fields/TownshipRange';

import { GenericForm, GenericFormItem } from '@twalk-tech/react-lib/nextjs';
import dayjs from 'dayjs';
import { useState } from 'react';
import { FormAction } from '../../types/form-action';
import {
  IdentificationConfidence,
  Relations,
  TFValue,
} from '../../types/relations';
import { Tooltips } from '../../types/tooltips';
import {
  remapRelationalFieldsForDisplay,
  updateFormFieldValues,
} from '../../utils/functions';
import {
  basisofrecord_lu,
  county,
  institution,
  occurrence,
  resourcetype_lu,
  source,
} from '../../utils/obis-prisma';
import { relationalFieldMappings } from '../../utils/relational_mappings/Occurrence';
import { theme } from '../../utils/theme';
import { tooltips } from '../../utils/tooltips/Occurrence';

import * as yup from 'yup';
import { Direction } from '../../types/relations';
import { OpenSnackbarFn } from '../../utils/hooks/snackbar';

export type OccurrenceFormProps = {
  relations: Relations;
  action: FormAction;
  username: string;
  values?: occurrence;
  acodeValue?: OccurrenceScientificName;
  onSubmit: (values: occurrence) => Promise<void>;
  openSnackbar: OpenSnackbarFn;
};

export default function OccurrenceForm(props: OccurrenceFormProps) {
  const {
    basisofrecord_lu,
    county,
    institution,
    resourcetype_lu,
    source,
    tfValues,
    identificationConfidences,
    ns,
    ew,
  } = props.relations;
  const [individualsEstimatedValue, setIndividualsEstimatedValue] =
    useState<boolean>(
      props.values ? props.values.individuals_estimated : false,
    );
  const [nsValue, setNsValue] = useState<Direction | null>(
    props.values
      ? ns.find((direction) => direction.direction === props.values?.ns)!
      : null,
  );
  const [ewValue, setEwValue] = useState<Direction | null>(
    props.values
      ? ew.find((direction) => direction.direction === props.values?.ew)!
      : null,
  );
  const [hideRecordValue, setHideRecordValue] = useState<boolean>(
    props.values ? props.values.hiderecord : false,
  );
  const [username, setUsername] = useState(
    props.values ? props.values.entby : props.username,
  );

  const acodeValue = props.acodeValue;
  const scientificNames = props.acodeValue ? [props.acodeValue] : [];
  const individualsMinValue =
    props.values && props.values.individuals_min
      ? props.values.individuals_min
      : '';
  const individualdMaxValue =
    props.values && props.values.individuals_max
      ? props.values.individuals_max
      : '';
  const townshipValue =
    props.values && props.values.township ? props.values.township : '';
  const rangeValue =
    props.values && props.values.range ? props.values.range : '';
  const hideRecordCommentValue =
    props.values && props.values.hiderecordcomment
      ? props.values.hiderecordcomment
      : '';

  const fields: Array<GenericFormItem> = [
    {
      fcName: 'sname',
      name: 'Scientific Name',
      type: 'custom',
      component: (
        <ScientificNameField
          occurrenceScientificName={acodeValue as OccurrenceScientificName}
          occurrenceScientificNames={scientificNames}
          openSnackbar={props.openSnackbar}
          tooltip={tooltips['acode']}
        />
      ),
    },
    { fcName: 'eventdate', name: 'Event Date', type: 'date' },
    { fcName: 'verbatimeventdate', name: 'Verbatim Event Date', type: 'text' },
    { fcName: 'recordedby', name: 'Recorded By', type: 'text' },
    {
      fcName: 'county',
      name: 'County',
      type: 'dropdown',
      options: county,
      value: county.find((county) => county.county === 'Unknown'),
      dropdownLabelFunctionPred: (county: county) => county.county,
      optionKey: 'gid',
      optionValueKey: 'county',
    },
    { fcName: 'locality', name: 'Locality', type: 'text' },
    { fcName: 'verbatimlocality', name: 'Verbatim Locality', type: 'text' },
    { fcName: 'behavior', name: 'Behavior', type: 'text' },
    { fcName: 'habitat', name: 'Habitat', type: 'text' },
    { fcName: 'sex', name: 'Sex', type: 'text' },
    { fcName: 'lifestage', name: 'Lifestage', type: 'text' },
    { fcName: 'associatedtaxa', name: 'Associated Taxa', type: 'text' },
    { fcName: 'elevation', name: 'Elevation', type: 'number' },
    { fcName: 'depth', name: 'Depth', type: 'number' },
    { fcName: 'depthaccuracy', name: 'Depth Accuracy', type: 'number' },
    {
      fcName: 'individuals',
      name: 'Individuals',
      type: 'custom',
      component: (
        <IndividualsField
          individualsMinValue={individualsMinValue}
          individualsMinTooltip={tooltips['individuals_min']}
          individualsMaxValue={individualdMaxValue}
          individualsMaxTooltip={tooltips['individuals_max']}
          individualsEstimatedValue={individualsEstimatedValue}
          setIndividualsEstimatedValue={setIndividualsEstimatedValue}
        />
      ),
    },
    {
      fcName: 'occurrenceremarks',
      name: 'Occurrence Remarks',
      type: 'multiline',
    },
    { fcName: 'taxonremarks', name: 'Taxon Remarks', type: 'multiline' },
    {
      fcName: 'institutioncode',
      name: 'Institution Code',
      type: 'dropdown',
      options: institution,
      value: institution.find(
        (institution) => institution.institutioncode === '',
      ),
      dropdownLabelFunctionPred: (option: institution) =>
        option.institution === 'None'
          ? option.institution
          : `${option.institutioncode}: ${option.institution}`,
      optionKey: 'institutioncode',
      optionValueKey: 'institution',
      optionValueKeyPrefix: 'institutioncode',
    },
    {
      fcName: 'basisofrecord',
      name: 'Basis of Record',
      type: 'dropdown',
      options: basisofrecord_lu,
      value: basisofrecord_lu.find((basisOfRecord) => basisOfRecord.id === -1),
      dropdownLabelFunctionPred: (option: basisofrecord_lu) =>
        option.basisofrecord,
      optionKey: 'id',
      optionValueKey: 'basisofrecord',
    },
    { fcName: 'catalognumber', name: 'Catalog Number', type: 'text' },
    {
      fcName: 'othercatalognumbers',
      name: 'Other Catalog Numbers',
      type: 'text',
    },
    { fcName: 'typestatus', name: 'Type Status', type: 'text' },
    { fcName: 'recordnumber', name: 'Record Number', type: 'text' },
    { fcName: 'samplingprotocol', name: 'Sampling Protocol', type: 'text' },
    { fcName: 'preparations', name: 'Preparations', type: 'text' },
    { fcName: 'primary_data', name: 'Primary Data', type: 'text' },
    {
      fcName: 'associatedreferences',
      name: 'Associated References',
      type: 'text',
    },
    {
      fcName: 'datasetname',
      name: 'Dataset Name',
      type: 'dropdown',
      options: source,
      value: source.find((source) => source.source === ''),
      dropdownLabelFunctionPred: (option: source) => option.description,
      optionKey: 'source',
      optionValueKey: 'description',
    },
    {
      fcName: 'coordinateprecision',
      name: 'Coordinate Precision',
      type: 'number',
    },
    { fcName: 'decimallatitude', name: 'Decimal Latitude', type: 'number' },
    { fcName: 'decimallongitude', name: 'Decimal Longitude', type: 'number' },
    { fcName: 'georeferencedby', name: 'Georeferenced By', type: 'text' },
    { fcName: 'georeferenceddate', name: 'Georeferenced Date', type: 'date' },
    {
      fcName: 'georeferenceremarks',
      name: 'Georeference Remarks',
      type: 'multiline',
    },
    {
      fcName: 'georeferencesources',
      name: 'Georeference Sources',
      type: 'text',
    },
    {
      fcName: 'georeferenceverificationstatus',
      name: 'Georeference Verification Status',
      type: 'dropdown',
      options: tfValues,
      value: tfValues.find((tfValue) => tfValue.display_name === 'False'),
      dropdownLabelFunctionPred: (option: TFValue) => option.display_name,
      optionKey: 'display_name',
      optionValueKey: 'display_name',
    },
    {
      fcName: 'problem_with_record',
      name: 'Problem With Record',
      type: 'multiline',
    },
    {
      fcName: 'previousidentifications',
      name: 'Previous Identifications',
      type: 'text',
    },
    {
      fcName: 'identificationverificationstatus',
      name: 'Identification Verification Status',
      type: 'multiline',
    },
    {
      fcName: 'identificationconfidence',
      name: 'Identification Confidence',
      type: 'dropdown',
      options: identificationConfidences,
      value: identificationConfidences.find(
        (identificationConfidence) =>
          identificationConfidence.confidence === 'Unknown',
      ),
      dropdownLabelFunctionPred: (option: IdentificationConfidence) =>
        option.confidence,
      optionKey: 'id',
      optionValueKey: 'confidence',
    },
    {
      fcName: 'identificationremarks',
      name: 'Identification Remarks',
      type: 'multiline',
    },
    {
      fcName: 'associatedoccurrences',
      name: 'Associated Occurrences',
      type: 'text',
    },
    {
      fcName: 'associatedsequences',
      name: 'Associated Sequences',
      type: 'text',
    },
    {
      fcName: 'entby',
      name: 'Entered By',
      type: 'text',
      value: username,
      disabled: true,
    },
    { fcName: 'mtr', name: 'MTR', type: 'text' },
    {
      fcName: 'township_ns',
      name: 'Township NS',
      type: 'custom',
      component: (
        <TownshipRangeField
          trType="township"
          trValue={townshipValue}
          trTooltip={tooltips['township']}
          directionType="ns"
          directions={ns}
          directionValue={nsValue}
          directionTooltip={tooltips['ns']}
          setDirectionValue={setNsValue}
        />
      ),
    },
    {
      fcName: 'range_ew',
      name: 'Range EW',
      type: 'custom',
      component: (
        <TownshipRangeField
          trType="range"
          trValue={rangeValue}
          trTooltip={tooltips['range']}
          directionType="ew"
          directions={ew}
          directionValue={ewValue}
          directionTooltip={tooltips['ew']}
          setDirectionValue={setEwValue}
        />
      ),
    },
    { fcName: 'section', name: 'Section', type: 'number' },
    { fcName: 'quarter', name: 'Quarter Section', type: 'text' },
    {
      fcName: 'relationshipremarks',
      name: 'Relationship Remarks',
      type: 'multiline',
    },
    {
      fcName: 'resourcetype',
      name: 'Resource Type',
      type: 'dropdown',
      options: resourcetype_lu,
      value: resourcetype_lu.find((resourceType) => resourceType.id === -1),
      dropdownLabelFunctionPred: (resourceType: resourcetype_lu) =>
        resourceType.resourcetype,
      optionKey: 'id',
      optionValueKey: 'resourcetype',
    },
    {
      fcName: 'hiderecordfield',
      name: 'Hide Record',
      type: 'custom',
      component: (
        <HideRecordField
          hideRecordValue={hideRecordValue}
          hideRecordCommentValue={hideRecordCommentValue}
          setHideRecordValue={setHideRecordValue}
        />
      ),
    },
  ];

  updateFormFieldValues<Tooltips<occurrence>>(tooltips, fields, 'tooltip');

  const validationSchema = yup.object({
    eventdate: yup.date(),
    county: yup.mixed<county>().required('County is required'),
    sex: yup
      .string()
      .matches(
        /^(((((1\x20male)|(((?!1{1})[0-9]+\x20males)|(1[0-9]+\x20males)))((\,\x20)?((1\x20female)|(((?!1{1})[0-9]+\x20females)|(1[0-9]+\x20females))))*)|(((1\x20female)|(((?!1{1})[0-9]+\x20females)|(1[0-9]+\x20females)))((\,\x20)?((1\x20male)|(((?!1{1})[0-9]+\x20males)|(1[0-9]+\x20males))))*))|male|female)$/,
        "Allowable values: 'female', 'male', or a string in the format 'x male(s), y female(s)' (e.g. 2 males, 4 females; 1 male, 1 female; 1 female, 4 males)",
      ),
    elevation: yup.number(),
    depth: yup.number(),
    depthAccuracy: yup.number(),
    institutioncode: yup
      .mixed<institution>()
      .required('Institution Code is required'),
    basisofrecord: yup
      .mixed<basisofrecord_lu>()
      .required('Basis of Record is required'),
    typestatus: yup.string().max(25),
    datasetname: yup.mixed<source>().required('Dataset Name is required'),
    coordinateprecision: yup.number().integer(),
    decimallatitude: yup
      .number()
      .min(33.6, 'Latitudes less than 33.6 are outside of Oklahoma')
      .max(37.1, 'Latitudes greater than 37.1 are outside of Oklahoma'),
    decimallongitude: yup
      .number()
      .min(-103.1, 'Longitudes less than -103.1 are outside of Oklahoma')
      .max(-94.4, 'Longitudes greater than -94.4 are outside of Oklahoma'),
    georeferenceverificationstatus: yup
      .mixed<TFValue>()
      .required('Georeference Verification Status is required'),
    identificationconfidence: yup
      .mixed<IdentificationConfidence>()
      .required('Identification Confidence is required'),
    section: yup
      .number()
      .integer()
      .min(1, 'Sections less than 1 are not valid')
      .max(36, 'Sections greater than 36 are not valid'),
    resourcetype: yup
      .mixed<resourcetype_lu>()
      .required('Resource Type is required'),
  });

  const handleValidationError = () => {
    props.openSnackbar(
      'Please correct the errors highlighted in red.',
      'error',
    );
  };

  if (props.values) {
    fields.unshift({
      fcName: 'gid',
      name: 'gid',
      type: 'text',
      value: props.values.gid,
      disabled: true,
    });

    remapRelationalFieldsForDisplay(
      relationalFieldMappings,
      props.values,
      props.relations,
    );
    updateFormFieldValues<occurrence>(props.values, fields, 'value');

    fields.find((field) => field.fcName === 'eventdate')!.value = props.values
      .eventdate
      ? new Date(
          props.values!.eventdate!.getUTCFullYear(),
          props.values!.eventdate!.getUTCMonth(),
          props.values!.eventdate!.getUTCDate(),
        )
      : null;
    fields.find(
      (field) => field.fcName === 'georeferenceverificationstatus',
    )!.value = props.relations.tfValues.find(
      (tfValue) =>
        tfValue.display_name === props.values?.georeferenceverificationstatus,
    );
    props.values.identificationconfidence
      ? (fields.find(
          (field) => field.fcName === 'identificationconfidence',
        )!.value = props.relations.identificationConfidences.find(
          (identificationConfidence) =>
            identificationConfidence.confidence ===
            props.values?.identificationconfidence,
        ))
      : (fields.find(
          (field) => field.fcName === 'identificationconfidence',
        )!.value = props.relations.identificationConfidences.find(
          (identificationConfidence) => identificationConfidence.id === 0,
        ));
  }

  const handleSubmit = async (values: occurrence) => {
    delete values['sname'];
    delete values['individuals'];
    delete values['township_ns'];
    delete values['range_ew'];
    delete values['hiderecordfield'];

    // If individuals_min was set via adding an estimate, and then removed, need to ensure that its value is set to null
    if (
      !values['individuals_estimated'] &&
      (values['individuals_min'] as unknown as string) !== ''
    ) {
      values['individuals_min'] = null;
    }

    // We want to have a date string provided to this field, even though it expects a Date object
    // @ts-expect-error
    values['entrydate'] = dayjs().format();

    await props.onSubmit(values);
  };

  const download = async () => {
    // TODO: implement
    props.openSnackbar('Functionality coming soon!');
  };

  return (
    <GenericForm
      title="Occurrence Data"
      fields={fields}
      onSubmit={handleSubmit}
      handleValidationError={handleValidationError}
      submitBtnConfig={{
        title: `${props.action} Occurrence`,
        icon: props.action === 'Create' ? <AddIcon /> : <EditIcon />,
      }}
      actionButtonConfig={
        props.values
          ? {
              title: 'Download Record',
              icon: <DownloadIcon />,
              action: download,
            }
          : undefined
      }
      validationSchema={validationSchema}
      borderColor={theme.palette.primary.main}
      width="100%"
      additionalDefaultValues={{
        acode: acodeValue,
        individuals_min: individualsMinValue,
        individuals_max: individualdMaxValue,
        individuals_estimated: individualsEstimatedValue,
        township: townshipValue,
        ns: nsValue,
        range: rangeValue,
        ew: ewValue,
        hiderecord: hideRecordValue,
        hiderecordcomment: hideRecordCommentValue,
      }}
    />
  );
}
