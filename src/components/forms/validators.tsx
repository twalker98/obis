import {
  getHightaxRecord,
  getVwAllTaxaRecord,
  searchHightaxRecords,
} from '../../app/taxa/actions';
import { cacheValue } from '../../utils/functions';

const familyValidator = cacheValue(
  (family: string) =>
    new Promise((resolve) => {
      setTimeout(async () => {
        const { prismaCallResponse, snackbarProps } = await getHightaxRecord(
          family,
        );

        if (!prismaCallResponse) {
          resolve(snackbarProps.message);
        }

        resolve(null);
      }, 1000);
    }),
);

const hightaxValidator = cacheValue(
  (value: string, field: 'class' | 'order' | 'phylum') =>
    new Promise((resolve) => {
      setTimeout(async () => {
        const { prismaCallResponse, snackbarProps } =
          await searchHightaxRecords(field, value);

        if (!prismaCallResponse) {
          resolve(snackbarProps.message);
        }

        if (prismaCallResponse?.length === 0) {
          resolve(`Value for ${field} doesn't exist in OBIS`);
        }

        resolve(prismaCallResponse);
      }, 1000);
    }),
);

const uniqueCodeValidator = cacheValue(
  (uniqueCode: string) =>
    new Promise((resolve) => {
      setTimeout(async () => {
        const { prismaCallResponse, snackbarProps } = await getVwAllTaxaRecord(
          uniqueCode,
        );

        if (
          !prismaCallResponse &&
          snackbarProps.message !== "Taxon doesn't exist"
        ) {
          resolve(snackbarProps.message);
        }

        if (prismaCallResponse) {
          resolve(`{{field}} is already in use by ${prismaCallResponse.sname}`);
        }

        resolve(null);
      }, 1000);
    }),
);

export { familyValidator, hightaxValidator, uniqueCodeValidator };
