'use client';

import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';

import { GenericForm, GenericFormItem } from '@twalk-tech/react-lib/nextjs';
import type { TFValue } from '../../types/relations';
import type { Tooltips } from '../../types/tooltips';
import {
  capitalizeFirstLetter,
  updateFormFieldValues,
} from '../../utils/functions';
import { comtax } from '../../utils/managers/obis/Comtax';
import { theme } from '../../utils/theme';
import { tooltips } from '../../utils/tooltips/Comtax';

import * as yup from 'yup';

type ComtaxFormProps = {
  action: 'create' | 'edit';
  tfValues: Array<TFValue>;
  values?: comtax;
  onSubmit: (values: comtax) => Promise<void>;
};

export default function ComtaxForm(props: ComtaxFormProps) {
  const fields: Array<GenericFormItem> = [
    {
      fcName: 'vernacularname',
      name: 'Vernacular Name',
      type: 'text',
      value: '',
    },
    {
      fcName: 'primary_name',
      name: 'Primary Name',
      type: 'dropdown',
      options: props.tfValues,
      value: props.tfValues.find((tfValue) => tfValue.display_name === 'False'),
      dropdownLabelFunctionPred: (option: TFValue) => option.display_name,
      optionKey: 'display_name',
      optionValueKey: 'display_name',
    },
  ];

  updateFormFieldValues<Tooltips<comtax>>(tooltips, fields, 'tooltip');

  const validationSchema = yup.object({
    vernacularname: yup.string().required('Vernacular Name is required'),
    primary_name: yup.mixed<TFValue>().required('Primary Name is required'),
  });

  if (props.values) {
    fields.find((field) => field.fcName === 'vernacularname')!.value =
      props.values.vernacularname;
    fields.find((field) => field.fcName === 'primary_name')!.value = props
      .values.primary_name
      ? props.tfValues.find((tfValue) => tfValue.display_name === 'True')
      : props.tfValues.find((tfValue) => tfValue.display_name === 'False');
  }

  return (
    <GenericForm
      title="Common Name Data"
      fields={fields}
      onSubmit={props.onSubmit}
      submitBtnConfig={{
        title: `${capitalizeFirstLetter(props.action)} Common Name`,
        icon: props.action === 'create' ? <AddIcon /> : <EditIcon />,
      }}
      validationSchema={validationSchema}
      borderColor={theme.palette.primary.main}
      width="100%"
    />
  );
}
