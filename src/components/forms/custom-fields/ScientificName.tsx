'use client';

import { GenericDropdown } from '@twalk-tech/react-lib/nextjs';
import { useState } from 'react';
import { searchAcctaxRecords } from '../../../app/taxa/actions';
import { cacheValue, sort } from '../../../utils/functions';
import { OpenSnackbarFn } from '../../../utils/hooks/snackbar';

export type OccurrenceScientificName = {
  acode: string;
  sname: string;
};

export type ScientificNameFieldProps = {
  occurrenceScientificName: OccurrenceScientificName | null;
  occurrenceScientificNames: Array<OccurrenceScientificName>;
  tooltip: string;
  openSnackbar: OpenSnackbarFn;
};

export default function ScientificNameField(props: ScientificNameFieldProps) {
  const [occurrenceScientificName, setOccurrenceScientificName] = useState(
    props.occurrenceScientificName,
  );
  const [searching, setSearching] = useState(false);
  const [occurrenceScientificNames, setOccurrenceScientificNames] = useState(
    props.occurrenceScientificNames,
  );

  const searchScientificNames = cacheValue(
    (scientificName: string) =>
      new Promise((resolve) => {
        setTimeout(async () => {
          const { prismaCallResponse, snackbarProps } =
            await searchAcctaxRecords(scientificName);

          if (!prismaCallResponse) {
            resolve(snackbarProps.message);
          }

          if (prismaCallResponse?.length === 0) {
            resolve('No scientific names found');
          }

          const scientificNames: Array<OccurrenceScientificName> = sort(
            prismaCallResponse?.map<OccurrenceScientificName>(
              (acctaxRecord) => {
                return {
                  acode: acctaxRecord.acode,
                  sname: acctaxRecord.sname!,
                };
              },
            )!,
            'sname',
          );

          setOccurrenceScientificNames(scientificNames);

          resolve(null);
        }, 1000);
      }),
  );

  const handleInputChange = async (
    event: React.ChangeEvent<HTMLInputElement>,
    newValue: OccurrenceScientificName,
  ) => {
    // If this is triggered by setting the value from the form the field exists in, there is no event
    if (!event) {
      return;
    }

    const value = event.target.value;

    // If the value passed is a number, need to just set the scientific name to be the new value (which is of the `OccurrenceScientificName` type)
    // If it's undefined, then it was cleared, and we don't want to search for it, otherwise an error message will be shown incorrectly
    if (typeof value === 'number' || value === undefined) {
      setOccurrenceScientificName(newValue);
      return;
    }

    if (occurrenceScientificName && value === occurrenceScientificName.sname) {
      return;
    }

    setSearching(true);

    const message = await searchScientificNames(
      value,
      occurrenceScientificName?.sname,
    );

    if (typeof message === 'string') {
      // searchScientificNames will return a string if and only if there was an error when searching
      props.openSnackbar(message, 'error');
    }

    setSearching(false);
  };

  const handleValueChange = async (
    field: string,
    value: OccurrenceScientificName,
  ) => {
    if (field !== 'acode') {
      throw Error('Unexpected error!');
    }

    setOccurrenceScientificName(value);
  };

  const validate = (value: OccurrenceScientificName) => {
    if (!value) {
      return 'Scientific Name is required';
    }
  };

  return (
    <GenericDropdown
      fcName="acode"
      name="Scientific Name"
      type="dropdown"
      value={occurrenceScientificName}
      tooltip={props.tooltip}
      options={occurrenceScientificNames}
      labelFunctionPred={(scientificName: OccurrenceScientificName) =>
        scientificName.sname
      }
      optionKey="acode"
      optionValueKey="sname"
      required={true}
      onInputChange={handleInputChange}
      searching={searching}
      onValueChange={handleValueChange}
      validateFn={validate}
    />
  );
}
