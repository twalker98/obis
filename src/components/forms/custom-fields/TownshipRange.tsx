'use client';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

import {
  GenericDropdown,
  GenericTextField,
} from '@twalk-tech/react-lib/nextjs';
import { useState } from 'react';
import { Direction } from '../../../types/relations';
import { capitalizeFirstLetter } from '../../../utils/functions';

export type TownshipRangeFieldProps = {
  trType: 'township' | 'range';
  trValue: number | string;
  trTooltip: string;
  directionType: 'ns' | 'ew';
  directions: Array<Direction>;
  directionValue: Direction | null;
  directionTooltip: string;
  setDirectionValue: (value: Direction) => void;
};

export default function TownshipRangeField(props: TownshipRangeFieldProps) {
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | undefined>(
    undefined,
  );

  const minValue = 1;
  const maxValue = props.trType === 'township' ? 29 : 28;
  const title = capitalizeFirstLetter(props.trType);

  const handleDirectionChange = async (
    field: string,
    value: Direction,
    shouldValidate?: boolean,
  ) => {
    if (field !== props.directionType) {
      throw Error('Unexpected error!');
    }

    props.setDirectionValue(value);
  };

  const validate = (value: number | null) => {
    let error = false;
    let errorMessage: string | undefined = undefined;

    if (value) {
      if (value < minValue) {
        error = true;
        errorMessage = `${title}s less than ${minValue} are not valid`;
      } else if (value > maxValue) {
        error = true;
        errorMessage = `${title}s greater than ${maxValue} are not valid`;
      }
    }

    setError(error);
    setErrorMessage(errorMessage);
    return errorMessage;
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={8}>
          <GenericTextField
            fcName={props.trType}
            type="number"
            name={title}
            value={props.trValue as unknown as string}
            tooltip={props.trTooltip}
            validateFn={validate}
            error={error}
            errorMessage={errorMessage}
          />
        </Grid>
        <Grid item xs={4}>
          <GenericDropdown
            fcName={props.directionType}
            type="dropdown"
            name={props.directionType
              .split('')
              .map((dir) => dir.toLocaleUpperCase())
              .join('/')}
            value={props.directionValue}
            tooltip={props.directionTooltip}
            onValueChange={handleDirectionChange}
            options={props.directions}
            labelFunctionPred={(dir: Direction) => dir.display_name}
            optionKey="direction"
            optionValueKey="display_name"
          />
        </Grid>
      </Grid>
    </Box>
  );
}
