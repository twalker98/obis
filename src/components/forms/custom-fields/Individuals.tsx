'use client';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

import { CheckboxField, GenericTextField } from '@twalk-tech/react-lib/nextjs';

export type TIndividualsProps = {
  estimated: boolean;
  individualsMinValue: number | string;
  individualsMinTooltip: string;
  individualsMaxValue: number | string;
  individualsMaxTooltip: string;
};

export type IndividualsFieldProps = {
  individualsMinValue: number | string;
  individualsMinTooltip: string;
  individualsMaxValue: number | string;
  individualsMaxTooltip: string;
  individualsEstimatedValue: boolean;
  setIndividualsEstimatedValue: (value: boolean) => void;
};

export function TIndividuals(props: TIndividualsProps) {
  if (props.estimated) {
    return (
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <GenericTextField
            fcName="individuals_min"
            type="number"
            name="Minimum Number of Individuals"
            value={props.individualsMinValue as unknown as string}
            tooltip={props.individualsMinTooltip}
          />
        </Grid>
        <Grid item xs={6}>
          <GenericTextField
            fcName="individuals_max"
            type="number"
            name="Maximum Number of Individuals"
            value={props.individualsMaxValue as unknown as string}
            tooltip={props.individualsMaxTooltip}
          />
        </Grid>
      </Grid>
    );
  } else {
    return (
      <GenericTextField
        fcName="individuals_max"
        type="number"
        name="Individual Count"
        value={props.individualsMaxValue as unknown as string}
        tooltip="Exact number of individuals."
      />
    );
  }
}

export default function IndividualsField(props: IndividualsFieldProps) {
  const handleIndividualsEstimatedChange = (value: boolean) => {
    props.setIndividualsEstimatedValue(value);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={10}>
          <TIndividuals
            estimated={props.individualsEstimatedValue as boolean}
            individualsMinValue={props.individualsMinValue}
            individualsMinTooltip={props.individualsMinTooltip}
            individualsMaxValue={props.individualsMaxValue}
            individualsMaxTooltip={props.individualsMaxTooltip}
          />
        </Grid>
        <Grid item xs={2}>
          <CheckboxField
            fcName="individuals_estimated"
            name="Estimate?"
            value={props.individualsEstimatedValue as boolean}
            onValueChange={handleIndividualsEstimatedChange}
          />
        </Grid>
      </Grid>
    </Box>
  );
}
