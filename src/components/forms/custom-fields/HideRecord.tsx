import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { CheckboxField, GenericTextField } from '@twalk-tech/react-lib/nextjs';

export type HideRecordFieldProps = {
  hideRecordValue: boolean;
  hideRecordCommentValue: string;
  setHideRecordValue: (value: boolean) => void;
};

export default function HideRecordField(props: HideRecordFieldProps) {
  const handleHideRecordChange = (value: boolean) => {
    props.setHideRecordValue(value);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={9}>
          <GenericTextField
            fcName="hiderecordcomment"
            type="text"
            name="Hide Record Comment"
            value={props.hideRecordCommentValue}
            disabled={!props.hideRecordValue}
          />
        </Grid>
        <Grid item xs={3}>
          <CheckboxField
            fcName="hiderecord"
            name="Hide Record?"
            value={props.hideRecordValue as boolean}
            onValueChange={handleHideRecordChange}
          />
        </Grid>
      </Grid>
    </Box>
  );
}
