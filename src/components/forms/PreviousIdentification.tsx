'use client';

import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';

import { GenericForm, GenericFormItem } from '@twalk-tech/react-lib/nextjs';
import type { Tooltips } from '../../types/tooltips';
import { capitalizeFirstLetter } from '../../utils/functions';
import { identification_verification } from '../../utils/obis-prisma';
import { tooltips } from '../../utils/tooltips/IdentificationVerification';

import { updateFormFieldValues } from '../../utils/functions';
import { theme } from '../../utils/theme';

type PreviousIdentificationFormProps = {
  action: 'create' | 'edit';
  values: identification_verification;
  catalogNumber: string;
  onSubmit: (values: identification_verification) => Promise<void>;
};

export default function PreviousIdentificationForm(
  props: PreviousIdentificationFormProps,
) {
  const fields: Array<GenericFormItem> = [
    {
      fcName: 'catalognumber',
      name: 'Catalog Number',
      type: 'text',
      value: props.catalogNumber,
      disabled: true,
    },
    {
      fcName: 'identifiedacode',
      name: 'Identified Acode',
      type: 'text',
      value: '',
    },
    {
      fcName: 'identifiedby',
      name: 'Identified By',
      type: 'text',
      value: '',
    },
    {
      fcName: 'identificationremarks',
      name: 'Identification Remarks',
      type: 'text',
      value: '',
    },
    {
      fcName: 'datelastmodified',
      name: 'Date Last Modified',
      type: 'text',
      value: '',
    },
  ];

  if (props.action === 'edit') {
    fields.unshift({
      fcName: 'pkey',
      name: 'pkey',
      type: 'text',
      value: props.values ? props.values.pkey : '',
      disabled: true,
    });

    fields.unshift({
      fcName: 'gid',
      name: 'gid',
      type: 'number',
      value: props.values ? props.values.gid : '',
      disabled: true,
    });
  }

  updateFormFieldValues<Tooltips<identification_verification>>(
    tooltips,
    fields,
    'tooltip',
  );

  if (props.values) {
    updateFormFieldValues<identification_verification>(
      props.values,
      fields,
      'value',
    );
  }

  return (
    <GenericForm
      title="Previous Identification Data"
      fields={fields}
      onSubmit={props.onSubmit}
      submitBtnConfig={{
        title: `${capitalizeFirstLetter(props.action)} Previous Identification`,
        icon: props.action === 'create' ? <AddIcon /> : <EditIcon />,
      }}
      borderColor={theme.palette.primary.main}
      width="100%"
    />
  );
}
