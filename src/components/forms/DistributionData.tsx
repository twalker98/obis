'use client';

import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';

import { GenericForm, GenericFormItem } from '@twalk-tech/react-lib/nextjs';
import { FormAction } from '../../types/form-action';
import { Relations } from '../../types/relations';
import {
  remapRelationalFieldsForDisplay,
  updateFormFieldValues,
} from '../../utils/functions';
import {
  d_dist_confidence,
  d_origin,
  d_population,
  d_presence_absence,
  d_regularity,
  distribution_data,
} from '../../utils/obis-prisma';
import { relationalFieldMappings } from '../../utils/relational_mappings/DistributionData';
import { theme } from '../../utils/theme';

import * as yup from 'yup';

export type DistributionDataFormProps = {
  relations: Relations;
  action: FormAction;
  acctaxRecordSubmitted: boolean;
  values?: distribution_data;
  onSubmit: (values: any) => Promise<void>;
};

export default function DistributionDataForm(props: DistributionDataFormProps) {
  const {
    d_dist_confidence,
    d_origin,
    d_population,
    d_presence_absence,
    d_regularity,
  } = props.relations;

  const fields: Array<GenericFormItem> = [
    {
      fcName: 'dist_confidence',
      name: 'Distribution Confidence',
      type: 'dropdown',
      options: d_dist_confidence,
      value: d_dist_confidence.find(
        (distributionConfidence) =>
          distributionConfidence.d_dist_confidence_id === -1,
      ),
      dropdownLabelFunctionPred: (distConfidence: d_dist_confidence) =>
        distConfidence.dist_confidence,
      optionKey: 'd_dist_confidence_id',
      optionValueKey: 'dist_confidence',
    },
    {
      fcName: 'origin',
      name: 'Origin',
      type: 'dropdown',
      options: d_origin,
      value: d_origin.find((origin) => origin.d_origin_id === -1),
      dropdownLabelFunctionPred: (origin: d_origin) => origin.origin,
      optionKey: 'd_origin_id',
      optionValueKey: 'origin',
    },
    {
      fcName: 'population',
      name: 'Population',
      type: 'dropdown',
      options: d_population,
      value: d_population.find(
        (population) => population.d_population_id === -1,
      ),
      dropdownLabelFunctionPred: (origin: d_population) => origin.population,
      optionKey: 'd_population_id',
      optionValueKey: 'population',
    },
    {
      fcName: 'presence_absence',
      name: 'Presence/Absence',
      type: 'dropdown',
      options: d_presence_absence,
      value: d_presence_absence.find(
        (presenceAbsence) => presenceAbsence.d_presence_absence_id === -1,
      ),
      dropdownLabelFunctionPred: (presenceAbsence: d_presence_absence) =>
        presenceAbsence.presence_absence,
      optionKey: 'd_presence_absence_id',
      optionValueKey: 'presence_absence',
    },
    {
      fcName: 'regularity',
      name: 'Regularity',
      type: 'dropdown',
      options: d_regularity,
      value: d_regularity.find(
        (regularity) => regularity.d_regularity_id === -1,
      ),
      dropdownLabelFunctionPred: (regularity: d_regularity) =>
        regularity.regularity,
      optionKey: 'd_regularity_id',
      optionValueKey: 'regularity',
    },
    {
      fcName: 'distribution_data_remarks',
      name: 'Distribution Data Remarks',
      type: 'multiline',
      value: '',
    },
  ];

  const validationSchema = yup.object({
    dist_confidence: yup
      .mixed<d_dist_confidence>()
      .required('Distribution Confidence is required'),
    origin: yup.mixed<d_origin>().required('Origin is required'),
    population: yup.mixed<d_population>().required('Population is required'),
    presence_absence: yup
      .mixed<d_presence_absence>()
      .required('Presence/Absence is required'),
    regularity: yup.mixed<d_regularity>().required('Regularity is required'),
  });

  if (props.values) {
    remapRelationalFieldsForDisplay(
      relationalFieldMappings,
      props.values,
      props.relations,
    );
    updateFormFieldValues<distribution_data>(props.values, fields, 'value');
  }

  return (
    <GenericForm
      title="Distribution Data"
      fields={fields}
      onSubmit={props.onSubmit}
      disabled={!props.acctaxRecordSubmitted}
      submitBtnConfig={{
        title: `${props.action} Distribution Data`,
        icon: props.action === 'Create' ? <AddIcon /> : <EditIcon />,
      }}
      validationSchema={validationSchema}
      borderColor={theme.palette.primary.main}
      width="100%"
    />
  );
}
