'use client';

import AddIcon from '@mui/icons-material/Add';

import {
  GenericForm,
  GenericFormItem,
  invalidInputAdornmentProps,
  validInputAdornmentProps,
  validatingInputAdornmentProps,
} from '@twalk-tech/react-lib/nextjs';
import { useState } from 'react';
import { Relations } from '../../types/relations';
import { Tooltips } from '../../types/tooltips';
import { updateFormFieldValues } from '../../utils/functions';
import {
  category_lu,
  hightax,
  kingdom_lu,
  name_category_desc_lu,
} from '../../utils/obis-prisma';
import { theme } from '../../utils/theme';
import { tooltips } from '../../utils/tooltips/Hightax';
import { hightaxValidator } from './validators';

import * as yup from 'yup';

export type HightaxFormProps = {
  relations: Relations;
  family: string;
  setCreateFamilyDialogOpen: (open: boolean) => void;
};

export default function HightaxForm(props: HightaxFormProps) {
  const { category_lu, kingdoms, name_category_desc_lu } = props.relations;

  category_lu.unshift({ a_id: -1, category: 'None' });
  kingdoms.unshift({ id: -1, kingdom: 'None' });
  name_category_desc_lu.unshift({ a_id: -1, name_category_desc: 'None' });

  // TODO: create a custom hook for all this
  const [orderValidationMessage, setOrderValidationMessage] = useState('');
  const [validatingOrder, setValidatingOrder] = useState(false);
  const [orderValid, setOrderValid] = useState<boolean | null>(null);
  const [orderValue, setOrderValue] = useState('');

  const [classValidationMessage, setClassValidationMessage] = useState('');
  const [validatingclass, setValidatingClass] = useState(false);
  const [classValid, setClassValid] = useState<boolean | null>(null);
  const [classValue, setClassValue] = useState('');

  const [phylumValidationMessage, setPhylumValidationMessage] = useState('');
  const [validatingphylum, setValidatingPhylum] = useState(false);
  const [phylumValid, setPhylumValid] = useState<boolean | null>(null);
  const [phylumValue, setPhylumValue] = useState('');

  const orderInputAdornmentProps = validatingOrder
    ? validatingInputAdornmentProps
    : orderValid
    ? validInputAdornmentProps
    : orderValid !== null
    ? invalidInputAdornmentProps
    : undefined;

  const classInputAdornmentProps = validatingclass
    ? validatingInputAdornmentProps
    : classValid
    ? validInputAdornmentProps
    : classValid !== null
    ? invalidInputAdornmentProps
    : undefined;

  const phylumInputAdornmentProps = validatingphylum
    ? validatingInputAdornmentProps
    : phylumValid
    ? validInputAdornmentProps
    : phylumValid !== null
    ? invalidInputAdornmentProps
    : undefined;

  const fields: Array<GenericFormItem> = [
    {
      fcName: 'family',
      name: 'Family',
      type: 'text',
      value: props.family,
      disabled: true,
      tooltipZIndex: 9999,
    },
    {
      fcName: 'taxorder',
      name: 'Order',
      type: 'text',
      value: orderValue,
      tooltipZIndex: 9999,
      inputProps: orderInputAdornmentProps,
    },
    {
      fcName: 'taxclass',
      name: 'Class',
      type: 'text',
      value: classValue,
      tooltipZIndex: 9999,
      inputProps: classInputAdornmentProps,
    },
    {
      fcName: 'phylum',
      name: 'Phylum',
      type: 'text',
      value: phylumValue,
      tooltipZIndex: 9999,
      inputProps: phylumInputAdornmentProps,
    },
    {
      fcName: 'kingdom',
      name: 'Kingdom',
      type: 'dropdown',
      options: kingdoms,
      value: kingdoms.find((kingdom) => kingdom.id === -1),
      dropdownLabelFunctionPred: (kingdom: kingdom_lu) => kingdom.kingdom,
      optionKey: 'id',
      optionValueKey: 'kingdom',
      tooltipZIndex: 9999,
    },
    {
      fcName: 'category',
      name: 'Category',
      type: 'dropdown',
      options: category_lu,
      value: category_lu.find((category) => category.a_id === -1),
      dropdownLabelFunctionPred: (category: category_lu) => category.category,
      optionKey: 'a_id',
      optionValueKey: 'category',
      tooltipZIndex: 9999,
    },
    {
      fcName: 'name_category_desc',
      name: 'Name Category Description',
      type: 'dropdown',
      options: name_category_desc_lu,
      value: name_category_desc_lu.find(
        (name_category_desc) => name_category_desc.a_id === -1,
      ),
      dropdownLabelFunctionPred: (name_category_desc: name_category_desc_lu) =>
        name_category_desc.name_category_desc,
      optionKey: 'a_id',
      optionValueKey: 'name_category_desc',
      tooltipZIndex: 9999,
    },
  ];

  updateFormFieldValues<Tooltips<hightax>>(tooltips, fields, 'tooltip');

  const createHightax = async (values: hightax) => {
    console.log('create hightax');
    console.log(values);

    props.setCreateFamilyDialogOpen(false);
  };

  const validationSchema = yup.object({
    family: yup.string().required('Family is required'),
    taxorder: yup
      .string()
      .required('Order is required')
      .test('order_validator', orderValidationMessage, async function (value) {
        if (value !== orderValue) {
          setValidatingOrder(true);
          setOrderValid(null);
        }

        let message = await hightaxValidator(value, orderValue, 'order');
        setOrderValue(value);

        if (typeof message === 'string') {
          setOrderValidationMessage(message);
          setOrderValid(false);
          setValidatingOrder(false);
          return this.createError({ message: message });
        } else if (typeof message === 'object') {
          const hightax = (message as Array<hightax>)[0];
          setOrderValidationMessage('');
          setOrderValid(true);
          setValidatingOrder(false);

          return true;
        }

        return orderValid as boolean;
      }),
    taxclass: yup
      .string()
      .required('Class is required')
      .test('class_validator', classValidationMessage, async function (value) {
        if (value !== classValue) {
          setValidatingClass(true);
          setClassValid(null);
        }

        let message = await hightaxValidator(value, classValue, 'class');
        setClassValue(value);

        if (typeof message === 'string') {
          setClassValidationMessage(message);
          setClassValid(false);
          setValidatingClass(false);
          return this.createError({ message: message });
        } else if (typeof message === 'object') {
          const hightax = (message as Array<hightax>)[0];
          setClassValidationMessage('');
          setClassValid(true);
          setValidatingClass(false);

          return true;
        }

        return classValid as boolean;
      }),
    phylum: yup
      .string()
      .required('Phylum is required')
      .test(
        'phylum_validator',
        phylumValidationMessage,
        async function (value) {
          if (value !== phylumValue) {
            setValidatingPhylum(true);
            setPhylumValid(null);
          }

          let message = await hightaxValidator(value, phylumValue, 'phylum');
          setPhylumValue(value);

          if (typeof message === 'string') {
            setPhylumValidationMessage(message);
            setPhylumValid(false);
            setValidatingPhylum(false);
            return this.createError({ message: message });
          } else if (typeof message === 'object') {
            const hightax = (message as Array<hightax>)[0];
            setPhylumValidationMessage('');
            setPhylumValid(true);
            setValidatingPhylum(false);

            return true;
          }

          return phylumValid as boolean;
        },
      ),
    kingdom: yup.mixed<kingdom_lu>().required('Kingdom is required'),
    category: yup.mixed<category_lu>().required('Category is required'),
    name_category_desc: yup
      .mixed<name_category_desc_lu>()
      .required('Name Category Description is required'),
  });

  return (
    <GenericForm
      title="Higher Taxonomic Data"
      fields={fields}
      onSubmit={createHightax}
      submitBtnConfig={{
        title: 'Create Higher Taxonomic Record',
        icon: <AddIcon />,
      }}
      validationSchema={validationSchema}
      borderColor={theme.palette.primary.main}
      width="100%"
    />
  );
}
