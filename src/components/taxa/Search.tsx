'use client';

import SearchIcon from '@mui/icons-material/Search';
import Container from '@mui/material/Container';

import {
  ActionButton,
  GenericTextField,
  buildInputAdornmentProps,
} from '@twalk-tech/react-lib/nextjs/components';
import { KeyboardEvent, useContext, useEffect, useState } from 'react';
import {
  TTaxaSearchbarValueContext,
  TaxaSearchbarValueContext,
} from '../../app/taxa/context';

type SearchProps = {
  search: (query: string) => any;
};

export default function Search(props: SearchProps) {
  const { value, setValue } = useContext(
    TaxaSearchbarValueContext,
  ) as TTaxaSearchbarValueContext;
  const [query, setQuery] = useState(value);

  useEffect(() => {
    setQuery(value);
  }, [value]);

  const enterKeyPress = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      props.search(query);
    }
  };

  const handleValueChange = (event: any) => {
    setValue(event.target.value);
  };

  const searchIconInputAdornmentProps = buildInputAdornmentProps({
    type: 'text',
    position: 'start',
    adornment: <SearchIcon />,
  });

  return (
    <Container sx={{ mb: 2 }}>
      <GenericTextField
        fcName="taxaSearch"
        variant="outlined"
        type="text"
        width="75%"
        inputProps={searchIconInputAdornmentProps}
        onValueChange={handleValueChange}
        value={query}
        onKeyDown={enterKeyPress}
        standalone={true}
      />
      <ActionButton
        title="Search"
        onButtonClick={() => props.search(query)}
        startIcon={<SearchIcon />}
      />
    </Container>
  );
}
