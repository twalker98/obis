'use client';

import CheckIcon from '@mui/icons-material/Check';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';

import { useState } from 'react';
import { searchHightaxRecords } from '../../../../app/taxa/actions';

import {
  ActionButton,
  GenericTextField,
  invalidInputAdornmentProps,
  validInputAdornmentProps,
  validatingInputAdornmentProps,
} from '@twalk-tech/react-lib/nextjs';
import { hightax } from '../../../../utils/managers/obis/Hightax';

export type ClassFieldProps = {
  classValid: boolean | null;
  classValue: string;
  createNewClass: boolean;
  setClassValid: (valid: boolean) => void;
  setClassValue: (value: string) => void;
  setCreateNewClass: (value: boolean) => void;
  setHightax: (value: hightax) => void;
};

export default function ClassField(props: ClassFieldProps) {
  const [classValidationMessage, setClassValidationMessage] =
    useState<string>('');
  const [validatingClass, setValidatingClass] = useState(false);
  const [classDisabled, setClassDisabled] = useState(false);

  const classInputAdornmentProps = validatingClass
    ? validatingInputAdornmentProps
    : props.classValid
    ? validInputAdornmentProps
    : props.classValid !== null
    ? invalidInputAdornmentProps
    : undefined;

  const handleValueChange = (event: any) => {
    const value = event.target.value;

    if (value === '') {
      props.setClassValid(false);
      setClassValidationMessage('');
      setValidatingClass(false);
      props.setClassValue('');
      props.setCreateNewClass(false);
    }

    props.setClassValue(event.target.value);
  };

  const handleCreateNewClass = (event: React.ChangeEvent<HTMLInputElement>) => {
    const checked = event.target.checked;
    props.setCreateNewClass(checked);

    if (checked) {
      props.setClassValid(true);
      setClassValidationMessage('');
    }
  };

  const validate = async () => {
    setValidatingClass(true);
    props.setCreateNewClass(false);

    const { prismaCallResponse, snackbarProps } = await searchHightaxRecords(
      'class',
      props.classValue,
    );

    if (!prismaCallResponse) {
      props.setClassValid(false);
      setClassValidationMessage(snackbarProps.message);
    }

    if (prismaCallResponse?.length === 0) {
      props.setClassValid(false);
      setClassValidationMessage("Value for class doesn't exist in OBIS");
    } else {
      props.setClassValid(true);
      setClassDisabled(true);
      setClassValidationMessage('');
      props.setHightax(prismaCallResponse![0]);
    }

    setValidatingClass(false);
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={8}>
        <GenericTextField
          fcName="taxclass"
          name="Class"
          type="text"
          value={props.classValue}
          onValueChange={handleValueChange}
          error={classValidationMessage !== ''}
          errorMessage={classValidationMessage}
          inputProps={classInputAdornmentProps}
          disabled={classDisabled}
          standalone
        />
        {props.classValid !== null && !props.classValid ? (
          <FormControlLabel
            label="Create class?"
            control={
              <Checkbox
                color="primary"
                checked={props.createNewClass}
                onChange={handleCreateNewClass}
                inputProps={{ 'aria-label': 'controlled' }}
              />
            }
          />
        ) : (
          <></>
        )}
      </Grid>
      <Grid item xs={4}>
        <ActionButton
          title="Validate"
          startIcon={<CheckIcon />}
          onButtonClick={validate}
          disabled={props.classValue.length === 0}
        />
      </Grid>
    </Grid>
  );
}
