'use client';

import CheckIcon from '@mui/icons-material/Check';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';

import { useState } from 'react';
import { searchHightaxRecords } from '../../../../app/taxa/actions';

import {
  ActionButton,
  GenericTextField,
  invalidInputAdornmentProps,
  validInputAdornmentProps,
  validatingInputAdornmentProps,
} from '@twalk-tech/react-lib/nextjs';
import { hightax } from '../../../../utils/managers/obis/Hightax';

export type OrderFieldProps = {
  orderValid: boolean | null;
  orderValue: string;
  createNewOrder: boolean;
  setOrderValid: (valid: boolean) => void;
  setOrderValue: (value: string) => void;
  setCreateNewOrder: (value: boolean) => void;
  setHightax: (value: hightax) => void;
};

export default function OrderField(props: OrderFieldProps) {
  const [orderValidationMessage, setOrderValidationMessage] =
    useState<string>('');
  const [validatingOrder, setValidatingOrder] = useState(false);
  const [orderDisabled, setOrderDisabled] = useState(false);

  const orderInputAdornmentProps = validatingOrder
    ? validatingInputAdornmentProps
    : props.orderValid
    ? validInputAdornmentProps
    : props.orderValid !== null
    ? invalidInputAdornmentProps
    : undefined;

  const handleValueChange = (event: any) => {
    const value = event.target.value;

    if (value === '') {
      props.setOrderValid(false);
      setOrderValidationMessage('');
      setValidatingOrder(false);
      props.setOrderValue('');
      props.setCreateNewOrder(false);
    }

    props.setOrderValue(event.target.value);
  };

  const handleCreateNewOrder = (event: React.ChangeEvent<HTMLInputElement>) => {
    const checked = event.target.checked;
    props.setCreateNewOrder(checked);

    if (checked) {
      props.setOrderValid(true);
      setOrderValidationMessage('');
    }
  };

  const validate = async () => {
    setValidatingOrder(true);
    props.setCreateNewOrder(false);

    const { prismaCallResponse, snackbarProps } = await searchHightaxRecords(
      'order',
      props.orderValue,
    );

    if (!prismaCallResponse) {
      props.setOrderValid(false);
      setOrderValidationMessage(snackbarProps.message);
    }

    if (prismaCallResponse?.length === 0) {
      props.setOrderValid(false);
      setOrderValidationMessage("Value for order doesn't exist in OBIS");
    } else {
      props.setOrderValid(true);
      setOrderDisabled(true);
      setOrderValidationMessage('');
      props.setHightax(prismaCallResponse![0]);
    }

    setValidatingOrder(false);
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={8}>
        <GenericTextField
          fcName="taxorder"
          name="Order"
          type="text"
          value={props.orderValue}
          onValueChange={handleValueChange}
          error={orderValidationMessage !== ''}
          errorMessage={orderValidationMessage}
          inputProps={orderInputAdornmentProps}
          disabled={orderDisabled}
          standalone
        />
        {props.orderValid !== null && !props.orderValid ? (
          <FormControlLabel
            label="Create order?"
            control={
              <Checkbox
                color="primary"
                checked={props.createNewOrder}
                onChange={handleCreateNewOrder}
                inputProps={{ 'aria-label': 'controlled' }}
              />
            }
          />
        ) : (
          <></>
        )}
      </Grid>
      <Grid item xs={4}>
        <ActionButton
          title="Validate"
          startIcon={<CheckIcon />}
          onButtonClick={validate}
          disabled={props.orderValue.length === 0}
        />
      </Grid>
    </Grid>
  );
}
