'use client';

import { GenericDropdown } from '@twalk-tech/react-lib/nextjs';
import { capitalizeFirstLetter } from '../../../../utils/functions';
import { category_lu } from '../../../../utils/managers/obis/Category';
import { kingdom_lu } from '../../../../utils/managers/obis/Kingdom';
import { name_category_desc_lu } from '../../../../utils/managers/obis/NameCategoryDescription';

type HightaxFormDropdown = category_lu | kingdom_lu | name_category_desc_lu;

export type DropdownFieldProps = {
  options: Array<HightaxFormDropdown>;
  field: string;
  value: HightaxFormDropdown | null;
  labelFunctionPred: (option: HightaxFormDropdown) => string;
  optionKey: string;
  optionValueKey: string;
  setValue: (value: any) => void;
};

export default function DropdownField(props: DropdownFieldProps) {
  const handleChange = async (field: string, value: HightaxFormDropdown) => {
    if (field !== props.field) {
      throw Error('Unexpected error!');
    }

    props.setValue(value);
  };

  return (
    <GenericDropdown
      fcName={props.field}
      name={capitalizeFirstLetter(props.field)}
      type="dropdown"
      options={props.options}
      value={!props.value ? '' : props.value}
      onValueChange={handleChange}
      labelFunctionPred={props.labelFunctionPred}
      optionKey={props.optionKey}
      optionValueKey={props.optionValueKey}
      standalone
    />
  );
}
