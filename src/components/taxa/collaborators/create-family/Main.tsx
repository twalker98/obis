'use client';

import Check from '@mui/icons-material/Check';
import ErrorIcon from '@mui/icons-material/Error';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import Divider from '@mui/material/Divider';
import Paper from '@mui/material/Paper';
import Step from '@mui/material/Step';
import StepContent from '@mui/material/StepContent';
import StepLabel from '@mui/material/StepLabel';
import Stepper from '@mui/material/Stepper';
import Typography from '@mui/material/Typography';
import { useState } from 'react';
import { createHightaxRecordAction } from '../../../../app/taxa/actions';
import { Relations } from '../../../../types/relations';
import { OpenSnackbarFn } from '../../../../utils/hooks/snackbar';
import {
  category_lu,
  hightax,
  kingdom_lu,
  name_category_desc_lu,
} from '../../../../utils/obis-prisma';
import { theme } from '../../../../utils/theme';
import ClassField from './Class';
import DropdownField from './Dropdown';
import OrderField from './Order';
import PhylumField from './Phylum';

export type CreateFamilyMainComponentProps = {
  relations: Relations;
  family: string;
  familyCreated: boolean | null;
  setFamilyCreated: (familyCreated: boolean) => void;
  openSnackbar: OpenSnackbarFn;
};

export type Step = {
  id: number;
  label: string;
  component: JSX.Element;
  valid: boolean | null;
  value: string;
};

export default function CreateFamilyMainComponent(
  props: CreateFamilyMainComponentProps,
) {
  const { category_lu, kingdoms, name_category_desc_lu } = props.relations;
  const [activeStep, setActiveStep] = useState(0);
  const [hightax, setHightax] = useState<hightax | null>(null);

  const [orderValid, setOrderValid] = useState<boolean | null>(null);
  const [orderValue, setOrderValue] = useState('');
  const [createNewOrder, setCreateNewOrder] = useState(false);

  const [classValid, setClassValid] = useState<boolean | null>(null);
  const [classValue, setClassValue] = useState('');
  const [createNewClass, setCreateNewClass] = useState(false);

  const [phylumValid, setPhylumValid] = useState<boolean | null>(null);
  const [phylumValue, setPhylumValue] = useState('');
  const [createNewPhylum, setCreateNewPhylum] = useState(false);
  const [phylumDisabled, setPhylumDisabled] = useState(false);

  const [kingdomValue, setKingdomValue] = useState<kingdom_lu | null>(null);
  const [categoryValue, setCategoryValue] = useState<category_lu | null>(null);
  const [nameCategoryDescriptionValue, setNameCategoryDescriptionValue] =
    useState<name_category_desc_lu | null>(null);

  const [creatingFamily, setCreatingFamily] = useState(false);

  const steps: Array<Step> = [
    {
      id: 0,
      label: 'Input existing or create new order',
      component: (
        <OrderField
          orderValid={orderValid}
          orderValue={orderValue}
          createNewOrder={createNewOrder}
          setOrderValid={setOrderValid}
          setOrderValue={setOrderValue}
          setCreateNewOrder={setCreateNewOrder}
          setHightax={setHightax}
        />
      ),
      valid: orderValid,
      value: orderValue,
    },
    {
      id: 1,
      label: 'Input existing or create new class',
      component: (
        <ClassField
          classValid={classValid}
          classValue={classValue}
          createNewClass={createNewClass}
          setClassValid={setClassValid}
          setClassValue={setClassValue}
          setCreateNewClass={setCreateNewClass}
          setHightax={setHightax}
        />
      ),
      valid: classValid,
      value: classValue,
    },
    {
      id: 2,
      label: 'Input existing or create new phylum',
      component: (
        <PhylumField
          phylumValid={phylumValid}
          phylumValue={phylumValue}
          createNewPhylum={createNewPhylum}
          phylumDisabled={phylumDisabled}
          setPhylumValid={setPhylumValid}
          setPhylumValue={setPhylumValue}
          setCreateNewPhylum={setCreateNewPhylum}
          setHightax={setHightax}
        />
      ),
      valid: phylumValid,
      value: phylumValue,
    },
    {
      id: 3,
      label: 'Select kingdom',
      component: (
        <DropdownField
          options={kingdoms}
          field="kingdom"
          value={kingdomValue}
          labelFunctionPred={(kingdom: kingdom_lu) => kingdom.kingdom}
          optionKey="id"
          optionValueKey="kingdom"
          setValue={setKingdomValue}
        />
      ),
      valid: kingdomValue !== null,
      value: kingdomValue?.kingdom!,
    },
    {
      id: 4,
      label: 'Select category',
      component: (
        <DropdownField
          options={category_lu}
          field="category"
          value={categoryValue}
          labelFunctionPred={(category: category_lu) => category.category}
          optionKey="a_id"
          optionValueKey="category"
          setValue={setCategoryValue}
        />
      ),
      valid: categoryValue !== null,
      value: categoryValue?.category!,
    },
    {
      id: 5,
      label: 'Select name category description',
      component: (
        <DropdownField
          options={name_category_desc_lu}
          field="Name Category Description"
          value={nameCategoryDescriptionValue}
          labelFunctionPred={(nameCategoryDescription: name_category_desc_lu) =>
            nameCategoryDescription.name_category_desc
          }
          optionKey="a_id"
          optionValueKey="name_category_desc"
          setValue={setNameCategoryDescriptionValue}
        />
      ),
      valid: nameCategoryDescriptionValue !== null,
      value: nameCategoryDescriptionValue?.name_category_desc!,
    },
  ];

  const handleNext = () => {
    if (!createNewOrder && activeStep === 0) {
      setClassValue(hightax?.taxclass!);
      setPhylumValue(hightax?.phylum!);
      setKingdomValue(
        kingdoms.find((kingdom) => kingdom.kingdom === hightax?.kingdom)!,
      );
      setActiveStep(3);
    }

    if (!createNewClass && activeStep === 1) {
      setPhylumValue(hightax?.phylum!);
      setKingdomValue(
        kingdoms.find((kingdom) => kingdom.kingdom === hightax?.kingdom)!,
      );
      setPhylumDisabled(true);
      setCreateNewClass(true);
      setPhylumValid(true);
      setActiveStep(3);
    } else {
      setPhylumValid(null);
      setPhylumDisabled(false);
    }

    if (!createNewPhylum && activeStep === 2) {
      setKingdomValue(
        kingdoms.find((kingdom) => kingdom.kingdom === hightax?.kingdom)!,
      );
      setCreateNewPhylum(true);
      setActiveStep(3);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);

    if (activeStep === steps.length - 1) {
      createHightaxRecord();
    }
  };

  const buildHightaxValues = () => {
    const hightax: hightax = {
      kingdom: kingdomValue?.kingdom!,
      phylum: phylumValue,
      taxclass: classValue,
      taxorder: orderValue,
      family: props.family,
      category: categoryValue?.category!,
      name_category_desc: nameCategoryDescriptionValue?.name_category_desc!,
      name_type_desc: kingdomValue?.kingdom === 'Animalia' ? 'Animal' : 'Plant',
    };

    return hightax;
  };

  const createHightaxRecord = async () => {
    setCreatingFamily(true);
    const hightax = buildHightaxValues();

    const { prismaCallResponse, snackbarProps } =
      await createHightaxRecordAction(hightax);

    if (!prismaCallResponse) {
      props.openSnackbar(snackbarProps.message, snackbarProps.severity);
      setCreatingFamily(false);
      props.setFamilyCreated(false);
      return;
    }

    setCreatingFamily(false);
    props.setFamilyCreated(true);
  };

  return (
    <Box sx={{ minWidth: 500, maxWidth: 500 }}>
      <Typography variant="h5" color={theme.palette.primary.main}>
        Create New Family: <b>{props.family}</b>
      </Typography>
      <Divider
        sx={{
          borderBottomWidth: 3,
          backgroundColor: theme.palette.primary.main,
          mb: 1,
        }}
      />
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((step, index) => (
          <Step key={step.label}>
            <StepLabel
              optional={<Typography variant="caption">{step.value}</Typography>}
            >
              {step.label}
            </StepLabel>
            <StepContent>
              {step.component}
              <Box sx={{ mb: 2 }}>
                <div>
                  <Button
                    variant="contained"
                    onClick={handleNext}
                    sx={{ mt: 1, mr: 1 }}
                    disabled={!step.valid}
                  >
                    {index === steps.length - 1
                      ? 'Create New Family'
                      : 'Continue'}
                  </Button>
                </div>
              </Box>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} sx={{ p: 3 }}>
          {creatingFamily ? (
            <>
              <CircularProgress color="primary" />
              <Typography>Creating family...</Typography>
            </>
          ) : props.familyCreated ? (
            <>
              <Check color="primary" />
              <Typography>Created new family</Typography>
            </>
          ) : (
            <>
              <ErrorIcon color="error" />
              <Typography>Failed to create new family</Typography>
            </>
          )}
        </Paper>
      )}
    </Box>
  );
}
