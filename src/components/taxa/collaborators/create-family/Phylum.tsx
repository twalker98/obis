'use client';

import CheckIcon from '@mui/icons-material/Check';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';

import { useState } from 'react';
import { searchHightaxRecords } from '../../../../app/taxa/actions';

import {
  ActionButton,
  GenericTextField,
  invalidInputAdornmentProps,
  validInputAdornmentProps,
  validatingInputAdornmentProps,
} from '@twalk-tech/react-lib/nextjs';
import { hightax } from '../../../../utils/managers/obis/Hightax';

export type PhylumFieldProps = {
  phylumValid: boolean | null;
  phylumValue: string;
  createNewPhylum: boolean;
  phylumDisabled: boolean;
  setPhylumValid: (valid: boolean) => void;
  setPhylumValue: (value: string) => void;
  setCreateNewPhylum: (value: boolean) => void;
  setHightax: (value: hightax) => void;
};

export default function PhylumField(props: PhylumFieldProps) {
  const [phylumValidationMessage, setPhylumValidationMessage] =
    useState<string>('');
  const [validatingPhylum, setValidatingPhylum] = useState(false);
  const [phylumDisabled, setPhylumDisabled] = useState(props.phylumDisabled);

  const phylumInputAdornmentProps = validatingPhylum
    ? validatingInputAdornmentProps
    : props.phylumValid
    ? validInputAdornmentProps
    : props.phylumValid !== null
    ? invalidInputAdornmentProps
    : undefined;

  const handleValueChange = (event: any) => {
    const value = event.target.value;

    if (value === '') {
      props.setPhylumValid(false);
      setPhylumValidationMessage('');
      setValidatingPhylum(false);
      props.setPhylumValue('');
      props.setCreateNewPhylum(false);
    }

    props.setPhylumValue(event.target.value);
  };

  const handleCreateNewPhylum = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    const checked = event.target.checked;
    props.setCreateNewPhylum(checked);

    if (checked) {
      props.setPhylumValid(true);
      setPhylumValidationMessage('');
    }
  };

  const validate = async () => {
    setValidatingPhylum(true);
    props.setCreateNewPhylum(false);

    const { prismaCallResponse, snackbarProps } = await searchHightaxRecords(
      'phylum',
      props.phylumValue,
    );

    if (!prismaCallResponse) {
      props.setPhylumValid(false);
      setPhylumValidationMessage(snackbarProps.message);
    }

    if (prismaCallResponse?.length === 0) {
      props.setPhylumValid(false);
      setPhylumValidationMessage("Value for phylum doesn't exist in OBIS");
    } else {
      props.setPhylumValid(true);
      setPhylumDisabled(true);
      setPhylumValidationMessage('');
      props.setHightax(prismaCallResponse![0]);
    }

    setValidatingPhylum(false);
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={8}>
        <GenericTextField
          fcName="taxphylum"
          name="Phylum"
          type="text"
          value={props.phylumValue}
          onValueChange={handleValueChange}
          error={phylumValidationMessage !== ''}
          errorMessage={phylumValidationMessage}
          inputProps={phylumInputAdornmentProps}
          disabled={phylumDisabled}
          standalone
        />
        {props.phylumValid !== null && !props.phylumValid ? (
          <FormControlLabel
            label="Create phylum?"
            control={
              <Checkbox
                color="primary"
                checked={props.createNewPhylum}
                onChange={handleCreateNewPhylum}
                inputProps={{ 'aria-label': 'controlled' }}
              />
            }
          />
        ) : (
          <></>
        )}
      </Grid>
      <Grid item xs={4}>
        <ActionButton
          title="Validate"
          startIcon={<CheckIcon />}
          onButtonClick={validate}
          disabled={false}
        />
      </Grid>
    </Grid>
  );
}
