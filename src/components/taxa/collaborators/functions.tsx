import { TaxaListItem } from '../../../types/taxa-list-item';
import { OpenSnackbarFn } from '../../../utils/hooks/snackbar';
import { comtax } from '../../../utils/managers/obis/Comtax';
import { syntax } from '../../../utils/managers/obis/Syntax';

function getPrimaryName(
  commonNames: Array<TaxaListItem>,
): TaxaListItem | undefined {
  return commonNames.find(
    (commonName) => (commonName.item as comtax).primary_name !== null,
  );
}

function getSynonym(
  synonyms: Array<TaxaListItem>,
  scode: string,
): TaxaListItem | undefined {
  return synonyms.find((synoynm) => (synoynm.item as syntax).scode === scode);
}

export function validateCommonNameSubmission(
  values: comtax,
  commonNames: Array<TaxaListItem>,
  openSnackbar: OpenSnackbarFn,
  originalValues?: comtax,
  edit?: boolean,
): boolean {
  const primaryName = getPrimaryName(commonNames);
  const existingCommonName = commonNames.find(
    (commonName) => commonName.name === values.vernacularname,
  );

  if (!edit) {
    // This is ignored, as the value from the form WILL be 'True' or 'False'
    // @ts-ignore
    if (primaryName && values.primary_name.display_name === 'True') {
      openSnackbar(
        "There is already a primary name listed for this taxon. Please ensure this new common name should be marked as the taxon's primary name, or update the existing one first.",
        'error',
        10000,
      );
      return false;
    }

    if (existingCommonName) {
      openSnackbar(
        `The common name "${values.vernacularname}" already exists for this taxon.`,
        'error',
      );
      return false;
    }
  } else {
    if (
      primaryName &&
      existingCommonName &&
      originalValues &&
      primaryName !== existingCommonName &&
      originalValues.primary_name !== values.primary_name &&
      originalValues?.vernacularname === values.vernacularname
    ) {
      openSnackbar(
        "There is already a primary name listed for this taxon. Please ensure this new common name should be marked as the taxon's primary name, or update the existing one first.",
        'error',
        10000,
      );
      return false;
    }

    if (existingCommonName) {
      if (
        originalValues &&
        originalValues.vernacularname === values.vernacularname &&
        originalValues.primary_name !== values.primary_name
      ) {
        return true;
      }

      openSnackbar(
        `The common name "${values.vernacularname}" already exists for this taxon.`,
        'error',
      );
      return false;
    }
  }

  return true;
}

export function createCommonNameItem(
  values: comtax,
  acode: string,
): TaxaListItem {
  const newCommonName: TaxaListItem = {
    name: values.vernacularname!,
    secondary:
      // This is ignored, as the value from the form WILL be 'True' or 'False'
      // @ts-ignore
      values.primary_name.display_name === 'True' ? 'primary name' : undefined,
    item: {
      ...values,
      acode: acode,
      // This is ignored, as the value from the form WILL be 'True' or 'False'
      // @ts-ignore
      primary_name: values.primary_name.display_name === 'True' ? true : false,
    },
  };

  return newCommonName;
}

export function validateSynonymSubmission(
  values: syntax,
  synonyms: Array<TaxaListItem>,
  openSnackbar: OpenSnackbarFn,
  originalValues?: syntax,
  edit?: boolean,
): boolean {
  const synonymWithScode = getSynonym(synonyms, values.scode);
  const existingSynonym = synonyms.find(
    (synonym) => synonym.name === values.sname,
  );

  if (!edit) {
    if (synonymWithScode) {
      openSnackbar(
        `A synonym with the scode "${values.scode}" already exists for this taxon.`,
        'error',
      );
      return false;
    }

    if (existingSynonym) {
      openSnackbar(
        `The synonym "${values.sname}" already exists for this taxon.`,
        'error',
      );
      return false;
    }
  } else {
    if (
      synonymWithScode &&
      existingSynonym &&
      originalValues &&
      synonymWithScode.item !== existingSynonym.item &&
      originalValues.scode !== values.scode
    ) {
      openSnackbar(
        `A synonym with the scode "${values.scode}" already exists for this taxon.`,
        'error',
      );
      return false;
    }

    if (
      existingSynonym &&
      originalValues &&
      originalValues.sname !== values.sname
    ) {
      openSnackbar(
        `The synonym "${values.sname}" already exists for this taxon.`,
        'error',
      );
      return false;
    }
  }

  return true;
}

export function createSynonymItem(values: syntax, acode: string): TaxaListItem {
  const newSynonym: TaxaListItem = {
    name: values.sname!,
    secondary: values.scode,
    item: {
      ...values,
      acode: acode,
    },
  };

  return newSynonym;
}
