'use client';

import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ComtaxForm from '../../forms/Comtax';
import SyntaxForm from '../../forms/Syntax';

import { ConfirmDialog } from '@twalk-tech/react-lib/nextjs/components';
import { useState } from 'react';
import { TFValue } from '../../../types/relations';
import { TaxaListItem } from '../../../types/taxa-list-item';
import { OpenSnackbarFn } from '../../../utils/hooks/snackbar';
import { comtax } from '../../../utils/managers/obis/Comtax';
import { syntax } from '../../../utils/managers/obis/Syntax';
import { theme } from '../../../utils/theme';
import {
  createCommonNameItem,
  createSynonymItem,
  validateCommonNameSubmission,
  validateSynonymSubmission,
} from './functions';

import {
  createComtaxRecord,
  createSyntaxRecord,
  deleteComtaxRecord,
  deleteSyntaxRecord,
  editComtaxRecord,
  editSyntaxRecord,
} from '../../../app/taxa/actions';

import {
  PrismaCallAction,
  ProcessedPrismaCall,
} from '../../../utils/server-action-functions';

type Values = comtax | syntax;

export type TaxaListComponentProps = {
  type: 'comtax' | 'syntax';
  items: Array<TaxaListItem>;
  acctaxRecordSubmitted: boolean;
  acode: string;
  tfValues?: Array<TFValue>;
  openSnackbar: OpenSnackbarFn;
};

export default function TaxaListComponent(props: TaxaListComponentProps) {
  const [selectedIndex, setSelectedIndex] = useState<number>(-1);
  const [confirmDialogOpen, setConfirmDialogOpen] = useState(false);
  const [confirmDialogMessage, setConfirmDialogMessage] = useState('');
  const [action, setAction] = useState<'create' | 'edit'>('create');
  const [formDialogOpen, setFormDialogOpen] = useState(false);
  const [formValues, setFormValues] = useState<Values | null>(null);

  const listDisplayName = props.type === 'comtax' ? 'Common Name' : 'Synonym';

  const handleListItemClick = (index: number) => {
    setSelectedIndex(index);
  };

  const handleCreateClick = () => {
    setAction('create');
    setFormDialogOpen(true);
  };

  const handleEditClick = () => {
    setAction('edit');
    setFormValues(props.items[selectedIndex].item!);
    setFormDialogOpen(true);
  };

  const setupModification = (
    values: Values,
    action: PrismaCallAction,
    indexPredicateValues?: Values,
  ) => {
    let validateFn: (
      values: Values,
      items: Array<TaxaListItem>,
      openSnackbar: typeof props.openSnackbar,
      originalValues?: Values,
      edit?: boolean,
    ) => boolean;
    let createItemFn: (values: Values, acode: string) => TaxaListItem;
    let displayName = '';
    let submitFn: (values: Values) => Promise<ProcessedPrismaCall<Values>>;
    let id: string | number = -1;

    if ((values as comtax).vernacularname) {
      validateFn = validateCommonNameSubmission;
      createItemFn = createCommonNameItem;
      displayName = (values as comtax).vernacularname!;

      if (action === 'create') {
        submitFn = createComtaxRecord;
      } else if (action === 'delete') {
        submitFn = deleteComtaxRecord;
      } else if (action === 'edit') {
        submitFn = editComtaxRecord;
      } else {
        throw Error('Invalid action!');
      }

      if (indexPredicateValues) {
        id =
          indexPredicateValues !== values
            ? (indexPredicateValues as comtax).c_id
            : (props.items[selectedIndex].item as comtax).c_id;
      }
    } else {
      validateFn = validateSynonymSubmission;
      createItemFn = createSynonymItem;
      displayName = (values as syntax).sname!;

      if (action === 'create') {
        submitFn = createSyntaxRecord;
      } else if (action === 'delete') {
        submitFn = deleteSyntaxRecord;
      } else if (action === 'edit') {
        submitFn = editSyntaxRecord;
      } else {
        throw Error('Invalid action!');
      }

      if (indexPredicateValues) {
        id =
          indexPredicateValues !== values
            ? (indexPredicateValues as syntax).scode
            : (props.items[selectedIndex].item as syntax).scode;
      }
    }

    return { validateFn, createItemFn, displayName, submitFn, id };
  };

  const nameCreated = async (values: Values) => {
    const { validateFn, createItemFn, displayName, submitFn } =
      setupModification(values, 'create');

    if (!validateFn(values, props.items, props.openSnackbar)) {
      return;
    }

    const newName = createItemFn(values, props.acode);

    if (newName) {
      const { prismaCallResponse, snackbarProps } = await submitFn(
        newName.item,
      );

      if (prismaCallResponse) {
        if ((values as comtax).vernacularname) {
          (newName.item as comtax).c_id = (prismaCallResponse as comtax).c_id;
        }

        props.items.push(newName);
      }

      props.openSnackbar(snackbarProps.message, snackbarProps.severity);
    } else {
      props.openSnackbar(
        'An unexpected error occurred. Please try again.',
        'error',
      );
    }
  };

  const nameEdited = async (values: Values, originalValues: Values) => {
    const { validateFn, createItemFn, displayName, submitFn, id } =
      setupModification(values, 'edit', originalValues);

    if (
      !validateFn(values, props.items, props.openSnackbar, originalValues, true)
    ) {
      return;
    }

    if ((values as comtax).vernacularname) {
      (values as comtax).c_id = (originalValues as comtax).c_id;
    }

    const editedName = createItemFn(values, props.acode);

    if (editedName && selectedIndex !== -1) {
      if ((values as comtax).vernacularname) {
        (editedName.item as comtax).c_id = id as number;
      }

      const { prismaCallResponse, snackbarProps } = await submitFn(values);

      if (prismaCallResponse) {
        props.items.splice(selectedIndex, 1, editedName);
      }

      props.openSnackbar(snackbarProps.message, snackbarProps.severity);
    } else {
      props.openSnackbar(
        'An unexpected error occurred. Please try again.',
        'error',
      );
    }
  };

  const nameRemoved = async (values: Values) => {
    const { validateFn, createItemFn, displayName, submitFn, id } =
      setupModification(values, 'delete', values);

    if (selectedIndex !== -1) {
      const { prismaCallResponse, snackbarProps } = await submitFn(values);

      if (prismaCallResponse) {
        props.items.splice(selectedIndex, 1);
      }

      props.openSnackbar(snackbarProps.message, snackbarProps.severity);
    } else {
      props.openSnackbar(
        'An unexpected error occurred. Please try again.',
        'error',
      );
    }
  };

  const handleFormSubmit = async (values: Values) => {
    if (action === 'create') {
      await nameCreated(values);
    } else if (action === 'edit') {
      await nameEdited(values, props.items[selectedIndex].item);
      setFormValues(null);
    }

    setFormDialogOpen(false);
  };

  const handleFormDialogClose = () => {
    setFormDialogOpen(false);
    setFormValues(null);
  };

  const handleRemoveClick = () => {
    setConfirmDialogMessage(
      `Are you sure you want to delete ${listDisplayName.toLowerCase()} "${
        props.items[selectedIndex].name
      }"?`,
    );
    setConfirmDialogOpen(true);
  };

  const handleCancelRemove = () => {
    setConfirmDialogOpen(false);
  };

  const handleConfirmRemove = () => {
    setConfirmDialogOpen(false);
    nameRemoved(props.items[selectedIndex].item);
  };

  return (
    <>
      <Box
        sx={{
          border: 2,
          borderColor: theme.palette.primary.main,
          borderRadius: '16px',
          p: 3,
          pt: 1.5,
          mt: 2,
          mx: 'auto',
        }}
      >
        <Card sx={{ textAlign: 'left' }}>
          <CardHeader
            title={`${listDisplayName}s`}
            sx={{ color: theme.palette.primary.main }}
          />
          <CardContent
            sx={{
              border: 2,
              borderColor: theme.palette.secondary.main,
              p: 0,
              height: 200,
              maxHeight: 200,
              overflow: 'auto',
            }}
          >
            <List sx={{ p: 0 }}>
              {props.items.map((item, index) => (
                <ListItemButton
                  key={index}
                  selected={selectedIndex === index}
                  onClick={() => handleListItemClick(index)}
                >
                  <ListItemText
                    primary={item.name}
                    secondary={item.secondary}
                  />
                </ListItemButton>
              ))}
            </List>
          </CardContent>
          <CardActions>
            <IconButton
              onClick={handleCreateClick}
              disabled={!props.acctaxRecordSubmitted}
            >
              <AddIcon
                color={props.acctaxRecordSubmitted ? 'success' : 'disabled'}
              />
            </IconButton>
            <IconButton
              onClick={handleEditClick}
              disabled={selectedIndex === -1 || !props.acctaxRecordSubmitted}
            >
              <EditIcon
                color={
                  selectedIndex !== -1 && props.acctaxRecordSubmitted
                    ? 'info'
                    : 'disabled'
                }
              />
            </IconButton>
            <IconButton
              onClick={handleRemoveClick}
              disabled={selectedIndex === -1 || !props.acctaxRecordSubmitted}
            >
              <DeleteIcon
                color={
                  selectedIndex !== -1 && props.acctaxRecordSubmitted
                    ? 'warning'
                    : 'disabled'
                }
              />
            </IconButton>
          </CardActions>
        </Card>
      </Box>
      <Dialog open={formDialogOpen} onClose={handleFormDialogClose}>
        <DialogContent sx={{ minWidth: '500px' }}>
          {props.type === 'comtax' && props.tfValues ? (
            <ComtaxForm
              action={action}
              tfValues={props.tfValues}
              values={formValues as comtax}
              onSubmit={handleFormSubmit}
            />
          ) : (
            <SyntaxForm
              action={action}
              values={formValues as syntax}
              onSubmit={handleFormSubmit}
              openSnackbar={props.openSnackbar}
            />
          )}
        </DialogContent>
      </Dialog>
      <ConfirmDialog
        open={confirmDialogOpen}
        message={confirmDialogMessage}
        onCancel={handleCancelRemove}
        onConfirm={handleConfirmRemove}
      />
    </>
  );
}
