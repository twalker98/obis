'use client';

import { EsriMap } from '@twalk-tech/react-lib/nextjs/components';
import { loadModules, setDefaultOptions } from 'esri-loader';
import { TaxonResult } from '../../../../types/taxon-result';

type CollaboratorsTaxonResultDistributionMapProps = {
  result: TaxonResult;
};

function initializeMap(sname: string, setLoading: (loading: boolean) => void) {
  loadModules([
    'esri/geometry/Extent',
    'esri/layers/FeatureLayer',
    'esri/widgets/Fullscreen',
    'esri/widgets/Home',
    'esri/Map',
    'esri/views/MapView',
    'esri/core/reactiveUtils',
  ]).then(
    ([Extent, FeatureLayer, Fullscreen, Home, Map, MapView, reactiveUtils]) => {
      // map extent: Need this since no basemap; otherwise extent is pretty wonky
      const bounds = new Extent({
        xmin: -103.5,
        ymin: 33.0,
        xmax: -93.5,
        ymax: 37.5,
        spatialReference: { wkid: 4326 }, // this is for the extent only; need to set map spatial reference in view.
      });

      const speciesQuery = `sname='${sname}'`;

      // Oklahoma Counties Layer
      const okCounties = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/ArcGISServer_Counties/MapServer',
      });

      const ocTemplate = {
        // autocasts as new PopupTemplate()
        title: '<em>{sname}</em> ({vernacularname}): {datasetname}',
        content: [
          {
            type: 'fields',
            fieldInfos: [
              {
                fieldName: 'eventdate',
                label: 'Date of Occurrence:',
              },
              {
                fieldName: 'recordedby',
                label: 'Recorded By:',
              },
              {
                fieldName: 'catalognumber',
                label: 'Catalog Number:',
              },
              {
                fieldName: 'institutioncode',
                label: 'Institution:',
              },
              {
                fieldName: 'locality',
                label: 'Locality:',
              },
              {
                fieldName: 'habitat',
                label: 'Habitat:',
              },
              {
                fieldName: 'basisofrecord',
                label: 'Basis of Record:',
              },
            ],
          },
        ],
      };

      // Occurrences Layer
      const ocQuery = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/All_Occurrences/MapServer/0/',
        definitionExpression: speciesQuery,
        title: 'OBIS Occurrences',
        outFields: ['*'],
        popupTemplate: ocTemplate,
      });

      const map = new Map({
        layers: [okCounties, ocQuery],
      });

      const view = new MapView({
        container: 'mapContainer',
        map,
        extent: bounds,
        spatialReference: { wkid: 3857 }, // spatial reference of map; different from the extent
      });

      reactiveUtils.when(
        () => !view.updating,
        () => setLoading(false),
      );

      // Home button
      const homeBtn = new Home({
        view,
      });

      // Add the home button to the top left corner of the view
      view.ui.add(homeBtn, 'top-left');

      const fullscreen = new Fullscreen({
        view,
      });

      view.ui.add(fullscreen, 'top-right');
    },
  );
}

export default function CollaboratorsTaxonResultDistributionMap(
  props: CollaboratorsTaxonResultDistributionMapProps,
) {
  // Load CSS from ESRI JS API
  setDefaultOptions({ css: true });

  return props.result.taxon.sname ? (
    <EsriMap
      queryValue={props.result.taxon.sname}
      initializeMap={initializeMap}
      height={700}
    />
  ) : (
    <></>
  );
}
