'use client';

import ExtendedDate from '@twalk-tech/react-lib/nextjs/utils/ExtendedDate';

import {
  DataTable,
  GridColDef,
  GridValueFormatterParams,
} from '@twalk-tech/react-lib/nextjs/components/data-table';
import { useRouter } from 'next/navigation';
import {
  TaxonOccurrenceTableResult,
  TaxonResult,
} from '../../../../types/taxon-result';

type TaxonResultOccurrenceTableProps = {
  result: TaxonResult;
};

export default function CollaboratorsWithEditPermissionsTaxonResultOccurrenceTable(
  props: TaxonResultOccurrenceTableProps,
) {
  const router = useRouter();

  if (!props.result.occurrences) {
    return null;
  }

  const dateValueFormatter = (
    params:
      | GridValueFormatterParams<TaxonOccurrenceTableResult['_min']>
      | GridValueFormatterParams<TaxonOccurrenceTableResult['_max']>,
  ) => {
    if (typeof params.value.eventdate === 'object') {
      return 'Invalid date format';
    }

    return new ExtendedDate(params.value.eventdate).getStringRepresentation();
  };

  const occurrenceTableColumns: Array<GridColDef> = [
    {
      field: 'county',
      headerName: 'County',
      width: 150,
      hideable: false,
    },
    {
      field: '_count',
      headerName: 'Count',
      width: 150,
      hideable: false,
      valueFormatter: (
        params: GridValueFormatterParams<TaxonOccurrenceTableResult['_count']>,
      ) => {
        return params.value.acode;
      },
    },
    {
      field: '_min',
      headerName: 'Earliest Event Date',
      width: 250,
      hideable: false,
      valueFormatter: dateValueFormatter,
    },
    {
      field: '_max',
      headerName: 'Latest Event Date',
      width: 250,
      hideable: false,
      valueFormatter: dateValueFormatter,
    },
    {
      field: 'viewLink',
      headerName: 'County Occurrences',
      width: 250,
      hideable: false,
      cellClassName: 'tableLink',
    },
  ];

  const viewCountyOccurrences = (county: string) => {
    router.push(
      `/occurrences/search/edit/${encodeURIComponent(
        `acode=${props.result.taxon.acode}&county=${county}`,
      )}`,
    );
  };

  return (
    <DataTable
      height={400}
      getRowIdPredicate={(row: TaxonOccurrenceTableResult) =>
        row.county as string
      }
      data={props.result.occurrences}
      columns={occurrenceTableColumns}
      // 77 counties in Oklahoma + 'Unknown'
      pageSize={78}
      cellSelected={{
        field: 'viewLink',
        func: viewCountyOccurrences,
      }}
      filterEnabled
      exportEnabled
    />
  );
}
