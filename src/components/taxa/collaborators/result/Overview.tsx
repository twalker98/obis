'use client';

import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';

import { DataTable } from '@twalk-tech/react-lib/nextjs/components';
import { Relations } from '../../../../types/relations';
import { TaxonResult } from '../../../../types/taxon-result';
import { ResultName } from '../../../../utils/result-name';

type TaxonResultOverviewProps = {
  result: TaxonResult;
  relations: Relations;
};

export default function CollaboratorsTaxonResultOverview(
  props: TaxonResultOverviewProps,
) {
  return (
    <>
      <Container style={{ padding: 0, marginBottom: 4, marginLeft: 0 }}>
        <ResultName result={props.result.taxon} />{' '}
        {props.result.taxon.scientificnameauthorship}
      </Container>
      <Container style={{ padding: 0, marginLeft: 0 }}>
        <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
          ACODE:{' '}
        </Typography>
        {props.result.taxon.acode}
      </Container>
      {props.result.synonyms && props.result.synonyms.length > 0 ? (
        <>
          <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
            Synonyms:{' '}
          </Typography>
          {props.result.synonyms.map((synonym, index) => (
            <Typography key={`synonym-${index}`} style={{ display: 'inline' }}>
              {index ? ', ' : ''} <ResultName result={synonym} />
            </Typography>
          ))}
          <br />
        </>
      ) : null}
      {props.result.commonNames && props.result.commonNames.length > 0 ? (
        <>
          <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
            Common Names:{' '}
          </Typography>
          {props.result.commonNames.map((commonName, index) => (
            <Typography
              key={`commonName-${index}`}
              style={{ display: 'inline' }}
            >
              {(index ? ', ' : '') + commonName.vernacularname}
            </Typography>
          ))}
          <br />
        </>
      ) : null}
      <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
        State Rank:{' '}
      </Typography>
      <a
        href="http://www.oknaturalheritage.ou.edu/content/biodiversity-info/ranking-guide/"
        rel="noopener noreferrer"
        target="_blank"
        style={{ textDecoration: 'underline' }}
      >
        {props.result.taxon.s_rank}
      </a>
      <br />
      <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
        Global Rank:{' '}
      </Typography>
      <a
        href="http://www.oknaturalheritage.ou.edu/content/biodiversity-info/ranking-guide/"
        rel="noopener noreferrer"
        target="_blank"
        style={{ textDecoration: 'underline' }}
      >
        {props.result.taxon.g_rank}
      </a>
      <br />
      <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
        Species of Greatest Conservation Need Tier:{' '}
      </Typography>
      {props.relations.ok_swap
        .filter((swap) => swap.swap_id === props.result.taxon.swap_id)!
        .map((swap) =>
          swap.tier ? `${swap.tier}: ${swap.description}` : swap.description,
        )}
      <br />
      <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
        Federal Status:{' '}
      </Typography>
      {
        props.relations.fed_status.find(
          (fedStatus) =>
            fedStatus.status_id === props.result.taxon.fed_status_id,
        )!.description
      }
      <br />
      <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
        State Status:{' '}
      </Typography>
      {
        props.relations.st_status.find(
          (stateStatus) =>
            stateStatus.status_id === props.result.taxon.st_status_id,
        )!.description
      }
      <br />
      {props.result.hightax ? (
        <DataTable
          height={175}
          getRowIdPredicate={(row) => row.genus}
          data={props.result.hightax.data}
          columns={props.result.hightax.columns}
          pageSize={1}
        />
      ) : (
        <></>
      )}
    </>
  );
}
