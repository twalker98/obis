'use client';

import { EsriMap } from '@twalk-tech/react-lib/nextjs/components';
import { loadModules, setDefaultOptions } from 'esri-loader';
import { useContext, useEffect, useState } from 'react';
import {
  TTaxaSearchbarValueContext,
  TaxaSearchbarValueContext,
} from '../../../../app/taxa/context';
import { TaxonResult } from '../../../../types/taxon-result';

type CollaboratorsTaxonMapResultComponentProps = {
  result: TaxonResult;
};

function initializeMap(sname: string, setLoading: (loading: boolean) => void) {
  loadModules([
    'esri/widgets/BasemapGallery',
    'esri/widgets/Expand',
    'esri/geometry/Extent',
    'esri/layers/FeatureLayer',
    'esri/widgets/Fullscreen',
    'esri/layers/GroupLayer',
    'esri/widgets/Home',
    'esri/widgets/LayerList',
    'esri/Map',
    'esri/layers/MapImageLayer',
    'esri/views/MapView',
    'esri/widgets/Search',
    'esri/core/reactiveUtils',
  ]).then(
    ([
      BasemapGallery,
      Expand,
      Extent,
      FeatureLayer,
      Fullscreen,
      GroupLayer,
      Home,
      LayerList,
      Map,
      MapImageLayer,
      MapView,
      Search,
      reactiveUtils,
    ]) => {
      // map extent: Need this since no basemap; otherwise extent is pretty wonky
      const bounds = new Extent({
        xmin: -103.5,
        ymin: 33.0,
        xmax: -93.5,
        ymax: 37.5,
        spatialReference: { wkid: 4326 }, // this is for the extent only; need to set map spatial reference in view.
      });

      const speciesQuery = `sname='${sname}'`;

      const countyTemplate = {
        // autocasts as new PopupTemplate()
        title: '<strong>{namelsad}</strong>',
        content: '{name}',
      };

      // Oklahoma Counties Layer
      const okCounties = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/ArcGISServer_Counties/FeatureServer',
        title: 'Oklahoma Counties',
        outFields: ['*'],
        popupTemplate: countyTemplate,
      });

      // Ecological Systems
      const okEcos = new MapImageLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/EcologicalSystems/OKECOS/MapServer',
        title: 'Ecological Systems',
        visible: false,
      });

      const ecoIvTemplate = {
        // autocasts as new PopupTemplate()
        title: '<strong>Level IV Ecoregion</strong>: {us_l4name}',
        content:
          'Level III Ecoregion: {us_l3name}<br> Level II Ecoregion: {na_l2name}<br> Level I Ecoregion: {na_l1name}',
      };

      // Ecoregions
      const ecoIv = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/ok_eco_l4/FeatureServer',
        title: 'Level IV Ecoregions of Oklahoma',
        popupTemplate: ecoIvTemplate,
        visible: false,
      });

      const okPadIvTemplate = {
        // autocasts as new PopupTemplate()
        title: '<strong>{unit_nm}</strong>',
        content:
          'Protected Area Type: {loc_ds}<br> Protected Area Manager: {loc_own}',
      };

      // Protected Areas
      const okPad = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/PAD_OK/FeatureServer',
        title: 'Protected Areas of Oklahoma',
        popupTemplate: okPadIvTemplate,
        visible: false,
      });

      const dfTemplate = {
        // autocasts as new PopupTemplate()
        title: '<strong>Game Types of Oklahoma</strong>',
        content: '{gametype}',
      };

      // Duck and Fletcher
      const df = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/Duck_and_Fletcher/FeatureServer',
        title: 'Duck and Fletcher: Game Types',
        popupTemplate: dfTemplate,
        visible: false,
      });

      const geoTemplate = {
        // autocasts as new PopupTemplate()
        title: '<strong>Geomorphic Province</strong>',
        content: '{province}',
      };

      // Geomorphic Provinces
      const geo = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/geomorphic/FeatureServer',
        title: 'Geomorphic Provinces',
        popupTemplate: geoTemplate,
        visible: false,
      });

      const swapTemplate = {
        // autocasts as new PopupTemplate()
        title:
          '<strong>Comprehensive Wildlife Conservation Strategy Region</strong>',
        content: '{name}',
      };

      // Comprehensive Wildlife Conservation Strategy Regions
      const swap = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/OKWCSR/FeatureServer',
        title: 'Comprehensive Wildlife Conservation Strategy Regions',
        popupTemplate: swapTemplate,
        visible: false,
      });

      const trTemplate = {
        // autocasts as new PopupTemplate()
        title: '<strong>Township/Range</strong>',
        content: '{label2}',
      };

      // Township/Range
      const townships = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/PLSS/MapServer/0/',
        outFields: ['*'],
        title: 'Township/Range',
        popupTemplate: trTemplate,
      });

      const secTemplate = {
        // autocasts as new PopupTemplate()
        title: '<strong>Section</strong>',
        content: '{label}',
      };

      // Sections
      const sections = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/PLSS/MapServer/1/',
        outFields: ['*'],
        title: 'Section',
        popupTemplate: secTemplate,
      });

      // Create GroupLayer for PLSS data
      const plss = new GroupLayer({
        title: 'PLSS Data',
        visible: false,
        visibilityMode: 'independent',
        layers: [townships, sections],
      });

      const ocTemplate = {
        // autocasts as new PopupTemplate()
        title: '<em>{sname}</em> ({vernacularname}): {datasetname}',
        content: [
          {
            type: 'fields',
            fieldInfos: [
              {
                fieldName: 'eventdate',
                label: 'Date of Occurrence:',
              },
              {
                fieldName: 'recordedby',
                label: 'Recorded By:',
              },
              {
                fieldName: 'catalognumber',
                label: 'Catalog Number:',
              },
              {
                fieldName: 'institutioncode',
                label: 'Institution:',
              },
              {
                fieldName: 'locality',
                label: 'Locality:',
              },
              {
                fieldName: 'habitat',
                label: 'Habitat:',
              },
              {
                fieldName: 'basisofrecord',
                label: 'Basis of Record:',
              },
            ],
          },
        ],
      };

      // Occurrences Layer
      const ocQuery = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/All_Occurrences/MapServer/0/',
        definitionExpression: speciesQuery,
        title: 'OBIS Occurrences',
        outFields: ['*'],
        popupTemplate: ocTemplate,
      });

      const map = new Map({
        // basemap: "satellite",
        layers: [
          plss,
          okEcos,
          geo,
          swap,
          ecoIv,
          df,
          okPad,
          okCounties,
          ocQuery,
        ],
      });

      const view = new MapView({
        container: 'mapContainer',
        map,
        extent: bounds,
        spatialReference: 3857, // spatial reference of map; different from the extent
      });

      reactiveUtils.when(
        () => !view.updating,
        () => setLoading(false),
      );

      // Home button
      const homeBtn = new Home({
        view,
      });

      // Add the home button to the top left corner of the view
      view.ui.add(homeBtn, 'top-left');

      // create a search widget
      const searchWidget = new Search({
        view,
        sources: [
          {
            layer: new FeatureLayer({
              // Notice the property is called layer Not featureLayer new to 4.11
              url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/PLSS/MapServer/0/',
              popupTemplate: {
                // autocasts as new PopupTemplate()
                title: '{label2}',
                overwriteActions: true,
              },
            }),

            searchFields: ['label2'],
            displayField: 'label2',
            exactMatch: false,
            outFields: ['label2'],
            name: 'Township/Range',
            placeholder: 'example: 12N 10W IM',
          },

          {
            layer: new FeatureLayer({
              // Notice the property is called layer Not featureLayer new to 4.11
              url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/PLSS/MapServer/1/',
              popupTemplate: {
                // autocasts as new PopupTemplate()
                title: '{str_label2}',
                overwriteActions: true,
              },
            }),

            searchFields: ['str_label2'],
            displayField: 'str_label2',
            exactMatch: false,
            outFields: ['str_label2'],
            name: 'Section Township/Range',
            placeholder: 'example: Sec. 15 12N 10W IM',
          },

          {
            layer: new FeatureLayer({
              // Notice the property is called layer Not featureLayer new to 4.11
              url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/ArcGISServer_Counties/FeatureServer/0',
              popupTemplate: {
                // autocasts as new PopupTemplate()
                title: '{name} County',
                overwriteActions: true,
              },
            }),

            searchFields: ['name'],
            displayField: 'name',
            exactMatch: false,
            outFields: ['name'],
            name: 'Counties',
            placeholder: 'example: Adair',
          },
        ],
      });

      // Add the search widget to the top right corner of the view
      view.ui.add(searchWidget, {
        position: 'top-right',
      });

      // Create a BasemapGallery widget instance and set
      // its container to a div element
      const basemapGallery = new BasemapGallery({
        view,
        container: document.createElement('div'),
      });

      // Create an Expand instance for basemap gallery
      const bgExpand = new Expand({
        view,
        expandTooltip: 'Select basemap',
        content: basemapGallery,
      });

      // Add the expand instance to the ui
      view.ui.add(bgExpand, 'top-left');

      // Add a legend instance to the panel of a
      // ListItem in a LayerList instance
      const layerList = new LayerList({
        view,
        listItemCreatedFunction(event: any) {
          const item = event.item;
          if (item.layer.type !== 'group') {
            // don't show legend twice
            item.panel = {
              content: 'legend',
              open: false,
            };
          }
        },
      });

      // Create an Expand instance for legend gallery
      const lgExpand = new Expand({
        view,
        expandTooltip: 'Expand Layer List',
        content: layerList,
      });

      // Add the expand instance to the ui
      view.ui.add(lgExpand, 'top-left');

      const fullscreen = new Fullscreen({
        view,
      });

      view.ui.add(fullscreen, 'top-right');
    },
  );
}

export default function CollaboratorsTaxonMapResultComponent(
  props: CollaboratorsTaxonMapResultComponentProps,
) {
  const [hydrated, setHydrated] = useState(false);
  const { setValue } = useContext(
    TaxaSearchbarValueContext,
  ) as TTaxaSearchbarValueContext;

  useEffect(() => {
    setValue(props.result.taxon.sname!);
    setHydrated(true);
  }, []);

  if (!hydrated) {
    // Returns null on first render, so the client and server match
    return null;
  }

  // Load CSS from ESRI JS API
  setDefaultOptions({ css: true });

  return props.result.taxon.sname ? (
    <EsriMap
      queryValue={props.result.taxon.sname}
      initializeMap={initializeMap}
      height={700}
    />
  ) : (
    <></>
  );
}
