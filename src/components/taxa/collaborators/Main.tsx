'use client';

import Grid from '@mui/material/Grid';
import AcctaxForm from '../../forms/Acctax';
import DistributionDataForm from '../../forms/DistributionData';
import TaxaListComponent from './List';

import { H } from '@highlight-run/next/client';
import { ConfirmDialog } from '@twalk-tech/react-lib/nextjs/components';
import { useContext, useEffect, useState } from 'react';
import {
  createAcctaxRecord,
  createDistributionDataRecord,
  editAcctaxRecord,
  editDistributionDataRecord,
} from '../../../app/taxa/actions';
import {
  TTaxaSearchbarValueContext,
  TaxaSearchbarValueContext,
} from '../../../app/taxa/context';
import type { Relations } from '../../../types/relations';
import type { TaxaListItem } from '../../../types/taxa-list-item';
import { useSnackbar } from '../../../utils/hooks/snackbar';
import { acctax } from '../../../utils/managers/obis/Acctax';
import { comtax } from '../../../utils/managers/obis/Comtax';
import { distribution_data } from '../../../utils/managers/obis/DistributionData';
import { syntax } from '../../../utils/managers/obis/Syntax';
import { createCommonNameItem, createSynonymItem } from './functions';

export type TaxaCollaboratorsMainComponentProps = {
  relations: Relations;
  acctaxRecordSubmitted: boolean;
  distributionDataSubmitted: boolean;
  acode?: string;
  acctax?: acctax;
  commonNames?: Array<comtax>;
  synonyms?: Array<syntax>;
  d_id?: number;
  distributionData?: distribution_data;
};

export default function TaxaCollaboratorsMainComponent(
  props: TaxaCollaboratorsMainComponentProps,
) {
  const [acctaxRecordSubmitted, setAcctaxRecordSubmitted] = useState(
    props.acctaxRecordSubmitted,
  );
  const [acode, setAcode] = useState(props.acode || '');
  const [elcode, setElcode] = useState<string | null>(
    props.acctax ? props.acctax.elcode : null,
  );
  const [commonNames, setCommonNames] = useState<Array<TaxaListItem>>(
    props.commonNames?.map((commonName) =>
      createCommonNameItem(commonName, acode),
    ) || [],
  );
  const [synonyms, setSynonyms] = useState<Array<TaxaListItem>>(
    props.synonyms?.map((synonym) => createSynonymItem(synonym, acode)) || [],
  );
  const [distDataSubmitted, setDistDataSubmitted] = useState(
    props.distributionDataSubmitted,
  );
  const [d_id, setD_id] = useState(props.d_id || -1);
  const [elcodeConfirmDialogOpen, setElcodeConfirmDialogOpen] = useState(false);
  const [elcodeConfirmDialogMessage, setElcodeConfirmDialogMessage] =
    useState('');
  const [newAcctaxValues, setNewAcctaxValues] = useState<acctax | null>(null);
  const taxaSearchbarValueContext = useContext(
    TaxaSearchbarValueContext,
  ) as TTaxaSearchbarValueContext;

  const { snackbar, openSnackbar } = useSnackbar();

  useEffect(() => {
    if (taxaSearchbarValueContext) {
      taxaSearchbarValueContext.setValue(props.acctax?.sname!);
    } else {
      H.consumeError(
        new Error('No context provided to update taxa searchbar value'),
      );
    }
  }, []);

  const handleCancelDifferentElcodeConfirmation = () => {
    setElcodeConfirmDialogOpen(false);
  };

  const handleConfirmDifferentElcodeConfirmation = async () => {
    setElcodeConfirmDialogOpen(false);

    if (props.distributionData && newAcctaxValues) {
      const distributionDataWithElcode = {
        ...props.distributionData,
        elcode: newAcctaxValues.elcode,
      };

      await editDistributionDataRecord(distributionDataWithElcode);

      // TODO: add some sort of feedback when this edit is occurring
      const { prismaCallResponse, snackbarProps } = await editAcctaxRecord(
        newAcctaxValues,
      );
      openSnackbar(snackbarProps.message, snackbarProps.severity);
    } else {
      openSnackbar('An unexpected error occurred.', 'error');
    }
  };

  const editTaxon = async (values: acctax) => {
    if (values.elcode !== '' && values.elcode !== props.acctax?.elcode) {
      if (distDataSubmitted && d_id !== -1 && props.distributionData) {
        if (values.elcode !== props.distributionData.elcode) {
          setNewAcctaxValues(values);
          setElcodeConfirmDialogMessage(
            `The distribution data record for this taxon has an element code of "${props.distributionData.elcode}", which is different from the element code submitted in this edit ("${values.elcode}"). NOTE: Selecting "Confirm" will update the element code of this distribution data record.`,
          );
          setElcodeConfirmDialogOpen(true);
        }

        if (
          props.distributionData.elcode === null ||
          props.distributionData.elcode === ''
        ) {
          const distributionDataWithElcode = {
            ...props.distributionData,
            elcode: values.elcode,
          };

          await editDistributionDataRecord(distributionDataWithElcode);
        }
      }
    } else {
      const { prismaCallResponse, snackbarProps } = await editAcctaxRecord(
        values,
      );
      openSnackbar(snackbarProps.message, snackbarProps.severity);
    }
  };

  const submitAcctaxRecord = async (values: acctax) => {
    if (acctaxRecordSubmitted && acode !== '') {
      await editTaxon(values);
      return;
    }

    const { prismaCallResponse, snackbarProps } = await createAcctaxRecord(
      values,
    );

    if (prismaCallResponse) {
      setAcode(prismaCallResponse.acode);
      setElcode(prismaCallResponse.elcode);
      setAcctaxRecordSubmitted(true);
    }

    openSnackbar(snackbarProps.message, snackbarProps.severity);
  };

  const editDistributionData = async (values: distribution_data) => {
    values['d_id'] = d_id;

    const { prismaCallResponse, snackbarProps } =
      await editDistributionDataRecord(values);
    openSnackbar(snackbarProps.message, snackbarProps.severity);
  };

  const submitDistributionDataRecord = async (values: distribution_data) => {
    if (!acctaxRecordSubmitted || acode === '') {
      openSnackbar('Please create a taxon first!', 'error');
      return;
    }

    values['acode'] = acode;
    values['elcode'] = elcode;

    if (distDataSubmitted && d_id !== -1) {
      await editDistributionData(values);
      return;
    }

    const { prismaCallResponse, snackbarProps } =
      await createDistributionDataRecord(values);

    if (prismaCallResponse) {
      setD_id(prismaCallResponse.d_id);
      setDistDataSubmitted(true);
    }

    openSnackbar(snackbarProps.message, snackbarProps.severity);
  };

  return (
    <>
      <Grid container px={2} width="95%" mx="auto">
        <Grid item xs={12} sm={6} px={2}>
          {/* TODO: ensure that the submit button reflects the correct action */}
          <AcctaxForm
            relations={props.relations}
            action={acctaxRecordSubmitted ? 'Edit' : 'Create'}
            values={props.acctax}
            onSubmit={submitAcctaxRecord}
            openSnackbar={openSnackbar}
          />
        </Grid>
        <Grid
          container
          item
          xs={12}
          sm={6}
          direction="column"
          px={2}
          spacing={2}
        >
          <Grid container item direction="row" flexBasis="0" spacing={2}>
            <Grid item xs={6}>
              <TaxaListComponent
                type="syntax"
                items={synonyms}
                acctaxRecordSubmitted={acctaxRecordSubmitted}
                acode={acode}
                openSnackbar={openSnackbar}
              />
            </Grid>
            <Grid item xs={6}>
              <TaxaListComponent
                type="comtax"
                items={commonNames}
                acctaxRecordSubmitted={acctaxRecordSubmitted}
                acode={acode}
                openSnackbar={openSnackbar}
                tfValues={props.relations.tfValues}
              />
            </Grid>
          </Grid>
          <Grid item>
            <DistributionDataForm
              relations={props.relations}
              action={props.distributionDataSubmitted ? 'Edit' : 'Create'}
              acctaxRecordSubmitted={acctaxRecordSubmitted}
              values={props.distributionData}
              onSubmit={submitDistributionDataRecord}
            />
          </Grid>
        </Grid>
      </Grid>
      <ConfirmDialog
        open={elcodeConfirmDialogOpen}
        message={elcodeConfirmDialogMessage}
        onCancel={handleCancelDifferentElcodeConfirmation}
        onConfirm={handleConfirmDifferentElcodeConfirmation}
      />
      {snackbar}
    </>
  );
}
