'use client';

import { EsriMap } from '@twalk-tech/react-lib/nextjs/components';
import { loadModules, setDefaultOptions } from 'esri-loader';
import { TaxonResult } from '../../../types/taxon-result';

type TaxonResultDistributionMapProps = {
  result: TaxonResult;
};

function initializeMap(acode: string, setLoading: (loading: boolean) => void) {
  loadModules([
    'esri/widgets/Expand',
    'esri/geometry/Extent',
    'esri/layers/FeatureLayer',
    'esri/widgets/Fullscreen',
    'esri/widgets/Home',
    'esri/widgets/Legend',
    'esri/Map',
    'esri/views/MapView',
    'esri/core/reactiveUtils',
  ]).then(
    ([
      Expand,
      Extent,
      FeatureLayer,
      Fullscreen,
      Home,
      Legend,
      Map,
      MapView,
      reactiveUtils,
    ]) => {
      // map extent: Need this since no basemap; otherwise extent is pretty wonky
      const bounds = new Extent({
        xmin: -103.5,
        ymin: 33.0,
        xmax: -93.5,
        ymax: 37.5,
        spatialReference: { wkid: 4326 }, // this is for the extent only; need to set map spatial reference in view.
      });

      const speciesQuery = `acode='${acode}'`;

      // Oklahoma counties layer
      const okCounties = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/ArcGISServer_Counties/MapServer',
      });

      const coTemplate = {
        // autocasts as new PopupTemplate()
        title: '<em>{sname}</em> ({vernacularname})',
        content:
          'ONHI has {count} occurrence record(s) for <em>{sname}</em> ({vernacularname}) in {county} County',
      };

      // County Occurrences Layer
      const coQuery = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/OBIS_County_Occurrences_Poly/MapServer/0/',
        definitionExpression: speciesQuery,
        title: 'County Occurrences',
        outFields: ['*'],
        popupTemplate: coTemplate,
        opacity: 0.75,
      });

      const hexTemplate = {
        // autocasts as new PopupTemplate()
        title: '<em>{sname}</em> ({vernacularname})',
        content:
          'ONHI has {count} occurrence record(s) for <em>{sname}</em> ({vernacularname}) in this hexagon',
      };

      // Hex Occurrences Layer
      const hexQuery = new FeatureLayer({
        url: 'https://obsgis.csa.ou.edu:6443/arcgis/rest/services/ONHI/OBIS_5km_Occurrences/MapServer/0/',
        definitionExpression: speciesQuery,
        title: 'Georeferenced Occurrences',
        outFields: ['*'],
        popupTemplate: hexTemplate,
      });

      const map = new Map({
        layers: [coQuery, okCounties, hexQuery],
      });

      const view = new MapView({
        container: 'mapContainer',
        map,
        extent: bounds,
        spatialReference: { wkid: 3857 }, // spatial reference of map; different from the extent
      });

      reactiveUtils.when(
        () => !view.updating,
        () => setLoading(false),
      );

      const legend = new Expand({
        content: new Legend({
          view,
          style: 'classic',
          layerInfos: [
            {
              layer: hexQuery,
            },
            {
              layer: coQuery,
            },
          ],
        }),

        view,
        expandTooltip: 'Legend',
        expanded: false,
      });

      view.ui.add(legend, 'bottom-left');

      // Home button
      const homeBtn = new Home({
        view,
      });

      view.ui.add(homeBtn, 'top-left');

      const fullscreen = new Fullscreen({
        view,
      });

      view.ui.add(fullscreen, 'top-right');
    },
  );
}

export default function TaxonResultDistributionMap(
  props: TaxonResultDistributionMapProps,
) {
  // Load CSS from ESRI JS API
  setDefaultOptions({ css: true });

  return props.result.taxon.acode ? (
    <EsriMap
      queryValue={props.result.taxon.acode}
      initializeMap={initializeMap}
      height={700}
    />
  ) : (
    <></>
  );
}
