'use client';

import { CustomTabs } from '@twalk-tech/react-lib/nextjs/components';
import { CustomTab } from '@twalk-tech/react-lib/nextjs/utils/types';
import { useContext, useEffect, useState } from 'react';
import {
  TTaxaSearchbarValueContext,
  TaxaSearchbarValueContext,
} from '../../../app/taxa/context';
import { theme } from '../../../utils/theme';

type TaxonMainResultComponentProps = {
  tabs: Array<CustomTab>;
  sname: string;
};

export default function TaxonMainResultComponent(
  props: TaxonMainResultComponentProps,
) {
  const [hydrated, setHydrated] = useState(false);
  const { setValue } = useContext(
    TaxaSearchbarValueContext,
  ) as TTaxaSearchbarValueContext;

  useEffect(() => {
    setValue(props.sname);
    setHydrated(true);
  }, []);

  if (!hydrated) {
    // Returns null on first render, so the client and server match
    return null;
  }

  return <CustomTabs tabs={props.tabs} theme={theme} />;
}
