'use client';

import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';
import Container from '@mui/material/Container';
import Pagination from '@mui/material/Pagination';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import Link from 'next/link';
import usePagination from '../../utils/hooks/pagination';

import { usePathname } from 'next/navigation';
import { useEffect, useState } from 'react';
import { TaxaSearchResult } from '../../types/taxa-search-result';
import { ResultName } from '../../utils/result-name';

type TaxaResultsComponentProps = {
  query: string;
  results: Array<[string, Array<TaxaSearchResult>]>;
};

export default function TaxaResultsComponent(props: TaxaResultsComponentProps) {
  const [hydrated, setHydrated] = useState(false);
  const pagination = usePagination(
    props.results || new Array<[string, Array<TaxaSearchResult>]>(),
  );
  const [page, setPage] = useState(pagination.currentPage);
  const pathnameFragments = usePathname()?.split('/');
  const location = pathnameFragments && pathnameFragments[3];

  useEffect(() => {
    setHydrated(true);
  }, []);

  if (!hydrated) {
    // Returns null on first render, so the client and server match
    return null;
  }

  const handlePageChange = (e, p) => {
    setPage(p);
    pagination.jump(p);
  };

  if (props.results === null || props.results.length === 0) {
    return (
      <>
        <ErrorOutlineIcon color="error" sx={{ width: 50, height: 50 }} />
        <br />
        <Typography style={{ display: 'inline' }}>
          No results found for query{' '}
        </Typography>
        <Typography style={{ display: 'inline' }} color="primary">
          {props.query}
        </Typography>
        !
      </>
    );
  }

  return (
    <Box sx={{ textAlign: 'center', width: '75vw', mx: 'auto' }}>
      <Pagination
        count={pagination.maxPage}
        page={page}
        variant="outlined"
        shape="rounded"
        color="primary"
        onChange={handlePageChange}
        sx={{ mb: 2 }}
        showFirstButton
        showLastButton
      />
      {pagination
        .currentData()
        .map((result: [string, Array<TaxaSearchResult>]) =>
          result[1].length === 1 ? (
            <Paper
              key={result[0]}
              sx={{
                height: 56,
                p: 2.3125,
                mb: 0.25,
                textAlign: 'left',
                borderRadius: 0,
              }}
            >
              <Container maxWidth={false} style={{ paddingLeft: 0 }}>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  spacing={0}
                >
                  <Link href={`/taxa/result/${location}/${result[0]}`}>
                    <ResultName result={result[1][0]} />
                  </Link>
                  {result[1][0].community ? (
                    <Chip
                      label="Community"
                      color="primary"
                      sx={{ width: 'inherit', mt: -0.8125 }}
                    />
                  ) : (
                    <></>
                  )}
                </Stack>
              </Container>
            </Paper>
          ) : (
            <Accordion
              key={result[0]}
              square={true}
              sx={{ mb: 0.25, display: 'flex', flexDirection: 'column' }}
            >
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  flexWrap: 'wrap',
                }}
              >
                <Container maxWidth={false} style={{ paddingLeft: 0 }}>
                  <Stack
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                    spacing={0}
                  >
                    <Typography sx={{ textAlign: 'left' }}>
                      <Link
                        href={`/taxa/result/${location}/${result[0]}`}
                        style={{ marginRight: 1 }}
                      >
                        <ResultName result={result[1][0]} />
                      </Link>
                    </Typography>
                    <Stack
                      direction="row"
                      spacing={1}
                      sx={{ color: 'text.secondary', textAlign: 'right' }}
                    >
                      {result[1].filter((result) => result.type === 'syntax')
                        .length > 0 ? (
                        <Chip label="Synonyms Found" color="primary" />
                      ) : (
                        <></>
                      )}
                      {result[1].filter((result) => result.type === 'comtax')
                        .length > 0 ? (
                        <Chip label="Common Names Found" color="primary" />
                      ) : (
                        <></>
                      )}
                    </Stack>
                  </Stack>
                </Container>
              </AccordionSummary>
              <AccordionDetails>
                <>
                  {result[1].map((result: TaxaSearchResult, index: number) => (
                    <Paper
                      key={index}
                      sx={{
                        p: 2.3125,
                        mb: 0.25,
                        textAlign: 'left',
                        borderRadius: 0,
                      }}
                    >
                      <ResultName result={result} />
                      {result.type === 'acctax' ? (
                        <Chip
                          label="Accepted Name"
                          color="primary"
                          sx={{ ml: 1, width: 'inherit' }}
                        />
                      ) : result.type === 'comtax' ? (
                        <Chip
                          label="Common Name"
                          color="primary"
                          sx={{ ml: 1, width: 'inherit' }}
                        />
                      ) : result.type === 'syntax' ? (
                        <Chip
                          label="Synonym"
                          color="primary"
                          sx={{ ml: 1, width: 'inherit' }}
                        />
                      ) : (
                        <></>
                      )}
                    </Paper>
                  ))}
                </>
              </AccordionDetails>
            </Accordion>
          ),
        )}
      <Pagination
        count={pagination.maxPage}
        page={page}
        variant="outlined"
        shape="rounded"
        color="primary"
        onChange={handlePageChange}
        sx={{ mt: 2 }}
        showFirstButton
        showLastButton
      />
    </Box>
  );
}
