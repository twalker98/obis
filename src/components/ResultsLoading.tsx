'use client';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';
import Typography from '@mui/material/Typography';
import usePagination from '../utils/hooks/pagination';

import { ReactNode } from 'react';

export default function ResultsLoadingComponent() {
  const pagination = usePagination(null);

  const elements = new Array<ReactNode>();

  for (let i = 0; i < pagination.itemsPerPage(); i++) {
    elements.push(
      <Accordion key={i} square={true} sx={{ mb: 0.25 }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Skeleton animation="wave" variant="text">
            <Typography>Fake Scientific Name Here</Typography>
          </Skeleton>
        </AccordionSummary>
      </Accordion>,
    );
  }

  return (
    <Box sx={{ textAlign: 'center', width: '75vw', mx: 'auto' }}>
      <Skeleton
        animation="wave"
        variant="rectangular"
        height={32}
        sx={{ mb: 2, width: '60vw' }}
      />
      {elements}
      <Skeleton
        animation="wave"
        variant="rectangular"
        height={32}
        sx={{ mt: 2, width: '60vw' }}
      />
    </Box>
  );
}
