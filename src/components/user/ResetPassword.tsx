'use client';

import EditIcon from '@mui/icons-material/Edit';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import IconButton from '@mui/material/IconButton';

import {
  GenericForm,
  GenericFormItem,
  buildInputAdornmentProps,
} from '@twalk-tech/react-lib/nextjs';
import { useContext, useEffect, useState } from 'react';
import { useSnackbar } from '../../utils/hooks/snackbar';
import { ObisAuth, ObisUser } from '../../utils/managers/auth/User';

import { SnackbarContext, TSnackbarContext } from '../ObisLayout';

import { omit } from 'lodash';
import { useRouter } from 'next/navigation';
import * as yup from 'yup';
import { changePasswordAction, checkCredentials } from '../../app/user/actions';
import { theme } from '../../utils/theme';

export type ResetPasswordComponentProps = {
  user: ObisUser;
};

export default function ResetPasswordComponent(
  props: ResetPasswordComponentProps,
) {
  const { snackbar, openSnackbar } = useSnackbar();
  const [showPassword, setShowPassword] = useState(false);
  const router = useRouter();
  const { setSnackbarProps } = useContext(SnackbarContext) as TSnackbarContext;

  useEffect(() => {
    openSnackbar(
      'You have a temporary password set. Please change your password before you login.',
      'info',
      10000,
    );
  }, []);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault();
  };

  const passwordInputProps = buildInputAdornmentProps({
    type: showPassword ? 'text' : 'password',
    position: 'end',
    adornment: (
      <IconButton
        aria-label="toggle-password-visibility"
        onClick={handleClickShowPassword}
        onMouseDown={handleMouseDownPassword}
      >
        {showPassword ? <VisibilityOff /> : <Visibility />}
      </IconButton>
    ),
  });

  const changePasswordFormFields: Array<GenericFormItem> = [
    {
      fcName: 'current_password',
      name: 'Current Password',
      type: 'text',
      value: '',
      inputProps: passwordInputProps,
    },
    {
      fcName: 'new_password',
      name: 'New Password',
      type: 'text',
      value: '',
      inputProps: passwordInputProps,
    },
    {
      fcName: 'confirm_new_password',
      name: 'Confirm New Password',
      type: 'text',
      value: '',
      inputProps: passwordInputProps,
    },
  ];

  const changePasswordFormValidationSchema = yup.object({
    current_password: yup.string().required('Current Password is required'),
    new_password: yup
      .string()
      .required('New Password is required')
      .test(
        'not-same-password',
        'Your new password cannot be the same as your current password',
        function (value) {
          return this.parent.current_password !== value;
        },
      ),
    confirm_new_password: yup
      .string()
      .required('Confirm New Password is required')
      .test('passwords-match', 'Passwords must match', function (value) {
        return this.parent.new_password === value;
      }),
  });

  const changePassword = async (values: any) => {
    const checkPasswordData: ObisAuth = {
      username: props.user.username,
      password: values.current_password,
    };

    const credsResponse = await checkCredentials(checkPasswordData);

    if (!credsResponse.prismaCallResponse) {
      openSnackbar(
        credsResponse.snackbarProps.message,
        credsResponse.snackbarProps.severity,
      );
      return;
    }

    const updatedUser = {
      ...props.user,
      password: values.new_password,
      tempPass: false,
    };

    const { prismaCallResponse, snackbarProps } = await changePasswordAction(
      // @ts-ignore
      omit(updatedUser, ['groups', 'permissions']),
      props.user.id,
    );
    openSnackbar(snackbarProps.message, snackbarProps.severity);

    if (prismaCallResponse) {
      setSnackbarProps({
        message:
          'Your password has been reset. Please login with your new password.',
        severity: 'info',
        duration: 10000,
      });
      router.push('/');
    }
  };

  return (
    <>
      <GenericForm
        title="Change Password"
        fields={changePasswordFormFields}
        onSubmit={changePassword}
        submitBtnConfig={{
          title: 'Change Password',
          icon: <EditIcon />,
        }}
        validationSchema={changePasswordFormValidationSchema}
        width="75%"
        borderColor={theme.palette.primary.main}
      />
      {snackbar}
    </>
  );
}
