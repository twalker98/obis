'use client';

import '../styles/globals.css';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import logo from '../public/assets/logo.png';
import { login } from '../utils/auth';

import { ThemeProvider } from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { Navbar, Route } from '@twalk-tech/react-lib/nextjs';
import { SessionProvider } from 'next-auth/react';
import { createContext, useEffect, useState } from 'react';
import { SnackbarProps } from '../utils/hooks/snackbar';
import { useSnackbar } from '../utils/hooks/snackbar';
import { Permission } from '../utils/obis-auth-prisma';
import { theme } from '../utils/theme';

export type TSnackbarContext = {
  snackbarProps: SnackbarProps;
  setSnackbarProps: (newSnackbarProps: SnackbarProps) => void;
};

export const PermissionsContext = createContext<Array<Permission>>([]);
export const SnackbarContext = createContext<TSnackbarContext | null>(null);

export type ObisLayoutProps = {
  navbarRoutes: Array<Route>;
  permissions: Array<Permission>;
  children: any;
};

export default function ObisLayout(props: ObisLayoutProps) {
  const [snackbarProps, setSnackbarProps] = useState<SnackbarProps | null>(
    null,
  );
  const { snackbar, openSnackbar } = useSnackbar();
  const title = 'oklahoma biodiversity information system';

  useEffect(() => {
    if (snackbarProps) {
      openSnackbar(
        snackbarProps.message,
        snackbarProps.severity,
        snackbarProps.duration,
      );
    }
  }, [snackbarProps]);

  // This function can only run client side, so it needs to be added from a client component
  props.navbarRoutes.find((route) => route.login === true)!.loginFn = login;

  return (
    <SessionProvider>
      <ThemeProvider theme={theme}>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <PermissionsContext.Provider value={props.permissions}>
            <SnackbarContext.Provider
              value={{
                snackbarProps: snackbarProps!,
                setSnackbarProps: setSnackbarProps,
              }}
            >
              <Navbar
                name={title}
                shortName="OBIS"
                routes={props.navbarRoutes}
                logo={{
                  src: logo.src,
                }}
                theme={theme}
              />
              <Box m={2} p={2} sx={{ textAlign: 'center' }}>
                <Typography
                  variant="h4"
                  color={theme.palette.primary.main}
                  fontWeight="medium"
                >
                  {title}
                </Typography>
                {props.children}
                {snackbar}
              </Box>
            </SnackbarContext.Provider>
          </PermissionsContext.Provider>
        </LocalizationProvider>
      </ThemeProvider>
    </SessionProvider>
  );
}
