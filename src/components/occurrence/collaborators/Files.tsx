'use client';

import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import IconButton from '@mui/material/IconButton';

import { Typography } from '@mui/material';
import { DataTable, GridColDef } from '@twalk-tech/react-lib/nextjs';
import { theme } from '../../../utils/theme';

export type FilesProps = {
  occurrenceRecordSubmitted: boolean;
};

export default function Files(props: FilesProps) {
  type FileTest = {
    title: string;
    fileName: string;
    type: 'PDF' | 'image';
  };

  const data: Array<FileTest> = [];

  // const data: Array<FileTest> = [
  //   {
  //     title: 'Test Document',
  //     fileName: 'file1.pdf',
  //     type: 'PDF'
  //   },
  //   {
  //     title: 'Test Image',
  //     fileName: 'file2.png',
  //     type: 'image'
  //   }
  // ];

  const columns: Array<GridColDef> = [];

  // const columns: Array<GridColDef> = [
  //   {
  //     field: 'title',
  //     headerName: 'Title',
  //     width: 150,
  //   },
  //   {
  //     field: 'fileName',
  //     headerName: 'File Name',
  //     width: 150,
  //   },
  //   {
  //     field: 'type',
  //     headerName: 'Type',
  //     width: 150,
  //   },
  // ];

  return (
    <Box
      sx={{
        border: 2,
        borderColor: theme.palette.primary.main,
        borderRadius: '16px',
        p: 3,
        pt: 1.5,
        mt: 2,
        mx: 'auto',
      }}
    >
      <Typography color="primary" variant="h5">
        Coming Soon!
      </Typography>
      <div className="blur">
        <Card sx={{ textAlign: 'left' }}>
          <CardHeader
            title="Files"
            sx={{ color: theme.palette.primary.main }}
          />
          <CardContent>
            <DataTable
              height={400}
              getRowIdPredicate={(row: FileTest) => row.fileName}
              data={data}
              columns={columns}
            />
          </CardContent>
          <CardActions>
            {/* <IconButton disabled={!props.occurrenceRecordSubmitted}>
              <AddIcon color={props.occurrenceRecordSubmitted ? 'success' : 'disabled'} />
            </IconButton>
            <IconButton disabled={!props.occurrenceRecordSubmitted}>
              <EditIcon color={props.occurrenceRecordSubmitted ? 'info' : 'disabled'} />
            </IconButton>
            <IconButton disabled={!props.occurrenceRecordSubmitted}>
              <DeleteIcon color={props.occurrenceRecordSubmitted ? 'warning' : 'disabled'} />
            </IconButton> */}
            <IconButton disabled={true}>
              <AddIcon color="disabled" />
            </IconButton>
            <IconButton disabled={true}>
              <EditIcon color="disabled" />
            </IconButton>
            <IconButton disabled={true}>
              <DeleteIcon color="disabled" />
            </IconButton>
          </CardActions>
        </Card>
      </div>
    </Box>
  );
}
