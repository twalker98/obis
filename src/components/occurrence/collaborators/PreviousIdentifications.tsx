'use client';

import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import IconButton from '@mui/material/IconButton';

import {
  ConfirmDialog,
  DataTable,
  GridColDef,
} from '@twalk-tech/react-lib/nextjs';
import { theme } from '../../../utils/theme';

import { useState } from 'react';
import {
  identification_verification,
  occurrence,
} from '../../../utils/obis-prisma';

import {
  createPreviousIdentificationRecord,
  deletePreviousIdentificationRecord,
  editPreviousIdentificationRecord,
} from '../../../app/occurrences/actions';
import { OpenSnackbarFn } from '../../../utils/hooks/snackbar';
import PreviousIdentificationForm from '../../forms/PreviousIdentification';

export type PreviousIdentificationsProps = {
  occurrenceRecordSubmitted: boolean;
  previousIdentifications: Array<identification_verification>;
  gid: number;
  catalogNumber: string;
  occurrence?: occurrence;
  openSnackbar: OpenSnackbarFn;
};

export default function PreviousIdentifications(
  props: PreviousIdentificationsProps,
) {
  const [previousIdentifications, setPreviousIdentifications] = useState<
    Array<identification_verification>
  >(props.previousIdentifications || []);
  const [selectedPkey, setSelectedPkey] = useState(-1);
  const [action, setAction] = useState<'create' | 'edit'>('create');
  const [formValues, setFormValues] =
    useState<identification_verification | null>(null);
  const [formDialogOpen, setFormDialogOpen] = useState(false);
  const [confirmDialogMessage, setConfirmDialogMessage] = useState('');
  const [confirmDialogOpen, setConfirmDialogOpen] = useState(false);

  const columns: Array<GridColDef> = [
    {
      field: 'pkey',
      headerName: 'pkey',
      width: 150,
    },
    {
      field: 'identifiedacode',
      headerName: 'Identified Acode',
      width: 150,
    },
    {
      field: 'identifiedby',
      headerName: 'Identified By',
      width: 150,
    },
    {
      field: 'datelastmodified',
      headerName: 'Date Last Modified',
      width: 150,
    },
  ];

  const handleRowSelected = (selectedId: string) => {
    setSelectedPkey(parseInt(selectedId));
  };

  const handleCreateClick = () => {
    if (!props.catalogNumber) {
      props.openSnackbar(
        "Cannot create a previous identification if the occurrence record doesn't have a catalog number set!",
        'error',
      );
      return;
    }

    setAction('create');
    setFormDialogOpen(true);
  };

  const handleEditClick = () => {
    setAction('edit');
    setFormValues(
      previousIdentifications.find(
        (previousIdentification) =>
          previousIdentification.pkey === selectedPkey,
      )!,
    );
    setFormDialogOpen(true);
  };

  const handleFormDialogClose = () => {
    setFormDialogOpen(false);
    setFormValues(null);
  };

  const handleRemoveClick = () => {
    setConfirmDialogMessage(
      'Are you sure you want to delete this previous identification?',
    );
    setConfirmDialogOpen(true);
  };

  const handleCancelRemove = () => {
    setConfirmDialogOpen(false);
  };

  const handleConfirmRemove = async () => {
    setConfirmDialogOpen(false);
    const { snackbarProps } = await deletePreviousIdentificationRecord(
      previousIdentifications.find(
        (previousIdentification) =>
          previousIdentification.pkey === selectedPkey,
      )!,
    );
    props.openSnackbar(snackbarProps.message, snackbarProps.severity);
    setSelectedPkey(-1);
    setPreviousIdentifications(
      previousIdentifications.filter(
        (previousIdentification) =>
          previousIdentification.pkey !== selectedPkey,
      ),
    );
  };

  const handleFormSubmit = async (values: identification_verification) => {
    if (action === 'create') {
      values.gid = props.gid;
      const { prismaCallResponse, snackbarProps } =
        await createPreviousIdentificationRecord(values);
      props.openSnackbar(snackbarProps.message, snackbarProps.severity);
      setPreviousIdentifications([
        ...previousIdentifications,
        prismaCallResponse as identification_verification,
      ]);
    } else if (action === 'edit') {
      const { prismaCallResponse, snackbarProps } =
        await editPreviousIdentificationRecord(values);
      props.openSnackbar(snackbarProps.message, snackbarProps.severity);
      setFormValues(null);
      setPreviousIdentifications(
        previousIdentifications.map((previousIdentification) =>
          previousIdentification.pkey === selectedPkey
            ? (prismaCallResponse as identification_verification)
            : previousIdentification,
        ),
      );
    }

    setFormDialogOpen(false);
  };

  return (
    <>
      <Box
        sx={{
          border: 2,
          borderColor: theme.palette.primary.main,
          borderRadius: '16px',
          p: 3,
          pt: 1.5,
          mt: 2,
          mx: 'auto',
        }}
      >
        <Card sx={{ textAlign: 'left' }}>
          <CardHeader
            title="Previous Identifications"
            sx={{ color: theme.palette.primary.main }}
          />
          <CardContent>
            <DataTable
              height={400}
              getRowIdPredicate={(row: identification_verification) =>
                row.pkey as unknown as string
              }
              data={previousIdentifications}
              columns={columns}
              rowSelected={handleRowSelected}
            />
          </CardContent>
          <CardActions>
            <IconButton
              onClick={handleCreateClick}
              disabled={!props.occurrenceRecordSubmitted}
            >
              <AddIcon
                color={props.occurrenceRecordSubmitted ? 'success' : 'disabled'}
              />
            </IconButton>
            <IconButton
              onClick={handleEditClick}
              disabled={selectedPkey === -1 || !props.occurrenceRecordSubmitted}
            >
              <EditIcon
                color={
                  selectedPkey !== -1 && props.occurrenceRecordSubmitted
                    ? 'info'
                    : 'disabled'
                }
              />
            </IconButton>
            <IconButton
              onClick={handleRemoveClick}
              disabled={selectedPkey === -1 || !props.occurrenceRecordSubmitted}
            >
              <DeleteIcon
                color={
                  selectedPkey !== -1 && props.occurrenceRecordSubmitted
                    ? 'warning'
                    : 'disabled'
                }
              />
            </IconButton>
          </CardActions>
        </Card>
      </Box>
      <Dialog open={formDialogOpen} onClose={handleFormDialogClose}>
        <DialogContent sx={{ minWidth: '500px' }}>
          <PreviousIdentificationForm
            action={action}
            values={formValues as identification_verification}
            catalogNumber={props.catalogNumber}
            onSubmit={handleFormSubmit}
          />
        </DialogContent>
      </Dialog>
      <ConfirmDialog
        open={confirmDialogOpen}
        message={confirmDialogMessage}
        onCancel={handleCancelRemove}
        onConfirm={handleConfirmRemove}
      />
    </>
  );
}
