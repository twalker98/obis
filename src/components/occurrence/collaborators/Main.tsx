'use client';

import Grid from '@mui/material/Grid';
import OccurrenceForm from '../../forms/Occurrence';
import Files from './Files';
import PreviousIdentifications from './PreviousIdentifications';

import { useState } from 'react';
import {
  createOccurrenceRecord,
  editOccurrenceRecord,
} from '../../../app/occurrences/actions';
import { Relations } from '../../../types/relations';
import { useSnackbar } from '../../../utils/hooks/snackbar';
import { occurrence } from '../../../utils/managers/obis/Occurrence';
import { identification_verification } from '../../../utils/obis-prisma';
import { OccurrenceScientificName } from '../../forms/custom-fields/ScientificName';

export type OccurrenceCollaboratorsMainComponentProps = {
  relations: Relations;
  username: string;
  occurrenceRecordSubmitted: boolean;
  occurrence?: occurrence;
  previousIdentifications?: Array<identification_verification>;
  gid?: number;
  acodeValue?: OccurrenceScientificName;
};

export default function OccurrenceCollaboratorsMainComponent(
  props: OccurrenceCollaboratorsMainComponentProps,
) {
  const [occurrenceRecordSubmitted, setOccurrenceRecordSubmitted] = useState(
    props.occurrenceRecordSubmitted,
  );
  const [gid, setGid] = useState(props.gid || -1);
  const [catalogNumber, setCatalogNumber] = useState(
    props.occurrence ? props.occurrence.catalognumber : '',
  );
  const { snackbar, openSnackbar } = useSnackbar();

  const previousIdentifications = props.previousIdentifications || [];

  const editOccurrence = async (values: occurrence) => {
    values.gid = gid;
    const { prismaCallResponse, snackbarProps } = await editOccurrenceRecord(
      values,
    );

    if (prismaCallResponse) {
      setCatalogNumber(prismaCallResponse.catalognumber!);
    }

    openSnackbar(snackbarProps.message, snackbarProps.severity);
  };

  const submitOccurrenceRecord = async (values: occurrence) => {
    if (occurrenceRecordSubmitted && gid !== -1) {
      await editOccurrence(values);
      return;
    }

    const { prismaCallResponse, snackbarProps } = await createOccurrenceRecord(
      values,
    );

    if (prismaCallResponse) {
      setGid(prismaCallResponse.gid);
      setCatalogNumber(prismaCallResponse.catalognumber!);
      setOccurrenceRecordSubmitted(true);
    }

    openSnackbar(snackbarProps.message, snackbarProps.severity);
  };

  return (
    <>
      <Grid container px={2} width="95%" mx="auto">
        <Grid item xs={12} sm={6} px={2}>
          <OccurrenceForm
            relations={props.relations}
            action={occurrenceRecordSubmitted ? 'Edit' : 'Create'}
            username={props.username}
            values={props.occurrence}
            acodeValue={props.acodeValue}
            onSubmit={submitOccurrenceRecord}
            openSnackbar={openSnackbar}
          />
        </Grid>
        <Grid
          container
          item
          xs={12}
          sm={6}
          direction="column"
          px={2}
          spacing={2}
        >
          <Grid item>
            <PreviousIdentifications
              occurrenceRecordSubmitted={occurrenceRecordSubmitted}
              previousIdentifications={previousIdentifications}
              gid={gid}
              catalogNumber={catalogNumber!}
              occurrence={props.occurrence}
              openSnackbar={openSnackbar}
            />
          </Grid>
          <Grid item>
            <Files occurrenceRecordSubmitted={occurrenceRecordSubmitted} />
          </Grid>
        </Grid>
      </Grid>
      {snackbar}
    </>
  );
}
