'use client';

// TODO: create generic component for this. duplicates a lot of code from County Report Search component

import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';

import {
  ActionButton,
  DateRangeField,
  GenericDropdown,
  GenericTextField,
} from '@twalk-tech/react-lib/nextjs';
import dayjs from 'dayjs';
import { isEqual } from 'lodash';
import { useParams, useRouter } from 'next/navigation';
import {
  Dispatch,
  KeyboardEvent,
  KeyboardEventHandler,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import { OccurrenceSearchField } from '../../types/relations';
import { useSnackbar } from '../../utils/hooks/snackbar';
import { county } from '../../utils/managers/obis/County';

type OccurrenceSearchCriteria = {
  field: OccurrenceSearchField | null;
  value: string | county;
};

type OccurrenceSearchValueFieldProps = OccurrenceSearchCriteria & {
  counties: Array<county>;
  handleValueChange: (field: string, value: string) => Promise<void>;
  onValueFieldEnterKeyPress: KeyboardEventHandler<HTMLDivElement>;
};

export function OccurrenceSearchValueField(
  props: OccurrenceSearchValueFieldProps,
) {
  if (props.field && props.field.key === 'eventdate') {
    const dateRangeTemplate = 'startDate={{startDate}}&endDate={{endDate}}';
    const [startDateValue, endDateValue] = props.value
      ? (props.value as string).split('&').map((date) => date.split('=')[1])
      : ['', ''];
    const [startDate, setStartDate] = useState(startDateValue);
    const [endDate, setEndDate] = useState(endDateValue);
    const [dateRange, setDateRange] = useState(dateRangeTemplate);

    const handleEventDateChange = async (field: string, value: string) => {
      const dateRangeValue = dateRange.replace(`{{${field}}}`, value);
      setDateRange(dateRangeValue);
      await props.handleValueChange(props.field?.key!, dateRangeValue);
    };

    const handleStartDateChange = async (value: string) => {
      // When typing the date (specifically the year portion), the update event is triggered too early, resulting in bad data being set.
      // Need to check if the year is a valid 4 digit year before performing any state updates
      if (dayjs(value).year().toString().length !== 4) {
        return;
      }

      setStartDate(value);
      await handleEventDateChange('startDate', value);
    };

    const handleEndDateChange = async (value: string) => {
      // When typing the date (specifically the year portion), the update event is triggered too early, resulting in bad data being set.
      // Need to check if the year is a valid 4 digit year before performing any state updates
      if (dayjs(value).year().toString().length !== 4) {
        return;
      }

      setEndDate(value);
      await handleEventDateChange('endDate', value);
    };

    return (
      <DateRangeField
        variant="outlined"
        startDateValue={startDate}
        onStartDateValueChange={handleStartDateChange}
        endDateValue={endDate}
        onEndDateValueChange={handleEndDateChange}
      />
    );
  } else if (props.field && props.field.key === 'county') {
    const handleChange = async (field: string, value: county) => {
      if (field !== 'county') {
        throw Error('Unexpecteed error!');
      }

      props.handleValueChange(props.field?.key!, value['county']);
    };

    return (
      <GenericDropdown
        fcName="county"
        name="County"
        type="dropdown"
        variant="outlined"
        options={props.counties}
        value={!props.value ? '' : props.value}
        onValueChange={handleChange}
        labelFunctionPred={(county: county) => county.county}
        optionKey="gid"
        optionValueKey="county"
        standalone
      />
    );
  } else {
    const handleChange = (event: any) => {
      if (event) {
        const value = event.target.value;
        props.handleValueChange(props.field?.key!, value);
      }
    };

    return (
      <GenericTextField
        fcName={props.field ? props.field.key : 'search'}
        name={props.field ? props.field.display_name : 'Search'}
        type="text"
        variant="outlined"
        value={props.value as string}
        onValueChange={handleChange}
        onKeyDown={props.onValueFieldEnterKeyPress}
        standalone
      />
    );
  }
}

type OccurrenceSearchCriterionFieldProps = {
  criteria: Array<OccurrenceSearchCriteria>;
  setCriteria: Dispatch<SetStateAction<OccurrenceSearchCriteria[]>>;
  index: number;
  field: OccurrenceSearchField | null;
  value: string | county;
  occurrenceSearchFields: Array<OccurrenceSearchField>;
  counties: Array<county>;
  onValueFieldEnterKeyPress: KeyboardEventHandler<HTMLDivElement>;
};

export function OccurrenceSearchCriterionField(
  props: OccurrenceSearchCriterionFieldProps,
) {
  const [field, setField] = useState<OccurrenceSearchField | null>(props.field);
  const [value, setValue] = useState<OccurrenceSearchCriteria['value']>(
    props.value,
  );

  const handleFieldChange = async (
    fieldName: string,
    value: OccurrenceSearchField,
  ) => {
    if (fieldName !== 'field') {
      throw Error('Unexpected error!');
    }

    if (field && field !== value) {
      props.occurrenceSearchFields.find(
        (occurrenceSearchField) => occurrenceSearchField.key === field.key,
      )!.disabled = false;
    }

    setValue('');
    setField(value);

    props.criteria[props.index] = { field: value, value: '' };
    props.setCriteria([...props.criteria]);

    props.occurrenceSearchFields.find(
      (occurrenceSearchField) => occurrenceSearchField.key === value.key,
    )!.disabled = true;
  };

  const handleValueChange = async (
    field: string,
    fieldValue: OccurrenceSearchCriteria['value'],
  ) => {
    setValue(fieldValue);
    props.criteria.find(
      (criterion: OccurrenceSearchCriteria) => criterion.field?.key === field,
    )!.value = fieldValue;
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={3}>
        <GenericDropdown
          fcName="field"
          name="Field"
          options={props.occurrenceSearchFields}
          labelFunctionPred={(option: OccurrenceSearchField) =>
            option.display_name
          }
          optionKey="key"
          optionValueKey="display_name"
          variant="outlined"
          value={field}
          onValueChange={handleFieldChange}
          disableClear
          standalone
        />
      </Grid>
      <Grid item xs={9}>
        <OccurrenceSearchValueField
          field={field}
          value={value}
          counties={props.counties}
          handleValueChange={handleValueChange}
          onValueFieldEnterKeyPress={props.onValueFieldEnterKeyPress}
        />
      </Grid>
    </Grid>
  );
}

type AddRemoveCriterionProps = {
  criteria: Array<OccurrenceSearchCriteria>;
  setCriteria: Dispatch<SetStateAction<OccurrenceSearchCriteria[]>>;
  index: number;
  occurrenceSearchFields: Array<OccurrenceSearchField>;
};

export function AddRemoveCriterion(props: AddRemoveCriterionProps) {
  const addCriterion = () => {
    props.setCriteria([...props.criteria, { field: null, value: '' }]);
  };

  const removeCriterion = () => {
    const updatedCriteria = [...props.criteria];
    updatedCriteria.splice(props.index, 1);
    props.setCriteria(updatedCriteria);

    if (props.criteria[props.index].field) {
      props.occurrenceSearchFields.find(
        (occurrenceSearchField) =>
          occurrenceSearchField.key === props.criteria[props.index].field?.key,
      )!.disabled = false;
    }
  };

  return (
    <Grid container spacing={2.5} sx={{ my: 'auto' }}>
      <Grid item xs={6}>
        <IconButton
          onClick={addCriterion}
          color="primary"
          disabled={
            props.criteria.length !== 1 &&
            props.index < props.criteria.length - 1
          }
        >
          <AddCircleIcon />
        </IconButton>
      </Grid>
      <Grid item xs={6}>
        <IconButton
          onClick={removeCriterion}
          color="error"
          disabled={
            props.criteria.length === 1 ||
            props.index === 0 ||
            props.criteria.length !== props.index + 1
          }
        >
          <RemoveCircleIcon />
        </IconButton>
      </Grid>
    </Grid>
  );
}

export type OccurrenceSearchProps = {
  occurrenceSearchFields: Array<OccurrenceSearchField>;
  counties: Array<county>;
};

export default function Search(props: OccurrenceSearchProps) {
  const router = useRouter();
  const { snackbar, openSnackbar } = useSnackbar();
  const [criteria, setCriteria] = useState<Array<OccurrenceSearchCriteria>>([]);
  const params = useParams();
  const query =
    params && params.query
      ? new Map<string, string>(
          decodeURIComponent(params.query as string)
            .split('&')
            .map((pair: string) => pair.split('='))
            .map(([key, value]) => [key, decodeURIComponent(value)]),
        )
      : null;

  const updateCriteria = () => {
    if (query) {
      const newCriteria: Array<OccurrenceSearchCriteria> = [];

      for (const queryObj of query) {
        // End date is handled via setting the field to be 'eventdate' and constructing the value if the field is 'startDate'
        if (queryObj[0] === 'endDate') {
          continue;
        }

        let fieldKey: string =
          queryObj[0] === 'startDate' ? 'eventdate' : queryObj[0];
        let value: OccurrenceSearchCriteria['value'] = queryObj[1];
        const occurrenceSearchField = props.occurrenceSearchFields.find(
          (occurrenceSearchField) => occurrenceSearchField.key === fieldKey,
        )!;
        occurrenceSearchField.disabled = true;

        if (fieldKey === 'county') {
          value = props.counties.find(
            (county) => county.county === queryObj[1],
          )!;
        } else if (queryObj[0] === 'startDate') {
          value = `startDate=${value}&endDate=${query.get('endDate')}`;
        }

        const criterion: OccurrenceSearchCriteria = {
          field: occurrenceSearchField,
          value: value,
        };

        newCriteria.push(criterion);
      }

      setCriteria(newCriteria);
    } else {
      setCriteria([{ field: null, value: '' }]);
    }
  };

  useEffect(() => {
    updateCriteria();
  }, []);

  const search = () => {
    if (
      criteria.length === 1 &&
      isEqual(criteria, [{ field: null, value: '' }])
    ) {
      openSnackbar('Must provide a search query!', 'error');
      return;
    }

    const queryValues = criteria.map((criterion: OccurrenceSearchCriteria) => {
      if (criterion.field?.key === 'eventdate') {
        return criterion.value;
      }

      return `${criterion.field?.key}=${criterion.value}`;
    });

    const query = queryValues.join('&');

    router.push(`/occurrences/search/edit/${encodeURIComponent(query)}`);
  };

  const enterKeyPress = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      search();
    }
  };

  return (
    <>
      <Box sx={{ flexGrow: 1, width: '75%', mx: 'auto', mb: 2 }}>
        {criteria.map((criterion, index) => (
          <Grid container spacing={2} key={index}>
            <Grid item xs={10}>
              <OccurrenceSearchCriterionField
                criteria={criteria}
                setCriteria={setCriteria}
                index={index}
                field={criterion.field}
                value={criterion.value}
                occurrenceSearchFields={props.occurrenceSearchFields}
                counties={props.counties}
                onValueFieldEnterKeyPress={enterKeyPress}
              />
            </Grid>
            <Grid item xs={2}>
              <AddRemoveCriterion
                criteria={criteria}
                setCriteria={setCriteria}
                index={index}
                occurrenceSearchFields={props.occurrenceSearchFields}
              />
            </Grid>
          </Grid>
        ))}
        <ActionButton
          title="Search"
          onButtonClick={search}
          startIcon={<SearchIcon />}
          // TODO: check if any criteria values are empty
          disabled={
            criteria.length === 1 &&
            isEqual(criteria, [{ field: null, value: '' }])
          }
        />
      </Box>
      {snackbar}
    </>
  );
}
