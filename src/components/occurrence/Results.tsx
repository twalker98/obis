'use client';

import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';
import Container from '@mui/material/Container';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import { useRouter } from 'next/navigation';
import usePagination from '../../utils/hooks/pagination';

import {
  DataTable,
  GridColDef,
} from '@twalk-tech/react-lib/nextjs/components/data-table';
import { useState } from 'react';
import { OccurrenceSearchResult } from '../../types/occurrence-search-result';

type OccurrenceResultsComponentProps = {
  results: Array<[string, Array<OccurrenceSearchResult>]>;
};

export default function OccurrenceResultsComponent(
  props: OccurrenceResultsComponentProps,
) {
  const pagination = usePagination(props.results);
  const [page, setPage] = useState(pagination.currentPage);
  const router = useRouter();

  const handlePageChange = (e, p) => {
    setPage(p);
    pagination.jump(p);
  };

  const editRecord = (gid: string) => {
    router.push(`/occurrences/result/edit/${parseInt(gid)}`);
  };

  const occurrenceSearchResultColumns: Array<GridColDef> = [
    {
      field: 'eventdate',
      headerName: 'Event Date',
      width: 200,
      hideable: false,
    },
    {
      field: 'recordedby',
      headerName: 'Recorded By',
      width: 150,
      hideable: false,
    },
    {
      field: 'county',
      headerName: 'County',
      width: 150,
      hideable: false,
    },
    {
      field: 'locality',
      headerName: 'Locality',
      width: 600,
      hideable: false,
    },
    {
      field: 'editLink',
      headerName: 'Edit Occurrence',
      width: 150,
      hideable: false,
      cellClassName: 'tableLink',
    },
  ];

  if (props.results === null || props.results.length === 0) {
    return (
      <>
        <ErrorOutlineIcon color="error" sx={{ width: 50, height: 50 }} />
        <br />
        <Typography style={{ display: 'inline' }}>No results found!</Typography>
      </>
    );
  }

  return (
    <Box sx={{ textAlign: 'center', width: '75vw', mx: 'auto' }}>
      <Pagination
        count={pagination.maxPage}
        page={page}
        variant="outlined"
        shape="rounded"
        color="primary"
        onChange={handlePageChange}
        sx={{ mb: 2 }}
        showFirstButton
        showLastButton
      />
      {pagination
        .currentData()
        .map((result: [string, Array<OccurrenceSearchResult>]) => (
          <Accordion
            key={result[0]}
            square={true}
            sx={{ mb: 0.25, display: 'flex', flexDirection: 'column' }}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
                flexWrap: 'wrap',
              }}
            >
              <Container maxWidth={false} style={{ paddingLeft: 0 }}>
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="center"
                  spacing={0}
                >
                  <Typography color="primary">{result[0]}</Typography>
                  <Chip
                    label={`${result[1].length} occurrences found`}
                    color="primary"
                  />
                </Stack>
              </Container>
            </AccordionSummary>
            <AccordionDetails>
              <DataTable
                height={400}
                getRowIdPredicate={(row: OccurrenceSearchResult) =>
                  row.gid as unknown as string
                }
                data={result[1]}
                columns={occurrenceSearchResultColumns}
                cellSelected={{
                  field: 'editLink',
                  func: editRecord,
                }}
                filterEnabled
              />
            </AccordionDetails>
          </Accordion>
        ))}
      <Pagination
        count={pagination.maxPage}
        page={page}
        variant="outlined"
        shape="rounded"
        color="primary"
        onChange={handlePageChange}
        sx={{ mt: 2 }}
        showFirstButton
        showLastButton
      />
    </Box>
  );
}
