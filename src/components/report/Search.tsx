'use client';

// TODO: create generic component for this. duplicates a lot of code from Occurrence Search component

import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';

import {
  ActionButton,
  GenericDropdown,
  GenericTextField,
} from '@twalk-tech/react-lib/nextjs';
import { isEqual } from 'lodash';
import { useParams, useRouter } from 'next/navigation';
import {
  Dispatch,
  KeyboardEvent,
  KeyboardEventHandler,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import { OccurrenceSearchField } from '../../types/relations';
import { useSnackbar } from '../../utils/hooks/snackbar';
import { county } from '../../utils/managers/obis/County';
import { institution } from '../../utils/managers/obis/Institution';
import { kingdom_lu } from '../../utils/managers/obis/Kingdom';
import { source } from '../../utils/managers/obis/Source';

type ReportSearchCriteria = {
  field: OccurrenceSearchField | null;
  value: string | county | institution | kingdom_lu | source;
};

type ReportSearchValueFieldProps = ReportSearchCriteria & {
  counties: Array<county>;
  institutions: Array<institution>;
  kingdoms: Array<kingdom_lu>;
  sources: Array<source>;
  handleValueChange: (field: string, value: string) => Promise<void>;
  onValueFieldEnterKeyPress: KeyboardEventHandler<HTMLDivElement>;
};

export function ReportSearchValueField(props: ReportSearchValueFieldProps) {
  if (props.field && props.field.key === 'county') {
    const handleChange = async (field: string, value: county) => {
      if (field !== 'county') {
        throw Error('Unexpected error!');
      }

      props.handleValueChange(props.field?.key!, value['county']);
    };

    return (
      <GenericDropdown
        fcName="county"
        name="County"
        type="dropdown"
        variant="outlined"
        options={props.counties}
        value={!props.value ? '' : props.value}
        onValueChange={handleChange}
        labelFunctionPred={(county: county) => county.county}
        optionKey="gid"
        optionValueKey="county"
        standalone
      />
    );
  } else if (props.field && props.field.key === 'institutioncode') {
    const handleChange = async (field: string, value: institution) => {
      if (field !== 'institutioncode') {
        throw Error('Unexpected error!');
      }

      props.handleValueChange(props.field?.key!, value['institutioncode']);
    };

    return (
      <GenericDropdown
        fcName="institutioncode"
        name="Institution Code"
        type="dropdown"
        variant="outlined"
        options={props.institutions}
        value={!props.value ? '' : props.value}
        onValueChange={handleChange}
        labelFunctionPred={(institution: institution) =>
          institution.institution === 'None'
            ? institution.institution
            : `${institution.institutioncode}: ${institution.institution}`
        }
        optionKey="institutioncode"
        optionValueKey="institution"
        optionValueKeyPrefix="institutioncode"
        standalone
      />
    );
  } else if (props.field && props.field.key === 'kingdom') {
    const handleChange = async (field: string, value: kingdom_lu) => {
      if (field !== 'kingdom') {
        throw Error('Unexpected error!');
      }

      props.handleValueChange(props.field?.key!, value['kingdom']);
    };

    return (
      <GenericDropdown
        fcName="kingdom"
        name="Kingdom"
        type="dropdown"
        variant="outlined"
        options={props.kingdoms}
        value={!props.value ? '' : props.value}
        onValueChange={handleChange}
        labelFunctionPred={(kingdom: kingdom_lu) => kingdom.kingdom}
        optionKey="id"
        optionValueKey="kingdom"
        standalone
      />
    );
  } else if (props.field && props.field.key === 'datasetname') {
    const handleChange = async (field: string, value: kingdom_lu) => {
      if (field !== 'datasetname') {
        throw Error('Unexpected error!');
      }

      props.handleValueChange(props.field?.key!, value['datasetname']);
    };

    return (
      <GenericDropdown
        fcName="datasetname"
        name="Dataset Name"
        type="dropdown"
        variant="outlined"
        options={props.sources}
        value={!props.value ? '' : props.value}
        onValueChange={handleChange}
        labelFunctionPred={(source: source) => source.description}
        optionKey="source"
        optionValueKey="description"
        standalone
      />
    );
  } else {
    const handleChange = (event: any) => {
      if (event) {
        const value = event.target.value;
        props.handleValueChange(props.field?.key!, value);
      }
    };

    return (
      <GenericTextField
        fcName={props.field ? props.field.key : 'search'}
        name={props.field ? props.field.display_name : 'Search'}
        type="text"
        variant="outlined"
        value={props.value as string}
        onValueChange={handleChange}
        onKeyDown={props.onValueFieldEnterKeyPress}
        standalone
      />
    );
  }
}

type ReportSearchCriterionFieldProps = {
  criteria: Array<ReportSearchCriteria>;
  setCriteria: Dispatch<SetStateAction<ReportSearchCriteria[]>>;
  index: number;
  field: OccurrenceSearchField | null;
  value: string | county | institution | kingdom_lu | source;
  reportSearchFields: Array<OccurrenceSearchField>;
  counties: Array<county>;
  institutions: Array<institution>;
  kingdoms: Array<kingdom_lu>;
  sources: Array<source>;
  onValueFieldEnterKeyPress: KeyboardEventHandler<HTMLDivElement>;
};

export function ReportSearchCriterionField(
  props: ReportSearchCriterionFieldProps,
) {
  const [field, setField] = useState<OccurrenceSearchField | null>(props.field);
  const [value, setValue] = useState<ReportSearchCriteria['value']>(
    props.value,
  );

  const handleFieldChange = async (
    fieldName: string,
    value: OccurrenceSearchField,
  ) => {
    if (fieldName !== 'field') {
      throw Error('Unexpected error!');
    }

    if (field && field !== value) {
      props.reportSearchFields.find(
        (occurrenceSearchField) => occurrenceSearchField.key === field.key,
      )!.disabled = false;
    }

    setValue('');
    setField(value);

    props.criteria[props.index] = { field: value, value: '' };
    props.setCriteria([...props.criteria]);

    props.reportSearchFields.find(
      (occurrenceSearchField) => occurrenceSearchField.key === value.key,
    )!.disabled = true;
  };

  const handleValueChange = async (
    field: string,
    fieldValue: ReportSearchCriteria['value'],
  ) => {
    setValue(fieldValue);
    props.criteria.find(
      (criterion: ReportSearchCriteria) => criterion.field?.key === field,
    )!.value = fieldValue;
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={3}>
        <GenericDropdown
          fcName="field"
          name="Field"
          options={props.reportSearchFields}
          labelFunctionPred={(option: OccurrenceSearchField) =>
            option.display_name
          }
          optionKey="key"
          optionValueKey="display_name"
          variant="outlined"
          value={field}
          onValueChange={handleFieldChange}
          disableClear
          standalone
        />
      </Grid>
      <Grid item xs={9}>
        <ReportSearchValueField
          field={field}
          value={value}
          counties={props.counties}
          institutions={props.institutions}
          kingdoms={props.kingdoms}
          sources={props.sources}
          handleValueChange={handleValueChange}
          onValueFieldEnterKeyPress={props.onValueFieldEnterKeyPress}
        />
      </Grid>
    </Grid>
  );
}

type AddRemoveCriterionProps = {
  criteria: Array<ReportSearchCriteria>;
  setCriteria: Dispatch<SetStateAction<ReportSearchCriteria[]>>;
  index: number;
  countyReportSearchFields: Array<OccurrenceSearchField>;
};

export function AddRemoveCriterion(props: AddRemoveCriterionProps) {
  const addCriterion = () => {
    props.setCriteria([...props.criteria, { field: null, value: '' }]);
  };

  const removeCriterion = () => {
    const updatedCriteria = [...props.criteria];
    updatedCriteria.splice(props.index, 1);
    props.setCriteria(updatedCriteria);

    if (props.criteria[props.index].field) {
      props.countyReportSearchFields.find(
        (occurrenceSearchField) =>
          occurrenceSearchField.key === props.criteria[props.index].field?.key,
      )!.disabled = false;
    }
  };

  return (
    <Grid container spacing={2.5} sx={{ my: 'auto' }}>
      <Grid item xs={6}>
        <IconButton
          onClick={addCriterion}
          color="primary"
          disabled={
            props.criteria.length !== 1 &&
            props.index < props.criteria.length - 1
          }
        >
          <AddCircleIcon />
        </IconButton>
      </Grid>
      <Grid item xs={6}>
        <IconButton
          onClick={removeCriterion}
          color="error"
          disabled={
            props.criteria.length === 1 ||
            props.index === 0 ||
            props.criteria.length !== props.index + 1
          }
        >
          <RemoveCircleIcon />
        </IconButton>
      </Grid>
    </Grid>
  );
}

export type ReportSearchProps = {
  reportSearchFields: Array<OccurrenceSearchField>;
  reportType: 'county' | 'occurrence';
  counties: Array<county>;
  institutions?: Array<institution>;
  kingdoms?: Array<kingdom_lu>;
  sources?: Array<source>;
};

export default function ReportSearch(props: ReportSearchProps) {
  const router = useRouter();
  const { snackbar, openSnackbar } = useSnackbar();
  const [criteria, setCriteria] = useState<Array<ReportSearchCriteria>>([]);
  const params = useParams();
  const query =
    params && params.query
      ? new Map<string, string>(
          decodeURIComponent(params.query as string)
            .split('&')
            .map((pair: string) => pair.split('='))
            .map(([key, value]) => [key, decodeURIComponent(value)]),
        )
      : null;

  const updateCriteria = () => {
    if (query) {
      const newCriteria: Array<ReportSearchCriteria> = [];

      for (const queryObj of query) {
        const fieldKey = queryObj[0];
        let value: ReportSearchCriteria['value'] = queryObj[1];
        const occurrenceSearchField = props.reportSearchFields.find(
          (occurrenceSearchField) => occurrenceSearchField.key === fieldKey,
        )!;
        occurrenceSearchField.disabled = true;

        if (fieldKey === 'county') {
          value = props.counties.find(
            (county) => county.county === queryObj[1],
          )!;
        } else if (
          fieldKey === 'institutioncode' &&
          props.reportType === 'occurrence'
        ) {
          value = props.institutions!.find(
            (institution) => institution.institution === queryObj[1],
          )!;
        } else if (fieldKey === 'kingdom' && props.reportType === 'county') {
          value = props.kingdoms!.find(
            (kingdom) => kingdom.kingdom === queryObj[1],
          )!;
        } else if (
          fieldKey === 'datasetname' &&
          props.reportType === 'occurrence'
        ) {
          value = props.sources!.find(
            (source) => source.description === queryObj[1],
          )!;
        }

        const criterion: ReportSearchCriteria = {
          field: occurrenceSearchField,
          value: value,
        };

        newCriteria.push(criterion);
      }

      setCriteria(newCriteria);
    } else {
      setCriteria([{ field: null, value: '' }]);
    }
  };

  useEffect(() => {
    updateCriteria();
  }, []);

  const search = () => {
    if (
      criteria.length === 1 &&
      isEqual(criteria, [{ field: null, value: '' }])
    ) {
      openSnackbar('Must provide a search query!', 'error');
      return;
    }

    const queryValues = criteria.map(
      (criterion: ReportSearchCriteria) =>
        `${criterion.field?.key}=${criterion.value}`,
    );

    const query = queryValues.join('&');

    router.push(
      `/${props.reportType}-report/search/${encodeURIComponent(query)}`,
    );
  };

  const enterKeyPress = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      search();
    }
  };

  return (
    <>
      <Box sx={{ flexGrow: 1, width: '75%', mx: 'auto', mb: 2 }}>
        {criteria.map((criterion, index) => (
          <Grid container spacing={2} key={index}>
            <Grid item xs={10}>
              <ReportSearchCriterionField
                criteria={criteria}
                setCriteria={setCriteria}
                index={index}
                field={criterion.field}
                value={criterion.value}
                reportSearchFields={props.reportSearchFields}
                counties={props.counties}
                institutions={props.institutions ? props.institutions : []}
                kingdoms={props.kingdoms ? props.kingdoms : []}
                sources={props.sources ? props.sources : []}
                onValueFieldEnterKeyPress={enterKeyPress}
              />
            </Grid>
            <Grid item xs={2}>
              <AddRemoveCriterion
                criteria={criteria}
                setCriteria={setCriteria}
                index={index}
                countyReportSearchFields={props.reportSearchFields}
              />
            </Grid>
          </Grid>
        ))}
        <ActionButton
          title={`Generate ${props.reportType} Report`}
          onButtonClick={search}
          startIcon={<SearchIcon />}
          // TODO: check if any criteria values are empty
          disabled={
            criteria.length === 1 &&
            isEqual(criteria, [{ field: null, value: '' }])
          }
        />
      </Box>
      {snackbar}
    </>
  );
}
