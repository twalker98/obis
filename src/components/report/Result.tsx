'use client';

import Box from '@mui/material/Box';
import { DataTable, GridColDef } from '@twalk-tech/react-lib/nextjs';
import {
  CountyReportResult,
  OccurrenceReportResult,
} from '../../types/report-result';

type ReportResultProps = {
  reportType: 'county' | 'occurrence';
  columns: Array<GridColDef>;
  results: Array<CountyReportResult | OccurrenceReportResult>;
};

export default function ReportResult(props: ReportResultProps) {
  return (
    <Box mt={3} maxWidth="80%" mx="auto">
      <DataTable
        height={750}
        getRowIdPredicate={
          props.reportType === 'county'
            ? (row: CountyReportResult) => row.acode as string
            : (row: OccurrenceReportResult) => row.gid as unknown as string
        }
        data={props.results}
        columns={props.columns}
        filterEnabled
        exportEnabled
      />
    </Box>
  );
}
