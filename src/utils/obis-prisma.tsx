import { PrismaClient as ObisPrismaClient } from '../prisma/generated/obis-client';
import {
  Prisma,
  acctax,
  basisofrecord_lu,
  category_lu,
  comtax,
  county,
  d_dist_confidence,
  d_origin,
  d_population,
  d_presence_absence,
  d_regularity,
  distribution_data,
  fed_status,
  global_rank_lu,
  hightax,
  identification_verification,
  institution,
  iucn_lu,
  kingdom_lu,
  name_category_desc_lu,
  nativity_lu,
  occurrence,
  ok_swap,
  resourcetype_lu,
  source,
  st_status,
  state_rank_lu,
  syntax,
  vw_all_taxa,
} from '../prisma/generated/obis-client';

let obisPrisma: ObisPrismaClient;

const globalForObisPrisma = global as unknown as {
  obisPrisma: ObisPrismaClient;
};

if (process.env.NODE_ENV === 'production') {
  obisPrisma = new ObisPrismaClient();
} else {
  if (!globalForObisPrisma.obisPrisma) {
    globalForObisPrisma.obisPrisma = new ObisPrismaClient();
  }

  obisPrisma = globalForObisPrisma.obisPrisma;
}

type ObisDataTransform = {
  field: string;
  table: string;
  tableField: string;
};

type ObisID = string | number;

export default obisPrisma;
export type {
  ObisDataTransform,
  ObisID,
  Prisma,
  acctax,
  basisofrecord_lu,
  category_lu,
  comtax,
  county,
  d_dist_confidence,
  d_origin,
  d_population,
  d_presence_absence,
  d_regularity,
  distribution_data,
  fed_status,
  global_rank_lu,
  hightax,
  identification_verification,
  institution,
  iucn_lu,
  kingdom_lu,
  name_category_desc_lu,
  nativity_lu,
  occurrence,
  ok_swap,
  resourcetype_lu,
  source,
  state_rank_lu,
  st_status,
  syntax,
  vw_all_taxa,
};
