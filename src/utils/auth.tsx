import { H } from '@highlight-run/next/client';
import { signIn } from 'next-auth/react';
import { ObisAuth } from './obis-auth-prisma';

const getSigninUrl = (callbackPath: string) => {
  return `/api/auth/signin?callbackUrl=${encodeURIComponent(
    `${process.env.NEXTAUTH_URL}/${callbackPath}`,
  )}`;
};

const login = async (credentials: ObisAuth, callbackUrl: string) => {
  try {
    const body = { ...credentials };
    await signIn('credentials', {
      ...body,
      callbackUrl: callbackUrl,
    });
  } catch (e) {
    H.consumeError(e);
    console.log(e);
  }
};

export { getSigninUrl, login };
