import obisAuthPrisma, { UserGroup } from '../../obis-auth-prisma';

const client = obisAuthPrisma['userGroup'];

export const getUserGroupRecordByUser = async (
  userId: string,
): Promise<Array<UserGroup>> => {
  return await client.findMany({ where: { userId: userId } });
};

export const getUserGroupRecordByGroup = async (
  groupId: string,
): Promise<Array<UserGroup>> => {
  return await client.findMany({ where: { groupId: groupId } });
};

export const createUserGroupRecord = async (
  userGroup: UserGroup,
): Promise<UserGroup> => {
  return await client.create({ data: { ...userGroup } });
};

// TODO: combine current arguments into one object
// Will need to update processPrismaCall in utils/functions.tsx
export const deleteUserGroupRecord = async (
  userId: string,
  groupId: string,
): Promise<UserGroup> => {
  return (await client.deleteMany({
    where: {
      userId: userId,
      groupId: groupId,
    },
  })) as unknown as UserGroup;
};

export type { UserGroup };
