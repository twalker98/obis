import obisAuthPrisma, { Permission } from '../../obis-auth-prisma';

const client = obisAuthPrisma['permission'];

export const getPermissionRecord = async (
  permissionId: string,
): Promise<Permission> => {
  return (await client.findUnique({
    where: { id: permissionId },
  })) as Permission;
};

export const listPermissionRecords = async (): Promise<Array<Permission>> => {
  return await client.findMany();
};

export const createPermissionRecord = async (
  permission: Permission,
): Promise<Permission> => {
  return await client.create({ data: permission });
};

export const updatePermissionRecord = async (
  permission: Permission,
  permissionId: string,
): Promise<Permission> => {
  return await client.update({
    where: { id: permissionId },
    data: { ...permission },
  });
};

export const deletePermissionRecord = async (
  values: Permission | null,
  permissionId: string,
): Promise<Permission> => {
  return await client.delete({ where: { id: permissionId } });
};

export type { Permission };
