import obisAuthPrisma, { UserPermission } from '../../obis-auth-prisma';

const client = obisAuthPrisma['userPermission'];

export const getUserPermissionRecordByUser = async (
  userId: string,
): Promise<Array<UserPermission>> => {
  return await client.findMany({ where: { userId: userId } });
};

export const getUserPermissionRecordByPermission = async (
  permissionId: string,
): Promise<Array<UserPermission>> => {
  return await client.findMany({ where: { permissionId: permissionId } });
};

export const createUserPermissionRecord = async (
  userPermission: UserPermission,
): Promise<UserPermission> => {
  return await client.create({ data: { ...userPermission } });
};

// TODO: combine current arguments into one object
// Will need to update processPrismaCall in utils/functions.tsx
export const deleteUserPermissionRecord = async (
  userId: string,
  permissionId: string,
): Promise<UserPermission> => {
  return (await client.deleteMany({
    where: {
      userId: userId,
      permissionId: permissionId,
    },
  })) as unknown as UserPermission;
};

export type { UserPermission };
