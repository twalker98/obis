import { hashPassword } from '../../functions';
import obisAuthPrisma, {
  ObisAuth,
  ObisUser,
  Prisma,
  User,
} from '../../obis-auth-prisma';

import { StatusCodes } from 'http-status-codes';
import { omit } from 'lodash';
import { NextResponse } from 'next/server';

const client = obisAuthPrisma['user'];

export const getUserRecord = async (userId: string): Promise<ObisUser> => {
  return (await client.findUnique({
    where: { id: userId },
    select: {
      id: true,
      firstName: true,
      lastName: true,
      username: true,
      email: true,
      groups: true,
      permissions: true,
      lastLogin: true,
    },
  })) as ObisUser;
};

export const listUserRecords = async (): Promise<Array<ObisUser>> => {
  return (await client.findMany({
    select: {
      id: true,
      firstName: true,
      lastName: true,
      username: true,
      email: true,
      groups: true,
      permissions: true,
      lastLogin: true,
    },
  })) as Array<ObisUser>;
};

export const createUserRecord = async (user: User): Promise<User> => {
  return await client.create({
    data: {
      ...user,
      password: hashPassword(user.password),
    },
  });
};

export const updateUserRecord = async (
  user: User,
  userId: string,
): Promise<User> => {
  return await client.update({
    where: { id: userId },
    data: { ...user },
  });
};

export const deleteUserRecord = async (
  values: User | null,
  userId: string,
): Promise<User> => {
  return await client.delete({ where: { id: userId } });
};

export const checkCredentialsNextResponse = async (
  auth: ObisAuth,
): Promise<NextResponse> => {
  const user = await client.findUnique({
    where: { username: auth.username },
  });

  if (user && user.password === hashPassword(auth.password)) {
    return NextResponse.json(omit(user as User, 'password') as ObisUser);
  }

  return NextResponse.json(
    { error: 'Invalid credentials!' },
    { status: StatusCodes.UNAUTHORIZED },
  );
};

export const checkCredentials = async (
  auth: ObisAuth,
): Promise<User | boolean> => {
  const user = await client.findUnique({
    where: { username: auth.username },
  });

  if (user && user.password === hashPassword(auth.password)) {
    return user;
  }

  return false;
};

// TODO: call checkCredentials() from here
export const changePassword = async (
  user: User,
  userId: string,
): Promise<User> => {
  return await client.update({
    where: { id: userId },
    data: {
      ...user,
      password: hashPassword(user.password),
    },
  });
};

export const searchUserRecords = async (
  whereClause: Prisma.UserWhereInput,
): Promise<Array<User>> => {
  return await client.findMany({
    where: whereClause,
  });
};

export type { ObisAuth, ObisUser, User };
