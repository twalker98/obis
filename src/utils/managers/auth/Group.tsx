import obisAuthPrisma, { Group } from '../../obis-auth-prisma';

const client = obisAuthPrisma['group'];

export const getGroupRecord = async (groupId: string): Promise<Group> => {
  return (await client.findUnique({ where: { id: groupId } })) as Group;
};

export const listGroupRecords = async (): Promise<Array<Group>> => {
  return await client.findMany();
};

export const createGroupRecord = async (group: Group): Promise<Group> => {
  return await client.create({ data: group });
};

export const updateGroupRecord = async (
  group: Group,
  groupId: string,
): Promise<Group> => {
  return await client.update({
    where: { id: groupId },
    data: { ...group },
  });
};

export const deleteGroupRecord = async (
  values: Group | null,
  groupId: string,
): Promise<Group> => {
  return await client.delete({ where: { id: groupId } });
};

export type { Group };
