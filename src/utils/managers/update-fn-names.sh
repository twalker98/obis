#!/bin/bash
SEARCH_DIR=auth
for FILE in "$SEARCH_DIR"/*
do
  TABLE=$(echo $FILE | cut -d'.' -f 1 | cut -d'/' -f 2)

  sed -i -E "s/export const get/export const get${TABLE}Record/" $FILE
  sed -i -E "s/export const list/export const list${TABLE}Records/" $FILE
  sed -i -E "s/export const create/export const create${TABLE}Record/" $FILE
  sed -i -E "s/export const update/export const update${TABLE}Record/" $FILE
  sed -i -E "s/export const search/export const search${TABLE}Records/" $FILE
done
