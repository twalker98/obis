import obisPrisma, { ok_swap } from '../../obis-prisma';

const client = obisPrisma['ok_swap'];

export const getOkSwapRecord = async (swap_id: number): Promise<ok_swap> => {
  return (await client.findUnique({
    where: { swap_id: swap_id },
  })) as ok_swap;
};

export const listOkSwapRecords = async (): Promise<Array<ok_swap>> => {
  return await client.findMany();
};

export type { ok_swap };
