import obisPrisma, { nativity_lu } from '../../obis-prisma';

const client = obisPrisma['nativity_lu'];

export const getNativityRecord = async (
  nativity: string,
): Promise<nativity_lu> => {
  return (await client.findUnique({
    where: { nativity: nativity },
  })) as nativity_lu;
};

export const listNativityRecords = async (): Promise<Array<nativity_lu>> => {
  return await client.findMany();
};

export type { nativity_lu };
