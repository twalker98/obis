import obisPrisma, { vw_all_taxa } from '../../obis-prisma';

const client = obisPrisma['vw_all_taxa'];

export const getVwAllTaxaRecord = async (
  unique_code: string,
): Promise<vw_all_taxa> => {
  return (await client.findUnique({
    where: { unique_code: unique_code },
  })) as vw_all_taxa;
};

export const listVwAllTaxaRecords = async (): Promise<Array<vw_all_taxa>> => {
  return await client.findMany();
};

export type { vw_all_taxa };
