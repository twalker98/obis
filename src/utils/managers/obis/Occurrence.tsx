import obisPrisma, { Prisma, occurrence } from '../../obis-prisma';

import dayjs from 'dayjs';
import {
  CountyReportResult,
  OccurrenceReportResult,
} from '../../../types/report-result';
import { TaxonOccurrenceTableResult } from '../../../types/taxon-result';
import {
  convertEmptyStringsToNull,
  flattenObject,
  remapRelationalFieldsForSubmission,
} from '../../functions';
import { relationalFieldMappings } from '../../relational_mappings/Occurrence';

const client = obisPrisma['occurrence'];

const transformOccurrenceData = (
  occurrence: occurrence,
): Prisma.occurrenceCreateInput => {
  remapRelationalFieldsForSubmission(relationalFieldMappings, occurrence);

  // We can ensure this is not null, because the form requires it as an input
  const acode = occurrence['acode']!['acode'];
  // For some reason, this line throws the error "The operand of a 'delete' operator must be optional." However, this is an optional argument.
  // @ts-expect-error
  delete occurrence['acode'];
  occurrence['acctax'] = {
    connect: {
      acode: acode,
    },
  };

  // We want to have a date string provided to this field, even though it expects a Date object
  // @ts-expect-error
  occurrence['datelastmodified'] = dayjs().format();

  // We can ensure these are not null, because the form requires them as an input
  occurrence['individuals_min'] = occurrence['individuals_min']
    ? parseInt(occurrence['individuals_min'] as unknown as string)
    : null;
  occurrence['individuals_max'] = occurrence['individuals_max']
    ? parseInt(occurrence['individuals_max'] as unknown as string)
    : null;
  occurrence['georeferenceverificationstatus'] =
    occurrence['georeferenceverificationstatus']!['display_name'];
  occurrence['identificationconfidence'] =
    occurrence['identificationconfidence']!['id'] === 0
      ? null
      : occurrence['identificationconfidence']!['confidence'];
  occurrence['township'] = occurrence['township']
    ? parseInt(occurrence['township'] as unknown as string)
    : null;
  occurrence['ns'] = occurrence['ns'] ? occurrence['ns']!['direction'] : null;
  occurrence['range'] = occurrence['range']
    ? parseInt(occurrence['range'] as unknown as string)
    : null;
  occurrence['ew'] = occurrence['ew'] ? occurrence['ew']!['direction'] : null;

  convertEmptyStringsToNull(occurrence);

  return occurrence as Prisma.occurrenceCreateInput;
};

export const getOccurrenceRecord = async (gid: number): Promise<occurrence> => {
  return (await client.findUnique({
    where: { gid: gid },
  })) as occurrence;
};

export const listOccurrenceRecords = async (): Promise<Array<occurrence>> => {
  return await client.findMany();
};

export const createOccurrenceRecord = async (
  occurrence: occurrence,
): Promise<occurrence> => {
  return await client.create({
    data: transformOccurrenceData(occurrence),
  });
};

export const updateOccurrenceRecord = async (
  occurrence: occurrence,
  gid: number,
): Promise<occurrence> => {
  return await client.update({
    where: { gid: parseInt(gid as unknown as string) },
    data: transformOccurrenceData(occurrence),
  });
};

export const searchOccurrenceRecords = async (
  whereClause: Prisma.occurrenceWhereInput,
): Promise<Array<occurrence>> => {
  return await client.findMany({
    where: whereClause,
  });
};

export const getTaxonOccurrenceTable = async (
  acode: string,
  authenticated: boolean,
): Promise<Array<TaxonOccurrenceTableResult>> => {
  const occurrenceWhereClause: Prisma.occurrenceWhereInput = {
    acode: acode,
  };

  if (!authenticated) {
    occurrenceWhereClause['hiderecord'] = false;
  }

  const occurrences = await client.groupBy({
    by: ['acode', 'county'],
    where: occurrenceWhereClause,
    _count: {
      acode: true,
    },
    _min: {
      eventdate: true,
    },
    _max: {
      eventdate: true,
    },
  });

  return occurrences as Array<TaxonOccurrenceTableResult>;
};

export const getCountyReport = async (
  whereClause: Prisma.occurrenceWhereInput,
): Promise<Array<CountyReportResult>> => {
  const results: Array<CountyReportResult> = [];

  const counts = await client.groupBy({
    by: ['acode', 'county'],
    where: whereClause,
    _count: {
      acode: true,
    },
  });

  const occurrences = await client.findMany({
    where: whereClause,
    select: {
      acode: true,
      county: true,
      acctax: {
        select: {
          genus: true,
          species: true,
          subspecies: true,
          variety: true,
          hightax: {
            select: {
              phylum: true,
              taxclass: true,
              taxorder: true,
              family: true,
            },
          },
        },
      },
    },
    orderBy: {
      acode: 'asc',
    },
    distinct: ['acode', 'county'],
  });

  for (const occurrence of occurrences) {
    const result = flattenObject<CountyReportResult>(occurrence);
    result.count = counts.find(
      (count) =>
        count.acode === occurrence.acode && count.county === occurrence.county,
    )?._count.acode;
    results.push(result);
  }

  return results;
};

export const getOccurrenceReport = async (
  whereClause: Prisma.occurrenceWhereInput,
): Promise<Array<OccurrenceReportResult>> => {
  const results: Array<OccurrenceReportResult> = [];

  const occurrences = await client.findMany({
    where: whereClause,
    select: {
      gid: true,
      recordedby: true,
      eventdate: true,
      datasetname: true,
      county: true,
      institutioncode: true,
      catalognumber: true,
      locality: true,
      acctax: {
        select: {
          sname: true,
          scientificname: true,
          scientificnameauthorship: true,
          hightax: {
            select: {
              family: true,
            },
          },
        },
      },
    },
  });

  for (const occurrence of occurrences) {
    const result = flattenObject<OccurrenceReportResult>(occurrence);
    results.push(result);
  }

  return results;
};

export type { Prisma, occurrence };
