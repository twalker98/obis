import obisPrisma, { basisofrecord_lu } from '../../obis-prisma';

const client = obisPrisma['basisofrecord_lu'];

export const getBasisOfRecordRecord = async (
  basisOfRecord: string,
): Promise<basisofrecord_lu> => {
  return (await client.findUnique({
    where: {
      basisofrecord: basisOfRecord,
    },
  })) as basisofrecord_lu;
};

export const listBasisOfRecordRecords = async (): Promise<
  Array<basisofrecord_lu>
> => {
  return await client.findMany();
};

export type { basisofrecord_lu };
