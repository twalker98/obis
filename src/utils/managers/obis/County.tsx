import obisPrisma, { county } from '../../obis-prisma';

const client = obisPrisma['county'];

export const getCountyRecord = async (county: string): Promise<county> => {
  return (await client.findUnique({
    where: { county: county },
  })) as county;
};

export const listCountyRecords = async (): Promise<Array<county>> => {
  return await client.findMany();
};

export type { county };
