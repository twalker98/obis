import obisPrisma, { iucn_lu } from '../../obis-prisma';

const client = obisPrisma['iucn_lu'];

export const getIUCNCodeRecord = async (code: string): Promise<iucn_lu> => {
  return (await client.findUnique({
    where: { code: code },
  })) as iucn_lu;
};

export const listIUCNCodeRecords = async (): Promise<Array<iucn_lu>> => {
  return await client.findMany();
};

export type { iucn_lu };
