import obisPrisma, { state_rank_lu } from '../../obis-prisma';

const client = obisPrisma['state_rank_lu'];

export const getStateRankRecord = async (
  code: string,
): Promise<state_rank_lu> => {
  return (await client.findUnique({
    where: { code: code },
  })) as state_rank_lu;
};

export const listStateRankRecords = async (): Promise<Array<state_rank_lu>> => {
  return await client.findMany();
};

export type { state_rank_lu };
