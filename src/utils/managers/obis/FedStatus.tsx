import obisPrisma, { fed_status } from '../../obis-prisma';

const client = obisPrisma['fed_status'];

export const getFedStatusRecord = async (
  status_id: number,
): Promise<fed_status> => {
  return (await client.findUnique({
    where: { status_id: status_id },
  })) as fed_status;
};

export const listFedStatusRecords = async (): Promise<Array<fed_status>> => {
  return await client.findMany();
};

export type { fed_status };
