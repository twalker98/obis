import obisPrisma, { Prisma, acctax } from '../../obis-prisma';

import {
  convertEmptyStringsToNull,
  remapRelationalFieldsForSubmission,
} from '../../functions';
import { relationalFieldMappings } from '../../relational_mappings/Acctax';

const client = obisPrisma['acctax'];

const transformAcctaxData = (acctax: acctax): Prisma.acctaxCreateInput => {
  remapRelationalFieldsForSubmission(relationalFieldMappings, acctax);

  const family = acctax['family'];
  // For some reason, this line throws the error "The operand of a 'delete' operator must be optional." However, this is an optional argument.
  // @ts-expect-error
  delete acctax['family'];
  acctax['hightax'] = {
    connect: {
      family: family,
    },
  };

  // These throw an error, because the schema for the acctax table requires a number for both of these fields. However, the form returns a string.
  // @ts-expect-error
  acctax['gelcode'] = acctax['gelcode'] === '' ? null : acctax['gelcode'];
  // @ts-expect-error
  acctax['tsn'] = acctax['tsn'] === '' ? null : acctax['tsn'];

  // We can ensure this is not null, because the form requires it as an input
  acctax['tracked'] = acctax['tracked']!['value'];

  convertEmptyStringsToNull(acctax);

  return acctax as Prisma.acctaxCreateInput;
};

export const getAcctaxRecord = async (acode: string): Promise<acctax> => {
  return (await client.findUnique({
    where: { acode: acode },
  })) as acctax;
};

export const listAcctaxRecords = async (): Promise<Array<acctax>> => {
  return await client.findMany();
};

export const createAcctaxRecord = async (values: acctax): Promise<acctax> => {
  return await client.create({ data: transformAcctaxData(values) });
};

export const updateAcctaxRecord = async (
  values: acctax,
  acode: string,
): Promise<acctax> => {
  return await client.update({
    where: { acode: acode },
    data: transformAcctaxData(values),
  });
};

export const searchAcctaxRecords = async (
  whereClause: Prisma.acctaxWhereInput,
): Promise<Array<acctax>> => {
  return await client.findMany({
    where: whereClause,
  });
};

export type { acctax };
