import obisPrisma, { d_regularity } from '../../obis-prisma';

const client = obisPrisma['d_regularity'];

export const getRegularityRecord = async (
  d_regularity_id: number,
): Promise<d_regularity> => {
  return (await client.findUnique({
    where: { d_regularity_id: d_regularity_id },
  })) as d_regularity;
};

export const listRegularityRecords = async (): Promise<Array<d_regularity>> => {
  return await client.findMany();
};

export type { d_regularity };
