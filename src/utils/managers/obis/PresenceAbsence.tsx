import obisPrisma, { d_presence_absence } from '../../obis-prisma';

const client = obisPrisma['d_presence_absence'];

export const getPresenceAbsenceRecord = async (
  d_presence_absence_id: number,
): Promise<d_presence_absence> => {
  return (await client.findUnique({
    where: { d_presence_absence_id: d_presence_absence_id },
  })) as d_presence_absence;
};

export const listPresenceAbsenceRecords = async (): Promise<
  Array<d_presence_absence>
> => {
  return await client.findMany();
};

export type { d_presence_absence };
