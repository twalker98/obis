import obisPrisma, { Prisma, comtax } from '../../obis-prisma';

const client = obisPrisma['comtax'];

const get_max_c_id = async (): Promise<number> => {
  const maxCID = await client.aggregate({
    _max: {
      c_id: true,
    },
  });

  return maxCID._max.c_id as number;
};

const transformComtaxData = (comtax: comtax): Prisma.comtaxCreateInput => {
  const primary_name = comtax.primary_name;

  // @ts-expect-error
  delete comtax['primary_name'];

  comtax['primary_name'] = primary_name!['value'];

  return comtax as Prisma.comtaxCreateInput;
};

export const getComtaxRecord = async (c_id: number): Promise<comtax> => {
  return (await client.findUnique({ where: { c_id: c_id } })) as comtax;
};

export const listComtaxRecords = async (): Promise<Array<comtax>> => {
  return await client.findMany();
};

export const createComtaxRecord = async (comtax: comtax): Promise<comtax> => {
  return await client.create({
    data: {
      ...comtax,
      c_id: (await get_max_c_id()) + 1,
    },
  });
};

export const updateComtaxRecord = async (
  comtax: comtax,
  c_id: number,
): Promise<comtax> => {
  return await client.update({
    where: { c_id: c_id },
    data: transformComtaxData(comtax),
  });
};

export const deleteComtaxRecord = async (
  values: comtax | null,
  c_id: number,
): Promise<comtax> => {
  return await client.delete({ where: { c_id: c_id } });
};

export const searchComtaxRecords = async (
  whereClause: Prisma.comtaxWhereInput,
): Promise<Array<comtax>> => {
  return await client.findMany({
    where: whereClause,
  });
};

export type { comtax };
