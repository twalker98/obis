import obisPrisma, { Prisma, hightax } from '../../obis-prisma';

import { convertEmptyStringsToNull } from '../../functions';

const client = obisPrisma['hightax'];

const transformHightaxData = (hightax: hightax): Prisma.hightaxCreateInput => {
  convertEmptyStringsToNull(hightax);

  return hightax as Prisma.hightaxCreateInput;
};

export const getHightaxRecord = async (family: string): Promise<hightax> => {
  return (await client.findUnique({
    where: { family: family },
  })) as hightax;
};

export const listHightaxRecords = async (): Promise<Array<hightax>> => {
  return await client.findMany();
};

export const createHightaxRecord = async (
  hightax: hightax,
): Promise<hightax> => {
  return await client.create({
    data: transformHightaxData(hightax),
  });
};

export const searchHightaxRecords = async (
  whereClause: Prisma.hightaxWhereInput,
): Promise<Array<hightax>> => {
  return await client.findMany({
    where: whereClause,
  });
};

export type { hightax };
