import obisPrisma, { source } from '../../obis-prisma';

const client = obisPrisma['source'];

export const getSourceRecord = async (source: string): Promise<source> => {
  return (await client.findUnique({
    where: { source: source },
  })) as source;
};

export const listSourceRecords = async (): Promise<Array<source>> => {
  return await client.findMany();
};

export type { source };
