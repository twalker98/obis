import obisPrisma, { global_rank_lu } from '../../obis-prisma';

const client = obisPrisma['global_rank_lu'];

export const getGlobalRankRecord = async (
  code: string,
): Promise<global_rank_lu> => {
  return (await client.findUnique({
    where: { code: code },
  })) as global_rank_lu;
};

export const listGlobalRankRecords = async (): Promise<
  Array<global_rank_lu>
> => {
  return await client.findMany();
};

export type { global_rank_lu };
