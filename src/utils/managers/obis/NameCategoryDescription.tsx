import obisPrisma, { name_category_desc_lu } from '../../obis-prisma';

const client = obisPrisma['name_category_desc_lu'];

export const getNameCategoryDescriptionRecord = async (
  name_category_desc: string,
): Promise<name_category_desc_lu> => {
  return (await client.findUnique({
    where: { name_category_desc: name_category_desc },
  })) as name_category_desc_lu;
};

export const listNameCategoryDescriptionRecords = async (): Promise<
  Array<name_category_desc_lu>
> => {
  return await client.findMany();
};

export type { name_category_desc_lu };
