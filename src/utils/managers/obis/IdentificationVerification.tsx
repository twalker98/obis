import obisPrisma, {
  Prisma,
  identification_verification,
} from '../../obis-prisma';

const client = obisPrisma['identification_verification'];

const transformIdentificationVerificationData = (
  identification_verification: identification_verification,
): Prisma.identification_verificationCreateInput => {
  identification_verification['gid'] = parseInt(
    identification_verification['gid'] as unknown as string,
  );

  return identification_verification as Prisma.identification_verificationCreateInput;
};

export const getIdentificationVerificationRecord = async (
  pkey: number,
): Promise<identification_verification> => {
  return (await client.findUnique({
    where: { pkey: pkey },
  })) as identification_verification;
};

export const createIdentificationVerificationRecord = async (
  identification_verification: identification_verification,
): Promise<identification_verification> => {
  return await client.create({
    data: transformIdentificationVerificationData(identification_verification),
  });
};

export const updateIdentificationVerificationRecord = async (
  identification_verification: identification_verification,
  pkey: number,
): Promise<identification_verification> => {
  return await client.update({
    where: { pkey: pkey },
    data: identification_verification,
  });
};

export const deleteIdentificationVerificationRecord = async (
  values: identification_verification | null,
  pkey: number,
): Promise<identification_verification> => {
  return await client.delete({ where: { pkey: pkey } });
};

export const searchIdentificationVerificationRecords = async (
  whereClause: Prisma.identification_verificationWhereInput,
): Promise<Array<identification_verification>> => {
  return await client.findMany({ where: whereClause });
};

export type { identification_verification };
