import obisPrisma, { Prisma, distribution_data } from '../../obis-prisma';

import { remapRelationalFieldsForSubmission } from '../../functions';
import { relationalFieldMappings } from '../../relational_mappings/DistributionData';

const client = obisPrisma['distribution_data'];

const transformDistributionDataData = (
  distribution_data: distribution_data,
): Prisma.distribution_dataCreateInput => {
  remapRelationalFieldsForSubmission(
    relationalFieldMappings,
    distribution_data,
  );

  const acode = distribution_data['acode'];
  // For some reason, this line throws the error "The operand of a 'delete' operator must be optional." However, this is an optional argument.
  // @ts-expect-error
  delete distribution_data['acode'];
  distribution_data['acctax'] = {
    connect: {
      acode: acode,
    },
  };

  return distribution_data as Prisma.distribution_dataCreateInput;
};

export const getDistributionDataRecord = async (
  d_id: number,
): Promise<distribution_data> => {
  return (await client.findUnique({
    where: { d_id: d_id },
  })) as distribution_data;
};

export const listDistributionDataRecords = async (): Promise<
  Array<distribution_data>
> => {
  return await client.findMany();
};

export const createDistributionDataRecord = async (
  values: distribution_data,
): Promise<distribution_data> => {
  return await client.create({ data: transformDistributionDataData(values) });
};

export const updateDistributionDataRecord = async (
  values: distribution_data,
  d_id: number,
): Promise<distribution_data> => {
  return await client.update({
    where: { d_id: d_id },
    data: transformDistributionDataData(values),
  });
};

export const searchDistributionDataRecords = async (
  whereClause: Prisma.distribution_dataWhereInput,
): Promise<Array<distribution_data>> => {
  return await client.findMany({
    where: whereClause,
  });
};

export type { distribution_data };
