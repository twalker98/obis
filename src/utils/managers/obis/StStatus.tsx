import obisPrisma, { st_status } from '../../obis-prisma';

const client = obisPrisma['st_status'];

export const getStStatusRecord = async (
  status_id: number,
): Promise<st_status> => {
  return (await client.findUnique({
    where: { status_id: status_id },
  })) as st_status;
};

export const listStStatusRecords = async (): Promise<Array<st_status>> => {
  return await client.findMany();
};

export type { st_status };
