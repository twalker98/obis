import obisPrisma, { d_origin } from '../../obis-prisma';

const client = obisPrisma['d_origin'];

export const getOriginRecord = async (
  d_origin_id: number,
): Promise<d_origin> => {
  return (await client.findUnique({
    where: { d_origin_id: d_origin_id },
  })) as d_origin;
};

export const listOriginRecords = async (): Promise<Array<d_origin>> => {
  return await client.findMany();
};

export type { d_origin };
