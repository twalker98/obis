import obisPrisma, { category_lu } from '../../obis-prisma';

const client = obisPrisma['category_lu'];

export const getCategoryRecord = async (
  category: string,
): Promise<category_lu> => {
  return (await client.findUnique({
    where: {
      category: category,
    },
  })) as category_lu;
};

export const listCategoryRecords = async (): Promise<Array<category_lu>> => {
  return await client.findMany();
};

export type { category_lu };
