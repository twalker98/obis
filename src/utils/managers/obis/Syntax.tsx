import { convertEmptyStringsToNull } from '../../functions';
import obisPrisma, { Prisma, syntax } from '../../obis-prisma';

const client = obisPrisma['syntax'];

const transformSyntaxData = (syntax: syntax): Prisma.syntaxCreateInput => {
  // These throw an error, because the schema for the acctax table requires a number for both of these fields. However, the form returns a string.
  // @ts-expect-error
  syntax['tsn'] = syntax['tsn'] === '' ? null : syntax['tsn'];

  convertEmptyStringsToNull(syntax);

  return syntax as Prisma.syntaxCreateInput;
};

export const getSyntaxRecord = async (scode: string): Promise<syntax> => {
  return (await client.findUnique({
    where: { scode: scode },
  })) as syntax;
};

export const listSyntaxRecords = async (): Promise<Array<syntax>> => {
  return await client.findMany();
};

export const createSyntaxRecord = async (syntax: syntax): Promise<syntax> => {
  return await client.create({ data: transformSyntaxData(syntax) });
};

export const updateSyntaxRecord = async (
  syntax: syntax,
  scode: string,
): Promise<syntax> => {
  return await client.update({
    where: { scode: scode },
    data: transformSyntaxData(syntax),
  });
};

export const deleteSyntaxRecord = async (
  values: syntax | null,
  scode: string,
): Promise<syntax> => {
  return await client.delete({ where: { scode: scode } });
};

export const searchSyntaxRecords = async (
  whereClause: Prisma.syntaxWhereInput,
): Promise<Array<syntax>> => {
  return await client.findMany({
    where: whereClause,
  });
};

export type { syntax };
