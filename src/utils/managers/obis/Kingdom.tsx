import obisPrisma, { kingdom_lu } from '../../obis-prisma';

const client = obisPrisma['kingdom_lu'];

export const getKingdomRecord = async (
  kingdom: string,
): Promise<kingdom_lu> => {
  return (await client.findUnique({
    where: { kingdom: kingdom },
  })) as kingdom_lu;
};

export const listKingdomRecords = async (): Promise<Array<kingdom_lu>> => {
  return await client.findMany();
};

export type { kingdom_lu };
