import obisPrisma, { d_dist_confidence } from '../../obis-prisma';

const client = obisPrisma['d_dist_confidence'];

export const getDistributionConfidenceRecord = async (
  d_dist_confidence_id: number,
): Promise<d_dist_confidence> => {
  return (await client.findUnique({
    where: { d_dist_confidence_id: d_dist_confidence_id },
  })) as d_dist_confidence;
};

export const listDistributionConfidenceRecords = async (): Promise<
  Array<d_dist_confidence>
> => {
  return await client.findMany();
};

export type { d_dist_confidence };
