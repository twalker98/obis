import obisPrisma, { institution } from '../../obis-prisma';

const client = obisPrisma['institution'];

export const getInstitutionRecord = async (
  institutionCode: string,
): Promise<institution> => {
  return (await client.findUnique({
    where: { institutioncode: institutionCode },
  })) as institution;
};

export const listInstitutionRecords = async (): Promise<Array<institution>> => {
  return await client.findMany();
};

export type { institution };
