import obisPrisma, { resourcetype_lu } from '../../obis-prisma';

const client = obisPrisma['resourcetype_lu'];

export const getResourceTypeRecord = async (
  resourcetype: string,
): Promise<resourcetype_lu> => {
  return (await client.findUnique({
    where: { resourcetype: resourcetype },
  })) as resourcetype_lu;
};

export const listResourceTypeRecords = async (): Promise<
  Array<resourcetype_lu>
> => {
  return await client.findMany();
};

export type { resourcetype_lu };
