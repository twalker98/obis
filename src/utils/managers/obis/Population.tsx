import obisPrisma, { d_population } from '../../obis-prisma';

const client = obisPrisma['d_population'];

export const getPopulationRecord = async (
  d_population_id: number,
): Promise<d_population> => {
  return (await client.findUnique({
    where: { d_population_id: d_population_id },
  })) as d_population;
};

export const listPopulationRecords = async (): Promise<Array<d_population>> => {
  return await client.findMany();
};

export type { d_population };
