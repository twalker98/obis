import {
  Direction,
  IdentificationConfidence,
  OccurrenceSearchField,
  Relations,
  TFValue,
} from '../types/relations';
import { sort } from './functions';
import {
  basisofrecord_lu,
  listBasisOfRecordRecords,
} from './managers/obis/BasisOfRecord';
import { category_lu, listCategoryRecords } from './managers/obis/Category';
import { county, listCountyRecords } from './managers/obis/County';
import {
  d_dist_confidence,
  listDistributionConfidenceRecords,
} from './managers/obis/DistributionConfidence';
import { fed_status, listFedStatusRecords } from './managers/obis/FedStatus';
import {
  global_rank_lu,
  listGlobalRankRecords,
} from './managers/obis/GlobalRank';
import { iucn_lu, listIUCNCodeRecords } from './managers/obis/IUCNCode';
import {
  institution,
  listInstitutionRecords,
} from './managers/obis/Institution';
import { kingdom_lu, listKingdomRecords } from './managers/obis/Kingdom';
import {
  listNameCategoryDescriptionRecords,
  name_category_desc_lu,
} from './managers/obis/NameCategoryDescription';
import { listNativityRecords, nativity_lu } from './managers/obis/Nativity';
import { listOkSwapRecords, ok_swap } from './managers/obis/OkSwap';
import { d_origin, listOriginRecords } from './managers/obis/Origin';
import {
  d_population,
  listPopulationRecords,
} from './managers/obis/Population';
import {
  d_presence_absence,
  listPresenceAbsenceRecords,
} from './managers/obis/PresenceAbsence';
import {
  d_regularity,
  listRegularityRecords,
} from './managers/obis/Regularity';
import {
  listResourceTypeRecords,
  resourcetype_lu,
} from './managers/obis/ResourceType';
import { listSourceRecords, source } from './managers/obis/Source';
import { listStStatusRecords, st_status } from './managers/obis/StStatus';
import { listStateRankRecords, state_rank_lu } from './managers/obis/StateRank';

class RelationsService {
  private relations: Relations;

  private async getBasisOfRecords(): Promise<Array<basisofrecord_lu>> {
    const basisOfRecords = sort(
      await listBasisOfRecordRecords(),
      'basisofrecord',
    );

    basisOfRecords.unshift({
      id: -1,
      basisofrecord: 'None',
    });

    return basisOfRecords;
  }

  private async getCategories(): Promise<Array<category_lu>> {
    return sort(await listCategoryRecords(), 'category');
  }

  private async getCounties(): Promise<Array<county>> {
    return sort(await listCountyRecords(), 'county');
  }

  private async getDistributionConfidences(): Promise<
    Array<d_dist_confidence>
  > {
    const distributionConfidences = await listDistributionConfidenceRecords();

    distributionConfidences.unshift({
      d_dist_confidence_id: -1,
      dist_confidence: 'None',
    });

    return distributionConfidences;
  }

  private async getFedStatuses(): Promise<Array<fed_status>> {
    const fedStatuses = await listFedStatusRecords();

    fedStatuses.unshift({
      status_id: -1,
      status: '',
      description: 'Not Listed',
    });

    return fedStatuses;
  }

  private async getGlobalRanks(): Promise<Array<global_rank_lu>> {
    return await listGlobalRankRecords();
  }

  private async getInstitutions(): Promise<Array<institution>> {
    const institutions = sort(await listInstitutionRecords(), 'institution');

    institutions.unshift({
      institutioncode: '',
      institution: 'None',
      curator: null,
      email: null,
      telephone: null,
      address: null,
      city: null,
      state: null,
      country: null,
      zipcode: null,
      institutiontype: null,
      link: null,
    });

    return institutions;
  }

  private async getIucnCodes(): Promise<Array<iucn_lu>> {
    const iucnCodes = await listIUCNCodeRecords();

    iucnCodes.unshift({
      id: -1,
      code: '',
      description: 'None',
    });

    return iucnCodes;
  }

  private async getKingdoms(): Promise<Array<kingdom_lu>> {
    return await listKingdomRecords();
  }

  private async getNameCategoryDescriptions(): Promise<
    Array<name_category_desc_lu>
  > {
    return sort(
      await listNameCategoryDescriptionRecords(),
      'name_category_desc',
    );
  }

  private async getNativities(): Promise<Array<nativity_lu>> {
    const nativities = await listNativityRecords();

    nativities.unshift({
      n_id: -1,
      nativity: 'None',
    });

    return nativities;
  }

  private async getOrigins(): Promise<Array<d_origin>> {
    const origins = await listOriginRecords();

    origins.unshift({
      d_origin_id: -1,
      origin: 'None',
    });

    return origins;
  }

  private async getPopulations(): Promise<Array<d_population>> {
    const populations = await listPopulationRecords();

    populations.unshift({
      d_population_id: -1,
      population: 'None',
    });

    return populations;
  }

  private async getPresenceAbsences(): Promise<Array<d_presence_absence>> {
    const presenceAbsences = await listPresenceAbsenceRecords();

    presenceAbsences.unshift({
      d_presence_absence_id: -1,
      presence_absence: 'None',
    });

    return presenceAbsences;
  }

  private async getRegularities(): Promise<Array<d_regularity>> {
    const regularities = await listRegularityRecords();

    regularities.unshift({
      d_regularity_id: -1,
      regularity: 'None',
    });

    return regularities;
  }

  private async getResourceTypes(): Promise<Array<resourcetype_lu>> {
    const resourceTypes = await listResourceTypeRecords();

    resourceTypes.unshift({
      id: -1,
      resourcetype: 'None',
    });

    return resourceTypes;
  }

  private async getSources(): Promise<Array<source>> {
    const sources = sort(await listSourceRecords(), 'description');

    sources.unshift({
      source: '',
      description: 'None',
    });

    return sources;
  }

  private async getStateRanks(): Promise<Array<state_rank_lu>> {
    return await listStateRankRecords();
  }

  private async getStateStatuses(): Promise<Array<st_status>> {
    const stateStatuses = await listStStatusRecords();

    stateStatuses.unshift({
      status_id: -1,
      status: '',
      description: 'Not Listed',
    });

    return stateStatuses;
  }

  private async getSwaps(): Promise<Array<ok_swap>> {
    const swaps = sort(await listOkSwapRecords(), 'tier');

    swaps.unshift({
      swap_id: -1,
      tier: '',
      description: 'Not Included',
    });

    return swaps;
  }

  private buildTFValues(): Array<TFValue> {
    return [
      {
        display_name: 'True',
        value: true,
      },
      {
        display_name: 'False',
        value: false,
      },
    ];
  }

  private buildIdentificationConfidences(): Array<IdentificationConfidence> {
    return [
      {
        id: 0,
        confidence: 'Unknown',
      },
      {
        id: 1,
        confidence: 'Low',
      },
      {
        id: 2,
        confidence: 'Medium',
      },
      {
        id: 3,
        confidence: 'High',
      },
    ];
  }

  private buildDirectionsNS(): Array<Direction> {
    return [
      {
        direction: 'N',
        display_name: 'North',
      },
      {
        direction: 'S',
        display_name: 'South',
      },
    ];
  }

  private buildDirectionsEW(): Array<Direction> {
    return [
      {
        direction: 'E',
        display_name: 'East',
      },
      {
        direction: 'W',
        display_name: 'West',
      },
    ];
  }

  private buildOccurrenceSearchFields(): Array<OccurrenceSearchField> {
    return [
      {
        key: 'gid',
        display_name: 'gid',
      },
      {
        key: 'acode',
        display_name: 'acode',
      },
      {
        key: 'county',
        display_name: 'County',
      },
      {
        key: 'sname',
        display_name: 'Scientific Name',
      },
      {
        key: 'family',
        display_name: 'Family',
      },
      {
        key: 'genus',
        display_name: 'Genus',
      },
      {
        key: 'ssp',
        display_name: 'Subspecies',
      },
      {
        key: 'var',
        display_name: 'Variety',
      },
      {
        key: 'vname',
        display_name: 'Common Name',
      },
      {
        key: 'recordedby',
        display_name: 'Collector',
      },
      {
        key: 'eventdate',
        display_name: 'Collection Date',
      },
      {
        key: 'catalognumber',
        display_name: 'Catalog Number',
      },
    ];
  }

  private buildCountyReportSearchFields(): Array<OccurrenceSearchField> {
    return [
      {
        key: 'county',
        display_name: 'County',
      },
      {
        key: 'kingdom',
        display_name: 'Kingdom',
      },
      {
        key: 'phylum',
        display_name: 'Phylum',
      },
      {
        key: 'taxclass',
        display_name: 'Class',
      },
      {
        key: 'taxorder',
        display_name: 'Order',
      },
      {
        key: 'family',
        display_name: 'Family',
      },
      {
        key: 'genus',
        display_name: 'Genus',
      },
      {
        key: 'species',
        display_name: 'Species',
      },
      {
        key: 'subspecies',
        display_name: 'Subspecies',
      },
      {
        key: 'variety',
        display_name: 'Variety',
      },
    ];
  }

  private buildOccurrenceReportSearchFields(): Array<OccurrenceSearchField> {
    return [
      {
        key: 'family',
        display_name: 'Family',
      },
      {
        key: 'genus',
        display_name: 'Genus',
      },
      {
        key: 'species',
        display_name: 'Species',
      },
      {
        key: 'subspecies',
        display_name: 'Subspecies',
      },
      {
        key: 'variety',
        display_name: 'Variety',
      },
      {
        key: 'forma',
        display_name: 'Forma',
      },
      {
        key: 'vernacularname',
        display_name: 'Vernacular Name',
      },
      {
        key: 'recordedby',
        display_name: 'Recorded By',
      },
      {
        key: 'county',
        display_name: 'County',
      },
      {
        key: 'institutioncode',
        display_name: 'Institution Code',
      },
      {
        key: 'catalognumber',
        display_name: 'Catalog Number',
      },
      {
        key: 'datasetname',
        display_name: 'Dataset Name',
      },
    ];
  }

  async getRelations(): Promise<Relations> {
    if (!this.relations) {
      this.relations = {
        basisofrecord_lu: await this.getBasisOfRecords(),
        category_lu: await this.getCategories(),
        county: await this.getCounties(),
        d_dist_confidence: await this.getDistributionConfidences(),
        d_origin: await this.getOrigins(),
        d_population: await this.getPopulations(),
        d_presence_absence: await this.getPresenceAbsences(),
        d_regularity: await this.getRegularities(),
        fed_status: await this.getFedStatuses(),
        global_rank_lu: await this.getGlobalRanks(),
        institution: await this.getInstitutions(),
        iucn_lu: await this.getIucnCodes(),
        kingdoms: await this.getKingdoms(),
        name_category_desc_lu: await this.getNameCategoryDescriptions(),
        nativity_lu: await this.getNativities(),
        state_rank_lu: await this.getStateRanks(),
        st_status: await this.getStateStatuses(),
        ok_swap: await this.getSwaps(),
        resourcetype_lu: await this.getResourceTypes(),
        source: await this.getSources(),
        tfValues: this.buildTFValues(),
        identificationConfidences: this.buildIdentificationConfidences(),
        ns: this.buildDirectionsNS(),
        ew: this.buildDirectionsEW(),
        occurrenceSearchFields: this.buildOccurrenceSearchFields(),
        countyReportSearchFields: this.buildCountyReportSearchFields(),
        occurrenceReportSearchFields: this.buildOccurrenceReportSearchFields(),
      };
    }

    return this.relations;
  }
}

const relationsService = new RelationsService();

export default relationsService;
