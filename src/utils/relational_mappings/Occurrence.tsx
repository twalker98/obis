import { ObisDataTransform } from '../obis-prisma';

export const relationalFieldMappings: Array<ObisDataTransform> = [
  {
    field: 'basisofrecord',
    table: 'basisofrecord_lu',
    tableField: 'basisofrecord',
  },
  {
    field: 'county',
    table: 'county_occurrence_countyTocounty',
    tableField: 'county',
  },
  {
    field: 'institutioncode',
    table: 'institution',
    tableField: 'institutioncode',
  },
  {
    field: 'resourcetype',
    table: 'resourcetype_lu',
    tableField: 'resourcetype',
  },
  {
    field: 'datasetname',
    table: 'source',
    tableField: 'source',
  },
];
