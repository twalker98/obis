import { ObisDataTransform } from '../obis-prisma';

export const relationalFieldMappings: Array<ObisDataTransform> = [
  {
    field: 'iucncode',
    table: 'iucn_lu',
    tableField: 'code',
  },
  {
    field: 'g_rank',
    table: 'global_rank_lu',
    tableField: 'code',
  },
  {
    field: 's_rank',
    table: 'state_rank_lu',
    tableField: 'code',
  },
  {
    field: 'nativity',
    table: 'nativity_lu',
    tableField: 'nativity',
  },
  {
    field: 'fed_status_id',
    table: 'fed_status',
    tableField: 'status_id',
  },
  {
    field: 'st_status_id',
    table: 'st_status',
    tableField: 'status_id',
  },
  {
    field: 'swap_id',
    table: 'ok_swap',
    tableField: 'swap_id',
  },
];
