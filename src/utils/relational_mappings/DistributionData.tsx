import { ObisDataTransform } from '../obis-prisma';

export const relationalFieldMappings: Array<ObisDataTransform> = [
  {
    field: 'dist_confidence',
    table: 'd_dist_confidence',
    tableField: 'dist_confidence',
  },
  {
    field: 'origin',
    table: 'd_origin',
    tableField: 'origin',
  },
  {
    field: 'population',
    table: 'd_population',
    tableField: 'population',
  },
  {
    field: 'presence_absence',
    table: 'd_presence_absence',
    tableField: 'presence_absence',
  },
  {
    field: 'regularity',
    table: 'd_regularity',
    tableField: 'regularity',
  },
];
