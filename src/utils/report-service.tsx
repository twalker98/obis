import {
  CountyReportResult,
  OccurrenceReportResult,
} from '../types/report-result';
import {
  Prisma,
  getCountyReport,
  getOccurrenceReport,
} from './managers/obis/Occurrence';

type ReportType = 'county' | 'occurrence';

type RelationFieldMappings = {
  acctax: Array<string>;
  comtax: Array<string>;
  hightax: Array<string>;
};

export default class ReportService {
  private reportType: ReportType;
  private results: Array<CountyReportResult | OccurrenceReportResult>;
  private relationFieldMappings: RelationFieldMappings = {
    acctax: ['genus', 'species', 'subspecies', 'variety', 'forma'],
    comtax: ['vernacularname'],
    hightax: ['kingdom', 'phylum', 'taxclass', 'taxorder', 'family'],
  };

  constructor(reportType: ReportType) {
    this.reportType = reportType;
    this.results =
      reportType === 'county'
        ? new Array<CountyReportResult>()
        : new Array<OccurrenceReportResult>();
  }

  private buildCountyReportWhereClause(
    query: Map<string, string>,
  ): Prisma.occurrenceWhereInput {
    let whereClause: Prisma.occurrenceWhereInput = {};

    for (const entry of query.entries()) {
      const field = entry[0];
      const value = entry[1];

      if (field === 'county') {
        whereClause['county_occurrence_countyTocounty'] = {
          county: { equals: value },
        };
      } else {
        if (!whereClause['acctax']) {
          whereClause['acctax'] = {};
        }

        if (this.relationFieldMappings.acctax.includes(field)) {
          whereClause['acctax'][field] = {
            contains: value,
            mode: 'insensitive',
          };
        } else if (this.relationFieldMappings.hightax.includes(field)) {
          if (!whereClause['acctax']['hightax']) {
            whereClause['acctax']['hightax'] = {};
          }

          whereClause['acctax']['hightax'][field] =
            field === 'kingdom'
              ? { equals: value }
              : { contains: value, mode: 'insensitive' };
        }
      }
    }

    return whereClause;
  }

  private buildOccurrenceReportWhereClause(
    query: Map<string, string>,
  ): Prisma.occurrenceWhereInput {
    let whereClause: Prisma.occurrenceWhereInput = {};

    for (const entry of query.entries()) {
      const field = entry[0];
      const value = entry[1];

      if (field === 'county') {
        whereClause['county_occurrence_countyTocounty'] = {
          county: { equals: value },
        };
      } else {
        if (!whereClause['acctax']) {
          whereClause['acctax'] = {};
        }

        if (this.relationFieldMappings.acctax.includes(field)) {
          whereClause['acctax'][field] = {
            contains: value,
            mode: 'insensitive',
          };
        } else if (this.relationFieldMappings.comtax.includes(field)) {
          if (!whereClause['acctax']['comtax']) {
            whereClause['acctax']['comtax'] = {};
          }

          whereClause['acctax']['comtax'][field] = {
            contains: value,
            mode: 'insensitive',
          };
        } else if (this.relationFieldMappings.hightax.includes(field)) {
          if (!whereClause['acctax']['hightax']) {
            whereClause['acctax']['hightax'] = {};
          }

          whereClause['acctax']['hightax'][field] = {
            contains: value,
            mode: 'insensitive',
          };
        } else {
          whereClause[field] = {
            contains: value,
            mode: 'insensitive',
          };
        }
      }
    }

    return whereClause;
  }

  private async getCountyReportResults(query: Map<string, string>) {
    return await getCountyReport(this.buildCountyReportWhereClause(query));
  }

  private async getOccurrenceReportResults(query: Map<string, string>) {
    return await getOccurrenceReport(
      this.buildOccurrenceReportWhereClause(query),
    );
  }

  async generateReport(
    query: Map<string, string>,
  ): Promise<Array<CountyReportResult | OccurrenceReportResult>> {
    this.results =
      this.reportType === 'county'
        ? new Array<CountyReportResult>()
        : new Array<OccurrenceReportResult>();

    if (this.reportType === 'county') {
      this.results = await this.getCountyReportResults(query);
    } else if (this.reportType === 'occurrence') {
      this.results = await this.getOccurrenceReportResults(query);
    }

    return this.results;
  }
}
