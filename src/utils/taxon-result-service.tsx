import { GridColDef } from '@twalk-tech/react-lib/nextjs/components';
import { omit } from 'lodash';
import {
  TaxonHightaxTable,
  TaxonHightaxTableData,
  TaxonOccurrenceTableResult,
  TaxonResult,
} from '../types/taxon-result';
import { communities } from './communities';
import { commonNamesCompareFn, synonymsCompareFn } from './functions';
import { acctax, getAcctaxRecord } from './managers/obis/Acctax';
import { comtax, searchComtaxRecords } from './managers/obis/Comtax';
import { getHightaxRecord, hightax } from './managers/obis/Hightax';
import { getTaxonOccurrenceTable } from './managers/obis/Occurrence';
import { searchSyntaxRecords, syntax } from './managers/obis/Syntax';

export default class TaxonResultService {
  private acode: string;

  constructor(acode: string) {
    this.acode = acode;
  }

  private occurrencesCompareFn(
    a: TaxonOccurrenceTableResult,
    b: TaxonOccurrenceTableResult,
  ): number {
    return (a.county as string).localeCompare(b.county as string);
  }

  private async getAcctaxRecord(): Promise<acctax | null> {
    const acctaxRecord = await getAcctaxRecord(this.acode);

    if (!acctaxRecord) {
      return null;
    }

    acctaxRecord.g_rank = acctaxRecord.g_rank ? acctaxRecord.g_rank : 'GNR';
    acctaxRecord.s_rank = acctaxRecord.s_rank ? acctaxRecord.s_rank : 'SNR';

    if (!acctaxRecord.swap_id) {
      acctaxRecord.swap_id = -1;
    }

    if (!acctaxRecord.fed_status_id) {
      acctaxRecord.fed_status_id = -1;
    }

    if (!acctaxRecord.st_status_id) {
      acctaxRecord.st_status_id = -1;
    }

    return acctaxRecord;
  }

  private async getCommonNames(): Promise<Array<comtax>> {
    const commonNames = await searchComtaxRecords({
      acode: this.acode,
    });

    if (commonNames.length !== 0) {
      commonNames.sort(commonNamesCompareFn);

      // @ts-ignore
      commonNames[0].primary_name = true;
    }

    return commonNames;
  }

  private buildTaxonHightaxData(
    hightax: hightax,
    genus: string,
  ): TaxonHightaxTable {
    const headerNames = {
      kingdom: 'Kingdom',
      phylum: 'Phylum',
      taxclass: 'Class',
      taxorder: 'Order',
      family: 'Family',
    };

    const taxonHightax = omit(
      hightax,
      'category',
      'name_type_desc',
      'name_category_desc',
    );

    const columns: Array<GridColDef> = Object.keys(taxonHightax)
      .map((field: string) => {
        return {
          field: field,
          headerName: headerNames[field],
          width: 175,
          hideable: false,
        };
      })
      .concat([
        {
          field: 'genus',
          headerName: 'Genus',
          width: 175,
          hideable: false,
        },
      ]);

    const data: Array<TaxonHightaxTableData> = [
      {
        ...taxonHightax,
        genus: genus,
      },
    ];

    return {
      columns: columns,
      data: data,
    };
  }

  private async getHightaxData(family: string): Promise<hightax> {
    return await getHightaxRecord(family);
  }

  private async getSynonyms(): Promise<Array<syntax>> {
    const synonyms = await searchSyntaxRecords({
      acode: this.acode,
    });

    if (synonyms.length !== 0) {
      synonyms.sort(synonymsCompareFn);
    }

    return synonyms;
  }

  private processOccurrences(occurrences: Array<TaxonOccurrenceTableResult>) {
    for (let occurrence of occurrences) {
      if (occurrence.county === null) {
        occurrence.county = 'Unknown';
      }

      occurrence._min.eventdate =
        occurrence._min.eventdate !== null &&
        typeof occurrence._min.eventdate === 'object'
          ? occurrence._min.eventdate.toISOString()
          : 'No date found';
      occurrence._max.eventdate =
        occurrence._max.eventdate !== null &&
        typeof occurrence._max.eventdate === 'object'
          ? occurrence._max.eventdate.toISOString()
          : 'No date found';
      occurrence.viewLink = 'View Occurrences ->';
    }
  }

  private async getOccurrences(
    authenticated: boolean,
  ): Promise<Array<TaxonOccurrenceTableResult>> {
    const occurrences = await getTaxonOccurrenceTable(
      this.acode,
      authenticated,
    );

    // Ensure date objects are not returned, only ISO strings
    this.processOccurrences(occurrences);
    occurrences.sort(this.occurrencesCompareFn);

    return occurrences;
  }

  async getTaxonData(authenticated: boolean): Promise<TaxonResult | null> {
    const taxon = await this.getAcctaxRecord();

    if (!taxon) {
      return null;
    }

    // If the record's acode is equal to its elementcode, it is a classification. If it is a classification,
    // or a community, it won't have common names/synonyms, hightax data, or occurrences.
    const community =
      communities.includes(taxon.family as string) ||
      taxon.acode === taxon.elcode;

    const result: TaxonResult = {
      taxon: taxon,
      community: community,
      commonNames: !community ? await this.getCommonNames() : null,
      synonyms: !community ? await this.getSynonyms() : null,
      hightax: !community
        ? this.buildTaxonHightaxData(
            await this.getHightaxData(taxon.family as string),
            taxon.genus!,
          )
        : null,
      occurrences: !community ? await this.getOccurrences(authenticated) : null,
    };

    return result;
  }
}
