export const communities: Array<string> = [
  'Terrestrial Community',
  'National Vegetation Classification',
  'Subterranean Community',
  'Freshwater Community',
  'Animal Assemblage',
];
