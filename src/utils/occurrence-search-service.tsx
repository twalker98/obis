import { OccurrenceSearchResult } from '../types/occurrence-search-result';
import { Prisma } from '../utils/obis-prisma';
import { searchAcctaxRecords } from './managers/obis/Acctax';
import { searchComtaxRecords } from './managers/obis/Comtax';
import {
  occurrence,
  searchOccurrenceRecords,
} from './managers/obis/Occurrence';

export default class OccurrenceSearchService {
  private results: Map<string, Array<OccurrenceSearchResult>>;
  private groupNames: Array<string>;

  constructor(groupNames: Array<string>) {
    this.results = new Map<string, Array<OccurrenceSearchResult>>();
    this.groupNames = groupNames;
  }

  private async getSname(acode: string): Promise<string> {
    const acctax = await searchAcctaxRecords({ acode: acode });
    return acctax[0].sname!;
  }

  private async getCommonNameAcodes(
    vernacularname: string,
  ): Promise<Array<string>> {
    const acodes: Array<string> = [];

    const commonNames = await searchComtaxRecords({
      vernacularname: {
        contains: vernacularname,
        mode: 'insensitive',
      },
    });

    for (const commonName of commonNames) {
      acodes.push(commonName.acode!);
    }

    return acodes;
  }

  private async buildOccurrenceSearchWhereClause(
    query: Map<string, string>,
  ): Promise<Prisma.occurrenceWhereInput> {
    let whereClause: Prisma.occurrenceWhereInput = {};

    for (const entry of query.entries()) {
      const field = entry[0];
      const value = entry[1];

      if (field === 'gid') {
        whereClause['gid'] = {
          equals: parseInt(value),
        };
      } else if (field === 'county') {
        whereClause['county_occurrence_countyTocounty'] = {
          county: { equals: value },
        };
      } else if (
        field === 'acode' ||
        field === 'sname' ||
        field === 'family' ||
        field === 'genus' ||
        field === 'ssp' ||
        field === 'var'
      ) {
        if (!whereClause['acctax']) {
          whereClause['acctax'] = {};
        }

        if (field === 'family') {
          whereClause['acctax']['hightax'] = {
            family: {
              contains: value,
              mode: 'insensitive',
            },
          };
        } else if (field === 'acode') {
          whereClause['acctax']['acode'] = {
            equals: value,
          };
        } else {
          whereClause['acctax'][field] = {
            contains: value,
            mode: 'insensitive',
          };
        }
      } else if (field === 'vname') {
        const acodes = await this.getCommonNameAcodes(value);

        if (!whereClause['acctax']) {
          whereClause['acctax'] = {};
        }

        whereClause['acctax']['acode'] = {
          in: acodes,
        };
      } else if (field === 'collector') {
        whereClause['recordedby'] = {
          contains: value,
          mode: 'insensitive',
        };
      } else if (field === 'startDate' && value !== '{{startDate}}') {
        if (!whereClause['eventdate']) {
          whereClause['eventdate'] = {};
        }

        whereClause['eventdate']['gte'] = value;
      } else if (field === 'endDate' && value !== '{{endDate}}') {
        if (!whereClause['eventdate']) {
          whereClause['eventdate'] = {};
        }

        whereClause['eventdate']['lte'] = value;
      } else if (field === 'catalognumber') {
        whereClause['catalognumber'] = {
          equals: value,
        };
      }
    }

    if (this.groupNames.length !== 0) {
      whereClause['institutioncode'] = {
        in: this.groupNames,
      };
    }

    return whereClause as Prisma.occurrenceWhereInput;
  }

  private async getOccurrenceResults(query: Map<string, string>) {
    return await searchOccurrenceRecords(
      await this.buildOccurrenceSearchWhereClause(query),
    );
  }

  private getDate(date: Date): string {
    const year = date.getUTCFullYear().toString();
    let month = (date.getUTCMonth() + 1).toString();
    let day = date.getUTCDate().toString();

    if (month.length === 1) {
      month = `0${month}`;
    }

    if (day.length === 1) {
      day = `0${day}`;
    }

    return `${year}-${month}-${day}`;
  }

  private async parseOccurrenceResults(
    occurrences: Array<occurrence>,
  ): Promise<Map<string, Array<OccurrenceSearchResult>>> {
    const results = new Map<string, Array<OccurrenceSearchResult>>();

    for (const occurrence of occurrences) {
      const acode = occurrence.acode!;
      const sname = await this.getSname(acode);

      const result: OccurrenceSearchResult = {
        gid: occurrence.gid,
        acode: acode,
        county: occurrence.county,
        recordedby: occurrence.recordedby,
        eventdate: occurrence.eventdate
          ? this.getDate(occurrence.eventdate)
          : undefined,
        locality: occurrence.locality,
        editLink: 'Edit Occurrence ->',
      };

      if (!results.has(sname)) {
        results.set(sname, [result]); // Initialize an array for the sname
      } else {
        results.get(sname)!.push(result); // Add the item to the existing array
      }
    }

    return results;
  }

  async search(
    query: Map<string, string>,
  ): Promise<Map<string, Array<OccurrenceSearchResult>>> {
    this.results = new Map<string, Array<OccurrenceSearchResult>>();

    this.results = await this.parseOccurrenceResults(
      await this.getOccurrenceResults(query),
    );

    return this.results;
  }
}
