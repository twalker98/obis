import { PrismaClient as ObisAuthPrismaClient } from '../prisma/generated/obis-auth-client';
import type {
  Group,
  Permission,
  Prisma,
  User,
  UserGroup,
  UserPermission,
} from '../prisma/generated/obis-auth-client';

type ObisAuth = {
  username: string;
  password: string;
};

type ObisUser = User & {
  username: string;
  groups: Array<UserGroup>;
  permissions: Array<UserPermission>;
};

let obisAuthPrisma: ObisAuthPrismaClient;

const globalForObisAuthPrisma = global as unknown as {
  obisAuthPrisma: ObisAuthPrismaClient;
};

if (process.env.NODE_ENV === 'production') {
  obisAuthPrisma = new ObisAuthPrismaClient();
} else {
  if (!globalForObisAuthPrisma.obisAuthPrisma) {
    globalForObisAuthPrisma.obisAuthPrisma = new ObisAuthPrismaClient();
  }

  obisAuthPrisma = globalForObisAuthPrisma.obisAuthPrisma;
}

export default obisAuthPrisma;
export type {
  Group,
  ObisAuth,
  ObisUser,
  Permission,
  Prisma,
  User,
  UserGroup,
  UserPermission,
};
