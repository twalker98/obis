import { TaxaSearchResult } from '../types/taxa-search-result';
import { communities } from './communities';
import { acctax, searchAcctaxRecords } from './managers/obis/Acctax';
import { searchComtaxRecords } from './managers/obis/Comtax';
import { searchSyntaxRecords } from './managers/obis/Syntax';

export default class TaxaSearchService {
  private results: Map<string, Array<TaxaSearchResult>>;

  constructor() {
    this.results = new Map<string, Array<TaxaSearchResult>>();
  }

  private resultsCompareFn(
    a: [string, TaxaSearchResult[]],
    b: [string, TaxaSearchResult[]],
  ): number {
    let pos: number = 0;

    if (
      (a[1][0].community && !b[1][0].community) ||
      (a[1][0].type === 'comtax' && b[1][0].type !== 'comtax')
    ) {
      pos = 1;
    } else if (
      (!a[1][0].community && b[1][0].community) ||
      (a[1][0].type !== 'comtax' && b[1][0].type === 'comtax')
    ) {
      pos = -1;
    } else {
      pos = a[1][0].displayName.localeCompare(b[1][0].displayName);
    }

    return pos;
  }

  private async getAcctaxResults(query: string, acode: boolean = false) {
    let taxa: Array<acctax>;

    if (acode) {
      taxa = await searchAcctaxRecords({
        acode: query,
      });
    } else {
      taxa = await searchAcctaxRecords({
        sname: {
          contains: query,
          mode: 'insensitive',
        },
      });
    }

    if (taxa.length === 0) {
      return null;
    }

    for (const taxon of taxa) {
      const acode: string = taxon.acode;
      const records: Array<TaxaSearchResult> = new Array<TaxaSearchResult>();

      const result: TaxaSearchResult = {
        acode: acode,
        displayName: taxon.sname ? taxon.sname : '',
        genus: taxon.genus,
        species: taxon.species,
        subspecies: taxon.subspecies,
        variety: taxon.variety,
        family: taxon.family,
        type: 'acctax',
        primaryName: false,
        community: communities.includes(taxon.family as string),
      };

      records.push(result);
      this.results.set(acode, records);
    }
  }

  private async getComtaxResults(query: string) {
    const commonNames = await searchComtaxRecords({
      vernacularname: {
        contains: query,
        mode: 'insensitive',
      },
    });

    if (commonNames.length === 0) {
      return null;
    }

    for (const commonName of commonNames) {
      const acode: string = commonName.acode as string;

      if (!this.results.has(acode)) {
        await this.getAcctaxResults(acode, true);
      }

      let records: Array<TaxaSearchResult> =
        this.results.get(acode) || new Array<TaxaSearchResult>();

      const result: TaxaSearchResult = {
        acode: acode,
        displayName: commonName.vernacularname as string,
        genus: null,
        species: null,
        subspecies: null,
        variety: null,
        family: null,
        type: 'comtax',
        primaryName: commonName.primary_name ? true : false,
        community: false,
      };

      records.push(result);
    }
  }

  private async getSyntaxResults(query: string) {
    const synonyms = await searchSyntaxRecords({
      sname: {
        contains: query,
        mode: 'insensitive',
      },
    });

    if (synonyms.length === 0) {
      return null;
    }

    for (const synonym of synonyms) {
      const acode: string = synonym.acode as string;

      if (!this.results.has(acode)) {
        await this.getAcctaxResults(acode, true);
      }

      let records: Array<TaxaSearchResult> =
        this.results.get(acode) || new Array<TaxaSearchResult>();

      const result: TaxaSearchResult = {
        acode: acode,
        displayName: synonym.sname as string,
        genus: synonym.genus,
        species: synonym.species,
        subspecies: synonym.subspecies,
        variety: synonym.variety,
        family: null,
        type: 'syntax',
        primaryName: false,
        community: communities.includes(synonym.family as string),
      };

      records.push(result);
    }
  }

  private sortResults() {
    return new Promise<void>((resolve) => {
      this.results = new Map([...this.results].sort(this.resultsCompareFn));
      resolve();
    });
  }

  async search(
    query: string,
  ): Promise<Map<string, Array<TaxaSearchResult>> | null> {
    this.results = new Map<string, Array<TaxaSearchResult>>();

    const acctaxResponse = await this.getAcctaxResults(query);
    const comtaxResponse = await this.getComtaxResults(query);
    const syntaxResponse = await this.getSyntaxResults(query);

    if (
      acctaxResponse === null &&
      comtaxResponse === null &&
      syntaxResponse === null
    ) {
      return null;
    }

    await this.sortResults();

    return this.results;
  }
}
