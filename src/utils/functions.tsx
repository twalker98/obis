import sha256 from 'crypto-js/sha256';

import { GenericFormItem } from '@twalk-tech/react-lib/nextjs';
import { User } from 'next-auth';
import { Relations } from '../types/relations';
import { comtax } from './managers/obis/Comtax';
import { syntax } from './managers/obis/Syntax';
import { ObisUser, Permission } from './obis-auth-prisma';
import { ObisDataTransform } from './obis-prisma';

const NULL_VALUES = [-1, '', 'None'];

export function hashPassword(password: string) {
  return sha256(password).toString();
}

export function isAdmin(user: User, permissions: Array<Permission>) {
  const userPermissions = (user as ObisUser).permissions;
  const adminPermission = permissions.find(
    (permission) => permission.permission_name === 'admin',
  );

  if (adminPermission && userPermissions) {
    return userPermissions
      .map((userPermission) => userPermission.permissionId)
      .includes(adminPermission.id);
  }

  return false;
}

export function userHasPermission(
  user: User,
  permissions: Array<Permission>,
  requiredPermissions: Array<string>,
): boolean {
  const userPermissions = (user as ObisUser).permissions;
  let permitted = false;

  if (userPermissions) {
    const requiredPermissionIds = permissions
      .filter((permission) =>
        requiredPermissions.includes(permission.permission_name),
      )
      .map((permission) => permission.id);

    for (const requiredPermissionId of requiredPermissionIds) {
      if (
        userPermissions.find(
          (userPermission) =>
            userPermission.permissionId === requiredPermissionId,
        )
      ) {
        permitted = true;
      }
    }
  }

  // Admins are allowed to do anything
  permitted = permitted || isAdmin(user, permissions);

  return permitted;
}

export function capitalizeFirstLetter(string: string): string {
  return string.charAt(0).toUpperCase() + string.substring(1);
}

export function generateQueryParams(values: any): string {
  const keys = Object.keys(values);
  const queryParams = new Array<string>();

  for (const key of keys) {
    const value = values[key];

    if (value === null || value === undefined || value === '') {
      continue;
    } else {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(value);

      queryParams.push(`${encodedKey}=${encodedValue}`);
    }
  }

  return queryParams.join('&');
}

export function parseQueryParams(queryParams: string): Map<string, string> {
  const params = new Map<string, string>();

  const paramPairs = queryParams.split('&');

  for (const pair of paramPairs) {
    const [key, value] = pair.split('=');

    const decodedKey = decodeURIComponent(key);
    const decodedValue = decodeURIComponent(value || '');

    params.set(decodedKey, decodedValue);
  }

  return params;
}

// Copied from: https://stackoverflow.com/a/55251598
export function flattenObject<T extends Object>(obj: T): T {
  const flattened = {};

  Object.keys(obj).forEach((key) => {
    const value = obj[key];

    if (typeof value === 'object' && value !== null && !Array.isArray(value)) {
      Object.assign(flattened, flattenObject(value));
    } else {
      flattened[key] = value;
    }
  });

  return flattened as T;
}

export function updateFormFieldValues<T extends Object>(
  data: T,
  fields: Array<GenericFormItem>,
  fieldParamKey: string,
): void {
  Object.keys(data).forEach((dataKey: string) => {
    const field = fields.find(
      (field: GenericFormItem) => field.fcName === dataKey,
    );

    if (field) {
      field[fieldParamKey] = data[dataKey];
    }
  });
}

export function remapRelationalFieldsForSubmission(
  fieldMappings: Array<ObisDataTransform>,
  data: any,
): void {
  for (const mapping of fieldMappings) {
    const value = NULL_VALUES.includes(
      data[mapping['field']][mapping['tableField']],
    )
      ? null
      : data[mapping['field']][mapping['tableField']];

    delete data[mapping['field']];

    data[mapping['table']] = value
      ? {
          connect: {
            [mapping['tableField']]: value,
          },
        }
      : undefined;
  }
}

export function remapRelationalFieldsForDisplay(
  fieldMappings: Array<ObisDataTransform>,
  data: any,
  relations: Relations,
): void {
  for (const mapping of fieldMappings) {
    const value = data[mapping['field']];

    if (value && typeof value === 'object') {
      continue;
    }

    const relationValues =
      mapping['field'] === 'county'
        ? relations['county']
        : (relations[mapping['table']] as Array<any>);

    delete data[mapping['field']];

    data[mapping['field']] = value
      ? relationValues.find(
          (relation) => relation[mapping['tableField']] === value,
        )
      : relationValues.find((relation) =>
          NULL_VALUES.includes(relation[mapping['tableField']]),
        );
  }
}

export function convertEmptyStringsToNull(data: any): void {
  for (const field of Object.keys(data)) {
    if (data[field] === '') {
      data[field] = null;
    }
  }
}

export const cacheValue = (
  asyncValidate: (val: string, ...args: any) => Promise<string | object | null>,
) => {
  return async (value: string, cachedValue?: string, ...args: any) => {
    if (value !== cachedValue) {
      const response = await asyncValidate(value, args);
      return response;
    }
  };
};

export const sort = (values: Array<any>, key: string): Array<any> => {
  return values.sort((a, b) =>
    a[key] < b[key] ? -1 : a[key] > b[key] ? 1 : 0,
  );
};

export const commonNamesCompareFn = (a: comtax, b: comtax): number => {
  if (a.primary_name) {
    return -1;
  } else if (a.vernacularname && b.vernacularname) {
    return a.vernacularname.localeCompare(b.vernacularname);
  } else {
    return 0;
  }
};

export const synonymsCompareFn = (a: syntax, b: syntax): number => {
  if (a.sname && b.sname) {
    return a.sname.localeCompare(b.sname);
  } else {
    return 0;
  }
};
