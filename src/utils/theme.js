import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  components: {
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          fontSize: '1em',
        },
      },
    },
  },
  palette: {
    type: 'light',
    primary: {
      main: '#548235',
    },
    secondary: {
      main: '#72b446',
    },
  },
});
