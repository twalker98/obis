import { TaxaSearchResult } from '../types/taxa-search-result';
import { acctax, syntax } from './obis-prisma';

type ResultNameProps = {
  result: acctax | syntax | TaxaSearchResult;
};

export function ResultName(props: ResultNameProps): JSX.Element {
  let result = props.result;
  const isTaxaSearchResult = (result as TaxaSearchResult).type !== undefined;
  let name =
    result.genus === null || result.species === null ? (
      <span>{(result as acctax).sname}</span>
    ) : (
      <span style={{ fontStyle: 'italic' }}>
        {result.genus?.trim()} {result.species?.trim()}
      </span>
    );

  if (isTaxaSearchResult) {
    result = result as TaxaSearchResult;
    name =
      result.type === 'comtax' ||
      result.community ||
      result.genus === null ||
      result.species === null ? (
        <>{result.displayName.trim()}</>
      ) : (
        name
      );
  }

  if (result.subspecies) {
    name = (
      <>
        {name} ssp.{' '}
        <span style={{ display: 'inline', fontStyle: 'italic' }}>
          {result.subspecies.trim()}
        </span>
      </>
    );
  }

  if (result.variety) {
    name = (
      <>
        {name} var.{' '}
        <span style={{ display: 'inline', fontStyle: 'italic' }}>
          {result.variety.trim()}
        </span>
      </>
    );
  }

  return name;
}
