import { getServerSession } from 'next-auth';
import { authOptions } from '../app/api/auth/[...nextauth]/route';

const config = authOptions;

export function getSession() {
  return getServerSession(config);
}
