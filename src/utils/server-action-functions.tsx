import { H } from '@highlight-run/next/client';
import { capitalizeFirstLetter } from './functions';
import { SnackbarProps } from './hooks/snackbar';
import {
  Group,
  Permission,
  User,
  UserGroup,
  UserPermission,
} from './obis-auth-prisma';
import {
  ObisID,
  acctax,
  comtax,
  distribution_data,
  hightax,
  identification_verification,
  occurrence,
  syntax,
  vw_all_taxa,
} from './obis-prisma';

type PrismaCallStatus = Exclude<SnackbarProps['severity'], 'info' | undefined>;
export type PrismaCallAction = 'create' | 'delete' | 'edit' | 'get';
type PrismaCallType =
  | Group
  | Permission
  | User
  | UserGroup
  | UserPermission
  | acctax
  | comtax
  | distribution_data
  | hightax
  | identification_verification
  | occurrence
  | syntax
  | vw_all_taxa;
type PrismaCallTypeName =
  | 'class'
  | 'common name'
  | 'distribution data'
  | 'family'
  | 'group'
  | 'occurrence'
  | 'order'
  | 'permission'
  | 'phylum'
  | 'previous identification'
  | 'synonym'
  | 'taxon'
  | 'user'
  | 'user group'
  | 'user permission';

const getPrismaCallSnackbarProps = (
  status: PrismaCallStatus,
  action: PrismaCallAction,
  type: PrismaCallTypeName,
  customMessage?: string,
  name?: string,
  serverError: boolean = false,
): SnackbarProps => {
  if (status === 'success' && customMessage) {
    throw TypeError(
      'Cannot specify a custom message for a successful function call!',
    );
  }

  if (status === 'success' && serverError) {
    throw TypeError(
      'A server error is not possible when a function call is successful!',
    );
  }

  const statusPrefixes: { [key in PrismaCallStatus]: string } = {
    success: 'Successfully',
    error: 'Error',
  };

  const pastTenseActions: { [key in PrismaCallAction]: string } = {
    create: status === 'success' ? 'created' : 'creating',
    delete: status === 'success' ? 'deleted' : 'deleting',
    edit: status === 'success' ? 'edited' : 'editing',
    get: status === 'success' ? 'got' : 'getting',
  };

  let message = customMessage
    ? customMessage
    : !serverError
    ? `${statusPrefixes[status]} ${pastTenseActions[action]} ${type}`
    : `Server error occurred when ${pastTenseActions[action]} ${type}`;

  if (!customMessage && name) {
    message.concat(' ', `"${name}"`);
  }

  return {
    message: message,
    severity: status,
  };
};

export type ProcessedPrismaCall<T> = {
  prismaCallResponse: T | null;
  snackbarProps: SnackbarProps;
};

type PrismaFn<T extends PrismaCallType | Array<PrismaCallType>, W> =
  | ((whereInput?: W) => Promise<T>)
  | (() => Promise<T>);

export const processPrismaGetCall = async <
  T extends PrismaCallType | Array<PrismaCallType>,
  W = undefined,
>(
  fn: PrismaFn<T, W>,
  type: PrismaCallTypeName,
  whereInput?: W,
): Promise<ProcessedPrismaCall<T>> => {
  let processedRecord: T;

  try {
    if (!whereInput) {
      processedRecord = await fn();
    } else {
      processedRecord = await fn(whereInput);
    }
  } catch (e) {
    console.error(e);

    if (!e.meta) {
      H.consumeError(e as Error, 'Error getting data using Prisma.', {
        call: `get ${type}`,
      });
    }

    return {
      prismaCallResponse: null,
      snackbarProps: getPrismaCallSnackbarProps(
        'error',
        'get',
        type,
        undefined,
        undefined,
        e.meta,
      ),
    };
  }

  if (!processedRecord) {
    return {
      prismaCallResponse: null,
      snackbarProps: getPrismaCallSnackbarProps(
        'error',
        'get',
        type,
        `${capitalizeFirstLetter(type)} doesn't exist`,
      ),
    };
  }

  return {
    prismaCallResponse: processedRecord,
    snackbarProps: getPrismaCallSnackbarProps('success', 'get', type),
  };
};

export const processPrismaPostCall = async <T extends PrismaCallType,>(
  fn: (values: T, key?: ObisID) => Promise<T>,
  fnValues: T,
  action: PrismaCallAction,
  type: PrismaCallTypeName,
  name?: string,
  fnKey?: ObisID,
): Promise<ProcessedPrismaCall<T>> => {
  let processedRecord: T;

  try {
    if (fnKey) {
      processedRecord = await fn(fnValues, fnKey);
    } else {
      processedRecord = await fn(fnValues);
    }
  } catch (e) {
    console.error(e);

    if (!e.meta) {
      H.consumeError(
        e as Error,
        'Error adding or updating data using Prisma.',
        { call: `${action} ${type}` },
      );
    }
    return {
      prismaCallResponse: null,
      snackbarProps: getPrismaCallSnackbarProps(
        'error',
        action,
        type,
        e.meta ? e.meta.cause : undefined,
        name,
        e.meta,
      ),
    };
  }

  return {
    prismaCallResponse: processedRecord,
    snackbarProps: getPrismaCallSnackbarProps(
      'success',
      action,
      type,
      undefined,
      name,
    ),
  };
};

export const processUserAccessTypeDeleteCall = async <
  T extends UserGroup | UserPermission,
>(
  fn: (userId: string, accessTypeId: string) => Promise<T>,
  userId: string,
  accessTypeId: string,
  type: 'user group' | 'user permission',
  name: string,
): Promise<ProcessedPrismaCall<T>> => {
  let processedRecord: T;

  try {
    processedRecord = await fn(userId, accessTypeId);
  } catch (e) {
    console.error(e);

    if (!e.meta) {
      H.consumeError(e as Error, `Error deleting ${type} ${name}.`, {
        call: `delete ${type} ${name}`,
      });
    }

    return {
      prismaCallResponse: null,
      snackbarProps: getPrismaCallSnackbarProps(
        'error',
        'delete',
        type,
        undefined,
        name,
        e.meta,
      ),
    };
  }

  return {
    prismaCallResponse: processedRecord,
    snackbarProps: getPrismaCallSnackbarProps(
      'success',
      'delete',
      type,
      undefined,
      name,
    ),
  };
};
