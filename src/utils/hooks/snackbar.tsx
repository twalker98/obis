import {
  GenericSnackbar,
  GenericSnackbarProps,
} from '@twalk-tech/react-lib/nextjs/components';
import { useCallback, useEffect, useRef, useState } from 'react';

export type Severity = GenericSnackbarProps['severity'];

export type OpenSnackbarFn = (
  message: string,
  severity?: Severity,
  duration?: number,
) => void;

export type SnackbarProps = {
  message: string;
  severity?: Severity;
  duration?: number;
};

export function useSnackbar() {
  const snackbarRef = useRef({ open: () => {} });
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState<Severity>('info');
  const [snackbarDuration, setSnackbarDuration] = useState(5000);
  const [showSnackbar, setShowSnackbar] = useState<boolean>(false);

  useEffect(() => {
    if (snackbarMessage && snackbarRef.current) {
      snackbarRef.current.open();
    }
  }, [snackbarMessage, showSnackbar]);

  const openSnackbar = useCallback(
    (message: string, severity?: Severity, duration?: number) => {
      setSnackbarMessage(message);
      setSnackbarSeverity(severity);
      setSnackbarDuration(duration || 5000);
      setShowSnackbar((prev) => !prev); // toggle showSnackbar to force useEffect to run
    },
    [],
  );

  const snackbar = (
    <GenericSnackbar
      ref={snackbarRef}
      message={snackbarMessage}
      severity={snackbarSeverity}
      duration={snackbarDuration}
    />
  );

  return { snackbar, openSnackbar };
}

export default useSnackbar;
