import { useState } from 'react';

export function usePagination(data: any) {
  const [currentPage, setCurrentPage] = useState(1);
  const ITEMS_PER_PAGE = 10;
  const maxPage = Math.ceil(data ? data.length / ITEMS_PER_PAGE : 1);

  function currentData() {
    const begin = (currentPage - 1) * ITEMS_PER_PAGE;
    const end = begin + ITEMS_PER_PAGE;
    return data.slice(begin, end);
  }

  function itemsPerPage(): number {
    return ITEMS_PER_PAGE;
  }

  function next() {
    setCurrentPage((currentPage) => Math.min(currentPage + 1, maxPage));
  }

  function prev() {
    setCurrentPage((currentPage) => Math.max(currentPage - 1, 1));
  }

  function jump(page: number) {
    const pageNumber = Math.max(1, page);
    setCurrentPage(Math.min(pageNumber, maxPage));
  }

  return { currentPage, maxPage, currentData, itemsPerPage, next, prev, jump };
}

export default usePagination;
