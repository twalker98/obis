import { Tooltips } from '../../types/tooltips';
import { syntax } from '../managers/obis/Syntax';

export const tooltips: Tooltips<syntax> = {
  acode: '',
  scode:
    'Unique code for the synonym For plants, use the TSN (if available). For animals, use the first letter of the taxonomic group to which it belogs (e.g. birds (B), fish (F), invertebrates (I), or mammals (M)), followed by a hypen, then the first two letters of the genus and species. For instance, Grus americana is B-GRAM and Ovis canadensis is M-OVCA. For a subspecies, such as Agkistrodon contortrix contortrix, also include the first letter of the subspecies, e.g. H-AGCOC. If an acode is already in use, such as F-CYCA, add a suquential number to the ending, e.g. F-CYCA2. Codes, except TSN, should be all upper case.',
  sname:
    'Scientific name without authority. Entered following standard scientific binomial and trinomial conventions.',
  scientificnameauthorship:
    'The scientific authority, e.g. L., Audubon 1827, or (Bellardi) Ehrend.',
  family:
    'Taxonomic family to which the taxon belongs, entered in proper case, e.g. Fagaceae or Apidae. The family must be in the higher taxonomy table. If it is not in that table, it must be added before a new taxon in that family can be added.',
  genus:
    'The taxonomic genus to which the taxon belongs, the values should be in proper case, e.g. Andropogon or Notropis.',
  species:
    'The specific taxonomic rank. The values should be in lowercase e.g. americanus or vulpes.',
  subspecies:
    'The sub-specific taxonomic rank. The values should be lowercase, e.g. sinuata or velatus.',
  variety:
    'The taxonomic variety (plants only). The values should be lowercase, e.g. hispida or hirsuta.',
  tsn: 'The Integrated Taxonomic Information System (ITIS) Taxonomic Serial Number.',
  scientificname:
    'Scientific name with authority. For instance, Vireo atricapilla Woodhouse 1852 or Rhus aromatica Aiton, entered following standard scientific binomial and trinomial conventions.',
  sspscientificnameauthorship:
    'The scientific authority for the subspecies (if different from the specific level), e.g. J. Presl or Greene',
  varscientificnameauthorship:
    'The scientific authority for the variety (if different from the specific level), e.g. (Walter) Barnhart or A. Gray',
  formascientificnameauthorship:
    'The scientific authority for the forma (if different from the specific level), e.g.(Warnock) Allred & Valdés-Reyna or (Scribn.) Fernald',
};
