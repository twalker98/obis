import { Tooltips } from '../../types/tooltips';
import { acctax } from '../managers/obis/Acctax';

export const tooltips: Tooltips<acctax> = {
  acode:
    'Unique code for the taxon. For plants, use the TSN (if available). For animals, use the first letter of the taxonomic group to which it belongs (e.g. birds (B), fish (F), invertebrates (I), or mammals (M)), followed by a hyphen, then the first two letters of the genus and species. For instance, Grus americana is B-GRAM and Ovis canadensis is M-OVCA. For a subspecies, such as Agkistrodon contortrix contortrix, also include the first letter of the subspecies, e.g. H-AGCOC. If an acode is already in use, such as F-CYCA, add a suquential number to the ending, e.g. F-CYCA2. Codes, except TSN, should be all upper case.',
  sname:
    'Scientific name without authority. For instance, Vireo atricapilla or Rhus aromatica, entered following standard scientific binomial and trinomial conventions.',
  scientificnameauthorship:
    'The scientific authority, e.g. L., Audubon 1827, or (Bellardi) Ehrend.',
  family:
    'Taxonomic family to which the taxon belongs, entered in proper case, e.g. Fagaceae or Apidae. The family must be in the higher taxonomy table. If it is not in that table, it must be added before a new taxon in that family can be added.',
  genus:
    'The taxonomic genus to which the taxon belongs, the values should be in proper case, e.g. Andropogon or Notropis.',
  species:
    'The specific taxonomic rank. The values should be in lowercase e.g. americanus or vulpes.',
  subspecies:
    'The sub-specific taxonomic rank. The values should be lowercase, e.g. sinuata or velatus.',
  variety:
    'The taxonomic variety (plants only). The values should be lowercase, e.g. hispida or hirsuta.',
  forma:
    'The taxonomic forma (plants only). The values should be lowercase, e.g. versicolori or brownii.',
  elcode: "NatureServe's element code. Unique for each taxon.",
  gelcode: "NatureServe's global element ID. Unique for each taxon.",
  iucncode:
    'International Union for Conservation of Nature (IUCN) Red List rank, allowable values available via a dropdown list.',
  g_rank: "NatureServe's Global Conservation Status rank.",
  s_rank:
    'Oklahoma Natural Heritage Inventory Subnational Conservation Status Definitions.',
  nativity:
    "Indication of taxon's origin, allowable values available via a dropdown list.",
  source: 'Taxonomic source reference, e.g. ITIS, 2020',
  usda_code: 'USDA plant code, when available. Plants only.',
  tsn: 'The Integrated Taxonomic Information System (ITIS) Taxonomic Serial Number.',
  fed_status_id:
    'Federal Endangered Species Act protection status, allowable values available via a dropdown list.',
  st_status_id:
    'State protection status, allowable values available via a dropdown list.',
  swap_id:
    'State Wildlife Action Plan tier, allowable values available via a dropdown list.',
  scientificname:
    'Scientific name with authority. For instance, Vireo atricapilla Woodhouse 1852 or Rhus aromatica Aiton, entered following standard scientific binomial and trinomial conventions.',
  sspscientificnameauthorship:
    'The scientific authority for the subspecies (if different from the specific level), e.g. J. Presl or Greene',
  varscientificnameauthorship:
    'The scientific authority for the variety (if different from the specific level), e.g. (Walter) Barnhart or A. Gray',
  formascientificnameauthorship:
    'The scientific authority for the forma (if different from the specific level), e.g.(Warnock) Allred & Valdés-Reyna or (Scribn.) Fernald',
  tracked:
    'Indication of whether a taxon is tracked, allowable values available via a dropdown list.',
  taxonremarks: 'Additional remarks about the taxon.',
};
