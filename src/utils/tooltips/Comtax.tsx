import { Tooltips } from '../../types/tooltips';
import { comtax } from '../managers/obis/Comtax';

export const tooltips: Tooltips<comtax> = {
  c_id: '',
  acode: '',
  vernacularname:
    'Common name for the taxon. If more than one, create multiple records. Follow standard case conventions when applicable, e.g. Bald Eagle or post oak.',
  primary_name:
    'An indication whether the common name used is the primary name, based on appropriate sources. Though a taxon can have many common names, only one should be marked as the primary name. Acceptable values are true or false.',
};
