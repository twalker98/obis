import { Tooltips } from '../../types/tooltips';
import { identification_verification } from '../managers/obis/IdentificationVerification';

export const tooltips: Tooltips<identification_verification> = {
  pkey: 'The pkey of the identification verification record.',
  gid: 'The gid of the identification verification record.',
  catalognumber: 'The catalog number field is not editable.',
  identifiedacode: 'The acode of the identification.',
  identifiedby:
    'Individual who confirmed the identification of/annotated the taxon.',
  identificationremarks:
    'Any comments regarding the identification/annotation.',
  datelastmodified:
    'The date of the identification/annotation. This is not a true date field due to the historical manner in which records were entered. Try to standardize entry.',
};
