import { Tooltips } from '../../types/tooltips';
import { occurrence } from '../managers/obis/Occurrence';

export const tooltips: Tooltips<occurrence> = {
  gid: 'The GID of the occurrence record.',
  acode:
    'Should be sname available via a dropdown list (and accessed via typing first couple of letters...)',
  eventdate: 'Date of occurrence, observation, collection, etc.',
  verbatimeventdate:
    'The verbatim original representation of the date and time information for an Event.',
  recordedby:
    'A list (concatenated and separated) of names of people, groups, or organizations responsible for recording the original occurrence.',
  county:
    'County of occurrence, allowable values available via a dropdown list.',
  locality:
    'Description of the place of the occurrence, e.g. Concrete tank 0.5 mi S of Baker Peak, Wichita Mountains Wildlife Refuge or 2 mi E & 0.5 mi S of Coalgate.',
  verbatimlocality: 'The original textual description of the place.',
  behavior:
    'A description of the behavior shown by the subject at the time the occurrence was recorded, e.g. roosting, foraging, or running.',
  habitat:
    'Category or description of the habitat in which the event occurred, e.g. Oak savanna.',
  sex: "Sex of the observed taxon(s). Allowable values: 'female', 'male', or a string in the format 'x male(s), y female(s)' (e.g. 2 males, 4 females; 1 male, 1 female; 1 female, 4 males)",
  lifestage:
    'The age class or life stage of the biological individual(s) at the time the occurrence was recorded, e.g. Adult, Juvenile, etc.',
  associatedtaxa:
    'Other taxa found with this observation. The proper way to separate is using a pipe (|). For instance: Post oak | Blackjack Oak.',
  elevation:
    'Elevation of occurrence, numeric field, values should be in meters, e,g, 220 m should be entered as 220.',
  depth: 'Depth of occurrence; use only for aquatic species.',
  depthaccuracy:
    'A numeric value indicating the accuracy of depth, e.g. +/- 6 m would be entered as 6.',
  individuals_max: 'Maximum number of individuals estimated.',
  individuals_min: 'Minimum number of individuals estimated.',
  occurrenceremarks:
    'Additional notes on the occurrence not captured in other columns.',
  taxonremarks:
    'Additional remarks about the taxon. For instance, if it is a questionable identification.',
  institutioncode:
    'ID of the institution of a collection record, allowable values available via a dropdown list.',
  basisofrecord:
    'The specific nature of the data record, allowable values available via a dropdown list.',
  catalognumber: 'Accession number or equivalent for collections.',
  othercatalognumbers:
    'Indication if the current record number is different from an older number used for the same record.',
  typestatus:
    'Indication whether the record refers to a type specimen (e.g., holotype, allotype, paratype).',
  recordnumber: 'Any field number associated with record.',
  samplingprotocol:
    'Name or description of the protocol used for the event. This field is not standardized.',
  preparations:
    'Describes how the specimen/voucher is preserved (e.g., herbarium sheet, in alcohol, pinned).',
  primary_data:
    'Verbatim specimen/voucher data or verbatim data from observer.',
  associatedreferences:
    'A list (concatenated and separated) of identifiers (publication, bibliographic reference, global unique identifier, URI) of literature associated with the occurrence.',
  datasetname:
    'The source of the data in our database, allowable values available via a dropdown list.',
  coordinateprecision:
    'Precision associated with coordinates. Estimated based on geocoding or GPS accuracy. Values should be entered in meters, e,g, 220 m should be entered as 220.',
  decimallatitude:
    'Latitude, in decimal degrees using WGS84. Values are always positive and should range from approximately 33.6 to 37.1.',
  decimallongitude:
    'Longitude in decimal degrees using WGS84. Values are always negative and should range from approximately -103.1 to -94.4.',
  georeferencedby:
    'Individual who collected the coordinates, preferably three letter initials e.g. TDF, though other naming formats, e.g. T. Fagin, are acceptable.',
  georeferenceddate:
    'Date of georeferencing. If based on GPS field collection, will be the same as eventdate.',
  georeferenceremarks: 'Any comments about the georeferencing.',
  georeferencesources:
    'Source of the coordinates (GPS, topo, Google Maps, etc.)',
  georeferenceverificationstatus: 'Whether the coordinates have been verified.',
  problem_with_record:
    'If there are any known issues with this particular record: e.g. taxonomic, biogeographic, etc.',
  identificationverificationstatus:
    'Indication of whether the identification has been verified (annotated).',
  identificationconfidence:
    'A rank of the confidence in the identifications; allowable values are Low, Medium, or High.',
  identificationremarks: 'Comments or notes about the identification.',
  associatedoccurrences:
    'Indicates if this record has any other records or documentation associated. For example, a plant may have an associated photo.',
  associatedsequences:
    'A list of genetic sequence information associated with the occurrence.',
  entby:
    'Individual who collected the coordinates, preferably three letter initials e.g. TDF, though other naming formats, e.g. T. Fagin, are acceptable.',
  mtr: 'Principle meridian of TRS. Should be 11 or 17.',
  township: 'Township number; values can range from 1 to 29.',
  ns: 'Township direction (N or S).',
  range: 'Range number; values can range from 1 to 28.',
  ew: 'Range direction (E or W).',
  section: 'Section number; values can range from 1 to 36.',
  quarter: 'Quarter section.',
  relationshipremarks:
    'Comments or notes about the relationship between the two resources. For instance, if a duplicate record is deleted, record the occurrenceid of the deleted record, the reason for deletion, and the dataset name of the deleted record. As general practice, museum or herbarium records should be maintained while other duplicates deleted.',
  resourcetype: 'The type of occurrence, such as an Event or PhysicalObject.',
  geodeticdatum: '',
  previousidentifications:
    'Initial identification (if is has been changed). Preferably ACODE or SCODE, but sname is acceptable.',
  datelastmodified: '',
  entrydate: '',
  obs_gid: '',
  zone: '',
  utme: '',
  utmn: '',
  hiderecord: '',
  hiderecordcomment: '',
  informationwitheld: '',
  awaitingreview: '',
  occurrenceid: '',
  individuals_estimated: '',
};
