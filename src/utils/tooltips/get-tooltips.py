import os
import re
import sys

REGEX = r'tooltip="(.+?(?="))" formControlName="(.+?(?="))'

def save_output(type: str, output: str):
  base_path = os.path.dirname(os.path.abspath(__file__))
  file_path = os.path.join(base_path, f'{type.capitalize()}.tsx')

  with open(file_path, 'w') as file:
    file.write(output)

def build_output(type: str, tooltip_map: dict) -> str:
  header = (
    'import { Tooltips } from \'../../types/tooltips\';\n'
    f'import {{ {type} }} from \'../managers/obis/{type.capitalize()}\';\n\n'
    f'export const tooltips: Tooltips<{type}> = {{'
  )

  content = ''

  for tooltip in tooltip_map:
    content += f'  {tooltip}: \'{tooltip_map[tooltip]}\',\n'

  content += '};'

  return '\n'.join([header, content])

def main(file_name: str, type: str):
  tooltip_map = {}

  with open(file_name, 'r') as file:
    file_contents = file.read()
    matches = re.findall(REGEX, file_contents)
    [tooltip_map.__setitem__(match[1], match[0]) for match in matches]

  save_output(type, build_output(type, tooltip_map))

if __name__ == '__main__':
  file_name = sys.argv[1]
  type = sys.argv[2]
  main(file_name, type)
