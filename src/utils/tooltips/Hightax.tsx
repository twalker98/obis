import { Tooltips } from '../../types/tooltips';
import { hightax } from '../managers/obis/Hightax';

export const tooltips: Tooltips<hightax> = {
  family: 'Taxonomic family, follows standard naming conventions.',
  taxorder: 'Taxonomic order, follows standard naming conventions.',
  taxclass: 'Taxonomic class, follows standard naming conventions.',
  phylum: 'Taxonomic phylum, follows standard naming conventions.',
  kingdom: 'Taxonomic kingdom, follows standard naming conventions.',
  category:
    'A vernacular taxonomic division, roughly equivalent to class or order (though with some flexibility, often to sub- or infraorder). Examples include Amphibians, Dragonfly, Bee, Bird, and Vascular Plant.',
  name_category_desc: '',
  name_type_desc: '',
};
