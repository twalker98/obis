/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  output: 'standalone',
  experimental: {
    serverActions: true,
  },

  async redirects() {
    return [
      {
        source: '/',
        destination: '/taxa/search/main',
        permanent: true,
      },
      {
        source: '/occurrence-report',
        destination: '/occurrence-report/search',
        permanent: true,
      },
      {
        source: '/occurrences',
        destination: '/occurrences/search/edit',
        permanent: true,
      },
      {
        source: '/occurrences/search',
        destination: '/occurrences/search/edit',
        permanent: true,
      },
      {
        source: '/taxa',
        destination: '/taxa/search/main',
        permanent: true,
      },
      {
        source: '/taxa/search',
        destination: '/taxa/search/main',
        permanent: true,
      },
    ];
  },
};

module.exports = nextConfig;
