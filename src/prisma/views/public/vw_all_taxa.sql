SELECT
  acctax.acode AS unique_code,
  acctax.sname,
  acctax.scientificname,
  'Accepted' :: text AS STATUS,
  acctax.acode AS accepted_code
FROM
  acctax
UNION
SELECT
  syntax.scode AS unique_code,
  syntax.sname,
  syntax.scientificname,
  'Synonym' :: text AS STATUS,
  syntax.acode AS accepted_code
FROM
  syntax;