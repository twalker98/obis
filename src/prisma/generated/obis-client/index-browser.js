
Object.defineProperty(exports, "__esModule", { value: true });

const {
  Decimal,
  objectEnumValues,
  makeStrictEnum,
  Public,
} = require('./runtime/index-browser')


const Prisma = {}

exports.Prisma = Prisma

/**
 * Prisma Client JS version: 4.16.2
 * Query Engine version: 4bc8b6e1b66cb932731fb1bdbbc550d1e010de81
 */
Prisma.prismaVersion = {
  client: "4.16.2",
  engine: "4bc8b6e1b66cb932731fb1bdbbc550d1e010de81"
}

Prisma.PrismaClientKnownRequestError = () => {
  throw new Error(`PrismaClientKnownRequestError is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)};
Prisma.PrismaClientUnknownRequestError = () => {
  throw new Error(`PrismaClientUnknownRequestError is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.PrismaClientRustPanicError = () => {
  throw new Error(`PrismaClientRustPanicError is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.PrismaClientInitializationError = () => {
  throw new Error(`PrismaClientInitializationError is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.PrismaClientValidationError = () => {
  throw new Error(`PrismaClientValidationError is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.NotFoundError = () => {
  throw new Error(`NotFoundError is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.Decimal = Decimal

/**
 * Re-export of sql-template-tag
 */
Prisma.sql = () => {
  throw new Error(`sqltag is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.empty = () => {
  throw new Error(`empty is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.join = () => {
  throw new Error(`join is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.raw = () => {
  throw new Error(`raw is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.validator = Public.validator

/**
* Extensions
*/
Prisma.getExtensionContext = () => {
  throw new Error(`Extensions.getExtensionContext is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.defineExtension = () => {
  throw new Error(`Extensions.defineExtension is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}

/**
 * Shorthand utilities for JSON filtering
 */
Prisma.DbNull = objectEnumValues.instances.DbNull
Prisma.JsonNull = objectEnumValues.instances.JsonNull
Prisma.AnyNull = objectEnumValues.instances.AnyNull

Prisma.NullTypes = {
  DbNull: objectEnumValues.classes.DbNull,
  JsonNull: objectEnumValues.classes.JsonNull,
  AnyNull: objectEnumValues.classes.AnyNull
}

/**
 * Enums
 */

exports.Prisma.TransactionIsolationLevel = makeStrictEnum({
  ReadUncommitted: 'ReadUncommitted',
  ReadCommitted: 'ReadCommitted',
  RepeatableRead: 'RepeatableRead',
  Serializable: 'Serializable'
});

exports.Prisma.AcctaxScalarFieldEnum = {
  acode: 'acode',
  sname: 'sname',
  scientificnameauthorship: 'scientificnameauthorship',
  family: 'family',
  genus: 'genus',
  species: 'species',
  subspecies: 'subspecies',
  variety: 'variety',
  forma: 'forma',
  elcode: 'elcode',
  gelcode: 'gelcode',
  iucncode: 'iucncode',
  g_rank: 'g_rank',
  s_rank: 's_rank',
  nativity: 'nativity',
  source: 'source',
  usda_code: 'usda_code',
  tsn: 'tsn',
  fed_status_id: 'fed_status_id',
  st_status_id: 'st_status_id',
  swap_id: 'swap_id',
  scientificname: 'scientificname',
  sspscientificnameauthorship: 'sspscientificnameauthorship',
  varscientificnameauthorship: 'varscientificnameauthorship',
  formascientificnameauthorship: 'formascientificnameauthorship',
  tracked: 'tracked',
  taxonremarks: 'taxonremarks'
};

exports.Prisma.Basisofrecord_luScalarFieldEnum = {
  id: 'id',
  basisofrecord: 'basisofrecord'
};

exports.Prisma.Category_luScalarFieldEnum = {
  a_id: 'a_id',
  category: 'category'
};

exports.Prisma.Co_trsScalarFieldEnum = {
  gid: 'gid',
  name: 'name',
  trs: 'trs'
};

exports.Prisma.ComtaxScalarFieldEnum = {
  c_id: 'c_id',
  acode: 'acode',
  vernacularname: 'vernacularname',
  primary_name: 'primary_name'
};

exports.Prisma.CountyScalarFieldEnum = {
  county: 'county',
  fips: 'fips',
  gid: 'gid'
};

exports.Prisma.D_dist_confidenceScalarFieldEnum = {
  d_dist_confidence_id: 'd_dist_confidence_id',
  dist_confidence: 'dist_confidence'
};

exports.Prisma.D_originScalarFieldEnum = {
  d_origin_id: 'd_origin_id',
  origin: 'origin'
};

exports.Prisma.D_populationScalarFieldEnum = {
  d_population_id: 'd_population_id',
  population: 'population'
};

exports.Prisma.D_presence_absenceScalarFieldEnum = {
  d_presence_absence_id: 'd_presence_absence_id',
  presence_absence: 'presence_absence'
};

exports.Prisma.D_regularityScalarFieldEnum = {
  d_regularity_id: 'd_regularity_id',
  regularity: 'regularity'
};

exports.Prisma.Distribution_dataScalarFieldEnum = {
  d_id: 'd_id',
  acode: 'acode',
  elcode: 'elcode',
  origin: 'origin',
  regularity: 'regularity',
  dist_confidence: 'dist_confidence',
  presence_absence: 'presence_absence',
  population: 'population',
  distribution_data_remarks: 'distribution_data_remarks'
};

exports.Prisma.Fed_statusScalarFieldEnum = {
  status_id: 'status_id',
  status: 'status',
  description: 'description'
};

exports.Prisma.Global_rank_luScalarFieldEnum = {
  code: 'code'
};

exports.Prisma.Hex5kmScalarFieldEnum = {
  gid: 'gid',
  id: 'id',
  grid_id: 'grid_id'
};

exports.Prisma.HightaxScalarFieldEnum = {
  kingdom: 'kingdom',
  phylum: 'phylum',
  taxclass: 'taxclass',
  taxorder: 'taxorder',
  family: 'family',
  category: 'category',
  name_type_desc: 'name_type_desc',
  name_category_desc: 'name_category_desc'
};

exports.Prisma.Identification_verificationScalarFieldEnum = {
  pkey: 'pkey',
  catalognumber: 'catalognumber',
  identifiedby: 'identifiedby',
  identificationremarks: 'identificationremarks',
  datelastmodified: 'datelastmodified',
  identifiedacode: 'identifiedacode',
  gid: 'gid'
};

exports.Prisma.InstitutionScalarFieldEnum = {
  institutioncode: 'institutioncode',
  institution: 'institution',
  curator: 'curator',
  email: 'email',
  telephone: 'telephone',
  address: 'address',
  city: 'city',
  state: 'state',
  country: 'country',
  zipcode: 'zipcode',
  institutiontype: 'institutiontype',
  link: 'link'
};

exports.Prisma.Iucn_luScalarFieldEnum = {
  code: 'code',
  description: 'description',
  id: 'id'
};

exports.Prisma.Kingdom_luScalarFieldEnum = {
  id: 'id',
  kingdom: 'kingdom'
};

exports.Prisma.Name_category_desc_luScalarFieldEnum = {
  a_id: 'a_id',
  name_category_desc: 'name_category_desc'
};

exports.Prisma.Name_type_desc_luScalarFieldEnum = {
  a_id: 'a_id',
  name_type_desc: 'name_type_desc'
};

exports.Prisma.Nativity_luScalarFieldEnum = {
  n_id: 'n_id',
  nativity: 'nativity'
};

exports.Prisma.OccurrenceScalarFieldEnum = {
  gid: 'gid',
  acode: 'acode',
  eventdate: 'eventdate',
  recordedby: 'recordedby',
  county: 'county',
  locality: 'locality',
  behavior: 'behavior',
  habitat: 'habitat',
  sex: 'sex',
  lifestage: 'lifestage',
  associatedtaxa: 'associatedtaxa',
  elevation: 'elevation',
  depth: 'depth',
  depthaccuracy: 'depthaccuracy',
  occurrenceremarks: 'occurrenceremarks',
  taxonremarks: 'taxonremarks',
  institutioncode: 'institutioncode',
  basisofrecord: 'basisofrecord',
  catalognumber: 'catalognumber',
  othercatalognumbers: 'othercatalognumbers',
  typestatus: 'typestatus',
  recordnumber: 'recordnumber',
  samplingprotocol: 'samplingprotocol',
  preparations: 'preparations',
  primary_data: 'primary_data',
  associatedreferences: 'associatedreferences',
  datasetname: 'datasetname',
  coordinateprecision: 'coordinateprecision',
  decimallatitude: 'decimallatitude',
  decimallongitude: 'decimallongitude',
  geodeticdatum: 'geodeticdatum',
  georeferencedby: 'georeferencedby',
  georeferenceddate: 'georeferenceddate',
  georeferenceremarks: 'georeferenceremarks',
  georeferencesources: 'georeferencesources',
  georeferenceverificationstatus: 'georeferenceverificationstatus',
  problem_with_record: 'problem_with_record',
  previousidentifications: 'previousidentifications',
  identificationverificationstatus: 'identificationverificationstatus',
  datelastmodified: 'datelastmodified',
  associatedoccurrences: 'associatedoccurrences',
  associatedsequences: 'associatedsequences',
  entby: 'entby',
  entrydate: 'entrydate',
  obs_gid: 'obs_gid',
  mtr: 'mtr',
  township: 'township',
  ns: 'ns',
  range: 'range',
  ew: 'ew',
  section: 'section',
  quarter: 'quarter',
  zone: 'zone',
  utme: 'utme',
  utmn: 'utmn',
  hiderecord: 'hiderecord',
  hiderecordcomment: 'hiderecordcomment',
  relationshipremarks: 'relationshipremarks',
  informationwitheld: 'informationwitheld',
  awaitingreview: 'awaitingreview',
  occurrenceid: 'occurrenceid',
  resourcetype: 'resourcetype',
  identificationconfidence: 'identificationconfidence',
  identificationremarks: 'identificationremarks',
  verbatimeventdate: 'verbatimeventdate',
  verbatimlocality: 'verbatimlocality',
  individuals_estimated: 'individuals_estimated',
  individuals_max: 'individuals_max',
  individuals_min: 'individuals_min'
};

exports.Prisma.Ok_swapScalarFieldEnum = {
  swap_id: 'swap_id',
  tier: 'tier',
  description: 'description'
};

exports.Prisma.OkcountiesScalarFieldEnum = {
  gid: 'gid',
  statefp: 'statefp',
  countyfp: 'countyfp',
  countyns: 'countyns',
  cntyidfp: 'cntyidfp',
  name: 'name',
  namelsad: 'namelsad',
  lsad: 'lsad',
  classfp: 'classfp',
  mtfcc: 'mtfcc',
  ur: 'ur',
  funcstat: 'funcstat'
};

exports.Prisma.Rank_changeScalarFieldEnum = {
  r_id: 'r_id',
  acode: 'acode',
  previous_s_rank: 'previous_s_rank',
  s_rank: 's_rank',
  changedby: 'changedby',
  rankremarks: 'rankremarks',
  datelastmodified: 'datelastmodified',
  previousdatemodified: 'previousdatemodified'
};

exports.Prisma.Resourcetype_luScalarFieldEnum = {
  id: 'id',
  resourcetype: 'resourcetype'
};

exports.Prisma.SourceScalarFieldEnum = {
  source: 'source',
  description: 'description'
};

exports.Prisma.Spatial_ref_sysScalarFieldEnum = {
  srid: 'srid',
  auth_name: 'auth_name',
  auth_srid: 'auth_srid',
  srtext: 'srtext',
  proj4text: 'proj4text'
};

exports.Prisma.St_statusScalarFieldEnum = {
  status_id: 'status_id',
  status: 'status',
  description: 'description'
};

exports.Prisma.State_rank_luScalarFieldEnum = {
  code: 'code'
};

exports.Prisma.SyntaxScalarFieldEnum = {
  acode: 'acode',
  scode: 'scode',
  sname: 'sname',
  scientificnameauthorship: 'scientificnameauthorship',
  family: 'family',
  genus: 'genus',
  species: 'species',
  subspecies: 'subspecies',
  variety: 'variety',
  scientificname: 'scientificname',
  sspscientificnameauthorship: 'sspscientificnameauthorship',
  varscientificnameauthorship: 'varscientificnameauthorship',
  formascientificnameauthorship: 'formascientificnameauthorship',
  tsn: 'tsn'
};

exports.Prisma.TncScalarFieldEnum = {
  gid: 'gid',
  objectid: 'objectid',
  dis: 'dis',
  shape_leng: 'shape_leng',
  shape_area: 'shape_area',
  acres: 'acres'
};

exports.Prisma.Vw_all_taxaScalarFieldEnum = {
  unique_code: 'unique_code',
  sname: 'sname',
  scientificname: 'scientificname',
  status: 'status',
  accepted_code: 'accepted_code'
};

exports.Prisma.SortOrder = {
  asc: 'asc',
  desc: 'desc'
};

exports.Prisma.QueryMode = {
  default: 'default',
  insensitive: 'insensitive'
};

exports.Prisma.NullsOrder = {
  first: 'first',
  last: 'last'
};


exports.Prisma.ModelName = {
  acctax: 'acctax',
  basisofrecord_lu: 'basisofrecord_lu',
  category_lu: 'category_lu',
  co_trs: 'co_trs',
  comtax: 'comtax',
  county: 'county',
  d_dist_confidence: 'd_dist_confidence',
  d_origin: 'd_origin',
  d_population: 'd_population',
  d_presence_absence: 'd_presence_absence',
  d_regularity: 'd_regularity',
  distribution_data: 'distribution_data',
  fed_status: 'fed_status',
  global_rank_lu: 'global_rank_lu',
  hex5km: 'hex5km',
  hightax: 'hightax',
  identification_verification: 'identification_verification',
  institution: 'institution',
  iucn_lu: 'iucn_lu',
  kingdom_lu: 'kingdom_lu',
  name_category_desc_lu: 'name_category_desc_lu',
  name_type_desc_lu: 'name_type_desc_lu',
  nativity_lu: 'nativity_lu',
  occurrence: 'occurrence',
  ok_swap: 'ok_swap',
  okcounties: 'okcounties',
  rank_change: 'rank_change',
  resourcetype_lu: 'resourcetype_lu',
  source: 'source',
  spatial_ref_sys: 'spatial_ref_sys',
  st_status: 'st_status',
  state_rank_lu: 'state_rank_lu',
  syntax: 'syntax',
  tnc: 'tnc',
  vw_all_taxa: 'vw_all_taxa'
};

/**
 * Create the Client
 */
class PrismaClient {
  constructor() {
    throw new Error(
      `PrismaClient is unable to be run in the browser.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
    )
  }
}
exports.PrismaClient = PrismaClient

Object.assign(exports, Prisma)
