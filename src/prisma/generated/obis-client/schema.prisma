generator client {
  provider        = "prisma-client-js"
  output          = "./generated/obis-client"
  previewFeatures = ["views"]
  binaryTargets   = ["native", "darwin-arm64", "linux-arm64-openssl-1.1.x"]
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model acctax {
  acode                         String              @id @db.VarChar
  sname                         String?             @db.VarChar
  scientificnameauthorship      String?             @db.VarChar
  family                        String?             @db.VarChar
  genus                         String?             @db.VarChar
  species                       String?             @db.VarChar
  subspecies                    String?             @db.VarChar
  variety                       String?             @db.VarChar
  forma                         String?             @db.VarChar
  elcode                        String?             @db.VarChar
  gelcode                       Int?
  iucncode                      String?             @db.VarChar
  g_rank                        String?             @db.VarChar
  s_rank                        String?             @db.VarChar
  nativity                      String?             @db.VarChar
  source                        String?             @db.VarChar
  usda_code                     String?             @db.VarChar
  tsn                           Int?
  fed_status_id                 Int?
  st_status_id                  Int?
  swap_id                       Int?
  scientificname                String?             @db.VarChar
  sspscientificnameauthorship   String?             @db.VarChar
  varscientificnameauthorship   String?             @db.VarChar
  formascientificnameauthorship String?             @db.VarChar
  tracked                       Boolean?
  taxonremarks                  String?             @db.VarChar(255)
  hightax                       hightax?            @relation(fields: [family], references: [family], onDelete: Cascade, map: "family_fk")
  fed_status                    fed_status?         @relation(fields: [fed_status_id], references: [status_id], onDelete: NoAction, onUpdate: NoAction, map: "fed_status_fk")
  global_rank_lu                global_rank_lu?     @relation(fields: [g_rank], references: [code], onDelete: NoAction, onUpdate: NoAction, map: "global_rank_fk")
  iucn_lu                       iucn_lu?            @relation(fields: [iucncode], references: [code], onDelete: NoAction, onUpdate: NoAction, map: "iucn_fk")
  nativity_lu                   nativity_lu?        @relation(fields: [nativity], references: [nativity], onDelete: Cascade, map: "nativity_fk")
  st_status                     st_status?          @relation(fields: [st_status_id], references: [status_id], onDelete: NoAction, onUpdate: NoAction, map: "st_status_fk")
  state_rank_lu                 state_rank_lu?      @relation(fields: [s_rank], references: [code], onDelete: NoAction, onUpdate: NoAction, map: "state_rank_fk")
  ok_swap                       ok_swap?            @relation(fields: [swap_id], references: [swap_id], onDelete: NoAction, onUpdate: NoAction, map: "swap_fk")
  comtax                        comtax[]
  distribution_data             distribution_data[]
  occurrence                    occurrence[]
  rank_change                   rank_change[]
  syntax                        syntax[]

  @@index([acode, sname], map: "acctax_idx")
}

model basisofrecord_lu {
  id            Int          @default(autoincrement())
  basisofrecord String       @id @db.VarChar(50)
  occurrence    occurrence[]
}

model category_lu {
  a_id     Int       @default(autoincrement())
  category String    @id @db.VarChar(30)
  hightax  hightax[]
}

model co_trs {
  gid  Int                      @id @default(autoincrement())
  name String?                  @db.VarChar(100)
  trs  String?                  @db.VarChar(30)
  geom Unsupported("geometry")?

  @@index([geom], map: "co_trs_geom_gist", type: Gist)
}

model comtax {
  c_id           Int     @id @default(autoincrement())
  acode          String? @db.VarChar
  vernacularname String? @db.VarChar
  primary_name   Boolean?
  acctax         acctax? @relation(fields: [acode], references: [acode], onDelete: Cascade, map: "acode_fk")
}

model county {
  county                               String                   @unique(map: "county_name_unique") @db.VarChar(25)
  fips                                 String                   @db.VarChar(3)
  gid                                  Int                      @id @default(autoincrement())
  geom                                 Unsupported("geometry")?
  occurrence_occurrence_countyTocounty occurrence[]             @relation("occurrence_countyTocounty")

  @@index([geom], map: "county_geom_gist", type: Gist)
}

model d_dist_confidence {
  d_dist_confidence_id Int                 @id @default(autoincrement())
  dist_confidence      String              @unique @db.VarChar
  distribution_data    distribution_data[]
}

model d_origin {
  d_origin_id       Int                 @id @default(autoincrement())
  origin            String              @unique @db.VarChar
  distribution_data distribution_data[]
}

model d_population {
  d_population_id   Int                 @id @default(autoincrement())
  population        String              @unique @db.VarChar
  distribution_data distribution_data[]
}

model d_presence_absence {
  d_presence_absence_id Int                 @id @default(autoincrement())
  presence_absence      String              @unique @db.VarChar
  distribution_data     distribution_data[]
}

model d_regularity {
  d_regularity_id   Int                 @id @default(autoincrement())
  regularity        String              @unique @db.VarChar
  distribution_data distribution_data[]
}

model distribution_data {
  d_id                      Int                 @id @default(autoincrement())
  acode                     String?             @db.VarChar
  elcode                    String?             @db.VarChar
  origin                    String?             @db.VarChar
  regularity                String?             @db.VarChar
  dist_confidence           String?             @db.VarChar
  presence_absence          String?             @db.VarChar
  population                String?             @db.VarChar
  distribution_data_remarks String?             @db.VarChar
  acctax                    acctax?             @relation(fields: [acode], references: [acode], onDelete: NoAction, map: "acode_fk")
  d_dist_confidence         d_dist_confidence?  @relation(fields: [dist_confidence], references: [dist_confidence], onDelete: NoAction, onUpdate: NoAction, map: "dist_confidence_fk")
  d_origin                  d_origin?           @relation(fields: [origin], references: [origin], onDelete: NoAction, onUpdate: NoAction, map: "origin_fk")
  d_population              d_population?       @relation(fields: [population], references: [population], onDelete: NoAction, onUpdate: NoAction, map: "population_fk")
  d_presence_absence        d_presence_absence? @relation(fields: [presence_absence], references: [presence_absence], onDelete: NoAction, onUpdate: NoAction, map: "presence_absence_fk")
  d_regularity              d_regularity?       @relation(fields: [regularity], references: [regularity], onDelete: NoAction, onUpdate: NoAction, map: "regularity_fk")
}

model fed_status {
  status_id   Int      @id
  status      String   @db.VarChar
  description String   @db.VarChar
  acctax      acctax[]
}

model global_rank_lu {
  code   String   @id @db.VarChar(15)
  acctax acctax[]
}

model hex5km {
  gid     Int                      @id @default(autoincrement())
  id      Decimal?                 @db.Decimal(10, 0)
  grid_id String?                  @db.VarChar(12)
  geom    Unsupported("geometry")?

  @@index([geom], map: "hex5km_geom_gist", type: Gist)
}

model hightax {
  kingdom               String?                @db.VarChar
  phylum                String?                @db.VarChar
  taxclass              String?                @db.VarChar
  taxorder              String?                @db.VarChar
  family                String                 @id @db.VarChar
  category              String?                @db.VarChar
  name_type_desc        String?                @db.VarChar
  name_category_desc    String?                @db.VarChar
  acctax                acctax[]
  category_lu           category_lu?           @relation(fields: [category], references: [category], onDelete: NoAction, onUpdate: NoAction, map: "category_fk")
  kingdom_lu            kingdom_lu?            @relation(fields: [kingdom], references: [kingdom], onDelete: NoAction, onUpdate: NoAction, map: "kingdom_fk")
  name_category_desc_lu name_category_desc_lu? @relation(fields: [name_category_desc], references: [name_category_desc], onDelete: NoAction, onUpdate: NoAction, map: "name_category_desc_fk")
  name_type_desc_lu     name_type_desc_lu?     @relation(fields: [name_type_desc], references: [name_type_desc], onDelete: NoAction, onUpdate: NoAction, map: "name_type_desc_fk")
}

model identification_verification {
  pkey                  Int         @id @default(autoincrement())
  catalognumber         String?     @db.VarChar
  identifiedby          String?     @db.VarChar
  identificationremarks String?     @db.VarChar
  datelastmodified      String?     @db.VarChar
  identifiedacode       String?     @db.VarChar
  gid                   Int?
  occurrence            occurrence? @relation(fields: [gid], references: [gid], onDelete: NoAction, onUpdate: NoAction, map: "gid_fk")
}

model institution {
  institutioncode String       @id @db.VarChar(10)
  institution     String       @db.VarChar(75)
  curator         String?      @db.VarChar(25)
  email           String?      @db.VarChar(40)
  telephone       String?      @db.VarChar(100)
  address         String?      @db.VarChar(100)
  city            String?      @db.VarChar(20)
  state           String?      @db.VarChar(2)
  country         String?      @db.VarChar(5)
  zipcode         String?      @db.VarChar(55)
  institutiontype String?      @db.VarChar(50)
  link            String?      @db.VarChar
  occurrence      occurrence[]
}

model iucn_lu {
  code        String   @id @db.VarChar(2)
  description String   @db.VarChar(60)
  id          Int
  acctax      acctax[]
}

model kingdom_lu {
  id      Int       @default(autoincrement())
  kingdom String    @id @db.VarChar(30)
  hightax hightax[]
}

model name_category_desc_lu {
  a_id               Int       @default(autoincrement())
  name_category_desc String    @id @db.VarChar(30)
  hightax            hightax[]
}

model name_type_desc_lu {
  a_id           Int       @default(autoincrement())
  name_type_desc String    @id @db.VarChar(30)
  hightax        hightax[]
}

model nativity_lu {
  n_id     Int      @default(autoincrement())
  nativity String   @id @db.VarChar(60)
  acctax   acctax[]
}

model occurrence {
  gid                              Int                           @id(map: "occurrences_pkey") @default(autoincrement())
  acode                            String?                       @db.VarChar
  eventdate                        DateTime?                     @db.Date
  recordedby                       String?                       @db.VarChar
  county                           String?                       @db.VarChar(25)
  locality                         String?                       @db.VarChar
  behavior                         String?                       @db.VarChar
  habitat                          String?                       @db.VarChar
  sex                              String?                       @db.VarChar
  lifestage                        String?                       @db.VarChar
  associatedtaxa                   String?                       @db.VarChar
  elevation                        Float?
  depth                            Float?
  depthaccuracy                    Int?
  occurrenceremarks                String?                       @db.VarChar(1454607)
  taxonremarks                     String?                       @db.VarChar
  institutioncode                  String?                       @db.VarChar
  basisofrecord                    String?                       @db.VarChar
  catalognumber                    String?                       @db.VarChar
  othercatalognumbers              String?                       @db.VarChar
  typestatus                       String?                       @db.VarChar(25)
  recordnumber                     String?                       @db.VarChar
  samplingprotocol                 String?                       @db.VarChar
  preparations                     String?                       @db.VarChar
  primary_data                     String?                       @db.VarChar
  associatedreferences             String?                       @db.VarChar
  datasetname                      String?                       @db.VarChar(30)
  coordinateprecision              Int?
  decimallatitude                  Float?
  decimallongitude                 Float?
  geodeticdatum                    String?                       @default("WGS84") @db.VarChar(10)
  georeferencedby                  String?                       @db.VarChar
  georeferenceddate                DateTime?                     @db.Date
  georeferenceremarks              String?                       @db.VarChar
  georeferencesources              String?                       @db.VarChar
  georeferenceverificationstatus   String?                       @db.VarChar
  geom                             Unsupported("geometry")?
  problem_with_record              String?                       @db.VarChar
  previousidentifications          String?                       @db.VarChar
  identificationverificationstatus String?                       @db.VarChar
  datelastmodified                 DateTime?                     @db.Date
  associatedoccurrences            String?                       @db.VarChar
  associatedsequences              String?                       @db.VarChar
  entby                            String?                       @db.VarChar
  entrydate                        DateTime?                     @db.Date
  obs_gid                          Int?
  mtr                              String?
  township                         Int?
  ns                               String?
  range                            Int?
  ew                               String?
  section                          Int?
  quarter                          String?
  zone                             Int?
  utme                             Int?
  utmn                             Int?
  hiderecord                       Boolean                       @default(false)
  hiderecordcomment                String?                       @db.VarChar
  relationshipremarks              String?                       @db.VarChar
  informationwitheld               Boolean?
  awaitingreview                   Int?
  occurrenceid                     String?                       @db.Uuid
  resourcetype                     String?                       @db.VarChar(20)
  identificationconfidence         String?                       @db.VarChar(10)
  identificationremarks            String?                       @db.VarChar
  verbatimeventdate                String?                       @db.VarChar(150)
  verbatimlocality                 String?                       @db.VarChar
  individuals_estimated            Boolean                       @default(false)
  individuals_max                  Int?
  individuals_min                  Int?
  identification_verification      identification_verification[]
  acctax                           acctax?                       @relation(fields: [acode], references: [acode], onDelete: NoAction, map: "acode_fk")
  basisofrecord_lu                 basisofrecord_lu?             @relation(fields: [basisofrecord], references: [basisofrecord], onDelete: NoAction, onUpdate: NoAction, map: "basisofrecord_fk")
  county_occurrence_countyTocounty county?                       @relation("occurrence_countyTocounty", fields: [county], references: [county], onDelete: NoAction, onUpdate: NoAction, map: "county_fk")
  institution                      institution?                  @relation(fields: [institutioncode], references: [institutioncode], onDelete: Cascade, map: "institution_fk")
  resourcetype_lu                  resourcetype_lu?              @relation(fields: [resourcetype], references: [resourcetype], onDelete: NoAction, onUpdate: NoAction, map: "resorucetype_fk")
  source                           source?                       @relation(fields: [datasetname], references: [source], onDelete: NoAction, onUpdate: NoAction, map: "source_fk")

  @@index([acode], map: "occurrence_idx")
}

model ok_swap {
  swap_id     Int      @id @default(autoincrement())
  tier        String   @db.VarChar
  description String   @db.VarChar
  acctax      acctax[]
}

model okcounties {
  gid      Int                      @id @default(autoincrement())
  statefp  String?                  @db.VarChar(2)
  countyfp String?                  @db.VarChar(3)
  countyns String?                  @db.VarChar(8)
  cntyidfp String?                  @db.VarChar(5)
  name     String?                  @db.VarChar(100)
  namelsad String?                  @db.VarChar(100)
  lsad     String?                  @db.VarChar(2)
  classfp  String?                  @db.VarChar(2)
  mtfcc    String?                  @db.VarChar(5)
  ur       String?                  @db.VarChar(1)
  funcstat String?                  @db.VarChar(1)
  geom     Unsupported("geometry")?

  @@index([geom], map: "okcounties_geom_gist", type: Gist)
}

model rank_change {
  r_id                 Int       @id @default(autoincrement())
  acode                String?   @db.VarChar
  previous_s_rank      String?   @db.VarChar
  s_rank               String?   @db.VarChar
  changedby            String?   @db.VarChar
  rankremarks          String?   @db.VarChar
  datelastmodified     DateTime? @db.Date
  previousdatemodified DateTime? @db.Date
  acctax               acctax?   @relation(fields: [acode], references: [acode], onDelete: NoAction)
}

model resourcetype_lu {
  id           Int          @default(autoincrement())
  resourcetype String       @id @db.VarChar(50)
  occurrence   occurrence[]
}

model source {
  source      String       @id @db.VarChar(30)
  description String       @db.VarChar
  occurrence  occurrence[]
}

model spatial_ref_sys {
  srid      Int     @id
  auth_name String? @db.VarChar(256)
  auth_srid Int?
  srtext    String? @db.VarChar(2048)
  proj4text String? @db.VarChar(2048)
}

model st_status {
  status_id   Int      @id
  status      String   @db.VarChar
  description String   @db.VarChar
  acctax      acctax[]
}

model state_rank_lu {
  code   String   @id @db.VarChar(15)
  acctax acctax[]
}

model syntax {
  acode                         String? @db.VarChar
  scode                         String  @id @db.VarChar
  sname                         String? @db.VarChar
  scientificnameauthorship      String? @db.VarChar
  family                        String? @db.VarChar
  genus                         String? @db.VarChar
  species                       String? @db.VarChar
  subspecies                    String? @db.VarChar
  variety                       String? @db.VarChar
  scientificname                String? @db.VarChar
  sspscientificnameauthorship   String? @db.VarChar
  varscientificnameauthorship   String? @db.VarChar
  formascientificnameauthorship String? @db.VarChar
  tsn                           Int?
  acctax                        acctax? @relation(fields: [acode], references: [acode], onDelete: Cascade, map: "acode_fk")

  @@index([acode, scode, sname], map: "syntax_idx")
}

model tnc {
  gid        Int                      @id @default(autoincrement())
  objectid   Decimal?                 @db.Decimal(10, 0)
  dis        Int?
  shape_leng Decimal?                 @db.Decimal
  shape_area Decimal?                 @db.Decimal
  acres      Decimal?                 @db.Decimal(10, 0)
  geom       Unsupported("geometry")?

  @@index([geom], map: "tnc_geom_gist", type: Gist)
}

view vw_all_taxa {
  unique_code    String  @unique @db.VarChar
  sname          String? @db.VarChar
  scientificname String? @db.VarChar
  status         String?
  accepted_code  String? @db.VarChar
}
