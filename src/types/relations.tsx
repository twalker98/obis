import {
  basisofrecord_lu,
  category_lu,
  county,
  d_dist_confidence,
  d_origin,
  d_population,
  d_presence_absence,
  d_regularity,
  fed_status,
  global_rank_lu,
  institution,
  iucn_lu,
  kingdom_lu,
  name_category_desc_lu,
  nativity_lu,
  ok_swap,
  resourcetype_lu,
  source,
  st_status,
  state_rank_lu,
} from '../utils/obis-prisma';

export type TFValue = {
  display_name: string;
  value: boolean;
};

export type IdentificationConfidence = {
  id: number;
  confidence: string;
};

export type Direction = {
  direction: 'N' | 'S' | 'E' | 'W';
  display_name: 'North' | 'South' | 'East' | 'West';
};

export type OccurrenceSearchField = {
  key: string;
  display_name: string;
  disabled?: boolean;
};

export type Relations = {
  basisofrecord_lu: Array<basisofrecord_lu>;
  category_lu: Array<category_lu>;
  county: Array<county>;
  d_dist_confidence: Array<d_dist_confidence>;
  fed_status: Array<fed_status>;
  global_rank_lu: Array<global_rank_lu>;
  institution: Array<institution>;
  iucn_lu: Array<iucn_lu>;
  kingdoms: Array<kingdom_lu>;
  name_category_desc_lu: Array<name_category_desc_lu>;
  nativity_lu: Array<nativity_lu>;
  d_origin: Array<d_origin>;
  d_population: Array<d_population>;
  d_presence_absence: Array<d_presence_absence>;
  d_regularity: Array<d_regularity>;
  ok_swap: Array<ok_swap>;
  resourcetype_lu: Array<resourcetype_lu>;
  source: Array<source>;
  st_status: Array<st_status>;
  state_rank_lu: Array<state_rank_lu>;
  tfValues: Array<TFValue>;
  identificationConfidences: Array<IdentificationConfidence>;
  ns: Array<Direction>;
  ew: Array<Direction>;
  occurrenceSearchFields: Array<OccurrenceSearchField>;
  countyReportSearchFields: Array<OccurrenceSearchField>;
  occurrenceReportSearchFields: Array<OccurrenceSearchField>;
};
