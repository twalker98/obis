export type CountyReportResult = {
  acode: string | null;
  count?: number;
  county: string | null;
  family?: string;
  genus?: string;
  phylum?: string;
  species?: string;
  subspecies?: string;
  taxclass?: string;
  taxorder?: string;
  variety?: string;
};

export type OccurrenceReportResult = {
  gid: number;
  recordedby: string | null;
  family?: string;
  sname?: string;
  eventdate: Date | null;
  datasetname: string | null;
  county: string | null;
  institutioncode: string | null;
  catalognumber: string | null;
  scientificname?: string;
  scientificnameauthorship?: string;
  locality: string | null;
};
