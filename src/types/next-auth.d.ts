import NextAuth, { DefaultSession } from 'next-auth';
import type { UserGroup, UserPermission } from '../lib/obis-auth-prisma';

declare module 'next-auth' {
  interface Session {
    user: {
      id: string;
      firstName: string;
      lastName: string;
      groups?: Array<UserGroup>;
      permissions?: Array<UserPermission>;
    } & Omit<DefaultSession['user'], 'name'>;
  }
}
