export type TaxaSearchResult = {
  acode: string;
  displayName: string;
  genus: string | null;
  species: string | null;
  subspecies: string | null;
  variety: string | null;
  family: string | null;
  type: string;
  primaryName: boolean;
  community: boolean;
};
