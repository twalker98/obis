import { comtax } from '../utils/managers/obis/Comtax';
import { syntax } from '../utils/managers/obis/Syntax';

export type TaxaListItem = {
  name: string;
  secondary?: string;
  item: comtax | syntax;
};
