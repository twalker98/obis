import { GridColDef } from '@twalk-tech/react-lib/nextjs/components';
import {
  acctax,
  comtax,
  hightax,
  occurrence,
  syntax,
} from '../utils/obis-prisma';

export type TaxonOccurrenceTableResultOld = {
  county: string;
  _count: { acode: number };
  _min: { eventdate: Date | string };
  _max: { eventdate: Date | string };
};

export type TaxonOccurrenceTableResult = Pick<
  occurrence,
  'county' | 'acode'
> & {
  _count: {
    acode: number;
  };
  _min: {
    eventdate: Date | string | null;
  };
  _max: {
    eventdate: Date | string | null;
  };
  viewLink: 'View Occurrences ->';
};

export type TaxonHightaxTableData = Omit<
  hightax,
  'category' | 'name_type_desc' | 'name_category_desc'
> & {
  genus: string;
};

export type TaxonHightaxTable = {
  columns: Array<GridColDef>;
  data: Array<TaxonHightaxTableData>;
};

export type TaxonResult = {
  taxon: acctax;
  commonNames: Array<comtax> | null;
  synonyms: Array<syntax> | null;
  hightax: TaxonHightaxTable | null;
  community: boolean;
  occurrences: Array<TaxonOccurrenceTableResult> | null;
};
