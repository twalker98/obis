export type OccurrenceSearchResult = {
  gid: number;
  acode: string;
  county: string | null;
  recordedby: string | null;
  eventdate: string | undefined;
  locality: string | null;
  editLink: 'Edit Occurrence ->';
};
