export type Tooltips<T> = {
  [k in keyof T]: string;
};
