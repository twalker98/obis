import ObisLayout from '../components/ObisLayout';

import { ErrorBoundary, HighlightInit } from '@highlight-run/next/client';
import { Route } from '@twalk-tech/react-lib/nextjs/utils/types';
import { Metadata } from 'next';
import { isAdmin, userHasPermission } from '../utils/functions';
import { listPermissionRecords } from '../utils/managers/auth/Permission';
import { getSession } from '../utils/server-auth';

export const metadata: Metadata = {
  applicationName: 'OBIS',
  authors: [
    { name: 'TWALK Technology', url: 'https://gitlab.com/twalk-tech' },
    { name: 'Tyler Walker', url: 'https://gitlab.com/twalker98' },
  ],
  description:
    'OBIS is a comprehensive web-based application that draws upon multiple existing biodiversity databases in order to facilitate biodiversity conservation planning in the state of Oklahoma and beyond.',
  keywords: [
    'OBIS',
    'obis',
    'oklahoma biodiversity',
    'oklahoma biodiversity information',
    'oklahoma biodiversity information system',
    'oklahoma biological survey',
    'oklahoma natural heritage',
    'oklahoma natural heritage program',
  ],
  title: 'oklahoma biodiversity information system',
};

export default async function RootLayout({ children }) {
  const session = await getSession();
  const permissions = await listPermissionRecords();

  const routes: Array<Route> = [
    {
      name: 'Taxon Details',
      paths: [
        '/taxa/search/main',
        '/taxa/search/main/*',
        '/taxa/result/main/*',
      ],
      activePathIndex: 0,
    },
    {
      name: 'Taxon Map',
      paths: ['/taxa/search/map', '/taxa/search/map/*', '/taxa/result/map/*'],
      activePathIndex: 0,
      divider: true,
    },
    {
      name: 'County Report',
      paths: ['/county-report/search', '/county-report/search/*'],
      activePathIndex: 0,
      divider: true,
    },
    {
      name: 'Menu',
      paths: [],
      options: [
        {
          name: 'About OBIS',
          paths: ['/about'],
        },
        {
          name: 'Information Request Form',
          paths: [
            'http://oknaturalheritage.ou.edu/content/info-request/online-request/',
          ],
        },
        {
          name: 'Submit an Observation',
          paths: [
            'http://oknaturalheritage.ou.edu/content/submit-data/online-submission/',
          ],
        },
        {
          name: 'Tracking Lists',
          paths: [
            'http://oknaturalheritage.ou.edu/content/biodiversity-info/tracking-list/',
          ],
          divider: true,
        },
        {
          name: 'Oklahoma Natural Heritage Inventory Home',
          paths: ['http://oknaturalheritage.ou.edu/'],
        },
        {
          name: 'Oklahoma Biological Survey Home',
          paths: ['https://www.ou.edu/biosurvey/'],
        },
      ],
    },
    {
      name: 'Login',
      paths: [],
      login: true,
    },
  ];

  // This function should only be called if both `session` and `permissions` is set (i.e., check prior)
  const addTaxonRoutes = (isUserAdmin: boolean) => {
    const startIndex = routes.findIndex((route) => route.name === 'Taxon Map');
    let numRoutesAdded = 0;

    if (
      isUserAdmin ||
      userHasPermission(session!.user, permissions, ['taxa:*', 'taxa:edit'])
    ) {
      numRoutesAdded += 1;
      routes.splice(startIndex + 1, 0, {
        name: 'Edit Taxon',
        paths: [
          '/taxa/search/edit',
          '/taxa/search/edit/*',
          '/taxa/result/edit/*',
        ],
        activePathIndex: 0,
      });
    }

    if (
      isUserAdmin ||
      userHasPermission(session!.user, permissions, ['taxa:*', 'taxa:add'])
    ) {
      numRoutesAdded += 1;
      routes.splice(startIndex + 1, 0, {
        name: 'Add Taxon',
        paths: ['/taxon/add'],
      });
    }

    // If there was any route added, add a divider to the last route
    if (numRoutesAdded > 0) {
      routes[startIndex + numRoutesAdded].divider = true;
    }
  };

  // This function should only be called if both `session` and `permissions` is set (i.e., check prior)
  const addOccurrenceRoutes = (isUserAdmin: boolean) => {
    const startIndex = routes.findIndex((route) => route.name === 'Taxon Map');
    let numRoutesAdded = 0;

    if (
      isUserAdmin ||
      userHasPermission(session!.user, permissions, [
        'occurrence:*',
        'occurrence:edit',
      ])
    ) {
      numRoutesAdded += 1;
      routes.splice(startIndex + 1, 0, {
        name: 'Edit Occurrence',
        paths: [
          '/occurrences/search/edit',
          '/occurrences/search/edit/*',
          '/occurrences/result/edit/*',
        ],
        activePathIndex: 0,
      });
    }

    if (
      isUserAdmin ||
      userHasPermission(session!.user, permissions, [
        'occurrence:*',
        'occurrence:add',
      ])
    ) {
      numRoutesAdded += 1;
      routes.splice(startIndex + 1, 0, {
        name: 'Add Occurrence',
        paths: ['/occurrence/add'],
      });
    }

    // If there was any route added, add a divider to the last route
    if (numRoutesAdded > 0) {
      routes[startIndex + numRoutesAdded].divider = true;
    }
  };

  // This will only be called if there is a logged in user
  const addOccurrenceReportRoute = () => {
    const countyReportIndex = routes.findIndex(
      (route) => route.name === 'County Report',
    );
    routes[countyReportIndex].divider = false;
    routes.splice(countyReportIndex + 1, 0, {
      name: 'Occurrence Report',
      paths: ['/occurrence-report/search', '/occurrence-report/search/*'],
      activePathIndex: 0,
      divider: true,
    });
  };

  // If there is a logged in user and we successfully got permissions back, we can add some more routes based on the user's permissions
  if (session && permissions) {
    // Add the avatar for the logged in user to the navbar
    routes.find((route) => route.login === true)!.user = {
      id: session.user.id,
      firstName: session.user.firstName,
      lastName: session.user.lastName,
    };

    const isUserAdmin = isAdmin(session.user, permissions);

    if (isUserAdmin) {
      routes.splice(
        routes.findIndex((route) => route.name === 'Menu'),
        0,
        {
          name: 'Admin',
          paths: ['/admin'],
        },
      );
    }

    addOccurrenceRoutes(isUserAdmin);
    addTaxonRoutes(isUserAdmin);
    addOccurrenceReportRoute();
  }

  return (
    <>
      {process.env.NODE_ENV === 'production' ? (
        <HighlightInit
          projectId={process.env.HIGHLIGHT_PROJECT_ID}
          serviceName="OBIS"
          networkRecording={{
            enabled: true,
            recordHeadersAndBody: true,
            urlBlocklist: [],
          }}
        />
      ) : (
        <></>
      )}
      <html lang="en">
        <body>
          {permissions && process.env.NODE_ENV === 'production' ? (
            <ErrorBoundary>
              <ObisLayout navbarRoutes={routes} permissions={permissions}>
                {children}
              </ObisLayout>
            </ErrorBoundary>
          ) : permissions ? (
            <ObisLayout navbarRoutes={routes} permissions={permissions}>
              {children}
            </ObisLayout>
          ) : (
            <></>
          )}
        </body>
      </html>
    </>
  );
}
