'use client';

import Search from '../../components/taxa/Search';

import { usePathname, useRouter } from 'next/navigation';
import React, { ReactNode, useState } from 'react';
import { useSnackbar } from '../../utils/hooks/snackbar';
import { TaxaSearchbarValueContext } from './context';

type SearchLayoutProps = {
  children: ReactNode;
};

export default function TaxaLayout(props: SearchLayoutProps) {
  const router = useRouter();
  const pathnameFragments = usePathname()?.split('/');
  const { snackbar, openSnackbar } = useSnackbar();
  const [query, setQuery] = useState(
    pathnameFragments && pathnameFragments[2] !== 'result'
      ? decodeURIComponent(pathnameFragments[pathnameFragments.length - 1])
      : '',
  );

  const location = pathnameFragments && pathnameFragments[3];

  if (!location) {
    return null;
  }

  // Set the query to null if we don't have any results
  if (location && query === location) {
    setQuery('');
  }

  const search = async (query: string) => {
    if (!query) {
      openSnackbar('Must provide a search query!', 'error');
      return;
    }

    router.push(
      `/taxa/search/${encodeURIComponent(location)}/${encodeURIComponent(
        query,
      )}`,
    );
  };

  return (
    <TaxaSearchbarValueContext.Provider
      value={{ value: query, setValue: setQuery }}
    >
      <Search search={search} />
      {props.children}
      {snackbar}
    </TaxaSearchbarValueContext.Provider>
  );
}
