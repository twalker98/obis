'use server';

import {
  acctax,
  createAcctaxRecord,
  searchAcctaxRecords,
  updateAcctaxRecord,
} from '../../utils/managers/obis/Acctax';
import {
  comtax,
  createComtaxRecord,
  deleteComtaxRecord,
  updateComtaxRecord,
} from '../../utils/managers/obis/Comtax';
import {
  createDistributionDataRecord,
  distribution_data,
  updateDistributionDataRecord,
} from '../../utils/managers/obis/DistributionData';
import {
  createHightaxRecord,
  getHightaxRecord,
  hightax,
  searchHightaxRecords,
} from '../../utils/managers/obis/Hightax';
import {
  createSyntaxRecord,
  deleteSyntaxRecord,
  syntax,
  updateSyntaxRecord,
} from '../../utils/managers/obis/Syntax';
import {
  getVwAllTaxaRecord,
  vw_all_taxa,
} from '../../utils/managers/obis/VwAllTaxa';
import {
  ProcessedPrismaCall,
  processPrismaGetCall,
  processPrismaPostCall,
} from '../../utils/server-action-functions';

import { revalidatePath } from 'next/cache';
import { Prisma } from '../../utils/obis-prisma';

const revalidateAllPaths = (acode: string) => {
  revalidatePath('/taxon/add');
  revalidatePath(`/taxa/result/edit/${acode}`);
};

const createAcctaxRecordAction = async (
  values: acctax,
): Promise<ProcessedPrismaCall<acctax>> => {
  const response = await processPrismaPostCall<acctax>(
    createAcctaxRecord,
    values,
    'create',
    'taxon',
    values.sname!,
  );

  if (response.prismaCallResponse) {
    revalidatePath('/taxon/add');
  }

  return response;
};

// TODO: fix bug with adding a primary common name after a non-primary common name
// Application gives an error that a primary name already exists, even though it doesn't
const createComtaxRecordAction = async (
  values: comtax,
): Promise<ProcessedPrismaCall<comtax>> => {
  const response = await processPrismaPostCall<comtax>(
    createComtaxRecord,
    values,
    'create',
    'common name',
    values.vernacularname!,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.acode!);
  }

  return response;
};

const createDistributionDataRecordAction = async (
  values: distribution_data,
): Promise<ProcessedPrismaCall<distribution_data>> => {
  const response = await processPrismaPostCall<distribution_data>(
    createDistributionDataRecord,
    values,
    'create',
    'distribution data',
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.acode!);
  }

  return response;
};

const createHightaxRecordAction = async (
  values: hightax,
): Promise<ProcessedPrismaCall<hightax>> => {
  const response = await processPrismaPostCall<hightax>(
    createHightaxRecord,
    values,
    'create',
    'family',
  );

  return response;
};

const createSyntaxRecordAction = async (
  values: syntax,
): Promise<ProcessedPrismaCall<syntax>> => {
  const response = await processPrismaPostCall<syntax>(
    createSyntaxRecord,
    values,
    'create',
    'synonym',
    values.sname!,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.acode!);
  }

  return response;
};

// TODO: fix bug with vernacularnames not displaying in snackbar
const deleteComtaxRecordAction = async (
  values: comtax,
): Promise<ProcessedPrismaCall<comtax>> => {
  const response = await processPrismaPostCall<comtax>(
    deleteComtaxRecord,
    values,
    'delete',
    'common name',
    values.vernacularname!,
    values.c_id,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.acode!);
  }

  return response;
};

// TODO: fix bug with snames not displaying in snackbar
const deleteSyntaxRecordAction = async (
  values: syntax,
): Promise<ProcessedPrismaCall<syntax>> => {
  const response = await processPrismaPostCall<syntax>(
    deleteSyntaxRecord,
    values,
    'delete',
    'synonym',
    values.sname!,
    values.scode,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.acode!);
  }

  return response;
};

const getHightaxRecordAction = async (
  family: string,
): Promise<ProcessedPrismaCall<hightax>> => {
  return await processPrismaGetCall<hightax, string>(
    getHightaxRecord,
    'family',
    family,
  );
};

const getVwAllTaxaRecordAction = async (
  unique_code: string,
): Promise<ProcessedPrismaCall<vw_all_taxa>> => {
  return await processPrismaGetCall<vw_all_taxa, string>(
    getVwAllTaxaRecord,
    'taxon',
    unique_code,
  );
};

const editAcctaxRecord = async (
  values: acctax,
): Promise<ProcessedPrismaCall<acctax>> => {
  const response = await processPrismaPostCall<acctax>(
    updateAcctaxRecord,
    values,
    'edit',
    'taxon',
    values.sname!,
    values.acode,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.acode);
  }

  return response;
};

const editComtaxRecord = async (
  values: comtax,
): Promise<ProcessedPrismaCall<comtax>> => {
  const response = await processPrismaPostCall<comtax>(
    updateComtaxRecord,
    values,
    'edit',
    'common name',
    values.vernacularname!,
    values.c_id,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.acode!);
  }

  return response;
};

const editDistributionDataRecord = async (
  values: distribution_data,
): Promise<ProcessedPrismaCall<distribution_data>> => {
  const d_id = values.d_id;

  // @ts-expect-error
  delete values['d_id'];

  const response = await processPrismaPostCall<distribution_data>(
    updateDistributionDataRecord,
    values,
    'edit',
    'distribution data',
    undefined,
    d_id,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.acode!);
  }

  return response;
};

const editSyntaxRecord = async (
  values: syntax,
): Promise<ProcessedPrismaCall<syntax>> => {
  const response = await processPrismaPostCall<syntax>(
    updateSyntaxRecord,
    values,
    'edit',
    'synonym',
    values.sname!,
    values.scode,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.acode!);
  }

  return response;
};

const searchAcctaxRecordsAction = async (
  sname: string,
): Promise<ProcessedPrismaCall<Array<acctax>>> => {
  return await processPrismaGetCall<Array<acctax>, Prisma.acctaxWhereInput>(
    searchAcctaxRecords,
    'taxon',
    {
      sname: {
        contains: sname,
        mode: 'insensitive',
      },
    },
  );
};

const searchHightaxRecordsAction = async (
  field: 'class' | 'order' | 'phylum',
  value: string,
): Promise<ProcessedPrismaCall<Array<hightax>>> => {
  let whereInput: Prisma.hightaxWhereInput = {};

  if (field === 'class') {
    whereInput = {
      taxclass: {
        equals: value,
      },
    };
  } else if (field === 'order') {
    whereInput = {
      taxorder: {
        equals: value,
      },
    };
  } else if (field === 'phylum') {
    whereInput = {
      phylum: {
        equals: value,
      },
    };
  }

  return await processPrismaGetCall<Array<hightax>, Prisma.hightaxWhereInput>(
    searchHightaxRecords,
    field,
    whereInput,
  );
};

export {
  createAcctaxRecordAction as createAcctaxRecord,
  createComtaxRecordAction as createComtaxRecord,
  createDistributionDataRecordAction as createDistributionDataRecord,
  createHightaxRecordAction,
  createSyntaxRecordAction as createSyntaxRecord,
  deleteComtaxRecordAction as deleteComtaxRecord,
  deleteSyntaxRecordAction as deleteSyntaxRecord,
  editAcctaxRecord,
  editComtaxRecord,
  editDistributionDataRecord,
  editSyntaxRecord,
  getHightaxRecordAction as getHightaxRecord,
  getVwAllTaxaRecordAction as getVwAllTaxaRecord,
  searchAcctaxRecordsAction as searchAcctaxRecords,
  searchHightaxRecordsAction as searchHightaxRecords,
};
