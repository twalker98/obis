import { createContext } from 'react';

export type TTaxaSearchbarValueContext = {
  value: string;
  setValue: (newValue: string) => void;
};

export const TaxaSearchbarValueContext =
  createContext<TTaxaSearchbarValueContext | null>(null);
