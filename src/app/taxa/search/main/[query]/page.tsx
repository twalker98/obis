import TaxaResultsComponent from '../../../../../components/taxa/Results';
import TaxaSearchService from '../../../../../utils/taxa-search-service';
import TaxaResultsLoading from './loading';

import { Suspense } from 'react';
import { TaxaSearchResult } from '../../../../../types/taxa-search-result';

export default async function TaxaResults({ params }) {
  const taxa = new TaxaSearchService();
  const query = decodeURIComponent(params.query);

  const results = Array.from(
    (await taxa.search(query)) || new Map<string, Array<TaxaSearchResult>>(),
  );

  return (
    <Suspense fallback={<TaxaResultsLoading />}>
      <TaxaResultsComponent query={query} results={results} />
    </Suspense>
  );
}
