'use client';

import ResultsLoadingComponent from '../../../../../components/ResultsLoading';

export default function TaxaResultsLoading() {
  return <ResultsLoadingComponent />;
}
