import TaxaResultsComponent from '../../../../../components/taxa/Results';
import TaxaSearchService from '../../../../../utils/taxa-search-service';
import TaxaResultsLoading from './loading';

import { redirect } from 'next/navigation';
import { Suspense } from 'react';
import { TaxaSearchResult } from '../../../../../types/taxa-search-result';
import { getSigninUrl } from '../../../../../utils/auth';
import { userHasPermission } from '../../../../../utils/functions';
import { listPermissionRecords } from '../../../../../utils/managers/auth/Permission';
import { getSession } from '../../../../../utils/server-auth';

export default async function TaxaResults({ params }) {
  const taxa = new TaxaSearchService();
  const query = decodeURIComponent(params.query);
  const session = await getSession();
  const permissions = await listPermissionRecords();

  if (!session?.user) {
    redirect(getSigninUrl(`taxa/search/edit/${query}`));
  }

  if (!userHasPermission(session.user, permissions, ['taxa:*', 'taxa:edit'])) {
    redirect('/unauthorized');
  }

  const results = Array.from(
    (await taxa.search(query)) || new Map<string, Array<TaxaSearchResult>>(),
  );

  return (
    <Suspense fallback={<TaxaResultsLoading />}>
      <TaxaResultsComponent query={query} results={results} />
    </Suspense>
  );
}
