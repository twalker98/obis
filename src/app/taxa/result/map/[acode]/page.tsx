import Container from '@mui/material/Container';
import CollaboratorsTaxonMapResultComponent from '../../../../../components/taxa/collaborators/result/Map';
import TaxonMapResultComponent from '../../../../../components/taxa/result/Map';
import TaxonResultService from '../../../../../utils/taxon-result-service';

import { TaxonResult } from '../../../../../types/taxon-result';
import { getSession } from '../../../../../utils/server-auth';

export default async function TaxonMapResult({ params }) {
  const session = await getSession();
  const acode = params.acode;
  const taxa = new TaxonResultService(acode);
  const authenticatedUser = session && session.user;

  const result = (await taxa.getTaxonData(
    authenticatedUser !== null,
  )) as TaxonResult;

  const mapComponent =
    session && session.user ? (
      <CollaboratorsTaxonMapResultComponent result={result} />
    ) : (
      <TaxonMapResultComponent result={result} />
    );

  return <Container sx={{ mt: 3 }}>{mapComponent}</Container>;
}
