// TODO: update search bar

import TaxaCollaboratorsMainComponent from '../../../../../components/taxa/collaborators/Main';
import relationsService from '../../../../../utils/relations-service';

import { redirect } from 'next/navigation';
import { TFValue } from '../../../../../types/relations';
import { getSigninUrl } from '../../../../../utils/auth';
import { userHasPermission } from '../../../../../utils/functions';
import {
  commonNamesCompareFn,
  synonymsCompareFn,
} from '../../../../../utils/functions';
import { listPermissionRecords } from '../../../../../utils/managers/auth/Permission';
import { getAcctaxRecord } from '../../../../../utils/managers/obis/Acctax';
import { searchComtaxRecords } from '../../../../../utils/managers/obis/Comtax';
import { searchDistributionDataRecords } from '../../../../../utils/managers/obis/DistributionData';
import { searchSyntaxRecords } from '../../../../../utils/managers/obis/Syntax';
import { getSession } from '../../../../../utils/server-auth';

export default async function TaxaEditResult({ params }) {
  const acode = params.acode;
  const session = await getSession();
  const relations = await relationsService.getRelations();
  const permissions = await listPermissionRecords();

  // Block if the logged in user does not have permissions to access this page
  if (!session?.user) {
    redirect(getSigninUrl(`taxa/result/edit/${acode}`));
  }

  if (!userHasPermission(session.user, permissions, ['taxa:*', 'taxa:edit'])) {
    redirect('/unauthorized');
  }

  const whereClause = {
    acode: acode,
  };

  const acctax = await getAcctaxRecord(acode);
  const commonNames = await searchComtaxRecords(whereClause);
  const synonyms = await searchSyntaxRecords(whereClause);
  const distributionData = await searchDistributionDataRecords(whereClause);

  for (const commonName of commonNames) {
    if (!commonName.primary_name) {
      // @ts-ignore
      commonName.primary_name = relations.tfValues.find(
        (value: TFValue) => value.display_name === 'False',
      )!;
    } else {
      // @ts-ignore
      commonName.primary_name = relations.tfValues.find(
        (value: TFValue) => value.display_name === 'True',
      )!;
    }
  }

  commonNames.sort(commonNamesCompareFn);
  synonyms.sort(synonymsCompareFn);

  return (
    <TaxaCollaboratorsMainComponent
      relations={relations}
      acctaxRecordSubmitted={true}
      distributionDataSubmitted={distributionData.length > 0}
      acode={acode}
      acctax={acctax}
      commonNames={commonNames}
      synonyms={synonyms}
      d_id={distributionData.length > 0 ? distributionData[0].d_id : -1}
      distributionData={
        distributionData.length > 0 ? distributionData[0] : undefined
      }
    />
  );
}
