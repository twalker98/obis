import CollaboratorsTaxonResultDistributionMap from '../../../../../components/taxa/collaborators/result/DistributionMap';
import CollaboratorsWithEditPermissionsTaxonResultOccurrenceTable from '../../../../../components/taxa/collaborators/result/OccurrenceTableWithEditPermissions';
import CollaboratorsTaxonResultOverview from '../../../../../components/taxa/collaborators/result/Overview';

import TaxonResultDistributionMap from '../../../../../components/taxa/result/DistributionMap';
import TaxonMainResultComponent from '../../../../../components/taxa/result/Main';
import TaxonResultOccurrenceTable from '../../../../../components/taxa/result/OccurrenceTable';
import TaxonResultOverview from '../../../../../components/taxa/result/Overview';
import relationsService from '../../../../../utils/relations-service';
import TaxonResultService from '../../../../../utils/taxon-result-service';

import { List, Map, TableRows } from '@mui/icons-material';
import { CustomTab } from '@twalk-tech/react-lib/nextjs/utils/types';
import { TaxonResult } from '../../../../../types/taxon-result';
import { userHasPermission } from '../../../../../utils/functions';
import { listPermissionRecords } from '../../../../../utils/managers/auth/Permission';
import { getSession } from '../../../../../utils/server-auth';

export default async function TaxonMainResult({ params }) {
  const session = await getSession();
  const acode = params.acode;
  const taxa = new TaxonResultService(acode);
  const relations = await relationsService.getRelations();
  const permissions = await listPermissionRecords();
  const authenticatedUser = session && session.user;

  const result = (await taxa.getTaxonData(
    authenticatedUser !== null,
  )) as TaxonResult;

  const overviewContent = authenticatedUser ? (
    <CollaboratorsTaxonResultOverview result={result} relations={relations} />
  ) : (
    <TaxonResultOverview result={result} relations={relations} />
  );

  const distributionMapContent = authenticatedUser ? (
    <CollaboratorsTaxonResultDistributionMap result={result} />
  ) : (
    <TaxonResultDistributionMap result={result} />
  );

  const occurrenceTableContent =
    authenticatedUser &&
    userHasPermission(session.user, permissions, [
      'occurrence:*',
      'occurrence:edit',
    ]) ? (
      <CollaboratorsWithEditPermissionsTaxonResultOccurrenceTable
        result={result}
      />
    ) : (
      <TaxonResultOccurrenceTable result={result} />
    );

  const tabs: Array<CustomTab> = [
    {
      title: 'Overview',
      icon: <List />,
      content: overviewContent,
    },
    {
      title: 'Distribution Map',
      icon: <Map />,
      content: distributionMapContent,
    },
    {
      title: 'Occurrence Table',
      icon: <TableRows />,
      content: occurrenceTableContent,
    },
  ];

  return result ? (
    <TaxonMainResultComponent tabs={tabs} sname={result.taxon.sname!} />
  ) : (
    <></>
  );
}
