'use client';

import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Skeleton from '@mui/material/Skeleton';
import Typography from '@mui/material/Typography';

import { theme } from '../../../../../utils/theme';

export default function TaxaMainResultLoading() {
  return (
    <>
      <Box p={2} sx={{ textAlign: 'left', width: '75vw', mx: 'auto' }}>
        <Skeleton
          animation="wave"
          variant="rectangular"
          height={48}
          sx={{ width: '60vw' }}
        />
      </Box>
      <Box
        p={2}
        sx={{
          textAlign: 'left',
          border: '1px solid',
          borderColor: theme.palette.primary.main,
          width: '75vw',
          mx: 'auto',
        }}
        borderRadius={2}
      >
        <Container style={{ padding: 0, marginBottom: 4, marginLeft: 0 }}>
          <Skeleton
            animation="wave"
            variant="text"
            style={{ display: 'inline' }}
          >
            <Typography variant="h6" style={{ display: 'inline' }}>
              Fake Scientific Name Here
            </Typography>
          </Skeleton>
        </Container>
        <Skeleton
          animation="wave"
          variant="text"
          style={{ display: 'inline', marginRight: 6 }}
        >
          <Typography variant="h6" style={{ display: 'inline' }}>
            Synonyms:{' '}
          </Typography>
        </Skeleton>
        <Skeleton animation="wave" variant="text" style={{ display: 'inline' }}>
          <Typography variant="h6" style={{ display: 'inline' }}>
            Fake Synonym List Here
          </Typography>
        </Skeleton>
        <br />
        <Skeleton
          animation="wave"
          variant="text"
          style={{ display: 'inline', marginRight: 6 }}
        >
          <Typography variant="h6" style={{ display: 'inline' }}>
            Common Names:{' '}
          </Typography>
        </Skeleton>
        <Skeleton animation="wave" variant="text" style={{ display: 'inline' }}>
          <Typography variant="h6" style={{ display: 'inline' }}>
            Fake Common Name List Here
          </Typography>
        </Skeleton>
        <br />
        <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
          State Rank:{' '}
        </Typography>
        <Skeleton animation="wave" variant="text" style={{ display: 'inline' }}>
          <Typography variant="h6" style={{ display: 'inline' }}>
            Fake State Rank Here
          </Typography>
        </Skeleton>
        <br />
        <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
          Global Rank:{' '}
        </Typography>
        <Skeleton animation="wave" variant="text" style={{ display: 'inline' }}>
          <Typography variant="h6" style={{ display: 'inline' }}>
            Fake Global Rank Here
          </Typography>
        </Skeleton>
        <br />
        <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
          Species of Greatest Conservation Need Tier:{' '}
        </Typography>
        <Skeleton animation="wave" variant="text" style={{ display: 'inline' }}>
          <Typography variant="h6" style={{ display: 'inline' }}>
            Fake Swap Here
          </Typography>
        </Skeleton>
        <br />
        <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
          Federal Status:{' '}
        </Typography>
        <Skeleton animation="wave" variant="text" style={{ display: 'inline' }}>
          <Typography variant="h6" style={{ display: 'inline' }}>
            Fake Federal Status Here
          </Typography>
        </Skeleton>
        <br />
        <Typography style={{ display: 'inline', fontWeight: 'bold' }}>
          State Status:{' '}
        </Typography>
        <Skeleton animation="wave" variant="text" style={{ display: 'inline' }}>
          <Typography variant="h6" style={{ display: 'inline' }}>
            Fake State Status Here
          </Typography>
        </Skeleton>
        <br />
        <table>
          <thead>
            <tr>
              <td>
                <Typography style={{ fontWeight: 'bold' }}>Kingdom</Typography>
              </td>
              <td>
                <Typography style={{ fontWeight: 'bold' }}>Phylum</Typography>
              </td>
              <td>
                <Typography style={{ fontWeight: 'bold' }}>Class</Typography>
              </td>
              <td>
                <Typography style={{ fontWeight: 'bold' }}>Order</Typography>
              </td>
              <td>
                <Typography style={{ fontWeight: 'bold' }}>Genus</Typography>
              </td>
              <td>
                <Typography style={{ fontWeight: 'bold' }}>Species</Typography>
              </td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <Skeleton
                  style={{ display: 'inline' }}
                  animation="wave"
                  variant="text"
                >
                  <Typography variant="h6" style={{ display: 'inline' }}>
                    Kingdom
                  </Typography>
                </Skeleton>
              </td>
              <td>
                <Skeleton
                  style={{ display: 'inline' }}
                  animation="wave"
                  variant="text"
                >
                  <Typography variant="h6" style={{ display: 'inline' }}>
                    Phylum
                  </Typography>
                </Skeleton>
              </td>
              <td>
                <Skeleton
                  style={{ display: 'inline' }}
                  animation="wave"
                  variant="text"
                >
                  <Typography variant="h6" style={{ display: 'inline' }}>
                    Class
                  </Typography>
                </Skeleton>
              </td>
              <td>
                <Skeleton
                  style={{ display: 'inline' }}
                  animation="wave"
                  variant="text"
                >
                  <Typography variant="h6" style={{ display: 'inline' }}>
                    Order
                  </Typography>
                </Skeleton>
              </td>
              <td>
                <Skeleton
                  style={{ display: 'inline' }}
                  animation="wave"
                  variant="text"
                >
                  <Typography variant="h6" style={{ display: 'inline' }}>
                    Genus
                  </Typography>
                </Skeleton>
              </td>
              <td>
                <Skeleton
                  style={{ display: 'inline' }}
                  animation="wave"
                  variant="text"
                >
                  <Typography variant="h6" style={{ display: 'inline' }}>
                    Species
                  </Typography>
                </Skeleton>
              </td>
            </tr>
          </tbody>
        </table>
      </Box>
    </>
  );
}
