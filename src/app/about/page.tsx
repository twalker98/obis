'use client';

import Container from '@mui/material/Container';
import Link from 'next/link';

export default function About() {
  return (
    <Container sx={{ width: '70%' }}>
      <p>
        Informed and accurate conservation planning relies on access to current,
        spatially explicit data on the occurrence of biological species and
        ecological communities. However, data relevant to conservation planning
        and biodiversity education have been historically distributed among a
        number of state and federal agencies and private entities. Collating
        this information into a single, centralized data repository has proven
        challenging. As a result, these data have been stored in a series of
        disparate databases with varying degrees of accessibility.
      </p>
      <p>
        Beginning in 2015, the{' '}
        <Link href="http://www.oknaturalheritage.ou.edu/">
          Oklahoma Natural Heritage Inventory
        </Link>
        , with funding from the Oklahoma Department of Wildlife Conservation,
        State Wildlife Grant Program (SWG), began the process of collating
        biodiversity occurrence data into a single, integrated database
        management system and developing a comprehensive, web-based application
        to access these data. The result is the{' '}
        <b>Oklahoma Biodiversity Information System</b>, or simply <b>OBIS</b>.
      </p>
      <p>
        OBIS is a comprehensive web-based application that draws upon multiple
        existing biodiversity databases in order to facilitate biodiversity
        conservation planning in the state of Oklahoma and beyond. OBIS began as
        a project to integrate data from five disparate databases into a single,
        integrated data management system. It has expanded to include
        biodiversity data from a number of stakeholders, including state and
        federal government agencies, non-governmental organizations, and citizen
        naturalists.
      </p>
      <p>
        The OBIS web application utilizes a simple search interface. Users can
        enter the scientific name or common name of an Oklahoma-occurring taxon.
        A list of taxa meeting the search criterion will be displayed and users
        can select it to view basic taxonomic and distribution information about
        the taxon. The OBIS web app also includes a map interface so users can
        see basic taxon distribution (aggregated to 5 km<sup>2</sup>) hexagons
        in association with other spatial datasets.
      </p>
      <p>
        Additionally, the OBIS web application has an editing interface for our
        various collaborators and data providers.
      </p>
      <p>
        If you have any questions about OBIS, please contact the Oklahoma
        Natural Heritage Coordinator, Dr. Bruce Hoagland at{' '}
        <Link href="mailto:bhoagland@ou.edu">bhoagland@ou.edu</Link> or the OBIS
        data manager, Dr. Todd Fagin, at{' '}
        <Link href="tfagin@ou.edu">tfagin@ou.edu</Link>.
      </p>
    </Container>
  );
}
