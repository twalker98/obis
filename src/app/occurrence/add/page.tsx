import relationsService from '../../../utils/relations-service';

import { redirect } from 'next/navigation';
import OccurrenceCollaboratorsMainComponent from '../../../components/occurrence/collaborators/Main';
import { getSigninUrl } from '../../../utils/auth';
import { userHasPermission } from '../../../utils/functions';
import { listPermissionRecords } from '../../../utils/managers/auth/Permission';
import { ObisUser } from '../../../utils/obis-auth-prisma';
import { getSession } from '../../../utils/server-auth';

export default async function AddOccurrence() {
  const session = await getSession();
  const relations = await relationsService.getRelations();
  const permissions = await listPermissionRecords();

  // Block if there is not a logged in user
  if (!session?.user) {
    redirect(getSigninUrl('occurrence/add'));
  }

  // Block if the logged in user does not have permissions to access this page
  if (
    !userHasPermission(session.user, permissions, [
      'occurrence:*',
      'occurrence:add',
    ])
  ) {
    redirect('/unauthorized');
  }

  return (
    <OccurrenceCollaboratorsMainComponent
      relations={relations}
      username={(session.user as ObisUser).username}
      occurrenceRecordSubmitted={false}
    />
  );
}
