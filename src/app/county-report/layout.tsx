import { ReactNode } from 'react';
import ReportSearch from '../../components/report/Search';
import relationsService from '../../utils/relations-service';

type CountyReportSearchLayoutProps = {
  children: ReactNode;
};

export default async function CountyReportSearchLayout(
  props: CountyReportSearchLayoutProps,
) {
  const relations = await relationsService.getRelations();

  return (
    <>
      <ReportSearch
        reportSearchFields={relations.countyReportSearchFields}
        reportType="county"
        counties={relations.county}
        kingdoms={relations.kingdoms}
      />
      {props.children}
    </>
  );
}
