'use client';

import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';

export default function CountyReportResultsLoading() {
  return (
    <Box mt={3} maxWidth="80%" mx="auto">
      <Skeleton variant="rectangular" height={750} />
    </Box>
  );
}
