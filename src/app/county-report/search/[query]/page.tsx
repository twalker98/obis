import ReportResult from '../../../../components/report/Result';
import ReportService from '../../../../utils/report-service';
import CountyReportResultsLoading from './loading';

import { GridColDef } from '@twalk-tech/react-lib/nextjs';
import { Suspense } from 'react';

export default async function CountyReportResults({ params }) {
  const reportService = new ReportService('county');
  const queryString = decodeURIComponent(params.query);
  const query = new Map<string, string>(
    queryString
      .split('&')
      .map((pair: string) => pair.split('='))
      .map(([key, value]) => [key, decodeURIComponent(value)]),
  );

  const results = await reportService.generateReport(query);

  const countyReportColumns: Array<GridColDef> = [
    {
      field: 'county',
      headerName: 'County',
      width: 150,
    },
    {
      field: 'phylum',
      headerName: 'Phylum',
      width: 150,
    },
    {
      field: 'taxclass',
      headerName: 'Class',
      width: 150,
    },
    {
      field: 'taxorder',
      headerName: 'Order',
      width: 150,
    },
    {
      field: 'family',
      headerName: 'Family',
      width: 150,
    },
    {
      field: 'genus',
      headerName: 'Genus',
      width: 150,
    },
    {
      field: 'species',
      headerName: 'Species',
      width: 150,
    },
    {
      field: 'subspecies',
      headerName: 'Subspecies',
      width: 150,
    },
    {
      field: 'variety',
      headerName: 'Variety',
      width: 150,
    },
    {
      field: 'count',
      headerName: 'Count',
      width: 150,
    },
  ];

  return (
    <Suspense fallback={<CountyReportResultsLoading />}>
      <ReportResult
        reportType="county"
        columns={countyReportColumns}
        results={results}
      />
    </Suspense>
  );
}
