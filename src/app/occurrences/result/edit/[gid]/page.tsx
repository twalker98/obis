import OccurrenceCollaboratorsMainComponent from '../../../../../components/occurrence/collaborators/Main';
import relationsService from '../../../../../utils/relations-service';

import { redirect } from 'next/navigation';
import { OccurrenceScientificName } from '../../../../../components/forms/custom-fields/ScientificName';
import { getSigninUrl } from '../../../../../utils/auth';
import { userHasPermission } from '../../../../../utils/functions';
import { listPermissionRecords } from '../../../../../utils/managers/auth/Permission';
import { getAcctaxRecord } from '../../../../../utils/managers/obis/Acctax';
import { searchIdentificationVerificationRecords } from '../../../../../utils/managers/obis/IdentificationVerification';
import { getOccurrenceRecord } from '../../../../../utils/managers/obis/Occurrence';
import { ObisUser } from '../../../../../utils/obis-auth-prisma';
import { getSession } from '../../../../../utils/server-auth';

export default async function OccurrenceResult({ params }) {
  const session = await getSession();
  const relations = await relationsService.getRelations();
  const permissions = await listPermissionRecords();
  const gid = params.gid;

  // Block if there is not a logged in user
  if (!session?.user) {
    redirect(getSigninUrl(`occurrences/result/edit/${gid}`));
  }

  // Block if the logged in user does not have permissions to access this page
  if (
    !userHasPermission(session.user, permissions, [
      'occurrence:*',
      'occurrence:edit',
    ])
  ) {
    redirect('/unauthorized');
  }

  const whereClause = {
    gid: parseInt(gid),
  };

  const occurrence = await getOccurrenceRecord(parseInt(gid));
  const previousIdentifications = await searchIdentificationVerificationRecords(
    whereClause,
  );
  const acode = occurrence.acode as string;

  const acodeValue: OccurrenceScientificName = {
    acode: acode,
    sname: (await getAcctaxRecord(acode)).sname as string,
  };

  return (
    <OccurrenceCollaboratorsMainComponent
      relations={relations}
      username={(session.user as ObisUser).username}
      occurrenceRecordSubmitted={true}
      occurrence={occurrence}
      previousIdentifications={previousIdentifications}
      gid={gid}
      acodeValue={acodeValue}
    />
  );
}
