'use server';

import {
  createIdentificationVerificationRecord,
  deleteIdentificationVerificationRecord,
  identification_verification,
  updateIdentificationVerificationRecord,
} from '../../utils/managers/obis/IdentificationVerification';
import {
  createOccurrenceRecord,
  occurrence,
  updateOccurrenceRecord,
} from '../../utils/managers/obis/Occurrence';
import {
  ProcessedPrismaCall,
  processPrismaPostCall,
} from '../../utils/server-action-functions';

import { revalidatePath } from 'next/cache';

const revalidateAllPaths = (gid: number) => {
  revalidatePath('/occurrence/add');
  revalidatePath(`/occurrences/result/edit/${gid}`);
};

const createOccurrenceRecordAction = async (
  values: occurrence,
): Promise<ProcessedPrismaCall<occurrence>> => {
  const response = await processPrismaPostCall<occurrence>(
    createOccurrenceRecord,
    values,
    'create',
    'occurrence',
  );

  if (response.prismaCallResponse) {
    revalidatePath('/occurrence/add');
  }

  return response;
};

const createPreviousIdentificationRecord = async (
  values: identification_verification,
): Promise<ProcessedPrismaCall<identification_verification>> => {
  const response = await processPrismaPostCall<identification_verification>(
    createIdentificationVerificationRecord,
    values,
    'create',
    'previous identification',
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.gid!);
  }

  return response;
};

const deletePreviousIdentificationRecord = async (
  values: identification_verification,
): Promise<ProcessedPrismaCall<identification_verification>> => {
  const response = await processPrismaPostCall<identification_verification>(
    deleteIdentificationVerificationRecord,
    values,
    'delete',
    'previous identification',
    undefined,
    values.pkey,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.gid!);
  }

  return response;
};

const editOccurrenceRecord = async (
  values: occurrence,
): Promise<ProcessedPrismaCall<occurrence>> => {
  const gid = values.gid;

  // @ts-expect-error
  delete values['gid'];

  const response = await processPrismaPostCall<occurrence>(
    updateOccurrenceRecord,
    values,
    'edit',
    'occurrence',
    undefined,
    gid,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(gid);
  }

  return response;
};

const editPreviousIdentificationRecord = async (
  values: identification_verification,
): Promise<ProcessedPrismaCall<identification_verification>> => {
  const response = await processPrismaPostCall<identification_verification>(
    updateIdentificationVerificationRecord,
    values,
    'edit',
    'previous identification',
    undefined,
    values.pkey,
  );

  if (response.prismaCallResponse) {
    revalidateAllPaths(values.gid!);
  }

  return response;
};

export {
  createOccurrenceRecordAction as createOccurrenceRecord,
  createPreviousIdentificationRecord,
  deletePreviousIdentificationRecord,
  editOccurrenceRecord,
  editPreviousIdentificationRecord,
};
