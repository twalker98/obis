import { ReactNode } from 'react';
import Search from '../../../components/occurrence/Search';
import relationsService from '../../../utils/relations-service';

type OccurrenceSearchLayoutProps = {
  children: ReactNode;
};

export default async function OccurrenceSearchLayout(
  props: OccurrenceSearchLayoutProps,
) {
  const relations = await relationsService.getRelations();

  return (
    <>
      <Search
        occurrenceSearchFields={relations.occurrenceSearchFields}
        counties={relations.county}
      />
      {props.children}
    </>
  );
}
