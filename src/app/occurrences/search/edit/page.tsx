import { redirect } from 'next/navigation';
import { getSigninUrl } from '../../../../utils/auth';
import { getSession } from '../../../../utils/server-auth';

export default async function OccurrenceSearchResults() {
  const session = await getSession();

  if (!session) {
    redirect(getSigninUrl('occurrences/search/edit'));
  }
}
