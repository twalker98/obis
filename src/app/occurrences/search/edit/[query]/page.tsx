import OccurrenceResultsComponent from '../../../../../components/occurrence/Results';
import { OccurrenceSearchResult } from '../../../../../types/occurrence-search-result';
import OccurrenceSearchService from '../../../../../utils/occurrence-search-service';
import OccurrenceResultsLoading from './loading';

import { redirect } from 'next/navigation';
import { Suspense } from 'react';
import { getSigninUrl } from '../../../../../utils/auth';
import { userHasPermission } from '../../../../../utils/functions';
import { getGroupRecord } from '../../../../../utils/managers/auth/Group';
import { listPermissionRecords } from '../../../../../utils/managers/auth/Permission';
import { getSession } from '../../../../../utils/server-auth';

export default async function OccurrenceResults({ params }) {
  const session = await getSession();
  const groupNames: Array<string> = [];
  const permissions = await listPermissionRecords();
  const queryString = decodeURIComponent(params.query);
  const query = new Map<string, string>(
    queryString
      .split('&')
      .map((pair: string) => pair.split('='))
      .map(([key, value]) => [key, decodeURIComponent(value)]),
  );

  // Block if there is not a logged in user
  if (!session?.user) {
    redirect(getSigninUrl(`occurrences/search/edit/${queryString}`));
  }

  // Block if the logged in user does not have permissions to access this page
  if (
    !userHasPermission(session.user, permissions, [
      'occurrence:*',
      'occurrence:edit',
    ])
  ) {
    redirect('/unauthorized');
  }

  for (const userGroup of session?.user.groups!) {
    const group = await getGroupRecord(userGroup.groupId);
    groupNames.push(group.group_name);
  }

  const occurrence = new OccurrenceSearchService(groupNames);

  const results = Array.from(
    (await occurrence.search(query)) ||
      new Map<string, Array<OccurrenceSearchResult>>(),
  );

  return (
    <Suspense fallback={<OccurrenceResultsLoading />}>
      <OccurrenceResultsComponent results={results} />
    </Suspense>
  );
}
