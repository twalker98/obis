'use client';

import ResultsLoadingComponent from '../../../../../components/ResultsLoading';

export default function OccurrenceResultsLoading() {
  return <ResultsLoadingComponent />;
}
