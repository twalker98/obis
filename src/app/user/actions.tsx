'use server';

import {
  ObisAuth,
  ObisUser,
  User,
  changePassword,
  checkCredentials,
  getUserRecord,
  updateUserRecord,
} from '../../utils/managers/auth/User';
import {
  ProcessedPrismaCall,
  processPrismaGetCall,
  processPrismaPostCall,
} from '../../utils/server-action-functions';

const checkCredentialsAction = async (
  auth: ObisAuth,
): Promise<ProcessedPrismaCall<ObisUser>> => {
  const user = await checkCredentials(auth);

  if (!user) {
    return {
      prismaCallResponse: null,
      snackbarProps: {
        message: 'Current password is incorrect!',
        severity: 'error',
      },
    };
  }

  return {
    prismaCallResponse: user as ObisUser,
    snackbarProps: {
      message: 'Authenticated!',
      severity: 'success',
    },
  };
};

const changePasswordAction = async (
  user: ObisUser,
  userId: string,
): Promise<ProcessedPrismaCall<ObisUser>> => {
  const updatedUser = await changePassword(user, userId);

  if (!updatedUser) {
    return {
      prismaCallResponse: null,
      snackbarProps: {
        message: 'Error changing password!',
        severity: 'error',
      },
    };
  }

  return {
    prismaCallResponse: updatedUser as ObisUser,
    snackbarProps: {
      message: 'Successfully changed password!',
      severity: 'success',
    },
  };
};

const getUserRecordAction = (
  userId: string,
): Promise<ProcessedPrismaCall<ObisUser>> => {
  const response = processPrismaGetCall<ObisUser, string>(
    getUserRecord,
    'user',
    userId,
  );

  return response;
};

const updateUserRecordAction = async (
  user: User,
): Promise<ProcessedPrismaCall<ObisUser>> => {
  const response = await processPrismaPostCall<User>(
    updateUserRecord,
    user,
    'edit',
    'user',
    user.username,
    user.id,
  );

  return response as ProcessedPrismaCall<ObisUser>;
};

export {
  changePasswordAction,
  checkCredentialsAction as checkCredentials,
  getUserRecordAction as getUserRecord,
  updateUserRecordAction as updateUserRecord,
};
