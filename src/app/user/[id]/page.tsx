'use client';

import EditIcon from '@mui/icons-material/Edit';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Avatar from '@mui/material/Avatar';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CircularProgress from '@mui/material/CircularProgress';
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import React, { useEffect, useState } from 'react';
import {
  changePasswordAction,
  checkCredentials,
  getUserRecord,
  updateUserRecord,
} from '../actions';

import {
  GenericForm,
  buildInputAdornmentProps,
} from '@twalk-tech/react-lib/nextjs';
import type { GenericFormItem } from '@twalk-tech/react-lib/nextjs/utils';
import { stringAvatar } from '@twalk-tech/react-lib/nextjs/utils';
import { omit } from 'lodash';
import { useSession } from 'next-auth/react';
import { redirect } from 'next/navigation';
import { getSigninUrl } from '../../../utils/auth';
import { OpenSnackbarFn, useSnackbar } from '../../../utils/hooks/snackbar';
import { ObisAuth, ObisUser } from '../../../utils/managers/auth/User';
import { theme } from '../../../utils/theme';

import * as yup from 'yup';

export type UserProfileComponentProps = {
  user: ObisUser;
  openSnackbar: OpenSnackbarFn;
  updateUser: () => Promise<void>;
};

function ChangeEmailComponent(props: UserProfileComponentProps) {
  const changeEmailFormFields: Array<GenericFormItem> = [
    {
      fcName: 'email',
      name: 'Email Address',
      type: 'text',
      value: props.user.email,
    },
  ];

  const changeEmailFormValidationSchema = yup.object({
    email: yup.string().required('Email Address is required'),
  });

  const changeEmail = async (values: any) => {
    const newEmail = values.email;

    if (newEmail === props.user.email) {
      props.openSnackbar('Email address has not changed!', 'error');
      return;
    }

    const updatedUser = {
      ...props.user,
      email: newEmail,
    };

    const { prismaCallResponse, snackbarProps } = await updateUserRecord(
      omit(updatedUser, ['groups', 'permissions']),
    );
    props.openSnackbar(snackbarProps.message, snackbarProps.severity);

    if (prismaCallResponse) {
      props.updateUser();
    }
  };

  return (
    <GenericForm
      title="Change Email"
      fields={changeEmailFormFields}
      onSubmit={changeEmail}
      submitBtnConfig={{
        title: 'Change Email',
        icon: <EditIcon />,
      }}
      validationSchema={changeEmailFormValidationSchema}
      width="100%"
      borderColor={theme.palette.primary.main}
    />
  );
}

function ChangePasswordComponent(props: UserProfileComponentProps) {
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault();
  };

  const passwordInputProps = buildInputAdornmentProps({
    type: showPassword ? 'text' : 'password',
    position: 'end',
    adornment: (
      <IconButton
        aria-label="toggle-password-visibility"
        onClick={handleClickShowPassword}
        onMouseDown={handleMouseDownPassword}
      >
        {showPassword ? <VisibilityOff /> : <Visibility />}
      </IconButton>
    ),
  });

  const changePasswordFormFields: Array<GenericFormItem> = [
    {
      fcName: 'current_password',
      name: 'Current Password',
      type: 'text',
      value: '',
      inputProps: passwordInputProps,
    },
    {
      fcName: 'new_password',
      name: 'New Password',
      type: 'text',
      value: '',
      inputProps: passwordInputProps,
    },
    {
      fcName: 'confirm_new_password',
      name: 'Confirm New Password',
      type: 'text',
      value: '',
      inputProps: passwordInputProps,
    },
  ];

  const changePasswordFormValidationSchema = yup.object({
    current_password: yup.string().required('Current Password is required'),
    new_password: yup
      .string()
      .required('New Password is required')
      .test(
        'not-same-password',
        'Your new password cannot be the same as your current password',
        function (value) {
          return this.parent.current_password !== value;
        },
      ),
    confirm_new_password: yup
      .string()
      .required('Confirm New Password is required')
      .test('passwords-match', 'Passwords must match', function (value) {
        return this.parent.new_password === value;
      }),
  });

  const changePassword = async (values: any) => {
    const checkPasswordData: ObisAuth = {
      username: props.user.username,
      password: values.current_password,
    };

    const credsResponse = await checkCredentials(checkPasswordData);

    if (!credsResponse.prismaCallResponse) {
      props.openSnackbar(
        credsResponse.snackbarProps.message,
        credsResponse.snackbarProps.severity,
      );
      return;
    }

    const updatedUser = {
      ...props.user,
      password: values.new_password,
    };

    const { prismaCallResponse, snackbarProps } = await changePasswordAction(
      // @ts-ignore
      omit(updatedUser, ['groups', 'permissions']),
      props.user.id,
    );
    props.openSnackbar(snackbarProps.message, snackbarProps.severity);

    if (prismaCallResponse) {
      props.updateUser();
    }
  };

  return (
    <GenericForm
      title="Change Password"
      fields={changePasswordFormFields}
      onSubmit={changePassword}
      submitBtnConfig={{
        title: 'Change Password',
        icon: <EditIcon />,
      }}
      validationSchema={changePasswordFormValidationSchema}
      width="100%"
      borderColor={theme.palette.primary.main}
    />
  );
}

function UserProfileComponent(props: UserProfileComponentProps) {
  const userName = [props.user.firstName, props.user.lastName].join(' ');
  return (
    <Card sx={{ maxWidth: 500, mx: 'auto', textAlign: 'left' }}>
      <CardHeader
        avatar={
          <Avatar
            {...stringAvatar(userName, theme.palette.primary.main, '#fff')}
          />
        }
        title={userName}
        subheader={props.user.username}
      />
      <CardContent>
        <Container sx={{ alignItems: 'center' }} style={{ padding: 0 }}>
          <Stack
            spacing={2}
            divider={
              <Divider
                sx={{
                  borderBottomWidth: 3,
                  backgroundColor: theme.palette.primary.main,
                }}
                flexItem
              />
            }
          >
            <ChangeEmailComponent
              user={props.user}
              openSnackbar={props.openSnackbar}
              updateUser={props.updateUser}
            />
            <ChangePasswordComponent
              user={props.user}
              openSnackbar={props.openSnackbar}
              updateUser={props.updateUser}
            />
          </Stack>
        </Container>
      </CardContent>
    </Card>
  );
}

export default function UserProfile({ params }) {
  const { data: session, status } = useSession();
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState<ObisUser | null>(null);
  const { snackbar, openSnackbar } = useSnackbar();

  if (!params) {
    redirect('/');
  }

  const userId = params.id;

  if (status !== 'authenticated') {
    redirect(getSigninUrl(`user/${userId}`));
  } else if (session.user.id !== userId) {
    redirect('/unauthorized');
  }

  const fetchUserData = async () => {
    setLoading(true);
    const { prismaCallResponse, snackbarProps } = await getUserRecord(userId);

    if (!prismaCallResponse) {
      openSnackbar(snackbarProps.message, snackbarProps.severity);
    } else {
      setUser(prismaCallResponse);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchUserData();
  }, [userId]);

  return (
    <>
      <Container sx={{ alignItems: 'center', mt: 3 }}>
        {loading ? (
          <CircularProgress />
        ) : (
          user && (
            <UserProfileComponent
              user={user}
              openSnackbar={openSnackbar}
              updateUser={fetchUserData}
            />
          )
        )}
        {snackbar}
      </Container>
    </>
  );
}
