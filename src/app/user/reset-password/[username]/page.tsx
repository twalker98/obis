import ResetPasswordComponent from '../../../../components/user/ResetPassword';
import {
  ObisUser,
  searchUserRecords,
} from '../../../../utils/managers/auth/User';

export default async function ResetPassword({ params }) {
  const username = params.username;

  const user = await searchUserRecords({
    username: {
      equals: username,
    },
  });

  return <ResetPasswordComponent user={user[0] as ObisUser} />;
}
