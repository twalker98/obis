'use client';

import BlockIcon from '@mui/icons-material/Block';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import { redirect } from 'next/navigation';
import { useEffect, useState } from 'react';
import { theme } from '../../utils/theme';

export default function Unauthorized() {
  const [redirectSeconds, setRedirectSeconds] = useState<number>(5);

  useEffect(() => {
    if (redirectSeconds === 0) {
      redirect('/');
    }

    setTimeout(() => {
      setRedirectSeconds((redirectSeconds) => redirectSeconds - 1);
    }, 1000);
  }, [redirectSeconds]);

  return (
    <Box
      sx={{
        border: 2,
        borderColor: theme.palette.error.main,
        borderRadius: '16px',
        p: 3,
        pt: 1.5,
        mt: 2,
        mx: 'auto',
        width: '75%',
      }}
    >
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          flexWrap: 'wrap',
        }}
      >
        <BlockIcon color="error" fontSize="large" sx={{ mr: 1 }} />
        <Typography variant="h4" color="error">
          You are unauthorized to perform this action. Taking you home in{' '}
          {redirectSeconds} seconds...
        </Typography>
      </div>
    </Box>
  );
}
