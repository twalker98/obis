import { ReactNode } from 'react';
import ReportSearch from '../../components/report/Search';
import relationsService from '../../utils/relations-service';

type OccurrenceReportSearchLayoutProps = {
  children: ReactNode;
};

export default async function OccurrenceReportSearchLayout(
  props: OccurrenceReportSearchLayoutProps,
) {
  const relations = await relationsService.getRelations();

  return (
    <>
      <ReportSearch
        reportSearchFields={relations.occurrenceReportSearchFields}
        reportType="occurrence"
        counties={relations.county}
        institutions={relations.institution}
        sources={relations.source}
      />
      {props.children}
    </>
  );
}
