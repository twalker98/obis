import { redirect } from 'next/navigation';
import { getSigninUrl } from '../../../utils/auth';
import { getSession } from '../../../utils/server-auth';

export default async function OccurrenceReportSearch() {
  const session = await getSession();

  if (!session) {
    redirect(getSigninUrl('occurrence-report/search'));
  }

  return <></>;
}
