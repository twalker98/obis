import ReportResult from '../../../../components/report/Result';
import ReportService from '../../../../utils/report-service';
import OccurrenceReportResultsLoading from './loading';

import { GridColDef } from '@twalk-tech/react-lib/nextjs';
import { Suspense } from 'react';

export default async function OccurrenceReportResults({ params }) {
  const reportService = new ReportService('occurrence');
  const queryString = decodeURIComponent(params.query);
  const query = new Map<string, string>(
    queryString
      .split('&')
      .map((pair: string) => pair.split('='))
      .map(([key, value]) => [key, decodeURIComponent(value)]),
  );

  const results = await reportService.generateReport(query);

  const occurrenceReportColumns: Array<GridColDef> = [
    {
      field: 'recordedby',
      headerName: 'Recorded By',
      width: 150,
    },
    {
      field: 'family',
      headerName: 'Family',
      width: 150,
    },
    {
      field: 'sname',
      headerName: 'Species Name',
      width: 150,
    },
    {
      field: 'eventdate',
      headerName: 'Event Date',
      width: 150,
    },
    {
      field: 'datasetname',
      headerName: 'Dataset Name',
      width: 150,
    },
    {
      field: 'county',
      headerName: 'County',
      width: 150,
    },
    {
      field: 'institutioncode',
      headerName: 'Institution Code',
      width: 150,
    },
    {
      field: 'catalognumber',
      headerName: 'Catalog Number',
      width: 150,
    },
    {
      field: 'scientificname',
      headerName: 'Scientific Name',
      width: 300,
    },
    {
      field: 'scientificnameauthorship',
      headerName: 'Scientific Name Authorship',
      width: 200,
    },
    {
      field: 'locality',
      headerName: 'Locality',
      width: 150,
    },
  ];

  return (
    <Suspense fallback={<OccurrenceReportResultsLoading />}>
      <ReportResult
        reportType="occurrence"
        columns={occurrenceReportColumns}
        results={results}
      />
    </Suspense>
  );
}
