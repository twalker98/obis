import TaxaCollaboratorsMainComponent from '../../../components/taxa/collaborators/Main';
import relationsService from '../../../utils/relations-service';

import { redirect } from 'next/navigation';
import { getSigninUrl } from '../../../utils/auth';
import { userHasPermission } from '../../../utils/functions';
import { listPermissionRecords } from '../../../utils/managers/auth/Permission';
import { getSession } from '../../../utils/server-auth';

export default async function AddTaxon() {
  const session = await getSession();
  const relations = await relationsService.getRelations();
  const permissions = await listPermissionRecords();

  // Block if the logged in user does not have permissions to access this page
  if (!session?.user) {
    redirect(getSigninUrl('taxon/add'));
  }

  if (!userHasPermission(session.user, permissions, ['taxa:*', 'taxa:add'])) {
    redirect('/unauthorized');
  }

  return (
    <TaxaCollaboratorsMainComponent
      relations={relations}
      acctaxRecordSubmitted={false}
      distributionDataSubmitted={false}
    />
  );
}
