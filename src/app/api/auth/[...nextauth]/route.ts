import NextAuth, { NextAuthOptions } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { updateUserRecord } from '../../../../utils/managers/auth/User';
import { checkCredentialsNextResponse } from '../../../../utils/managers/auth/User';
import obisAuthPrisma, { ObisUser } from '../../../../utils/obis-auth-prisma';

import { PrismaAdapter } from '@next-auth/prisma-adapter';
import { NextApiHandler } from 'next';

export const authOptions: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      id: 'credentials',
      name: 'credentials',
      credentials: {
        username: {
          label: 'Username',
          type: 'text',
          placeholder: 'jsmith',
        },
        password: {
          label: 'Password',
          type: 'password',
        },
      },
      async authorize(credentials) {
        const res = await checkCredentialsNextResponse({
          username: credentials!.username,
          password: credentials!.password,
        });
        const user = res.json();

        if (res.ok) {
          return user;
        }

        throw new Error('CredentialsSignin');
      },
    }),
  ],
  pages: {
    error: '/api/auth/login',
    signIn: '/api/auth/login',
    signOut: '/auth/logout',
  },
  adapter: PrismaAdapter(obisAuthPrisma),
  session: {
    strategy: 'jwt',
  },
  callbacks: {
    async session({ session, token, user }) {
      session.user.id = token.sub ? token.sub : '';

      const userData = await obisAuthPrisma.user.findUnique({
        where: {
          id: session.user.id,
        },
        select: {
          firstName: true,
          lastName: true,
          username: true,
        },
      });

      const groups = await obisAuthPrisma.userGroup.findMany({
        where: {
          userId: session.user.id,
        },
      });

      const permissions = await obisAuthPrisma.userPermission.findMany({
        where: {
          userId: session.user.id,
        },
      });

      session.user.firstName = userData!.firstName;
      session.user.lastName = userData!.lastName;
      (session.user as ObisUser).username = userData!.username;
      session.user.groups = groups;
      session.user.permissions = permissions;

      return session;
    },
    async signIn({ user }) {
      const date = new Date();
      (user as ObisUser).lastLogin = date;

      updateUserRecord(user as ObisUser, user.id);

      return (user as ObisUser).tempPass
        ? `/user/reset-password/${(user as ObisUser).username}`
        : true;
    },
  },
};

const authHandler: NextApiHandler = (req, res) =>
  NextAuth(req, res, authOptions);

export { authHandler as GET, authHandler as POST };
