'use client';

import Container from '@mui/material/Container';
import { login } from '../../../../utils/auth';

import { LoginForm } from '@twalk-tech/react-lib/nextjs/components';
import { useSession } from 'next-auth/react';
import { redirect, useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import { useSnackbar } from '../../../../utils/hooks/snackbar';
import { theme } from '../../../../utils/theme';

export default function Login() {
  const { status } = useSession();
  const params = useSearchParams();
  const [invalidCredentials, setInvalidCredentials] = useState(false);
  const { snackbar, openSnackbar } = useSnackbar();
  const callbackUrl = params?.get('callbackUrl');
  const error = params?.get('error');

  if (status === 'authenticated') {
    redirect('/');
  }

  useEffect(() => {
    if (!error) {
      openSnackbar('You must login to view this content.');
    }
  }, []);

  useEffect(() => {
    if (error && error === 'CredentialsSignin') {
      setInvalidCredentials(true);
    }
  }, [error]);

  return (
    <Container sx={{ width: '75vw' }}>
      <LoginForm
        loginFn={login}
        callbackUrl={callbackUrl as string}
        theme={theme}
        invalidCredentials={invalidCredentials}
      />
      {snackbar}
    </Container>
  );
}
