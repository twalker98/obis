import AdminComponent from './components/AdminComponent';

import { redirect } from 'next/navigation';
import { getSigninUrl } from '../../utils/auth';
import { isAdmin } from '../../utils/functions';
import { listGroupRecords } from '../../utils/managers/auth/Group';
import { listPermissionRecords } from '../../utils/managers/auth/Permission';
import { listUserRecords } from '../../utils/managers/auth/User';
import { getSession } from '../../utils/server-auth';

export default async function Admin() {
  const session = await getSession();
  const signinUrl = getSigninUrl('admin');

  if (!session) {
    redirect(signinUrl);
  }

  const users = await listUserRecords();
  const groups = await listGroupRecords();
  const permissions = await listPermissionRecords();

  if (!isAdmin(session.user, permissions)) {
    redirect('/unauthorized');
  }

  return (
    <AdminComponent
      session={session}
      users={users}
      groups={groups}
      permissions={permissions}
    />
  );
}
