'use server';

import { omit } from 'lodash';
import { revalidatePath } from 'next/cache';
import {
  Group,
  createGroupRecord,
  deleteGroupRecord,
  listGroupRecords,
} from '../../utils/managers/auth/Group';
import {
  Permission,
  createPermissionRecord,
  deletePermissionRecord,
  listPermissionRecords,
} from '../../utils/managers/auth/Permission';
import {
  ObisUser,
  User,
  createUserRecord,
  deleteUserRecord,
  listUserRecords,
  updateUserRecord,
} from '../../utils/managers/auth/User';
import {
  UserGroup,
  createUserGroupRecord,
  deleteUserGroupRecord,
  getUserGroupRecordByGroup,
  getUserGroupRecordByUser,
} from '../../utils/managers/auth/UserGroup';
import {
  UserPermission,
  createUserPermissionRecord,
  deleteUserPermissionRecord,
  getUserPermissionRecordByPermission,
  getUserPermissionRecordByUser,
} from '../../utils/managers/auth/UserPermission';
import {
  ProcessedPrismaCall,
  processPrismaGetCall,
  processPrismaPostCall,
  processUserAccessTypeDeleteCall,
} from '../../utils/server-action-functions';

// TODO: fix bug with group name not displaying in snackbar
const createGroupRecordAction = async (
  group: Group,
): Promise<ProcessedPrismaCall<Group>> => {
  const response = await processPrismaPostCall<Group>(
    createGroupRecord,
    group,
    'create',
    'group',
    group.group_name,
  );

  if (response.prismaCallResponse) {
    revalidatePath('/admin');
  }

  return response;
};

// TODO: fix bug with permission name not displaying in snackbar
const createPermissionRecordAction = async (
  permission: Permission,
): Promise<ProcessedPrismaCall<Permission>> => {
  const response = await processPrismaPostCall<Permission>(
    createPermissionRecord,
    permission,
    'create',
    'permission',
    permission.permission_name,
  );

  if (response.prismaCallResponse) {
    revalidatePath('/admin');
  }

  return response;
};

const createUserGroupRecordAction = async (
  userGroup: UserGroup,
): Promise<ProcessedPrismaCall<UserGroup>> => {
  const response = await processPrismaPostCall<UserGroup>(
    createUserGroupRecord,
    userGroup,
    'create',
    'user group',
  );

  return response;
};

const createUserPermissionRecordAction = async (
  userPermission: UserPermission,
): Promise<ProcessedPrismaCall<UserPermission>> => {
  const response = await processPrismaPostCall<UserPermission>(
    createUserPermissionRecord,
    userPermission,
    'create',
    'user permission',
  );

  return response;
};

// TODO: fix bug with username not displaying in snackbar
const createUserRecordAction = async (
  user: User,
): Promise<ProcessedPrismaCall<ObisUser>> => {
  const response = await processPrismaPostCall<User>(
    createUserRecord,
    user,
    'create',
    'user',
    user.username,
  );

  if (response.prismaCallResponse) {
    revalidatePath('/admin');
    response.prismaCallResponse = omit(
      response.prismaCallResponse,
      'password',
    ) as ObisUser;
  }

  return response as ProcessedPrismaCall<ObisUser>;
};

const deleteGroupRecordAction = async (
  group: Group,
): Promise<ProcessedPrismaCall<Group>> => {
  const response = await processPrismaPostCall<Group>(
    deleteGroupRecord,
    group,
    'delete',
    'group',
    group.group_name,
    group.id,
  );

  return response;
};

const deletePermissionRecordAction = async (
  permission: Permission,
): Promise<ProcessedPrismaCall<Permission>> => {
  const response = await processPrismaPostCall<Permission>(
    deletePermissionRecord,
    permission,
    'delete',
    'permission',
    permission.permission_name,
    permission.id,
  );

  return response;
};

const deleteUserGroupRecordAction = async (
  userId: string,
  groupId: string,
  name: string,
): Promise<ProcessedPrismaCall<UserGroup>> => {
  const response = await processUserAccessTypeDeleteCall<UserGroup>(
    deleteUserGroupRecord,
    userId,
    groupId,
    'user group',
    name,
  );

  return response;
};

const deleteUserPermissionRecordAction = async (
  userId: string,
  permissionId: string,
  name: string,
): Promise<ProcessedPrismaCall<UserPermission>> => {
  const response = await processUserAccessTypeDeleteCall<UserPermission>(
    deleteUserPermissionRecord,
    userId,
    permissionId,
    'user permission',
    name,
  );

  return response;
};

const deleteUserRecordAction = async (
  user: User,
): Promise<ProcessedPrismaCall<ObisUser>> => {
  const response = await processPrismaPostCall<User>(
    deleteUserRecord,
    user,
    'delete',
    'user',
    user.username,
    user.id,
  );

  return response as ProcessedPrismaCall<ObisUser>;
};

const editUserRecord = async (
  userId: string,
  user: User,
): Promise<ProcessedPrismaCall<ObisUser>> => {
  const response = await processPrismaPostCall<User>(
    updateUserRecord,
    user,
    'edit',
    'user',
    user.username,
    userId,
  );

  if (response.prismaCallResponse) {
    revalidatePath('/admin');
    response.prismaCallResponse = omit(
      response.prismaCallResponse,
      'password',
    ) as ObisUser;
  }

  return response as ProcessedPrismaCall<ObisUser>;
};

const getUserGroupRecords = async (
  lookupType: 'group' | 'user',
  lookupValue: string,
): Promise<ProcessedPrismaCall<Array<UserGroup>>> => {
  const fn =
    lookupType === 'group'
      ? getUserGroupRecordByGroup
      : getUserGroupRecordByUser;
  const response = await processPrismaGetCall<Array<UserGroup>, string>(
    fn,
    'user group',
    lookupValue,
  );

  return response;
};

const getUserPermissionRecords = async (
  lookupType: 'permission' | 'user',
  lookupValue: string,
): Promise<ProcessedPrismaCall<Array<UserPermission>>> => {
  const fn =
    lookupType === 'permission'
      ? getUserPermissionRecordByPermission
      : getUserPermissionRecordByUser;
  const response = await processPrismaGetCall<Array<UserPermission>, string>(
    fn,
    'user permission',
    lookupValue,
  );

  return response;
};

const listGroupRecordsAction = async (): Promise<
  ProcessedPrismaCall<Array<Group>>
> => {
  const response = await processPrismaGetCall<Array<Group>>(
    listGroupRecords,
    'group',
  );

  return response;
};

const listPermissionRecordsAction = async (): Promise<
  ProcessedPrismaCall<Array<Permission>>
> => {
  const response = await processPrismaGetCall<Array<Permission>>(
    listPermissionRecords,
    'permission',
  );

  return response;
};

const listUserRecordsAction = async (): Promise<
  ProcessedPrismaCall<Array<ObisUser>>
> => {
  const response = await processPrismaGetCall<Array<ObisUser>>(
    listUserRecords,
    'user',
  );

  return response;
};

export {
  createGroupRecordAction as createGroupRecord,
  createPermissionRecordAction as createPermissionRecord,
  createUserGroupRecordAction as createUserGroupRecord,
  createUserPermissionRecordAction as createUserPermissionRecord,
  createUserRecordAction as createUserRecord,
  deleteGroupRecordAction as deleteGroupRecord,
  deletePermissionRecordAction as deletePermissionRecord,
  deleteUserGroupRecordAction as deleteUserGroupRecord,
  deleteUserPermissionRecordAction as deleteUserPermissionRecord,
  deleteUserRecordAction as deleteUserRecord,
  editUserRecord,
  getUserGroupRecords,
  getUserPermissionRecords,
  listGroupRecordsAction as listGroupRecords,
  listPermissionRecordsAction as listPermissionRecords,
  listUserRecordsAction as listUserRecords,
};
