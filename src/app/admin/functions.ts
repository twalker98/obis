import { UserGroup } from '../../utils/managers/auth/UserGroup';
import { UserPermission } from '../../utils/managers/auth/UserPermission';

export function buildUserGroupData(userId: string, groupId: string) {
  return {
    userId: userId,
    groupId: groupId,
  } as UserGroup;
}

export function buildUserPermissionData(userId: string, permissionId: string) {
  return {
    userId: userId,
    permissionId: permissionId,
  } as UserPermission;
}
