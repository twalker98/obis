'use client';

import AccessOptions from './AccessOptions';
import Users from './Users/Main';

import { Group as GroupIcon, Lock, Person } from '@mui/icons-material';
import { CustomTab, CustomTabs } from '@twalk-tech/react-lib/nextjs';
import { Session } from 'next-auth';
import { useState } from 'react';
import { useSnackbar } from '../../../utils/hooks/snackbar';
import { Group } from '../../../utils/managers/auth/Group';
import { Permission } from '../../../utils/managers/auth/Permission';
import { ObisUser } from '../../../utils/managers/auth/User';
import { theme } from '../../../utils/theme';
import {
  listGroupRecords,
  listPermissionRecords,
  listUserRecords,
} from '../actions';

export type AdminComponentProps = {
  session: Session;
  users: Array<ObisUser>;
  permissions: Array<Permission>;
  groups: Array<Group>;
};

export default function AdminComponent(props: AdminComponentProps) {
  const [users, setUsers] = useState<Array<ObisUser> | null>(props.users);
  const [groups, setGroups] = useState<Array<Group> | null>(props.groups);
  const [permissions, setPermissions] = useState<Array<Permission> | null>(
    props.permissions,
  );
  const { snackbar, openSnackbar } = useSnackbar();

  const fetchUsers = async () => {
    const { prismaCallResponse, snackbarProps } = await listUserRecords();

    if (!prismaCallResponse) {
      openSnackbar(snackbarProps.message, snackbarProps.severity);
    }

    setUsers(prismaCallResponse);
  };

  const fetchGroups = async () => {
    const { prismaCallResponse, snackbarProps } = await listGroupRecords();

    if (!prismaCallResponse) {
      openSnackbar(snackbarProps.message, snackbarProps.severity);
    }

    setGroups(prismaCallResponse);
  };

  const fetchPermissions = async () => {
    const { prismaCallResponse, snackbarProps } = await listPermissionRecords();

    if (!prismaCallResponse) {
      openSnackbar(snackbarProps.message, snackbarProps.severity);
    }

    setPermissions(prismaCallResponse);
  };

  const tabs: Array<CustomTab> = [
    {
      title: 'Users',
      icon: <Person />,
      content: (
        <Users
          users={users}
          updateUsers={fetchUsers}
          groups={groups}
          permissions={permissions}
        />
      ),
    },
    {
      title: 'Groups',
      icon: <GroupIcon />,
      content: (
        <AccessOptions
          type="group"
          accessOptions={groups}
          updateAccessOptions={fetchGroups}
        />
      ),
    },
    {
      title: 'Permissions',
      icon: <Lock />,
      content: (
        <AccessOptions
          type="permission"
          accessOptions={permissions}
          updateAccessOptions={fetchPermissions}
        />
      ),
    },
  ];

  return (
    <>
      <CustomTabs tabs={tabs} theme={theme} />
      {snackbar}
    </>
  );
}
