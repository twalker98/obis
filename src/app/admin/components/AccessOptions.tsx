import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import Container from '@mui/material/Container';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import React, { useState } from 'react';

import {
  ActionButton,
  ConfirmDialog,
  DataTable,
  GenericForm,
  GenericFormItem,
  GridColDef,
} from '@twalk-tech/react-lib/nextjs';
import { capitalizeFirstLetter } from '../../../utils/functions';
import { OpenSnackbarFn, useSnackbar } from '../../../utils/hooks/snackbar';
import { Group } from '../../../utils/managers/auth/Group';
import { Permission } from '../../../utils/managers/auth/Permission';
import { theme } from '../../../utils/theme';
import {
  createGroupRecord,
  createPermissionRecord,
  deleteGroupRecord,
  deletePermissionRecord,
  getUserGroupRecords,
  getUserPermissionRecords,
} from '../actions';

type TAccessOption = Group | Permission;
type TNewAccessOption =
  | (Omit<Group, 'id'> & {
      permission_name?: undefined;
    })
  | (Omit<Permission, 'id'> & {
      group_name?: undefined;
    });

const getSpecificAccessTypeName = (
  accessType: string,
  accessObject: TNewAccessOption,
) => {
  if (accessType === 'group') {
    accessObject = accessObject as Group;
    return accessObject.group_name;
  } else if (accessType === 'permission') {
    accessObject = accessObject as Permission;
    return accessObject.permission_name;
  }
};

export type AccessOptionsProps = {
  type: 'group' | 'permission';
  accessOptions: Array<TAccessOption> | null;
  updateAccessOptions: () => Promise<void>;
};

export type CreateAccessOptionFormProps = AccessOptionsProps & {
  displayName: string;
  dialogOpen: (open: boolean) => void;
  openSnackbar: OpenSnackbarFn;
};

export function CreateAccessOptionForm(props: CreateAccessOptionFormProps) {
  const formFields: Array<GenericFormItem> = [
    { fcName: 'access_name', name: props.displayName, type: 'text', value: '' },
  ];

  const createAccessOption = async (values: { access_name: string }) => {
    if (
      props.accessOptions?.find(
        (accessOption) =>
          getSpecificAccessTypeName(props.type, accessOption) ===
          values.access_name,
      )
    ) {
      props.openSnackbar(`${props.displayName} already exists!`, 'error');
      return;
    }

    const newAccessOption =
      props.type === 'group'
        ? {
            group_name: values.access_name,
          }
        : {
            permission_name: values.access_name,
          };

    const { snackbarProps } =
      props.type === 'group'
        ? await createGroupRecord(newAccessOption as Group)
        : await createPermissionRecord(newAccessOption as Permission);
    props.openSnackbar(snackbarProps.message, snackbarProps.severity);

    props.dialogOpen(false);
    await props.updateAccessOptions();
  };

  return (
    <GenericForm
      title={`Create ${props.displayName}`}
      fields={formFields}
      onSubmit={createAccessOption}
      submitBtnConfig={{
        title: `Create ${props.displayName}`,
        icon: <AddIcon />,
      }}
      width="100%"
      borderColor={theme.palette.primary.main}
    />
  );
}

export default function AccessOptions(props: AccessOptionsProps) {
  const [selectedId, setSelectedId] = useState('');
  const [selectedName, setSelectedName] = useState('');
  const [createAccessOptionDialogOpen, setCreateAccessOptionDialogOpen] =
    useState(false);
  const [confirmDialogOpen, setConfirmDialogOpen] = useState(false);
  const [confirmDialogMessage, setConfirmDialogMessage] = useState('');
  const [deletingAccessOption, setDeletingAccessOption] = useState(false);
  const displayName = capitalizeFirstLetter(props.type);
  const { snackbar, openSnackbar } = useSnackbar();

  if (!props.accessOptions) {
    return null;
  }

  const accessOptionsTableColumns: Array<GridColDef> = [
    {
      field: 'id',
      headerName: 'ID',
      width: 250,
      hideable: false,
    },
    {
      field: `${props.type}_name`,
      headerName: displayName,
      width: 250,
      hideable: false,
    },
  ];

  const rowSelected = (selectedId: string) => {
    setSelectedId(selectedId);

    if (props.type === 'group') {
      setSelectedName(
        (props.accessOptions as Array<Group>)?.find(
          (group: Group) => group.id === selectedId,
        )!.group_name,
      );
    } else if (props.type === 'permission') {
      setSelectedName(
        (props.accessOptions as Array<Permission>)?.find(
          (permission: Permission) => permission.id === selectedId,
        )!.permission_name,
      );
    }
  };

  const deleteAccessOption = () => {
    setDeletingAccessOption(true);
    setConfirmDialogMessage(
      `Are you sure you want to delete ${props.type} "${selectedName}"?`,
    );
    setConfirmDialogOpen(true);
  };

  const handleCancelDelete = () => {
    setConfirmDialogOpen(false);
    setDeletingAccessOption(false);
  };

  const handleConfirmDelete = async () => {
    setConfirmDialogOpen(false);

    const { prismaCallResponse, snackbarProps } =
      props.type === 'group'
        ? await getUserGroupRecords(props.type, selectedId)
        : await getUserPermissionRecords(props.type, selectedId);

    if (!prismaCallResponse) {
      openSnackbar(snackbarProps.message, snackbarProps.severity);
    } else {
      if (prismaCallResponse!.length === 0) {
        const { snackbarProps } =
          props.type === 'group'
            ? await deleteGroupRecord(
                props.accessOptions?.find(
                  (permission) => permission.id === selectedId,
                ) as Group,
              )
            : await deletePermissionRecord(
                props.accessOptions?.find(
                  (permission) => permission.id === selectedId,
                ) as Permission,
              );
        openSnackbar(snackbarProps.message, snackbarProps.severity);
      } else {
        openSnackbar(
          `Cannot delete ${props.type} "${selectedName}" because one or more users have this ${props.type}!`,
          'error',
        );
      }
    }

    setSelectedId('');
    setSelectedName('');
    setDeletingAccessOption(false);
    await props.updateAccessOptions();
  };

  return (
    <Container sx={{ alignItems: 'center' }}>
      <Container sx={{ mb: 1, mt: -4 }} style={{ paddingLeft: 0 }}>
        <ActionButton
          title={`Create ${displayName}`}
          onButtonClick={() => setCreateAccessOptionDialogOpen(true)}
          startIcon={<AddIcon />}
        />
        <ActionButton
          title={`Delete ${displayName}`}
          onButtonClick={() => deleteAccessOption()}
          startIcon={<DeleteIcon />}
          submitting={deletingAccessOption}
          disabled={!selectedId}
        />
      </Container>
      <DataTable
        height={500}
        getRowIdPredicate={(row) => row.id}
        data={props.accessOptions}
        pageSize={props.accessOptions.length}
        columns={accessOptionsTableColumns}
        filterEnabled={true}
        exportEnabled={true}
        rowSelected={rowSelected}
        defaultSort={{
          column: `${props.type}_name`,
          direction: 'asc',
        }}
      />
      <Dialog
        open={createAccessOptionDialogOpen}
        onClose={() => setCreateAccessOptionDialogOpen(false)}
      >
        <DialogContent sx={{ minWidth: '500px' }}>
          <CreateAccessOptionForm
            {...props}
            displayName={displayName}
            dialogOpen={setCreateAccessOptionDialogOpen}
            openSnackbar={openSnackbar}
          />
        </DialogContent>
      </Dialog>
      <ConfirmDialog
        open={confirmDialogOpen}
        message={confirmDialogMessage}
        onCancel={handleCancelDelete}
        onConfirm={handleConfirmDelete}
      />
      {snackbar}
    </Container>
  );
}
