import { GenericForm, GenericFormItem } from '@twalk-tech/react-lib/nextjs';
import { ReactElement } from 'react';
import { theme } from '../../../../utils/theme';

import * as yup from 'yup';

export const userFormFields: Array<GenericFormItem> = [
  { fcName: 'firstName', name: 'First Name', type: 'text', value: '' },
  { fcName: 'lastName', name: 'Last Name', type: 'text', value: '' },
  { fcName: 'username', name: 'Username', type: 'text', value: '' },
  { fcName: 'email', name: 'Email Address', type: 'text', value: '' },
];

export const userFormValidationSchema = yup.object({
  firstName: yup.string().required('First Name is required'),
  lastName: yup.string().required('Last Name is required'),
  username: yup.string().required('Username is required'),
  email: yup
    .string()
    .email('Must be a valid email address')
    .required('Email is required'),
});

export type UserFormProps = {
  action: string;
  fields: Array<GenericFormItem>;
  submitFn: (values: any) => Promise<void>;
  submitButtonIcon: ReactElement;
  validationSchema: any;
};

export default function UserForm(props: UserFormProps) {
  props = {
    ...props,
    action: `${props.action} User`,
  };

  return (
    <GenericForm
      title={props.action}
      fields={props.fields}
      onSubmit={props.submitFn}
      submitBtnConfig={{
        title: props.action,
        icon: props.submitButtonIcon,
      }}
      validationSchema={props.validationSchema}
      width="100%"
      borderColor={theme.palette.primary.main}
    />
  );
}
