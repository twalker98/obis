import AddIcon from '@mui/icons-material/Add';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import IconButton from '@mui/material/IconButton';
import React from 'react';
import UserForm from './UserForm';

import { buildInputAdornmentProps } from '@twalk-tech/react-lib/nextjs';
import { omit } from 'lodash';
import { useState } from 'react';
import { OpenSnackbarFn } from '../../../../utils/hooks/snackbar';
import { Group } from '../../../../utils/managers/auth/Group';
import { Permission } from '../../../../utils/managers/auth/Permission';
import { User } from '../../../../utils/managers/auth/User';
import {
  createUserGroupRecord,
  createUserPermissionRecord,
  createUserRecord,
} from '../../actions';
import { buildUserGroupData, buildUserPermissionData } from '../../functions';
import { userFormFields, userFormValidationSchema } from './UserForm';

import * as yup from 'yup';

export type CreateUserFormProps = {
  updateUsers: () => Promise<void>;
  groups: Array<Group>;
  permissions: Array<Permission>;
  setCreateUserDialogOpen: (open: boolean) => void;
  openSnackbar: OpenSnackbarFn;
};

export default function CreateUserForm(props: CreateUserFormProps) {
  const createUserFormFields = structuredClone(userFormFields);
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault();
  };

  const passwordInputProps = buildInputAdornmentProps({
    type: showPassword ? 'text' : 'password',
    position: 'end',
    adornment: (
      <IconButton
        aria-label="toggle-password-visibility"
        onClick={handleClickShowPassword}
        onMouseDown={handleMouseDownPassword}
      >
        {showPassword ? <VisibilityOff /> : <Visibility />}
      </IconButton>
    ),
  });

  createUserFormFields.push(
    {
      fcName: 'password',
      name: 'Password',
      type: 'text',
      value: '',
      inputProps: passwordInputProps,
    },
    {
      fcName: 'confirm_password',
      name: 'Confirm Password',
      type: 'text',
      value: '',
      inputProps: passwordInputProps,
    },
    {
      fcName: 'groups',
      name: 'Groups',
      type: 'dropdown',
      options: props.groups,
      multiple: true,
      value: [],
      dropdownLabelFunctionPred: (option: Group) => option.group_name,
      optionKey: 'id',
      optionValueKey: 'group_name',
    },
    {
      fcName: 'permissions',
      name: 'Permissions',
      type: 'dropdown',
      options: props.permissions,
      multiple: true,
      value: [],
      dropdownLabelFunctionPred: (option: Permission) => option.permission_name,
      optionKey: 'id',
      optionValueKey: 'permission_name',
    },
  );

  const passwordValidationSchema = yup.object({
    password: yup.string().required('Password is required'),
    confirm_password: yup
      .string()
      .required('Confirm Password is required')
      .test('passwords-match', 'Passwords must match', function (value) {
        return this.parent.password === value;
      }),
  });

  const createUserFormValidationSchema = userFormValidationSchema.concat(
    passwordValidationSchema,
  );

  const createUserGroups = async (values: User, user: Partial<User>) => {
    for (const userGroupName of values['groups']) {
      const groupId = (userGroupName as Group).id;

      const userGroupData = buildUserGroupData(user!.id!, groupId);

      const { prismaCallResponse, snackbarProps } = await createUserGroupRecord(
        userGroupData,
      );

      if (!prismaCallResponse) {
        props.openSnackbar(snackbarProps.message, snackbarProps.severity);
      }
    }
  };

  const createUserPermissions = async (values: User, user: Partial<User>) => {
    for (const userPermissionName of values['permissions']) {
      const permissionId = (userPermissionName as Permission).id;

      const userPermissionData = buildUserPermissionData(
        user.id!,
        permissionId,
      );

      const { prismaCallResponse, snackbarProps } =
        await createUserPermissionRecord(userPermissionData);

      if (!prismaCallResponse) {
        props.openSnackbar(snackbarProps.message, snackbarProps.severity);
      }
    }
  };

  const createUser = async (values: User) => {
    const userData = omit(values, [
      'groups',
      'permissions',
      'confirm_password',
    ]);

    userData.lastLogin = new Date();
    userData.tempPass = true;

    const { prismaCallResponse, snackbarProps } = await createUserRecord(
      // @ts-ignore
      userData,
    );

    if (!prismaCallResponse) {
      props.openSnackbar(snackbarProps.message, snackbarProps.severity);
    } else {
      await Promise.all([
        createUserGroups(values, prismaCallResponse!),
        createUserPermissions(values, prismaCallResponse!),
      ]);

      props.openSnackbar(snackbarProps.message, snackbarProps.severity);
      await props.updateUsers();
    }

    props.setCreateUserDialogOpen(false);
  };

  return (
    <UserForm
      action="Create"
      fields={createUserFormFields}
      submitFn={createUser}
      submitButtonIcon={<AddIcon />}
      validationSchema={createUserFormValidationSchema}
    />
  );
}
