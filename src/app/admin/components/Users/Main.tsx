import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Container from '@mui/material/Container';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import CreateUserForm from './CreateUser';
import EditUserForm from './EditUser';

import {
  ActionButton,
  ConfirmDialog,
  DataTable,
  GridColDef,
} from '@twalk-tech/react-lib/nextjs';
import { useSession } from 'next-auth/react';
import { useState } from 'react';
import { useSnackbar } from '../../../../utils/hooks/snackbar';
import { Group } from '../../../../utils/managers/auth/Group';
import { Permission } from '../../../../utils/managers/auth/Permission';
import { ObisUser } from '../../../../utils/managers/auth/User';
import { deleteUserRecord } from '../../actions';

export type UsersProps = {
  users: Array<ObisUser> | null;
  updateUsers: () => Promise<void>;
  groups: Array<Group> | null;
  permissions: Array<Permission> | null;
};

export default function Users(props: UsersProps) {
  const [selectedId, setSelectedId] = useState('');
  const [selectedName, setSelectedName] = useState('');
  const [createUserDialogOpen, setCreateUserDialogOpen] = useState(false);
  const [editUserDialogOpen, setEditUserDialogOpen] = useState(false);
  const [confirmDialogOpen, setConfirmDialogOpen] = useState(false);
  const [confirmDialogMessage, setConfirmDialogMessage] = useState('');
  const [deletingUser, setDeletingUser] = useState(false);
  const { snackbar, openSnackbar } = useSnackbar();
  const { data: session, status } = useSession();

  if (!session || !props.users || !props.groups || !props.permissions) {
    return null;
  }

  const usersTableColumns: Array<GridColDef> = [
    {
      field: 'id',
      headerName: 'ID',
      width: 250,
      hideable: false,
    },
    {
      field: 'firstName',
      headerName: 'First Name',
      width: 150,
      hideable: false,
    },
    {
      field: 'lastName',
      headerName: 'Last Name',
      width: 150,
      hideable: false,
    },
    {
      field: 'username',
      headerName: 'Username',
      width: 150,
      hideable: false,
    },
    {
      field: 'email',
      headerName: 'Email Address',
      width: 250,
      hideable: false,
    },
    {
      field: 'lastLogin',
      headerName: 'Last Login',
      width: 500,
      hideable: false,
    },
  ];

  const rowSelected = (selectedId: string) => {
    setSelectedId(selectedId);
    setSelectedName(
      props.users?.find((user) => user.id === selectedId)!.username!,
    );
  };

  const deleteUser = () => {
    setDeletingUser(true);
    setConfirmDialogMessage(
      `Are you sure you want to delete user "${selectedName}"?`,
    );
    setConfirmDialogOpen(true);
  };

  const handleCancelDelete = () => {
    setConfirmDialogOpen(false);
    setDeletingUser(false);
  };

  const handleConfirmDelete = async () => {
    setConfirmDialogOpen(false);

    const { snackbarProps } = await deleteUserRecord(
      props.users?.find((user) => user.id === selectedId)!,
    );
    openSnackbar(snackbarProps.message, snackbarProps.severity);

    setSelectedId('');
    setSelectedName('');
    setDeletingUser(false);
    await props.updateUsers();
  };

  return (
    <Container sx={{ alignItems: 'center' }}>
      <Container sx={{ mb: 1, mt: -4 }} style={{ paddingLeft: 0 }}>
        <ActionButton
          title="Create User"
          onButtonClick={() => setCreateUserDialogOpen(true)}
          startIcon={<AddIcon />}
        />
        <ActionButton
          title="Edit User"
          onButtonClick={() => setEditUserDialogOpen(true)}
          startIcon={<EditIcon />}
          disabled={!selectedId}
        />
        <ActionButton
          title="Delete User"
          onButtonClick={() => deleteUser()}
          startIcon={<DeleteIcon />}
          submitting={deletingUser}
          disabled={!selectedId || selectedId === session.user.id}
        />
      </Container>
      <DataTable
        height={500}
        getRowIdPredicate={(row) => row.id}
        data={props.users}
        pageSize={props.users.length}
        columns={usersTableColumns}
        filterEnabled={true}
        exportEnabled={true}
        rowSelected={rowSelected}
        defaultSort={{
          column: 'username',
          direction: 'asc',
        }}
      />
      <Dialog
        open={createUserDialogOpen}
        onClose={() => setCreateUserDialogOpen(false)}
      >
        <DialogContent sx={{ minWidth: '500px' }}>
          <CreateUserForm
            updateUsers={props.updateUsers}
            groups={props.groups}
            permissions={props.permissions}
            setCreateUserDialogOpen={setCreateUserDialogOpen}
            openSnackbar={openSnackbar}
          />
        </DialogContent>
      </Dialog>
      <Dialog
        open={editUserDialogOpen}
        onClose={() => setEditUserDialogOpen(false)}
      >
        <DialogContent sx={{ minWidth: '500px' }}>
          <EditUserForm
            users={props.users}
            updateUsers={props.updateUsers}
            groups={props.groups}
            permissions={props.permissions}
            selectedId={selectedId}
            setEditUserDialogOpen={setEditUserDialogOpen}
            openSnackbar={openSnackbar}
          />
        </DialogContent>
      </Dialog>
      <ConfirmDialog
        open={confirmDialogOpen}
        message={confirmDialogMessage}
        onCancel={handleCancelDelete}
        onConfirm={handleConfirmDelete}
      />
      {snackbar}
    </Container>
  );
}
