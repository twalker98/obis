import EditIcon from '@mui/icons-material/Edit';
import CircularProgress from '@mui/material/CircularProgress';
import React, { useEffect, useState } from 'react';
import UserForm from './UserForm';

import { omit } from 'lodash';
import { updateFormFieldValues } from '../../../../utils/functions';
import { OpenSnackbarFn } from '../../../../utils/hooks/snackbar';
import { Group } from '../../../../utils/managers/auth/Group';
import { Permission } from '../../../../utils/managers/auth/Permission';
import { ObisUser } from '../../../../utils/managers/auth/User';
import { UserGroup } from '../../../../utils/managers/auth/UserGroup';
import { UserPermission } from '../../../../utils/managers/auth/UserPermission';
import {
  createUserGroupRecord,
  createUserPermissionRecord,
  deleteUserGroupRecord,
  deleteUserPermissionRecord,
  editUserRecord,
} from '../../actions';
import { buildUserGroupData, buildUserPermissionData } from '../../functions';
import { userFormFields, userFormValidationSchema } from './UserForm';

export type EditUserFormProps = {
  users: Array<ObisUser>;
  updateUsers: () => Promise<void>;
  groups: Array<Group>;
  permissions: Array<Permission>;
  selectedId: string;
  setEditUserDialogOpen: (open: boolean) => void;
  openSnackbar: OpenSnackbarFn;
};

type EditUser = {
  id: string;
  username: string;
};

export default function EditUserForm(props: EditUserFormProps) {
  const [userLoading, setUserLoading] = useState(true);
  const [userGroups, setUserGroups] = useState<Array<Group>>([]);
  const [userPermissions, setUserPermissions] = useState<Array<Permission>>([]);

  const user = props.users.find(
    (user: ObisUser) => user.id === props.selectedId,
  );

  const editUserFormFields = structuredClone(userFormFields);
  updateFormFieldValues<ObisUser>(
    user as ObisUser,
    editUserFormFields,
    'value',
  );

  editUserFormFields.push(
    {
      fcName: 'groups',
      name: 'Groups',
      type: 'dropdown',
      options: props.groups,
      multiple: true,
      value: userGroups,
      dropdownLabelFunctionPred: (option: Group) => option.group_name,
      optionKey: 'id',
      optionValueKey: 'group_name',
    },
    {
      fcName: 'permissions',
      name: 'Permissions',
      type: 'dropdown',
      options: props.permissions,
      multiple: true,
      value: userPermissions,
      dropdownLabelFunctionPred: (option: Permission) => option.permission_name,
      optionKey: 'id',
      optionValueKey: 'permission_name',
    },
  );

  useEffect(() => {
    async function getUserGroups() {
      const userGroupList = props.users.find(
        (user) => user.id === props.selectedId,
      )!.groups;

      props.groups.forEach((group: Group) => {
        userGroupList.forEach((userGroup: UserGroup) => {
          if (userGroup.groupId === group.id) {
            userGroups.push(group);
          }
        });
      });
    }

    async function getUserPermissions() {
      const userPermissionList = props.users.find(
        (user) => user.id === props.selectedId,
      )!.permissions;

      props.permissions.forEach((permission: Permission) => {
        userPermissionList.forEach((userPermission: UserPermission) => {
          if (userPermission.permissionId === permission.id) {
            userPermissions.push(permission);
          }
        });
      });
    }

    Promise.all([getUserGroups(), getUserPermissions()]).then(() => {
      setUserLoading(false);
    });
  }, []);

  const handleDeleteListItem = async (
    user: EditUser,
    userAccessType: 'group' | 'permission',
    userAccessNames: Array<Group | Permission>,
  ) => {
    const status = await new Promise<boolean>((resolve) => {
      userAccessNames.forEach(async (userAccessObj: Group | Permission) => {
        const userAccessId = userAccessObj.id;
        const { prismaCallResponse, snackbarProps } =
          userAccessType === 'group'
            ? await deleteUserGroupRecord(
                user.id,
                userAccessId,
                props.groups.find((userGroup) => userGroup.id === userAccessId)
                  ?.group_name!,
              )
            : await deleteUserPermissionRecord(
                user.id,
                userAccessId,
                props.permissions.find(
                  (userPermission) => userPermission.id === userAccessId,
                )?.permission_name!,
              );

        if (!prismaCallResponse) {
          props.openSnackbar(snackbarProps.message, snackbarProps.severity);
          resolve(false);
        } else {
          resolve(true);
        }
      });
    });

    return status;
  };

  const handleListEdit = async (
    values: Array<Group | Permission>,
    user: EditUser,
    userAccessType: 'group' | 'permission',
  ) => {
    let status = false;

    await values[`${userAccessType}s`].forEach(
      async (userAccessName: Group | Permission) => {
        const userAccessNames =
          userAccessType === 'group' ? userGroups : userPermissions;

        const userAccessNameIdx = userAccessNames.findIndex(
          (userAccessNameObj: Group | Permission) =>
            userAccessNameObj === userAccessName,
        );

        if (userAccessNameIdx < 0) {
          // This user access is new, so needs to be created
          const userAccessData =
            userAccessType === 'group'
              ? buildUserGroupData(
                  user.id,
                  props.groups.find((group) => group === userAccessName)!.id,
                )
              : buildUserPermissionData(
                  user.id,
                  props.permissions.find(
                    (permission) => permission === userAccessName,
                  )!.id,
                );

          const { prismaCallResponse, snackbarProps } =
            userAccessType === 'group'
              ? await createUserGroupRecord(userAccessData as UserGroup)
              : await createUserPermissionRecord(
                  userAccessData as UserPermission,
                );

          if (!prismaCallResponse) {
            props.openSnackbar(
              `${snackbarProps.message} for user "${user.username}"!`,
              snackbarProps.severity,
            );
            status = false;
          }
        } else {
          // This user access already existed, so remove it from the userAccessNames list
          const removed = userAccessNames.splice(userAccessNameIdx, 1);

          if (userAccessType === 'group') {
            setUserGroups(removed as Array<Group>);
          } else {
            setUserPermissions(removed as Array<Permission>);
          }
        }
      },
    );

    if (userAccessType === 'group' && userGroups.length > 0) {
      status = await handleDeleteListItem(user, 'group', userGroups);
    } else if (userAccessType === 'permission' && userPermissions.length > 0) {
      status = await handleDeleteListItem(user, 'permission', userPermissions);
    } else {
      status = true;
    }

    return status;
  };

  const editUser = async (values: any) => {
    const userData = omit(values, ['groups', 'permissions']);

    const userObj: EditUser = {
      id: props.selectedId,
      username: values.username,
    };

    let status = true;

    if (values['groups'] !== userGroups) {
      status = await handleListEdit(values, userObj, 'group');
    }

    if (values['permissions'] !== userPermissions) {
      status = await handleListEdit(values, userObj, 'permission');
    }

    if (status) {
      const { prismaCallResponse, snackbarProps } = await editUserRecord(
        props.selectedId,
        // @ts-ignore
        userData,
      );

      props.openSnackbar(snackbarProps.message, snackbarProps.severity);

      if (prismaCallResponse) {
        props.setEditUserDialogOpen(false);
        await props.updateUsers();
      }
    }
  };

  return (
    <>
      {userLoading ? (
        <CircularProgress />
      ) : (
        <UserForm
          action="Edit"
          fields={editUserFormFields}
          submitFn={editUser}
          submitButtonIcon={<EditIcon />}
          validationSchema={userFormValidationSchema}
        />
      )}
    </>
  );
}
