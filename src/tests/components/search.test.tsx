import '@testing-library/jest-dom';

import { render, screen } from '@testing-library/react';
import React from 'react';
import { TaxaSearchbarValueContext } from '../../app/taxa/context';
import Search from '../../components/taxa/Search';

const setValue = (newValue: string) => {
  return null;
};

describe('Search Component', () => {
  it('renders a search bar', () => {
    render(
      <TaxaSearchbarValueContext.Provider
        value={{ value: '', setValue: setValue }}
      >
        <Search search={() => null} />
      </TaxaSearchbarValueContext.Provider>,
    );

    const searchBar = screen.getByRole('textbox');
    const searchBtn = screen.getByRole('button', { name: 'Search' });

    expect(searchBar).toBeInTheDocument();
    expect(searchBtn).toBeInTheDocument();
  });
});
