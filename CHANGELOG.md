# Changelog

## Version 1.0.2 - December 11, 2023
  - Fixing bug with Add Taxon page not loading

## Version 1.0.1 - December 8, 2023
  - Fixing issue with authentication in production

## Version 1.0.0 - December 3, 2023
  - Initial release
